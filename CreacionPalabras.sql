USE FYNDDY
DELETE FROM PALABRAS
GO
DELETE FROM PERMUTACIONES_PRODUCTO_EMPRESA
GO
DELETE FROM PERMUTACIONES
GO
INSERT INTO dbo.PALABRAS(Palabra, ProductoId, EmpresaId)
select * from 
(
SELECT UPPER(dbo.fnConvertirCaracteres(dbo.fn_StripCharacters(t.Descripcion, '^a-z0-9'))) AS PALABRA, p.Id AS PRODUCTOID, NULL AS EMPRESAID
FROM dbo.PRODUCTO p
CROSS APPLY dbo.SplitString(p.Nombre,' ') t
WHERE t.Descripcion NOT LIKE 'mis' 
AND t.Descripcion NOT LIKE 'el'
AND t.Descripcion NOT LIKE 'la'
AND t.Descripcion NOT LIKE 'de'
AND t.Descripcion NOT LIKE 'con'
AND t.Descripcion NOT LIKE 'y'
AND t.Descripcion NOT LIKE 'en'
AND t.Descripcion NOT LIKE 'a'
AND t.Descripcion NOT LIKE 'o'
AND t.Descripcion NOT LIKE ''
AND t.Descripcion NOT LIKE 'del'
AND t.Descripcion NOT LIKE 'las'
AND t.Descripcion NOT LIKE 'los'
AND t.Descripcion NOT LIKE 'un'
AND t.Descripcion NOT LIKE 'una'
AND t.Descripcion NOT LIKE '+'
AND t.Descripcion NOT LIKE '-'
AND t.Descripcion NOT LIKE '/'
AND t.Descripcion NOT LIKE '"'
AND t.Descripcion NOT LIKE '('
AND t.Descripcion NOT LIKE ')'
AND t.Descripcion NOT LIKE '*'
AND t.Descripcion NOT LIKE ','
AND t.Descripcion NOT LIKE '.'
AND t.Descripcion NOT LIKE '?'
AND t.Descripcion NOT LIKE '�'
AND t.Descripcion NOT LIKE '!'
AND t.Descripcion NOT LIKE '�'
AND t.Descripcion NOT LIKE '�'
AND t.Descripcion NOT LIKE '$'
AND t.Descripcion NOT LIKE '>'
AND t.Descripcion NOT LIKE '<'
) A
UNION
(
SELECT UPPER(dbo.fnConvertirCaracteres(dbo.fn_StripCharacters(t.Descripcion, '^a-z0-9'))) AS PALABRA, NULL AS PRODUCTOID, p.Id AS EMPRESAID
FROM dbo.EMPRESA p
CROSS APPLY dbo.SplitString(p.Nombre,' ') t
WHERE t.Descripcion NOT LIKE 'mis' 
AND t.Descripcion NOT LIKE 'el'
AND t.Descripcion NOT LIKE 'la'
AND t.Descripcion NOT LIKE 'de'
AND t.Descripcion NOT LIKE 'con'
AND t.Descripcion NOT LIKE 'y'
AND t.Descripcion NOT LIKE 'en'
AND t.Descripcion NOT LIKE 'a'
AND t.Descripcion NOT LIKE 'o'
AND t.Descripcion NOT LIKE ''
AND t.Descripcion NOT LIKE 'del'
AND t.Descripcion NOT LIKE 'las'
AND t.Descripcion NOT LIKE 'los'
AND t.Descripcion NOT LIKE 'un'
AND t.Descripcion NOT LIKE 'una'
AND t.Descripcion NOT LIKE '+'
AND t.Descripcion NOT LIKE '-'
AND t.Descripcion NOT LIKE '/'
AND t.Descripcion NOT LIKE '"'
AND t.Descripcion NOT LIKE '('
AND t.Descripcion NOT LIKE ')'
AND t.Descripcion NOT LIKE '*'
AND t.Descripcion NOT LIKE ','
AND t.Descripcion NOT LIKE '.'
AND t.Descripcion NOT LIKE '?'
AND t.Descripcion NOT LIKE '�'
AND t.Descripcion NOT LIKE '!'
AND t.Descripcion NOT LIKE '�'
AND t.Descripcion NOT LIKE '�'
AND t.Descripcion NOT LIKE '$'
AND t.Descripcion NOT LIKE '>'
AND t.Descripcion NOT LIKE '<'
) ORDER BY EMPRESAID, PRODUCTOID
INSERT INTO dbo.PALABRAS(Palabra, ProductoId, EmpresaId)
select * from 
(
SELECT UPPER(dbo.fnConvertirCaracteres(dbo.fn_StripCharacters(t.Descripcion, '^a-z0-9'))) AS PALABRA, p.Id AS PRODUCTOID, NULL AS EMPRESAID
FROM dbo.PRODUCTO p
CROSS APPLY dbo.SplitString(p.PalabrasClave,',') t
WHERE t.Descripcion NOT LIKE 'mis' 
AND t.Descripcion NOT LIKE 'el'
AND t.Descripcion NOT LIKE 'la'
AND t.Descripcion NOT LIKE 'de'
AND t.Descripcion NOT LIKE 'con'
AND t.Descripcion NOT LIKE 'y'
AND t.Descripcion NOT LIKE 'en'
AND t.Descripcion NOT LIKE 'a'
AND t.Descripcion NOT LIKE 'o'
AND t.Descripcion NOT LIKE ''
AND t.Descripcion NOT LIKE 'del'
AND t.Descripcion NOT LIKE 'las'
AND t.Descripcion NOT LIKE 'los'
AND t.Descripcion NOT LIKE 'un'
AND t.Descripcion NOT LIKE 'una'
AND t.Descripcion NOT LIKE '+'
AND t.Descripcion NOT LIKE '-'
AND t.Descripcion NOT LIKE '/'
AND t.Descripcion NOT LIKE '"'
AND t.Descripcion NOT LIKE '('
AND t.Descripcion NOT LIKE ')'
AND t.Descripcion NOT LIKE '*'
AND t.Descripcion NOT LIKE ','
AND t.Descripcion NOT LIKE '.'
AND t.Descripcion NOT LIKE '?'
AND t.Descripcion NOT LIKE '�'
AND t.Descripcion NOT LIKE '!'
AND t.Descripcion NOT LIKE '�'
AND t.Descripcion NOT LIKE '�'
AND t.Descripcion NOT LIKE '$'
AND t.Descripcion NOT LIKE '>'
AND t.Descripcion NOT LIKE '<'
) B
UNION
(
SELECT UPPER(dbo.fnConvertirCaracteres(dbo.fn_StripCharacters(t.Descripcion, '^a-z0-9'))) AS PALABRA, NULL AS PRODUCTOID, p.Id AS EMPRESAID
FROM dbo.EMPRESA p
CROSS APPLY dbo.SplitString(p.PalabrasClave,',') t
WHERE t.Descripcion NOT LIKE 'mis' 
AND t.Descripcion NOT LIKE 'el'
AND t.Descripcion NOT LIKE 'la'
AND t.Descripcion NOT LIKE 'de'
AND t.Descripcion NOT LIKE 'con'
AND t.Descripcion NOT LIKE 'y'
AND t.Descripcion NOT LIKE 'en'
AND t.Descripcion NOT LIKE 'a'
AND t.Descripcion NOT LIKE 'o'
AND t.Descripcion NOT LIKE ''
AND t.Descripcion NOT LIKE 'del'
AND t.Descripcion NOT LIKE 'las'
AND t.Descripcion NOT LIKE 'los'
AND t.Descripcion NOT LIKE 'un'
AND t.Descripcion NOT LIKE 'una'
AND t.Descripcion NOT LIKE '+'
AND t.Descripcion NOT LIKE '-'
AND t.Descripcion NOT LIKE '/'
AND t.Descripcion NOT LIKE '"'
AND t.Descripcion NOT LIKE '('
AND t.Descripcion NOT LIKE ')'
AND t.Descripcion NOT LIKE '*'
AND t.Descripcion NOT LIKE ','
AND t.Descripcion NOT LIKE '.'
AND t.Descripcion NOT LIKE '?'
AND t.Descripcion NOT LIKE '�'
AND t.Descripcion NOT LIKE '!'
AND t.Descripcion NOT LIKE '�'
AND t.Descripcion NOT LIKE '�'
AND t.Descripcion NOT LIKE '$'
AND t.Descripcion NOT LIKE '>'
AND t.Descripcion NOT LIKE '<'
) 
ORDER BY EMPRESAID, PRODUCTOID