﻿using System;
using System.Threading.Tasks;
using Cliente.Core.Repositories;

namespace Cliente.Core
{
    public interface IUnitOfWork : IDisposable
    {
        IAccionRepository Accion { get; }
        IActividadEconomicaRepository ActividadEconomica { get; }
        IActividadesRepository Actividades { get; }
        IActividadesPersonaRepository ActividadesPersona { get; }
        ICategoriaRepository Categoria { get; }
        ICategoriaEmpresaRepository CategoriaEmpresa { get; }
        ICiudadRepository Ciudad { get; }
        ICodigoPostalRepository CodigoPostal { get; }
        IComentariosEmpresaRepository ComentariosEmpresa { get; }
        IDatosContactoRepository DatosContacto { get; }
        IDireccionRepository Direccion { get; }
        IEmpresaRepository Empresa { get; }
        IEstadisticasRepository Estadisticas { get; }
        IEstadisticasCategoriasRepository EstadisticasCategorias { get; }
        IEstadisticasEmpresaRepository EstadisticasEmpresa { get; }
        IEstadisticasProductoRepository EstadisticasProducto { get; }
        IEstadisticasProductoEmpresaRepository EstadisticasProductoEmpresa { get; }
        IEstadisticasRubroRepository EstadisticasRubro { get; }
        IEstadisticasRubroEmpresaRepository EstadisticasRubroEmpresa { get; }
        IEstadoCivilRepository EstadoCivil { get; }
        IGeneroRepository Genero { get; }
        IHorarioRepository Horario { get; }
        IHorarioContactoRepository HorarioContacto { get; }
        IImagenesEmpresaRepository ImagenesEmpresa { get; }
        ILocalidadRepository Localidad { get; }
        IPaisRepository Pais { get; }
        IPalabrasRepository Palabras { get; }
        IParametrosRepository Parametros { get; }
        IParametrosAccionRepository ParametrosAccion { get; }
        IPermutacionesRepository Permutaciones { get; }
        IPermutacionesProductoEmpresaRepository PermutacionesProductoEmpresa { get; }
        IPlanEmpresaRepository PlanEmpresa { get; }
        IPlanesRepository Planes { get; }
        IProductoRepository Producto { get; }
        IProductoEmpresaRepository ProductoEmpresa { get; }
        IRedSocialRepository RedSocial { get; }
        IRedSocialEmpresaRepository RedSocialEmpresa { get; }
        IRedSocialUsuarioRepository RedSocialUsuario { get; }
        IRubroRepository Rubro { get; }
        IRubroEmpresaRepository RubroEmpresa { get; }
        ISesionRepository Sesion { get; }
        ITipoEmpresaRepository TipoEmpresa { get; }
        ITipoPlanRepository TipoPlan { get; }
        ITipoUsuarioRepository TipoUsuario { get; }
        IUsuarioRepository Usuario { get; }
        IVisitaRepository Visita { get; }
        Task<int> Complete();
    }
}
