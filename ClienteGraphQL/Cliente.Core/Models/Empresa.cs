﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("EMPRESA")]
    public partial class Empresa
    {
        public Empresa()
        {
            CategoriaEmpresa = new HashSet<CategoriaEmpresa>();
            ComentariosEmpresa = new HashSet<ComentariosEmpresa>();
            DatosContacto = new HashSet<DatosContacto>();
            Direccion = new HashSet<Direccion>();
            EstadisticasEmpresa = new HashSet<EstadisticasEmpresa>();
            HorarioContacto = new HashSet<HorarioContacto>();
            ImagenesEmpresa = new HashSet<ImagenesEmpresa>();
            Palabras = new HashSet<Palabras>();
            PermutacionesProductoEmpresa = new HashSet<PermutacionesProductoEmpresa>();
            PlanEmpresa = new HashSet<PlanEmpresa>();
            ProductoEmpresa = new HashSet<ProductoEmpresa>();
            RedSocialEmpresa = new HashSet<RedSocialEmpresa>();
            Sesion = new HashSet<Sesion>();
            Visita = new HashSet<Visita>();
        }

        [Column("ID")]
        public long Id { get; set; }
        [Required]
        [StringLength(100)]
        public string Nombre { get; set; }
        public string FotoPerfil { get; set; }
        public string Descripcion { get; set; }
        [StringLength(40)]
        public string Latitud { get; set; }
        [StringLength(40)]
        public string Longuitud { get; set; }
        [Required]
        [StringLength(150)]
        public string Correo { get; set; }
        [Required]
        [StringLength(20)]
        public string Contraseña { get; set; }
        public int? PonderacionTotal { get; set; }
        public string PalabrasClave { get; set; }
        public long? TipoEmpresaId { get; set; }
        [Column(TypeName = "date")]
        public DateTime? InicioOperaciones { get; set; }
        public int? CantidadTrabajadores { get; set; }

        [ForeignKey("TipoEmpresaId")]
        [InverseProperty("Empresa")]
        public virtual TipoEmpresa TipoEmpresa { get; set; }
        [InverseProperty("Empresa")]
        public virtual ICollection<CategoriaEmpresa> CategoriaEmpresa { get; set; }
        [InverseProperty("Empresa")]
        public virtual ICollection<ComentariosEmpresa> ComentariosEmpresa { get; set; }
        [InverseProperty("Empresa")]
        public virtual ICollection<DatosContacto> DatosContacto { get; set; }
        [InverseProperty("Empresa")]
        public virtual ICollection<Direccion> Direccion { get; set; }
        [InverseProperty("Empresa")]
        public virtual ICollection<EstadisticasEmpresa> EstadisticasEmpresa { get; set; }
        [InverseProperty("Empresa")]
        public virtual ICollection<HorarioContacto> HorarioContacto { get; set; }
        [InverseProperty("Empresa")]
        public virtual ICollection<ImagenesEmpresa> ImagenesEmpresa { get; set; }
        [InverseProperty("Empresa")]
        public virtual ICollection<Palabras> Palabras { get; set; }
        [InverseProperty("Empresa")]
        public virtual ICollection<PermutacionesProductoEmpresa> PermutacionesProductoEmpresa { get; set; }
        [InverseProperty("Empresa")]
        public virtual ICollection<PlanEmpresa> PlanEmpresa { get; set; }
        [InverseProperty("Empresa")]
        public virtual ICollection<ProductoEmpresa> ProductoEmpresa { get; set; }
        [InverseProperty("Empresa")]
        public virtual ICollection<RedSocialEmpresa> RedSocialEmpresa { get; set; }
        [InverseProperty("Empresa")]
        public virtual ICollection<Sesion> Sesion { get; set; }
        [InverseProperty("Empresa")]
        public virtual ICollection<Visita> Visita { get; set; }
    }
}
