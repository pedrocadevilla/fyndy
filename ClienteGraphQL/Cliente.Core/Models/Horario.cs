﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("HORARIO")]
    public partial class Horario
    {
        public Horario()
        {
            HorarioContacto = new HashSet<HorarioContacto>();
        }

        [Column("ID")]
        public long Id { get; set; }
        [Column("Horario")]
        [StringLength(60)]
        public string Horario1 { get; set; }

        [InverseProperty("Horario")]
        public virtual ICollection<HorarioContacto> HorarioContacto { get; set; }
    }
}
