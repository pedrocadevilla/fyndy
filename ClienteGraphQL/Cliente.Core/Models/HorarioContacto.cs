﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("HORARIO_CONTACTO")]
    public partial class HorarioContacto
    {
        [Column("ID")]
        public long Id { get; set; }
        public long? EmpresaId { get; set; }
        public long? HorarioId { get; set; }

        [ForeignKey("EmpresaId")]
        [InverseProperty("HorarioContacto")]
        public virtual Empresa Empresa { get; set; }
        [ForeignKey("HorarioId")]
        [InverseProperty("HorarioContacto")]
        public virtual Horario Horario { get; set; }
    }
}
