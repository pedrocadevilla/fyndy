﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("PALABRAS")]
    public partial class Palabras
    {
        [Column("ID")]
        public long Id { get; set; }
        [StringLength(100)]
        public string Palabra { get; set; }
        public long? ProductoId { get; set; }
        public long? EmpresaId { get; set; }

        [ForeignKey("EmpresaId")]
        [InverseProperty("Palabras")]
        public virtual Empresa Empresa { get; set; }
        [ForeignKey("ProductoId")]
        [InverseProperty("Palabras")]
        public virtual Producto Producto { get; set; }
    }
}
