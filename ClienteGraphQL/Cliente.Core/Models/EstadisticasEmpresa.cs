﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("ESTADISTICAS_EMPRESA")]
    public partial class EstadisticasEmpresa
    {
        [Column("ID")]
        public long Id { get; set; }
        public string Valor { get; set; }
        public long? EmpresaId { get; set; }
        public long? EstadisticaId { get; set; }

        [ForeignKey("EmpresaId")]
        [InverseProperty("EstadisticasEmpresa")]
        public virtual Empresa Empresa { get; set; }
        [ForeignKey("EstadisticaId")]
        [InverseProperty("EstadisticasEmpresa")]
        public virtual Estadisticas Estadistica { get; set; }
    }
}
