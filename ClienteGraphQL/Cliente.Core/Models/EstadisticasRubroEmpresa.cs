﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("ESTADISTICAS_RUBRO_EMPRESA")]
    public partial class EstadisticasRubroEmpresa
    {
        [Column("ID")]
        public long Id { get; set; }
        public string Valor { get; set; }
        public long? RubroId { get; set; }
        public long? EstadisticaId { get; set; }

        [ForeignKey("EstadisticaId")]
        [InverseProperty("EstadisticasRubroEmpresa")]
        public virtual Estadisticas Estadistica { get; set; }
        [ForeignKey("RubroId")]
        [InverseProperty("EstadisticasRubroEmpresa")]
        public virtual RubroEmpresa Rubro { get; set; }
    }
}
