﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("TIPO_USUARIO")]
    public partial class TipoUsuario
    {
        public TipoUsuario()
        {
            ComentariosEmpresa = new HashSet<ComentariosEmpresa>();
        }

        [Column("ID")]
        public long Id { get; set; }
        [StringLength(60)]
        public string Nombre { get; set; }

        [InverseProperty("TipoUsuario")]
        public virtual ICollection<ComentariosEmpresa> ComentariosEmpresa { get; set; }
    }
}
