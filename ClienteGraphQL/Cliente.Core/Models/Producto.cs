﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("PRODUCTO")]
    public partial class Producto
    {
        public Producto()
        {
            EstadisticasProducto = new HashSet<EstadisticasProducto>();
            Palabras = new HashSet<Palabras>();
            PermutacionesProductoEmpresa = new HashSet<PermutacionesProductoEmpresa>();
            ProductoEmpresa = new HashSet<ProductoEmpresa>();
        }

        [Column("ID")]
        public long Id { get; set; }
        public string Nombre { get; set; }
        public long? RubroId { get; set; }
        [Column(TypeName = "date")]
        public DateTime? FechaCreacion { get; set; }
        [StringLength(40)]
        public string Usuario { get; set; }
        public string PalabrasClave { get; set; }
        [Column(TypeName = "date")]
        public DateTime? FechaEliminacion { get; set; }

        [ForeignKey("RubroId")]
        [InverseProperty("Producto")]
        public virtual Rubro Rubro { get; set; }
        [InverseProperty("Producto")]
        public virtual ICollection<EstadisticasProducto> EstadisticasProducto { get; set; }
        [InverseProperty("Producto")]
        public virtual ICollection<Palabras> Palabras { get; set; }
        [InverseProperty("Producto")]
        public virtual ICollection<PermutacionesProductoEmpresa> PermutacionesProductoEmpresa { get; set; }
        [InverseProperty("Producto")]
        public virtual ICollection<ProductoEmpresa> ProductoEmpresa { get; set; }
    }
}
