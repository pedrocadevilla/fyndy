﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("ESTADISTICAS")]
    public partial class Estadisticas
    {
        public Estadisticas()
        {
            EstadisticasCategorias = new HashSet<EstadisticasCategorias>();
            EstadisticasCategoriasEmpresa = new HashSet<EstadisticasCategoriasEmpresa>();
            EstadisticasEmpresa = new HashSet<EstadisticasEmpresa>();
            EstadisticasProducto = new HashSet<EstadisticasProducto>();
            EstadisticasProductoEmpresa = new HashSet<EstadisticasProductoEmpresa>();
            EstadisticasRubro = new HashSet<EstadisticasRubro>();
            EstadisticasRubroEmpresa = new HashSet<EstadisticasRubroEmpresa>();
        }

        [Column("ID")]
        public long Id { get; set; }
        [StringLength(50)]
        public string Nombre { get; set; }
        public string Descripcion { get; set; }

        [InverseProperty("Estadistica")]
        public virtual ICollection<EstadisticasCategorias> EstadisticasCategorias { get; set; }
        [InverseProperty("Estadistica")]
        public virtual ICollection<EstadisticasCategoriasEmpresa> EstadisticasCategoriasEmpresa { get; set; }
        [InverseProperty("Estadistica")]
        public virtual ICollection<EstadisticasEmpresa> EstadisticasEmpresa { get; set; }
        [InverseProperty("Estadistica")]
        public virtual ICollection<EstadisticasProducto> EstadisticasProducto { get; set; }
        [InverseProperty("Estadistica")]
        public virtual ICollection<EstadisticasProductoEmpresa> EstadisticasProductoEmpresa { get; set; }
        [InverseProperty("Estadistica")]
        public virtual ICollection<EstadisticasRubro> EstadisticasRubro { get; set; }
        [InverseProperty("Estadistica")]
        public virtual ICollection<EstadisticasRubroEmpresa> EstadisticasRubroEmpresa { get; set; }
    }
}
