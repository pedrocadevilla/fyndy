﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("ESTADISTICAS_RUBRO")]
    public partial class EstadisticasRubro
    {
        [Column("ID")]
        public long Id { get; set; }
        public string Valor { get; set; }
        public long? RubroId { get; set; }
        public long? EstadisticaId { get; set; }

        [ForeignKey("EstadisticaId")]
        [InverseProperty("EstadisticasRubro")]
        public virtual Estadisticas Estadistica { get; set; }
        [ForeignKey("RubroId")]
        [InverseProperty("EstadisticasRubro")]
        public virtual Rubro Rubro { get; set; }
    }
}
