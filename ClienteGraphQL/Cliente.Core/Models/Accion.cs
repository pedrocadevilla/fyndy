﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("ACCION")]
    public partial class Accion
    {
        public Accion()
        {
            ParametrosAccion = new HashSet<ParametrosAccion>();
            Visita = new HashSet<Visita>();
        }

        [Column("ID")]
        public long Id { get; set; }
        [StringLength(40)]
        public string Nombre { get; set; }
        [StringLength(300)]
        public string Descripcion { get; set; }

        [InverseProperty("Accion")]
        public virtual ICollection<ParametrosAccion> ParametrosAccion { get; set; }
        [InverseProperty("Accion")]
        public virtual ICollection<Visita> Visita { get; set; }
    }
}
