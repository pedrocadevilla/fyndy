﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("ESTADISTICAS_CATEGORIAS")]
    public partial class EstadisticasCategorias
    {
        [Column("ID")]
        public long Id { get; set; }
        public long? CategoriaId { get; set; }
        public string Valor { get; set; }
        public long? EstadisticaId { get; set; }

        [ForeignKey("CategoriaId")]
        [InverseProperty("EstadisticasCategorias")]
        public virtual Categoria Categoria { get; set; }
        [ForeignKey("EstadisticaId")]
        [InverseProperty("EstadisticasCategorias")]
        public virtual Estadisticas Estadistica { get; set; }
    }
}
