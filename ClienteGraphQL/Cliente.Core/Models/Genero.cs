﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("GENERO")]
    public partial class Genero
    {
        public Genero()
        {
            Usuario = new HashSet<Usuario>();
        }

        [Column("ID")]
        public long Id { get; set; }
        [StringLength(20)]
        public string Nombre { get; set; }

        [InverseProperty("Genero")]
        public virtual ICollection<Usuario> Usuario { get; set; }
    }
}
