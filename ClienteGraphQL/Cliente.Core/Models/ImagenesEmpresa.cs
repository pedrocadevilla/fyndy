﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("IMAGENES_EMPRESA")]
    public partial class ImagenesEmpresa
    {
        [Column("ID")]
        public long Id { get; set; }
        public string Imagen { get; set; }
        public long? EmpresaId { get; set; }

        [ForeignKey("EmpresaId")]
        [InverseProperty("ImagenesEmpresa")]
        public virtual Empresa Empresa { get; set; }
    }
}
