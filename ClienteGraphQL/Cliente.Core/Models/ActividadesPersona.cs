﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("ACTIVIDADES_PERSONA")]
    public partial class ActividadesPersona
    {
        [Column("ID")]
        public long Id { get; set; }
        public long? UsuarioId { get; set; }
        public long? ActividadId { get; set; }

        [ForeignKey("ActividadId")]
        [InverseProperty("ActividadesPersona")]
        public virtual Actividades Actividad { get; set; }
        [ForeignKey("UsuarioId")]
        [InverseProperty("ActividadesPersona")]
        public virtual Usuario Usuario { get; set; }
    }
}
