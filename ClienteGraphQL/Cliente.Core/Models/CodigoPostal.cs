﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("CODIGO_POSTAL")]
    public partial class CodigoPostal
    {
        public CodigoPostal()
        {
            Direccion = new HashSet<Direccion>();
            Usuario = new HashSet<Usuario>();
        }

        [Column("ID")]
        public long Id { get; set; }
        [StringLength(20)]
        public string Nombre { get; set; }
        public long? CiudadId { get; set; }

        [ForeignKey("CiudadId")]
        [InverseProperty("CodigoPostal")]
        public virtual Ciudad Ciudad { get; set; }
        [InverseProperty("CodigoPostal")]
        public virtual ICollection<Direccion> Direccion { get; set; }
        [InverseProperty("CodigoPostal")]
        public virtual ICollection<Usuario> Usuario { get; set; }
    }
}
