﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("RUBRO_EMPRESA")]
    public partial class RubroEmpresa
    {
        public RubroEmpresa()
        {
            EstadisticasRubroEmpresa = new HashSet<EstadisticasRubroEmpresa>();
            ProductoEmpresa = new HashSet<ProductoEmpresa>();
        }

        [Column("ID")]
        public long Id { get; set; }
        public long? CategoriaEmpresaId { get; set; }
        public long? RubroId { get; set; }
        [Column(TypeName = "date")]
        public DateTime? FechaCreacion { get; set; }
        [Column(TypeName = "date")]
        public DateTime? FechaEliminacion { get; set; }

        [ForeignKey("CategoriaEmpresaId")]
        [InverseProperty("RubroEmpresa")]
        public virtual CategoriaEmpresa CategoriaEmpresa { get; set; }
        [ForeignKey("RubroId")]
        [InverseProperty("RubroEmpresa")]
        public virtual Rubro Rubro { get; set; }
        [InverseProperty("Rubro")]
        public virtual ICollection<EstadisticasRubroEmpresa> EstadisticasRubroEmpresa { get; set; }
        [InverseProperty("RubroEmpresa")]
        public virtual ICollection<ProductoEmpresa> ProductoEmpresa { get; set; }
    }
}
