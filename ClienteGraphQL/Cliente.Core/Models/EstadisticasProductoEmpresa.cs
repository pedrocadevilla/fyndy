﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("ESTADISTICAS_PRODUCTO_EMPRESA")]
    public partial class EstadisticasProductoEmpresa
    {
        [Column("ID")]
        public long Id { get; set; }
        public string Valor { get; set; }
        public long? ProductoId { get; set; }
        public long? EstadisticaId { get; set; }

        [ForeignKey("EstadisticaId")]
        [InverseProperty("EstadisticasProductoEmpresa")]
        public virtual Estadisticas Estadistica { get; set; }
        [ForeignKey("ProductoId")]
        [InverseProperty("EstadisticasProductoEmpresa")]
        public virtual ProductoEmpresa Producto { get; set; }
    }
}
