﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("PLAN_EMPRESA")]
    public partial class PlanEmpresa
    {
        [Column("ID")]
        public long Id { get; set; }
        public long? PlanId { get; set; }
        public long? EmpresaId { get; set; }
        [Column(TypeName = "date")]
        public DateTime? FechaInicio { get; set; }
        [Column(TypeName = "date")]
        public DateTime? FechaFin { get; set; }

        [ForeignKey("EmpresaId")]
        [InverseProperty("PlanEmpresa")]
        public virtual Empresa Empresa { get; set; }
        [ForeignKey("PlanId")]
        [InverseProperty("PlanEmpresa")]
        public virtual Planes Plan { get; set; }
    }
}
