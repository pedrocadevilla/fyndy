﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("CATEGORIA_EMPRESA")]
    public partial class CategoriaEmpresa
    {
        public CategoriaEmpresa()
        {
            EstadisticasCategoriasEmpresa = new HashSet<EstadisticasCategoriasEmpresa>();
            RubroEmpresa = new HashSet<RubroEmpresa>();
        }

        [Column("ID")]
        public long Id { get; set; }
        public long? EmpresaId { get; set; }
        public long? CategoriaId { get; set; }
        [Column(TypeName = "date")]
        public DateTime? FechaCreacion { get; set; }
        [Column(TypeName = "date")]
        public DateTime? FechaEliminacion { get; set; }

        [ForeignKey("CategoriaId")]
        [InverseProperty("CategoriaEmpresa")]
        public virtual Categoria Categoria { get; set; }
        [ForeignKey("EmpresaId")]
        [InverseProperty("CategoriaEmpresa")]
        public virtual Empresa Empresa { get; set; }
        [InverseProperty("Categoria")]
        public virtual ICollection<EstadisticasCategoriasEmpresa> EstadisticasCategoriasEmpresa { get; set; }
        [InverseProperty("CategoriaEmpresa")]
        public virtual ICollection<RubroEmpresa> RubroEmpresa { get; set; }
    }
}
