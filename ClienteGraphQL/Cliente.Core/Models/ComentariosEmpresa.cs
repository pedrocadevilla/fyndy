﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("COMENTARIOS_EMPRESA")]
    public partial class ComentariosEmpresa
    {
        [Column("ID")]
        public long Id { get; set; }
        [StringLength(250)]
        public string Comentarios { get; set; }
        public int? Clasificacion { get; set; }
        public long? EmpresaId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? Fecha { get; set; }
        public long? UsuarioId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaEliminacion { get; set; }
        public long? TipoUsuarioId { get; set; }

        [ForeignKey("EmpresaId")]
        [InverseProperty("ComentariosEmpresa")]
        public virtual Empresa Empresa { get; set; }
        [ForeignKey("TipoUsuarioId")]
        [InverseProperty("ComentariosEmpresa")]
        public virtual TipoUsuario TipoUsuario { get; set; }
        [ForeignKey("UsuarioId")]
        [InverseProperty("ComentariosEmpresa")]
        public virtual Usuario Usuario { get; set; }
    }
}
