﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("PERMUTACIONES")]
    public partial class Permutaciones
    {
        public Permutaciones()
        {
            PermutacionesProductoEmpresa = new HashSet<PermutacionesProductoEmpresa>();
        }

        [Column("ID")]
        public long Id { get; set; }
        [StringLength(200)]
        public string Frase { get; set; }

        [InverseProperty("Permutacion")]
        public virtual ICollection<PermutacionesProductoEmpresa> PermutacionesProductoEmpresa { get; set; }
    }
}
