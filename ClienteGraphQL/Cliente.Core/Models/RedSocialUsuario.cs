﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("RED_SOCIAL_USUARIO")]
    public partial class RedSocialUsuario
    {
        [Column("ID")]
        public long Id { get; set; }
        public string Link { get; set; }
        public long? UsuarioId { get; set; }
        public long? RedSocialId { get; set; }

        [ForeignKey("RedSocialId")]
        [InverseProperty("RedSocialUsuario")]
        public virtual RedSocial RedSocial { get; set; }
        [ForeignKey("UsuarioId")]
        [InverseProperty("RedSocialUsuario")]
        public virtual Usuario Usuario { get; set; }
    }
}
