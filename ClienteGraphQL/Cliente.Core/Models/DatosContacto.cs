﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("DATOS_CONTACTO")]
    public partial class DatosContacto
    {
        [Column("ID")]
        public long Id { get; set; }
        public string Contacto { get; set; }
        public long? EmpresaId { get; set; }

        [ForeignKey("EmpresaId")]
        [InverseProperty("DatosContacto")]
        public virtual Empresa Empresa { get; set; }
    }
}
