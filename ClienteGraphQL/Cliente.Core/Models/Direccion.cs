﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("DIRECCION")]
    public partial class Direccion
    {
        [Column("ID")]
        public long Id { get; set; }
        public string InformacionAdicional { get; set; }
        public long? CodigoPostalId { get; set; }
        public long? EmpresaId { get; set; }
        public bool? Primaria { get; set; }
        public long? PaisId { get; set; }
        public long? LocalidadId { get; set; }
        public long? CiudadId { get; set; }

        [ForeignKey("CiudadId")]
        [InverseProperty("Direccion")]
        public virtual Ciudad Ciudad { get; set; }
        [ForeignKey("CodigoPostalId")]
        [InverseProperty("Direccion")]
        public virtual CodigoPostal CodigoPostal { get; set; }
        [ForeignKey("EmpresaId")]
        [InverseProperty("Direccion")]
        public virtual Empresa Empresa { get; set; }
        [ForeignKey("LocalidadId")]
        [InverseProperty("Direccion")]
        public virtual Localidad Localidad { get; set; }
        [ForeignKey("PaisId")]
        [InverseProperty("Direccion")]
        public virtual Pais Pais { get; set; }
    }
}
