﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("ACTIVIDADES_ECONOMICAS_PERSONA")]
    public partial class ActividadesEconomicasPersona
    {
        [Column("ID")]
        public long Id { get; set; }
        public long? ActividadEconomicaId { get; set; }
        public long? UsuarioId { get; set; }

        [ForeignKey("ActividadEconomicaId")]
        [InverseProperty("ActividadesEconomicasPersona")]
        public virtual ActividadEconomica ActividadEconomica { get; set; }
        [ForeignKey("UsuarioId")]
        [InverseProperty("ActividadesEconomicasPersona")]
        public virtual Usuario Usuario { get; set; }
    }
}
