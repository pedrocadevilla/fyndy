﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("ESTADISTICAS_PRODUCTO")]
    public partial class EstadisticasProducto
    {
        [Column("ID")]
        public long Id { get; set; }
        public string Valor { get; set; }
        public long? ProductoId { get; set; }
        public long? EstadisticaId { get; set; }

        [ForeignKey("EstadisticaId")]
        [InverseProperty("EstadisticasProducto")]
        public virtual Estadisticas Estadistica { get; set; }
        [ForeignKey("ProductoId")]
        [InverseProperty("EstadisticasProducto")]
        public virtual Producto Producto { get; set; }
    }
}
