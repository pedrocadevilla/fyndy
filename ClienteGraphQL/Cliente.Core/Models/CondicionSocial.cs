﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("CONDICION_SOCIAL")]
    public partial class CondicionSocial
    {
        public CondicionSocial()
        {
            Usuario = new HashSet<Usuario>();
        }

        [Column("ID")]
        public long Id { get; set; }
        [StringLength(80)]
        public string Nombre { get; set; }

        [InverseProperty("CondicionSocial")]
        public virtual ICollection<Usuario> Usuario { get; set; }
    }
}
