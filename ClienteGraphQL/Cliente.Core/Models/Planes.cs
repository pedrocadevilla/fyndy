﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("PLANES")]
    public partial class Planes
    {
        public Planes()
        {
            PlanEmpresa = new HashSet<PlanEmpresa>();
        }

        [Column("ID")]
        public long Id { get; set; }
        [StringLength(20)]
        public string Nombre { get; set; }
        public int? Ponderacion { get; set; }
        public long? TipoPlanId { get; set; }
        public bool? Estado { get; set; }

        [ForeignKey("TipoPlanId")]
        [InverseProperty("Planes")]
        public virtual TipoPlan TipoPlan { get; set; }
        [InverseProperty("Plan")]
        public virtual ICollection<PlanEmpresa> PlanEmpresa { get; set; }
    }
}
