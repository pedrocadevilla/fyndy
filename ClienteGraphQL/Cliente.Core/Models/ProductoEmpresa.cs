﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("PRODUCTO_EMPRESA")]
    public partial class ProductoEmpresa
    {
        public ProductoEmpresa()
        {
            EstadisticasProductoEmpresa = new HashSet<EstadisticasProductoEmpresa>();
        }

        [Column("ID")]
        public long Id { get; set; }
        public long? RubroEmpresaId { get; set; }
        public long? ProductoId { get; set; }
        public long? EmpresaId { get; set; }
        [Column(TypeName = "date")]
        public DateTime? FechaEliminacion { get; set; }
        [Column(TypeName = "date")]
        public DateTime? FechaCreacion { get; set; }

        [ForeignKey("EmpresaId")]
        [InverseProperty("ProductoEmpresa")]
        public virtual Empresa Empresa { get; set; }
        [ForeignKey("ProductoId")]
        [InverseProperty("ProductoEmpresa")]
        public virtual Producto Producto { get; set; }
        [ForeignKey("RubroEmpresaId")]
        [InverseProperty("ProductoEmpresa")]
        public virtual RubroEmpresa RubroEmpresa { get; set; }
        [InverseProperty("Producto")]
        public virtual ICollection<EstadisticasProductoEmpresa> EstadisticasProductoEmpresa { get; set; }
    }
}
