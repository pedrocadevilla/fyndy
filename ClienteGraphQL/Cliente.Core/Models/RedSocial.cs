﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("RED_SOCIAL")]
    public partial class RedSocial
    {
        public RedSocial()
        {
            RedSocialEmpresa = new HashSet<RedSocialEmpresa>();
            RedSocialUsuario = new HashSet<RedSocialUsuario>();
        }

        [Column("ID")]
        public long Id { get; set; }
        [StringLength(20)]
        public string Nombre { get; set; }

        [InverseProperty("RedSocial")]
        public virtual ICollection<RedSocialEmpresa> RedSocialEmpresa { get; set; }
        [InverseProperty("RedSocial")]
        public virtual ICollection<RedSocialUsuario> RedSocialUsuario { get; set; }
    }
}
