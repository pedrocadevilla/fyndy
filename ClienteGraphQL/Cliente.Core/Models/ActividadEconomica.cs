﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("ACTIVIDAD_ECONOMICA")]
    public partial class ActividadEconomica
    {
        public ActividadEconomica()
        {
            ActividadesEconomicasPersona = new HashSet<ActividadesEconomicasPersona>();
        }

        [Column("ID")]
        public long Id { get; set; }
        [StringLength(80)]
        public string Nombre { get; set; }

        [InverseProperty("ActividadEconomica")]
        public virtual ICollection<ActividadesEconomicasPersona> ActividadesEconomicasPersona { get; set; }
    }
}
