﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("RED_SOCIAL_EMPRESA")]
    public partial class RedSocialEmpresa
    {
        [Column("ID")]
        public long Id { get; set; }
        public long? RedSocialId { get; set; }
        public long? EmpresaId { get; set; }
        public byte[] Link { get; set; }

        [ForeignKey("EmpresaId")]
        [InverseProperty("RedSocialEmpresa")]
        public virtual Empresa Empresa { get; set; }
        [ForeignKey("RedSocialId")]
        [InverseProperty("RedSocialEmpresa")]
        public virtual RedSocial RedSocial { get; set; }
    }
}
