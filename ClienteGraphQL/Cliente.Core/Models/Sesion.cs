﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("SESION")]
    public partial class Sesion
    {
        [Column("ID")]
        public long Id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaInicio { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaFin { get; set; }
        public string Token { get; set; }
        public long? UsuarioId { get; set; }
        public long? EmpresaId { get; set; }

        [ForeignKey("EmpresaId")]
        [InverseProperty("Sesion")]
        public virtual Empresa Empresa { get; set; }
        [ForeignKey("UsuarioId")]
        [InverseProperty("Sesion")]
        public virtual Usuario Usuario { get; set; }
    }
}
