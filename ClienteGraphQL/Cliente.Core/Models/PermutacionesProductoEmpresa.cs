﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("PERMUTACIONES_PRODUCTO_EMPRESA")]
    public partial class PermutacionesProductoEmpresa
    {
        [Column("ID")]
        public long Id { get; set; }
        public long? PermutacionId { get; set; }
        public long? ProductoId { get; set; }
        public long? EmpresaId { get; set; }

        [ForeignKey("EmpresaId")]
        [InverseProperty("PermutacionesProductoEmpresa")]
        public virtual Empresa Empresa { get; set; }
        [ForeignKey("PermutacionId")]
        [InverseProperty("PermutacionesProductoEmpresa")]
        public virtual Permutaciones Permutacion { get; set; }
        [ForeignKey("ProductoId")]
        [InverseProperty("PermutacionesProductoEmpresa")]
        public virtual Producto Producto { get; set; }
    }
}
