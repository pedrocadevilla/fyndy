﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("PARAMETROS_ACCION")]
    public partial class ParametrosAccion
    {
        [Column("ID")]
        public long Id { get; set; }
        [StringLength(1)]
        public string Valor { get; set; }
        public long? AccionId { get; set; }
        public long? ParametrosId { get; set; }

        [ForeignKey("AccionId")]
        [InverseProperty("ParametrosAccion")]
        public virtual Accion Accion { get; set; }
        [ForeignKey("ParametrosId")]
        [InverseProperty("ParametrosAccion")]
        public virtual Parametros Parametros { get; set; }
    }
}
