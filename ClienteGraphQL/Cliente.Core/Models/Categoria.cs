﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("CATEGORIA")]
    public partial class Categoria
    {
        public Categoria()
        {
            CategoriaEmpresa = new HashSet<CategoriaEmpresa>();
            EstadisticasCategorias = new HashSet<EstadisticasCategorias>();
            Rubro = new HashSet<Rubro>();
        }

        [Column("ID")]
        public long Id { get; set; }
        [StringLength(50)]
        public string Nombre { get; set; }
        [Column(TypeName = "date")]
        public DateTime? FechaCreacion { get; set; }
        [StringLength(40)]
        public string Usuario { get; set; }
        [Column(TypeName = "date")]
        public DateTime? FechaEliminacion { get; set; }

        [InverseProperty("Categoria")]
        public virtual ICollection<CategoriaEmpresa> CategoriaEmpresa { get; set; }
        [InverseProperty("Categoria")]
        public virtual ICollection<EstadisticasCategorias> EstadisticasCategorias { get; set; }
        [InverseProperty("Categoria")]
        public virtual ICollection<Rubro> Rubro { get; set; }
    }
}
