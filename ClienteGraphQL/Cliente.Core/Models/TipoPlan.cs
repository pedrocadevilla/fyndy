﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("TIPO_PLAN")]
    public partial class TipoPlan
    {
        public TipoPlan()
        {
            Planes = new HashSet<Planes>();
        }

        [Column("ID")]
        public long Id { get; set; }
        [StringLength(20)]
        public string Nombre { get; set; }

        [InverseProperty("TipoPlan")]
        public virtual ICollection<Planes> Planes { get; set; }
    }
}
