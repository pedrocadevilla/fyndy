﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("VISITA")]
    public partial class Visita
    {
        [Column("ID")]
        public long Id { get; set; }
        [Column(TypeName = "date")]
        public DateTime? Fecha { get; set; }
        public long? UsuarioId { get; set; }
        public long? EmpresaId { get; set; }
        public long? PaisId { get; set; }
        public long? LocalidadId { get; set; }
        public long? CiudadId { get; set; }
        public long? AccionId { get; set; }

        [ForeignKey("AccionId")]
        [InverseProperty("Visita")]
        public virtual Accion Accion { get; set; }
        [ForeignKey("CiudadId")]
        [InverseProperty("Visita")]
        public virtual Ciudad Ciudad { get; set; }
        [ForeignKey("EmpresaId")]
        [InverseProperty("Visita")]
        public virtual Empresa Empresa { get; set; }
        [ForeignKey("LocalidadId")]
        [InverseProperty("Visita")]
        public virtual Localidad Localidad { get; set; }
        [ForeignKey("PaisId")]
        [InverseProperty("Visita")]
        public virtual Pais Pais { get; set; }
        [ForeignKey("UsuarioId")]
        [InverseProperty("Visita")]
        public virtual Usuario Usuario { get; set; }
    }
}
