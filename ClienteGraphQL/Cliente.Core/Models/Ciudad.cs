﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("CIUDAD")]
    public partial class Ciudad
    {
        public Ciudad()
        {
            CodigoPostal = new HashSet<CodigoPostal>();
            Direccion = new HashSet<Direccion>();
            Usuario = new HashSet<Usuario>();
            Visita = new HashSet<Visita>();
        }

        [Column("ID")]
        public long Id { get; set; }
        [StringLength(20)]
        public string Nombre { get; set; }
        public long? LocalidadId { get; set; }

        [ForeignKey("LocalidadId")]
        [InverseProperty("Ciudad")]
        public virtual Localidad Localidad { get; set; }
        [InverseProperty("Ciudad")]
        public virtual ICollection<CodigoPostal> CodigoPostal { get; set; }
        [InverseProperty("Ciudad")]
        public virtual ICollection<Direccion> Direccion { get; set; }
        [InverseProperty("Ciudad")]
        public virtual ICollection<Usuario> Usuario { get; set; }
        [InverseProperty("Ciudad")]
        public virtual ICollection<Visita> Visita { get; set; }
    }
}
