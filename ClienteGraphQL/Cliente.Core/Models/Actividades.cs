﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("ACTIVIDADES")]
    public partial class Actividades
    {
        public Actividades()
        {
            ActividadesPersona = new HashSet<ActividadesPersona>();
        }

        [Column("ID")]
        public long Id { get; set; }
        [StringLength(80)]
        public string Nombre { get; set; }

        [InverseProperty("Actividad")]
        public virtual ICollection<ActividadesPersona> ActividadesPersona { get; set; }
    }
}
