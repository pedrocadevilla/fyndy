﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("PARAMETROS")]
    public partial class Parametros
    {
        public Parametros()
        {
            ParametrosAccion = new HashSet<ParametrosAccion>();
        }

        [Column("ID")]
        public long Id { get; set; }
        [StringLength(40)]
        public string Nombre { get; set; }

        [InverseProperty("Parametros")]
        public virtual ICollection<ParametrosAccion> ParametrosAccion { get; set; }
    }
}
