﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("TIPO_EMPRESA")]
    public partial class TipoEmpresa
    {
        public TipoEmpresa()
        {
            Empresa = new HashSet<Empresa>();
        }

        [Column("ID")]
        public long Id { get; set; }
        [StringLength(40)]
        public string Nombre { get; set; }

        [InverseProperty("TipoEmpresa")]
        public virtual ICollection<Empresa> Empresa { get; set; }
    }
}
