﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("ESTADO_CIVIL")]
    public partial class EstadoCivil
    {
        public EstadoCivil()
        {
            Usuario = new HashSet<Usuario>();
        }

        [Column("ID")]
        public long Id { get; set; }
        [StringLength(20)]
        public string Nombre { get; set; }

        [InverseProperty("EstadoCivil")]
        public virtual ICollection<Usuario> Usuario { get; set; }
    }
}
