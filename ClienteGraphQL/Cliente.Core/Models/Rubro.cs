﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("RUBRO")]
    public partial class Rubro
    {
        public Rubro()
        {
            EstadisticasRubro = new HashSet<EstadisticasRubro>();
            Producto = new HashSet<Producto>();
            RubroEmpresa = new HashSet<RubroEmpresa>();
        }

        [Column("ID")]
        public long Id { get; set; }
        public string Nombre { get; set; }
        public long? CategoriaId { get; set; }
        [Column(TypeName = "date")]
        public DateTime? FechaCreacion { get; set; }
        [StringLength(40)]
        public string Usuario { get; set; }
        [Column(TypeName = "date")]
        public DateTime? FechaEliminacion { get; set; }

        [ForeignKey("CategoriaId")]
        [InverseProperty("Rubro")]
        public virtual Categoria Categoria { get; set; }
        [InverseProperty("Rubro")]
        public virtual ICollection<EstadisticasRubro> EstadisticasRubro { get; set; }
        [InverseProperty("Rubro")]
        public virtual ICollection<Producto> Producto { get; set; }
        [InverseProperty("Rubro")]
        public virtual ICollection<RubroEmpresa> RubroEmpresa { get; set; }
    }
}
