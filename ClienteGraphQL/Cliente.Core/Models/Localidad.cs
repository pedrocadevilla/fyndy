﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("LOCALIDAD")]
    public partial class Localidad
    {
        public Localidad()
        {
            Ciudad = new HashSet<Ciudad>();
            Direccion = new HashSet<Direccion>();
            Usuario = new HashSet<Usuario>();
            Visita = new HashSet<Visita>();
        }

        [Column("ID")]
        public long Id { get; set; }
        [StringLength(20)]
        public string Nombre { get; set; }
        public long? PaisId { get; set; }

        [ForeignKey("PaisId")]
        [InverseProperty("Localidad")]
        public virtual Pais Pais { get; set; }
        [InverseProperty("Localidad")]
        public virtual ICollection<Ciudad> Ciudad { get; set; }
        [InverseProperty("Localidad")]
        public virtual ICollection<Direccion> Direccion { get; set; }
        [InverseProperty("Localidad")]
        public virtual ICollection<Usuario> Usuario { get; set; }
        [InverseProperty("Localidad")]
        public virtual ICollection<Visita> Visita { get; set; }
    }
}
