﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("PAIS")]
    public partial class Pais
    {
        public Pais()
        {
            Direccion = new HashSet<Direccion>();
            Localidad = new HashSet<Localidad>();
            Usuario = new HashSet<Usuario>();
            Visita = new HashSet<Visita>();
        }

        [Column("ID")]
        public long Id { get; set; }
        [StringLength(20)]
        public string Nombre { get; set; }

        [InverseProperty("Pais")]
        public virtual ICollection<Direccion> Direccion { get; set; }
        [InverseProperty("Pais")]
        public virtual ICollection<Localidad> Localidad { get; set; }
        [InverseProperty("Pais")]
        public virtual ICollection<Usuario> Usuario { get; set; }
        [InverseProperty("Pais")]
        public virtual ICollection<Visita> Visita { get; set; }
    }
}
