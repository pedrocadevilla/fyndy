﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cliente.Core.Models
{
    [Table("USUARIO")]
    public partial class Usuario
    {
        public Usuario()
        {
            ActividadesEconomicasPersona = new HashSet<ActividadesEconomicasPersona>();
            ActividadesPersona = new HashSet<ActividadesPersona>();
            ComentariosEmpresa = new HashSet<ComentariosEmpresa>();
            RedSocialUsuario = new HashSet<RedSocialUsuario>();
            Sesion = new HashSet<Sesion>();
            Visita = new HashSet<Visita>();
        }

        [Column("ID")]
        public long Id { get; set; }
        [StringLength(20)]
        public string PrimerNombre { get; set; }
        [StringLength(20)]
        public string SegundoNombre { get; set; }
        [StringLength(20)]
        public string PrimerApellido { get; set; }
        [StringLength(20)]
        public string SegundoApellido { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaNacimiento { get; set; }
        [StringLength(150)]
        public string Correo { get; set; }
        [StringLength(20)]
        public string Contraseña { get; set; }
        public string FotoPerfil { get; set; }
        public long? GeneroId { get; set; }
        public long? EstadoCivilId { get; set; }
        public int? Tipo { get; set; }
        public long? CondicionSocialId { get; set; }
        public long? PaisId { get; set; }
        public long? LocalidadId { get; set; }
        public long? CiudadId { get; set; }
        public long? CodigoPostalId { get; set; }

        [ForeignKey("CiudadId")]
        [InverseProperty("Usuario")]
        public virtual Ciudad Ciudad { get; set; }
        [ForeignKey("CodigoPostalId")]
        [InverseProperty("Usuario")]
        public virtual CodigoPostal CodigoPostal { get; set; }
        [ForeignKey("CondicionSocialId")]
        [InverseProperty("Usuario")]
        public virtual CondicionSocial CondicionSocial { get; set; }
        [ForeignKey("EstadoCivilId")]
        [InverseProperty("Usuario")]
        public virtual EstadoCivil EstadoCivil { get; set; }
        [ForeignKey("GeneroId")]
        [InverseProperty("Usuario")]
        public virtual Genero Genero { get; set; }
        [ForeignKey("LocalidadId")]
        [InverseProperty("Usuario")]
        public virtual Localidad Localidad { get; set; }
        [ForeignKey("PaisId")]
        [InverseProperty("Usuario")]
        public virtual Pais Pais { get; set; }
        [InverseProperty("Usuario")]
        public virtual ICollection<ActividadesEconomicasPersona> ActividadesEconomicasPersona { get; set; }
        [InverseProperty("Usuario")]
        public virtual ICollection<ActividadesPersona> ActividadesPersona { get; set; }
        [InverseProperty("Usuario")]
        public virtual ICollection<ComentariosEmpresa> ComentariosEmpresa { get; set; }
        [InverseProperty("Usuario")]
        public virtual ICollection<RedSocialUsuario> RedSocialUsuario { get; set; }
        [InverseProperty("Usuario")]
        public virtual ICollection<Sesion> Sesion { get; set; }
        [InverseProperty("Usuario")]
        public virtual ICollection<Visita> Visita { get; set; }
    }
}
