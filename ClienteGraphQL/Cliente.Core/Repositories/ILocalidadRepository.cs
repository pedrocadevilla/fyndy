﻿using Cliente.Core.Models;
using Common.Utils.Repository;

namespace Cliente.Core.Repositories
{
    public interface ILocalidadRepository : IRepository<Localidad>
    {
    }
}