﻿using Cliente.Core.Models;
using Common.Utils.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cliente.Core.Repositories
{
    public interface IRubroEmpresaRepository : IRepository<RubroEmpresa>
    {
        Task<RubroEmpresa> ObtenerRubroEmpresaAsync(long? rubroId, long? categoriaId);
        Task<List<RubroEmpresa>> ObtenerListaRubroEmpresaAsync(List<long> lista);
    }
}