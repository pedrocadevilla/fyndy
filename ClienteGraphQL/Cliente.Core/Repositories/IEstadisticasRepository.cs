﻿using Cliente.Core.Models;
using Common.Utils.Repository;
using System.Threading.Tasks;

namespace Cliente.Core.Repositories
{
    public interface IEstadisticasRepository : IRepository<Estadisticas>
    {
        Task<long> obtenerEstatadisticaXNombre(string nombre);
    }
}