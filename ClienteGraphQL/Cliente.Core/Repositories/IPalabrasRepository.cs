﻿using Cliente.Core.Models;
using Common.Utils.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cliente.Core.Repositories
{
    public interface IPalabrasRepository : IRepository<Palabras>
    {
        Task<List<Palabras>> BuscarPalabrasProducto();
        Task<List<Palabras>> BuscarPalabrasEmpresa();
    }
}