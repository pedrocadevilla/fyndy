﻿using Cliente.Core.Models;
using Common.Utils.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cliente.Core.Repositories
{
    public interface IEmpresaRepository : IRepository<Empresa>
    {
        Task<Empresa> inicioSesion(string correo, string password);
        Task<Empresa> empresaExiste(string correo);
        Task<List<Empresa>> principalesEmpresasAsync();
    }
}