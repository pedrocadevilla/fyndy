﻿using Cliente.Core.Models;
using Common.Utils.Repository;
using System.Threading.Tasks;

namespace Cliente.Core.Repositories
{
    public interface IEstadisticasEmpresaRepository : IRepository<EstadisticasEmpresa>
    {
        Task<EstadisticasEmpresa> obtenerEstadisticaEmpresaXId(long categoriaId, long estadisticaId);
    }
}