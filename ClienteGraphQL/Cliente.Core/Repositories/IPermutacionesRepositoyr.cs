﻿using Cliente.Core.Clases;
using Cliente.Core.Models;
using Common.Utils.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cliente.Core.Repositories
{
    public interface IPermutacionesRepository : IRepository<Permutaciones>
    {
        Task<bool> ExistePermutacion(string Frase);
        Task<long> IdPermutacion(string Frase);
        Task<List<long>> IdPermutacionBusqueda(List<string> Palabras);
        Task<List<ElementoEncontrado>> busquedaProductosId(List<long> Permutaciones);
    }
}