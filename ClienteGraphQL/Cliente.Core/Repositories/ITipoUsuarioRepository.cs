﻿using Cliente.Core.Models;
using Common.Utils.Repository;
using System.Threading.Tasks;

namespace Cliente.Core.Repositories
{
    public interface ITipoUsuarioRepository : IRepository<TipoUsuario>
    {
        Task<TipoUsuario> ObtenerTipoUsuario(string nombre);
    }
}