﻿using Cliente.Core.Clases;
using Cliente.Core.Models;
using Common.Utils.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cliente.Core.Repositories
{
    public interface IProductoEmpresaRepository : IRepository<ProductoEmpresa>
    {
        Task<List<ProductoEmpresa>> busquedaProductos(List<ElementoEncontrado> Elementos, long? filtroId, long? tipoFiltroId);
        Task<List<ProductoEmpresa>> busquedaProductosXCategoriaAsync(List<long> rubros);
        Task<List<ProductoEmpresa>> busquedaProductosXEmpresaAsync(long empresaId);
    }
}