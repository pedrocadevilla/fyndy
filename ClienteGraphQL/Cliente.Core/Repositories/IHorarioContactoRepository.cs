﻿using Cliente.Core.Models;
using Common.Utils.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cliente.Core.Repositories
{
    public interface IHorarioContactoRepository : IRepository<HorarioContacto>
    {
        Task<IEnumerable<HorarioContacto>> ObtenerHorariosXEliminar(long empresaId, long[] horarioId);
        Task<HorarioContacto> ObtenerXIdHorario(long empresaId, long horarioId);
    }
}