﻿using Cliente.Core.Models;
using Common.Utils.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cliente.Core.Repositories
{
    public interface IComentariosEmpresaRepository : IRepository<ComentariosEmpresa>
    {
        Task<List<ComentariosEmpresa>> ObtenerComentariosEmpresaXId(long id);
    }
}