﻿using Cliente.Core.Models;
using Common.Utils.Repository;

namespace Cliente.Core.Repositories
{
    public interface IRubroRepository : IRepository<Rubro>
    {
    }
}