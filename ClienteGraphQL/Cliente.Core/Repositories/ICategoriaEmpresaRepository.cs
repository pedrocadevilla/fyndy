﻿using Cliente.Core.Models;
using Common.Utils.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cliente.Core.Repositories
{
    public interface ICategoriaEmpresaRepository : IRepository<CategoriaEmpresa>
    {
        Task<CategoriaEmpresa> obtenerCategoriaEmpresaAsync(long? categoriaEmpresa, long? empresaId);
        Task<List<CategoriaEmpresa>> obtenerListaCategoriaEmpresaAsync(long? categoriaEmpresa);
    }
}