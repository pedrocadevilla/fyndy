﻿using Cliente.Core.Models;
using Common.Utils.Repository;
using System.Threading.Tasks;

namespace Cliente.Core.Repositories
{
    public interface IUsuarioRepository : IRepository<Usuario>
    {
        Task<Usuario> inicioSesion(string correo, string password);
        Task<Usuario> usuarioExiste(string correo);
    }
}