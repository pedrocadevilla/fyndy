﻿using Cliente.Core.Models;
using Common.Utils.Repository;

namespace Cliente.Core.Repositories
{
    public interface IRedSocialEmpresaRepository : IRepository<RedSocialEmpresa>
    {
    }
}