﻿using Cliente.Core.Models;
using Common.Utils.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cliente.Core.Repositories
{
    public interface IEstadisticasCategoriasRepository : IRepository<EstadisticasCategorias>
    {
        Task<List<Categoria>> obtenerPrincipalesCategoriasAsync();
        Task<EstadisticasCategorias> obtenerEstadisticaCategoriaXId(long categoriaId, long estadisticaId);
    }
}