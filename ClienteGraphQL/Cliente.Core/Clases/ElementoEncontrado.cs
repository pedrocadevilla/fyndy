﻿namespace Cliente.Core.Clases
{
    public class ElementoEncontrado
    {
        public long? Producto { get; set; }
        public long? Empresa { get; set; }

        public ElementoEncontrado(long? producto, long? empresa)
        {
            Empresa = empresa;
            Producto = producto;
        }
    }

    
}
