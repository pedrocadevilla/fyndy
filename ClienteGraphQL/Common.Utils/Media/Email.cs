﻿using Common.Utils.General;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using MimeKit;
using System;

namespace Common.Utils.Media
{

    public class Email
    {
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _context;
      

        public Email(IHttpContextAccessor httpContextAccessor, IConfiguration configuration)
        {
            _configuration = configuration;
            _context = httpContextAccessor;
        
        }
        public bool SendEmail(DatosMensaje datosMensaje)
        {
            //Estructurar el Json para el mensaje junto con las cabeceras
            var httpcontext = _context.HttpContext.Request.Headers;


            string cadena = string.Empty;
            string componente = string.Empty;

            DateTime fecha = DateTime.Now;
            cadena += "Fecha:" + fecha.ToString("d") + " hora:" + fecha.ToString("HH:mm:ss") + " ";

            string bodyEmail = datosMensaje.Titulo + ": \n\n " + cadena + "\n\n" + datosMensaje.Mensaje + "\n";

         //   _logger.LogInformation(bodyEmail);
            try
            {
                var remitente = _configuration["ConfiguracionCorreo:remitente"];
                var password = _configuration["ConfiguracionCorreo:password"];
                var smtp = _configuration["ConfiguracionCorreo:smtp"];
                int puerto = Convert.ToInt32(_configuration["ConfiguracionCorreo:puerto"]);

                //instantiate a new MimeMessage
                var message = new MimeMessage();

                //A quien va a ser enviado el email
                message.To.Add(new MailboxAddress("Créditos Directos", datosMensaje.Correo));
                //Correo del servicio
                message.From.Add(new MailboxAddress("Adv. Workflow Creditos", remitente));
                //Email título
                message.Subject = _configuration["ConfiguracionCorreo:subject"];
                //Email cuerpo del mensaje
                message.Body = new TextPart("plain")
                {
                    Text = bodyEmail
                };

                using (var emailClient = new MailKit.Net.Smtp.SmtpClient())
                {

                    emailClient.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    emailClient.AuthenticationMechanisms.Remove("XOAUTH2");
                    emailClient.Connect(smtp, puerto, false);
                    emailClient.Authenticate(remitente, password);
                    emailClient.Send(message); //comentar esta linea para development
                    emailClient.Disconnect(true);

                    //_logger.LogInformation("Respuesta envio Email: " + response.StatusCode.ToString());
                    //Console.WriteLine("Respuesta envio Sms: " + response.StatusCode.ToString());
                    Console.WriteLine("Enviando Correo a: " + message.To, ToString());
                };
            }
            catch (Exception ex)
            {
               // _logger.LogInformation("Correo No enviado");
                Console.WriteLine("Correo No enviado \n");
            }


            return true;
        }
    }
}
