﻿using Common.Utils.General;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using RestSharp;
using System;

namespace Common.Utils.Media
{

    public class SMS
    {
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _context;
      

        public SMS(IHttpContextAccessor httpContextAccessor, IConfiguration configuration)
        {
            _configuration = configuration;
            _context = httpContextAccessor;
        
        }
        public bool SendSMS(DatosMensaje datosMensaje)
        {
            //Estructurar el Json para el mensaje junto con las cabeceras
            var httpcontext = _context.HttpContext.Request.Headers;


            string cadena = string.Empty;
            string componente = string.Empty;

            DateTime fecha = DateTime.Now;
            cadena += "Fecha:" + fecha.ToString("d") + " hora:" + fecha.ToString("HH:mm:ss") + " ";

            string bodySMS = datosMensaje.Titulo + ": " + cadena + "" + datosMensaje.Mensaje + "\n";

         //   _logger.LogInformation(bodyEmail);
            try
            {
                var token = _configuration["ConfiguracionSms:token"];
                var pin = _configuration["ConfiguracionSms:pin"];
                var client = new RestClient(_configuration["ConfiguracionSms:restClient"]);


                //var url = "?token=" + token + "&pin=" + pin + "&texto=" + mensajeSms.Codigo + mensajeSms.Mensaje + "&destino=" + emailAddress.Telefono;
                var url = "?token=" + token + "&pin=" + pin + "&texto=" + bodySMS + "&destino=" + datosMensaje.Telefono;
                var request = new RestRequest(url, Method.GET);
                //Esta Linea consume el servicio de sms hay que descomentarla para que funcione
                IRestResponse response = client.Execute(request);
                Console.WriteLine("Respuesta envio Sms: " + response.StatusCode.ToString());

                
               
            }
            catch (Exception ex)
            {
               // _logger.LogInformation("Correo No enviado");
                Console.WriteLine("Mensaje No enviado \n");
            }


            return true;
        }
    }
}
