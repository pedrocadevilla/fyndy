﻿using System;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;

namespace Common.Utils.Windows.Authentication
{
    public class AuthenticationUtils
    {

        public static SafeAccessTokenHandle Authenticate(string userName, string password, string domain)
        {
            IntPtr safeAccessPtr = WindowsAuthHandler.LogonUser(userName, domain, password);
            SafeAccessTokenHandle safeAccessTokenHandle = new SafeAccessTokenHandle(safeAccessPtr);
            return safeAccessTokenHandle;
        }

    }

    class WindowsAuthHandler
    {

        [DllImport("advapi32.dll", SetLastError = true)]
        public static extern bool LogonUser(String lpszUsername, String lpszDomain, String lpszPassword, int dwLogonType, int dwLogonProvider, out IntPtr phToken);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public extern static bool CloseHandle(IntPtr handle);

        public const int LOGON32_PROVIDER_DEFAULT = 0;

        public const int LOGON32_LOGON_INTERACTIVE = 2;
        public const int LOGON32_LOGON_NETWORK = 3;
        public const int LOGON32_LOGON_BATCH = 4;
        public const int LOGON32_LOGON_SERVICE = 5;
        public const int LOGON32_LOGON_UNLOCK = 7;
        public const int LOGON32_LOGON_NETWORK_CLEARTEXT = 8;
        public const int LOGON32_LOGON_NEW_CREDENTIALS = 9;

        public static IntPtr LogonUser(String UserName, String Domain, String Password)
        {
            IntPtr safeAccessPtr;
            bool returnValue = LogonUser(UserName,
                                            Domain,
                                            Password,
                                            LOGON32_LOGON_INTERACTIVE,
                                            LOGON32_PROVIDER_DEFAULT,
                                            out safeAccessPtr);
            if (false == returnValue)
            {
                int ret = Marshal.GetLastWin32Error();  // ???
                throw new System.ComponentModel.Win32Exception(ret);
            }

            return safeAccessPtr;

        }

    }
}
