﻿namespace Common.Utils.Redirecccionamiento
{
    class ResultRest<T>
    {

        public object ResultClass { get; set; }
        public T Result { get; set; }
        public string ResultCode { get; set; }
        public object ResultMessage { get; set; }
        public object ResultDescription { get; set; }
    }
}
