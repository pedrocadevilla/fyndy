﻿using Common.Utils.Exceptions;

using GraphQL.Client.Http;
using GraphQL.Client.Serializer.Newtonsoft;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Common.Utils.Redirecccionamiento
{
    public class Calls
    {
        private readonly IConfiguration _Configuration;
        private readonly string _token;

        public Calls(IConfiguration configuration, string token = null)
        {
            _Configuration = configuration;
            _token = token;
        }

        public async Task<string> PostRestAsync(string Direccion, string EndPoint, string Content, IHeaderDictionary Header)
        {
            HttpRequestMessage _Message = null;
            _Message = new HttpRequestMessage(HttpMethod.Post, _Configuration.GetValue<string>("DirectionCodes:" + Direccion) + EndPoint);
            _Message.Content = new StringContent(Content, Encoding.UTF8, "application/json");
            List<string> listaHeader = _Configuration.GetSection("Headers").Get<List<string>>();

            Header.Where(x => listaHeader.Contains(x.Key)).ToList().ForEach(x => _Message.Headers.Add(x.Key, x.Value.ToString()));

            HttpClient Client = new HttpClient();
            string Result;
            var Response = await Client.SendAsync(_Message);

            if (Response.IsSuccessStatusCode)
            {
                Result = await Response.Content
                    .ReadAsStringAsync();
                Client.Dispose();
                return Result;
            }
            else
            {
                Client.Dispose();
                // return null;
                throw new BusinessException("ID20");

            }
        }
        /// <summary>
        /// Metodo que permite hacer un llamado a un query de un proyecto externo para poder usarlo se deben crear 2 clases
        /// 1) Clase response con el siguiente formato 
        /// public class consultarRedesExternasResponse
        ///{
        ///    public IEnumerable<consultarRedesExternas> consultarRedesExternas { get; set; }
        ///}
        /// 2) Clase del tipo que se recibira (el nombre debe ser igual al del response del query que se solicita)
        /// public class consultarRedesExternas 
        ///{
        ///   public long Id { get; set; }
        ///   public string Nombre { get; set; }
        ///   public long DepartamentoId { get; set; }
        ///}
        /// Para hacer el llamado al metodo se hace de la siguiente manera tomando en cuenta que la clase a devolver debe tener su type generado
        /// public async Task<IEnumerable<consultarRedesExternas>> ObtenerRedesExternas(long departamentoId)
        ///{
        ///   Calls Llamada = new Calls(_Configuration);
        ///   string Query = @"query{consultarRedesExternas(departamentoId:" + departamentoId + "){id,nombre,departamentoId}}";
        ///   consultarRedesExternasResponse Result = await Llamada.QueryAsync<consultarRedesExternasResponse>(Query, "DireccionID1");
        ///   return Result.consultarRedesExternas;
        ///}
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_Query"></param>
        /// <param name="Direccion"></param>
        /// <returns></returns>
        public async Task<T> QueryAsync<T, U>(string query, string operacion, U variables, string direccion)
        {
            GraphQLHttpClient _ClientGraphQL = new GraphQLHttpClient(_Configuration.GetValue<string>("DirectionCodes:" + direccion));

            _ClientGraphQL.HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _token);
            var Query = new GraphQLRequest
            {
                Query = query,
                OperationName = operacion,
                Variables = variables
            };
            var response = await _ClientGraphQL.SendQueryAsync<T>(Query);
            _ClientGraphQL.Dispose();
            if (response.Errors != null)
            {
                if (response.Errors.Length > 0 && response.Errors[0].Extensions.Count() > 0)
                {
                    throw new BusinessParameterException(response.Errors[0].Message + "$" + response.Errors[0].Extensions["code"]);
                }
                throw new BusinessParameterException(response.Errors.ElementAt(0).Message + "$ID20");
            }
            return response.Data;
        }

        public async Task<T> MutationAsync<T, U>(string _Query, string Descripcion, U Objeto, string Direccion)
        {
            string url = _Configuration.GetSection("DirectionCodes").GetSection(Direccion).Value;

            GraphQLHttpClient _ClientGraphQL = new GraphQLHttpClient(url);

            var Query = new GraphQLRequest
            {
                Query = _Query,
                OperationName = Descripcion,
                Variables = Objeto
            };

            _ClientGraphQL.HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _token);
            var response = await _ClientGraphQL.SendMutationAsync<T>(Query);
            _ClientGraphQL.Dispose();
            if (response.Errors != null && response.Errors.Length > 0)
            {
                if (response.Errors[0].Extensions != null && response.Errors[0].Extensions["code"] != null && response.Errors[0].Extensions["code"].ToString() == "E0010")
                {
                    throw new BusinessControllerException(response.Errors[0].Extensions["code"].ToString() + ": " + response.Errors.ElementAt(0).Message);

                }
                if (response.Errors[0].Message != null)
                {
                    throw new BusinessParameterException(response.Errors[0].Message + ":" + "$ID22");
                }
                throw new BusinessParameterException(response.Errors[0].Extensions["code"].ToString() + ": " + response.Errors.ElementAt(0).Message + ":" + "$ID22");
            }
            return response.Data;
        }

    }
}
