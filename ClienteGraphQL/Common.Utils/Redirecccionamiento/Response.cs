﻿using System;
using System.Collections.Generic;

namespace Workflow.Core.Utilidades
{
    public abstract class Response
    {

        public const string PROCESS_OK = "0";
        public const string PROCESS_ERROR = "1";
    }

    /// <summary>
    /// Representa la respuesta de un servicio.
    /// </summary>
    /// <remarks>
    /// <para>La clase 'Response' representa la respuesta de un servicio web. Funciona como un envoltorio de el o los resultados, guardando
    /// el tipo del resultado, un codigo de respuesta que generalmente es 0 para proceso correcto y 1 para proceso con errores.</para>
    /// <para>si el resultado es una instacia unica debe userse el campo 'Result', si no, se debe instanciar una lista y añadir los resultados,
    /// y referenciar la lista al campo 'Results'.</para>
    /// <para>El campo 'ResultCode' generalmente ser 0 cuando el proceso ejecutado haya sido correcto; y 1 si existio algun error durande el procedimiento.
    /// EL campo 'ResultCode' sera la descripcion del 'ResultMessage'</para>
    /// </remarks>
    /// <typeparam name="T">T debe ser un tipo que contenga un tipo que posea objetos simples u otros objetos que contengan objetos simples</typeparam>
    public class Response<T> : Response
    {

        /// <summary>
        /// Obtiene o almacena el nombre de la clase ingresada por el generico {T}.
        /// </summary>
        /// <remarks>
        /// en este campo se almacena el nombre de la clase ingresada por el generico {T}. se llena automaticamente al usar algun constructor definido.
        /// </remarks>
        public string ResultClass { get; set; }
        /// <summary>
        /// Obtiene o almacena un objeto de los datos a ser enviados por la respuesta del servicio.
        /// </summary>
        /// <remarks>
        /// <para>En este campo se almacena el objeto con los resultados para ser la respuesta de algun servicio.</para>
        /// <para>Debe ser usado en caso de que la respuesta sea un objeto unico. En caso que la respuesta sean varios
        /// objetos use el campo lista 'Results'</para>
        /// <para>Debe ser incluido mediante el uso del contructor Response(T Result, long ResultCode, string ResultMessage)</para>
        /// </remarks>
        public T Result { get; set; }
        /// <summary>
        /// Obtiene o almacena una lista de objetos de datos a ser enviados por la respuesta del servicio.
        /// </summary>
        /// <remarks>
        /// <para>En este campo se almacena la lista de objetos con los restultados para ser la respuesta de algun servicio.</para>
        /// <para>Debe ser usado en caso de que la respuesta sea varios objetos. En caso que la respuesta sean unica
        /// use el campo 'Result'</para>
        /// <para>Debe ser incluido mediante el uso del contructor  public Response(List<T> Results, long ResultCode, string ResultMessage)</para>
        /// <para>Se marca como obsoleto para implementar funcionalidades como paginacion sobre resultados o cualquer otra que emplee listas.</para>
        /// </remarks>
        [Obsolete("Results es un campo obsoleto, use la clase 'ResultsList' o derivada en generico 'T' y guarde la instancia en el campo 'Result'")]
        public List<T> Results { get; set; }

        /// <summary>
        /// Obtiene el numero de resultados almacenados en la lista 'Results'. 
        /// </summary>
        /// <remarks>
        /// <para>Este campo es calculado apartir del campo 'Results'.</para>
        /// <para>Si el campo 'Result' no es especificado o es null, 'ResultsCount' sera nulo.</para>
        /// </remarks>
        [Obsolete("Results es un campo obsoleto, use la clase 'ResultsList' o derivada en generico 'T' y guarde la instancia en el campo 'Result'")]

        public string ResultsCount
        {
            get
            {
                if (Results != null)
                    return Results.Count.ToString();
                return null;
            }

            set
            {

            }
        }
        /// <summary>
        /// Obtiene o almacena el codigo del resultado a ser enviado por la respuesta del servicio.
        /// </summary>
        /// <remarks>
        /// <para>En este campo se almacena el estado de la respuesta del servicio.</para>
        /// <para>Su uso debera ser: 0 si el proceso que genera la respuesta se ejecuto sin problemas, 1 si hubo algun problema durante dicho proceso</para>
        /// </remarks>
        public string ResultCode { get; set; }
        /// <summary>
        /// Obtiene o almacena el mensaje enviado por la respuesta del servicio.
        /// </summary>
        /// <remarks>
        /// <para>En este campo se almacena mensaje del estado de la respuesta del servicio</para>
        /// </remarks>
        public string ResultMessage { get; set; }

        /// <summary>
        /// Obtiene o almacena la descripcion completa del mensaje generado por la respuesta del servicio.
        /// </summary>
        /// <remarks>
        /// <para>En este campo se almacena la descripcion del estado de la respuesta del servicio</para>
        /// <para>En caso de algun problema con el proceso que genera la respuesta, esta campo sirve de especificacion del problema</para>
        /// </remarks>
        public string ResultDescription { get; set; }

        /// <summary>
        /// Inicializa una nueva instancia (no deberia ser usado).
        /// </summary>
        public Response() { }

        /// <summary>
        /// Inicializa una nueva instancia sin resultados.
        /// </summary>
        /// <remarks>
        /// Generalmente usado cuando el proceso generador de la respuesta falla. Dejando vacios los campos 'Result' y 'Results', y, dejando 'ResultClass' nulo. 
        /// </remarks>
        /// <param name="ResultCode">Codigo de Respuesta de la operacion.</param>
        /// <param name="ResultMessage">Mensaje de respuesta de la operacion.</param>
        public Response(string ResultCode, string ResultMessage, string ResultDescription)
        {

            this.ResultCode = ResultCode;
            this.ResultMessage = ResultMessage;
            this.ResultDescription = ResultDescription;
            this.ResultClass = null;
        }

        /// <summary>
        /// Inicializa una nueva instancia para Resultados con un solo objeto.
        /// </summary>
        /// <remarks>
        /// Generalmente usado cuando el proceso retorna un objeto de datos unico y el proceso se ejecuto correctamente. 
        /// </remarks>
        /// <param name="Result">Objeto resultado del proceso.</param>
        /// <param name="ResultCode">Codigo de Respuesta de la operacion.</param>
        /// <param name="ResultMessage">Mensaje de respuesta de la operacion.</param>
        /// <param name="ResultDescription">Descripcion de respuesta de la operacion.</param>
        public Response(T Result, string ResultCode, string ResultMessage, string ResultDescription = null) : this(ResultCode, ResultMessage, ResultDescription)
        {
            this.Result = Result;
            this.ResultClass = Result.GetType().Name.ToString();
        }

        /// <summary>
        /// Inicializa una nueva instancia para Resultados con un solo objeto.
        /// </summary>
        /// <remarks>
        /// Generalmente usado cuando el proceso retorna un objeto de datos unico y el proceso se ejecuto correctamente. 
        /// </remarks>
        /// <param name="Results">Lista de objetos del tipo { T } resultados del proceso.</param>
        /// <param name="ResultCode">Codigo de Respuesta de la operacion.</param>
        /// <param name="ResultMessage">Mensaje de respuesta de la operacion.</param>
        /// <param name="ResultDescription">Descripcion de respuesta de la operacion.</param>
        public Response(List<T> Results, string ResultCode, string ResultMessage, string ResultDescription = null) : this(ResultCode, ResultMessage, ResultDescription)
        {
            this.Results = Results;
            this.ResultClass = Results.GetType().Name.ToString();
        }
    }

}
