﻿
using HotChocolate;
using HotChocolate.Execution;
using HotChocolate.Execution.Instrumentation;
using HotChocolate.Resolvers;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DiagnosticAdapter;
using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Utils.Logging
{
    public class DiagnosticObserver : IDiagnosticObserver
    {

        private ILogger _logger;
        /// <summary>
        /// Constructor de la clase, recibe el ILogger para generar los logs
        /// </summary>
        /// <param name="logger"></param>
        public DiagnosticObserver(ILogger<DiagnosticObserver> logger)
        {
            _logger = logger;
        }

        [DiagnosticName("HotChocolate.Execution.Query")]
        public void OnQuery(IQueryContext context)
        {

            // _logger.LogWarning(context.ContextData.Values.ToString());
            // This method is used to enable start/stop events for query.
        }

        [DiagnosticName("HotChocolate.Execution.Query.Start")]
        public void BeginQueryExecute(IQueryContext context)
        {
            // _logger.LogWarning ("entrando en el query con la data: ");
            // DefaultHttpContext httpContext;
            // Generar el random cuando sea null idMensaje idRequest
            int rmMensaje = Math.Abs(Guid.NewGuid().GetHashCode());
            int rmRequest = Math.Abs(Guid.NewGuid().GetHashCode());
            int idMensajeEnt = 0;
            int idRequestEnt = 0;

            string cadena = String.Empty;
            DateTime fecha = DateTime.Now;
            // cadena += "fecha: " + fecha + " Entrando, Petición " + action.ActionName + " ";
            cadena += "fecha:" + fecha.ToString("d") + " hora:" + fecha.ToString("HH:mm:ss") + " ";

            if (context.ContextData.TryGetValue("HttpContext", out object httpContext))
            {
                DefaultHttpContext HttpContext = (DefaultHttpContext)httpContext;

                string headers = String.Empty;
                int element = 0;
                foreach (var key in HttpContext.Request.Headers.Keys)
                {
                    if (key == "idmensaje")
                    {
                        if (HttpContext.Request.Headers[key] == "null")
                        {
                            //valor = rmMensaje.ToString();
                            idMensajeEnt = rmMensaje;
                            // context.ContextData["identrada"] = rmMensaje;

                        }
                        else
                        {
                            idMensajeEnt = Int32.Parse(HttpContext.Request.Headers.Values.ElementAt(element));
                            // context.ContextData["identrada"] = idMensajeEnt;

                        }
                    }
                    if (key == "idrequest")
                    {
                        if (HttpContext.Request.Headers[key] == "null")
                        {
                            idRequestEnt = rmRequest;
                            // context.ContextData["idrequest"] = idRequestEnt;
                        }
                        else
                        {
                            idRequestEnt = Int32.Parse(HttpContext.Request.Headers.Values.ElementAt(element));
                            // context.ContextData["idrequest"] = idRequestEnt;

                        }

                    }
                    if (key.CompareTo("Authorization") != 0)
                        // Cabeceras en graphql como estan implementadas en rest
                        if (key == "idusuario" || key == "idsession" || key == "idmensaje" || key == "canal" || key == "direccionip" || key == "sistema-operativo" ||
                            key == "navegador" || key == "version-navegador" || key == "actividad" || key == "idrequest" || key == "fecharequest" || key == "horarequest")
                        {
                            // headers += key + "=" + Request.Headers[key] + " ";
                            headers += key + "=" + HttpContext.Request.Headers[key] + " ";

                        }
                    element++;
                    // headers += key + "=" + HttpContext.Request.Headers[key] + Environment.NewLine;
                }
                // ... your code
                //object[] parametros = { idMensajeEnt, idRequestEnt };
                // context.ContextData["idMensajeEnt"] = Guid.NewGuid ();
                context.ContextData["idMensajeEnt"] = idMensajeEnt;
                context.ContextData["idRequestEnt"] = idRequestEnt;

                _logger.LogInformation("Entrando al query: " + context.Request.Query + " " + cadena + " " + headers);
            }
        }

        [DiagnosticName("HotChocolate.Execution.Query.Stop")]
        public void EndQueryExecute(IQueryContext context, IExecutionResult result)
        {
            // ... your code
            // Guid idMensajeEnt = (Guid) context.ContextData["idmensajeent"];
            // Guid idRequestEnt = (Guid) context.ContextData["idrequestent"];

            var mentradaId = context.ContextData.ContainsKey("idMensajeEnt") ? context.ContextData["idMensajeEnt"] : 0;
            var mrequestId = context.ContextData.ContainsKey("idRequestEnt") ? context.ContextData["idRequestEnt"] : 0;

            string cadena = String.Empty;
            DateTime fecha = DateTime.Now;
            // cadena += "fecha: " + fecha + " Entrando, Petición " + action.ActionName + " ";
            cadena += "fecha:" + fecha.ToString("d") + " hora:" + fecha.ToString("HH:mm:ss") + " ";
            if (context.ContextData.TryGetValue("HttpContext", out object httpContext))
            {
                DefaultHttpContext HttpContext = (DefaultHttpContext)httpContext;
                string headers = String.Empty;
                foreach (var key in HttpContext.Request.Headers.Keys)
                {
                    if (key.CompareTo("Authorization") != 0)
                        // Cabeceras en graphql como estan implementadas en rest
                        if (key == "idusuario" || key == "idsession" || key == "canal" || key == "direccionip" || key == "sistema-operativo" ||
                            key == "navegador" || key == "version-navegador" || key == "actividad" || key == "fecharequest" || key == "horarequest")
                        {
                            // headers += key + "=" + Request.Headers[key] + " ";
                            headers += key + "=" + HttpContext.Request.Headers[key] + " ";

                        }

                    // headers += key + "=" + HttpContext.Request.Headers[key] + Environment.NewLine;
                }
                _logger.LogInformation("saliendo del query: " + context.Request.Query + " " + cadena + " " + headers + " " + "idMensaje=" + mentradaId + " " + "idRequest=" + mrequestId);
            }

        }

        [DiagnosticName("HotChocolate.Execution.Query.Error")]
        public virtual void OnQueryError(IQueryContext context, Exception exception)
        {
            // ... your code
            string cadena = String.Empty;
            DateTime fecha = DateTime.Now;
            // cadena += "fecha: " + fecha + " Entrando, Petición " + action.ActionName + " ";
            cadena += "fecha:" + fecha.ToString("d") + " hora:" + fecha.ToString("HH:mm:ss") + " ";
            if (context.ContextData.TryGetValue("HttpContext", out object httpContext))
            {
                DefaultHttpContext HttpContext = (DefaultHttpContext)httpContext;
                string headers = String.Empty;
                foreach (var key in HttpContext.Request.Headers.Keys)
                {
                    if (key.CompareTo("Authorization") != 0)
                        // Cabeceras en graphql como estan implementadas en rest
                        if (key == "idusuario" || key == "idsession" || key == "idmensaje" || key == "canal" || key == "direccionip" || key == "sistema-operativo" ||
                            key == "navegador" || key == "version-navegador" || key == "actividad" || key == "idrequest" || key == "fecharequest" || key == "horarequest")
                        {
                            // headers += key + "=" + Request.Headers[key] + " ";
                            headers += key + "=" + HttpContext.Request.Headers[key] + " ";

                        }

                    // headers += key + "=" + HttpContext.Request.Headers[key] + Environment.NewLine;
                }
                //_logger.LogWarning ("Error en query" + cadena + " " + headers + " " + exception.Message);
                _logger.LogInformation("Error en query: " + context.Request.Query + " " + cadena + " " + headers);

            }
        }

        [DiagnosticName("HotChocolate.Execution.Validation.Error")]
        public void OnValidationError(IQueryContext context, IReadOnlyCollection<IError> errors)
        {
            // ... your code
            string cadena = String.Empty;
            DateTime fecha = DateTime.Now;
            // cadena += "fecha: " + fecha + " Entrando, Petición " + action.ActionName + " ";
            if (context.ContextData.TryGetValue("HttpContext", out object httpContext))
            {
                DefaultHttpContext HttpContext = (DefaultHttpContext)httpContext;
                cadena += "fecha:" + fecha.ToString("d") + " hora:" + fecha.ToString("HH:mm:ss") + " ";
                string headers = String.Empty;
                foreach (var key in HttpContext.Request.Headers.Keys)
                {
                    if (key.CompareTo("Authorization") != 0)
                        // Cabeceras en graphql como estan implementadas en rest
                        if (key == "idusuario" || key == "idsession" || key == "idmensaje" || key == "canal" || key == "direccionip" || key == "sistema-operativo" ||
                            key == "navegador" || key == "version-navegador" || key == "actividad" || key == "idrequest" || key == "fecharequest" || key == "horarequest")
                        {
                            // headers += key + "=" + Request.Headers[key] + " ";
                            headers += key + "=" + HttpContext.Request.Headers[key] + " ";

                        }

                    // headers += key + "=" + HttpContext.Request.Headers[key] + Environment.NewLine;
                }
                // _logger.LogError ("Error en validación: " + cadena + " " + headers);

                _logger.LogWarning("Error en validación: " + context.Request.Query + " " + cadena + " " + headers);

                foreach (var error in errors)
                {

                    if (error.Exception != null)
                    {
                        if (error.Exception.InnerException != null)
                        {
                            _logger.LogError(error.Exception.InnerException.Message);

                        }
                        else
                        {
                            _logger.LogError(error.Exception.Message);

                        }

                    }
                    else
                    {
                        _logger.LogError(error.Message);

                    }
                }
            }
            throw new QueryException(errors);
        }

        [DiagnosticName("HotChocolate.Execution.Resolver.Error")]
        public void OnResolverError(IResolverContext context, IEnumerable<IError> errors)
        {
            // ... your code
            bool authorizationError = false;
            string cadena = String.Empty;
            DateTime fecha = DateTime.Now;
            // cadena += "fecha: " + fecha + " Entrando, Petición " + action.ActionName + " ";

            cadena += "fecha:" + fecha.ToString("d") + " hora:" + fecha.ToString("HH:mm:ss") + " ";
            if (context.ContextData.TryGetValue("HttpContext", out object httpContext))
            {
                DefaultHttpContext HttpContext = (DefaultHttpContext)httpContext;
                string headers = String.Empty;
                foreach (var key in HttpContext.Request.Headers.Keys)
                {
                    if (key.CompareTo("Authorization") != 0)
                        // Cabeceras en graphql como estan implementadas en rest
                        if (key == "idusuario" || key == "idsession" || key == "idmensaje" || key == "canal" || key == "direccionip" || key == "sistema-operativo" ||
                            key == "navegador" || key == "version-navegador" || key == "actividad" || key == "idrequest" || key == "fecharequest" || key == "horarequest")
                        {
                            // headers += key + "=" + Request.Headers[key] + " ";
                            headers += key + "=" + HttpContext.Request.Headers[key] + " ";

                        }
                    // headers += key + "=" + HttpContext.Request.Headers[key] + Environment.NewLine;
                }
                _logger.LogError("Error en resolver: " + cadena + " " + headers);
                // _logger.LogWarning ("Error en validación: " + context.Request.Query + " " + cadena + " " + headers);
                foreach (var error in errors)
                {
                    if (error.Exception != null)
                    {
                        if (error.Exception.InnerException != null)
                        {
                            _logger.LogError(error.Exception.InnerException.Message);

                        }
                        else
                        {
                            _logger.LogError(error.Exception.Message);

                        }

                    }
                    else
                    {
                        _logger.LogError(error.Message);

                    }
                    if (error.Message.Equals("The current user is not authorized to access this resource."))
                    {
                        authorizationError = true;
                    }
                }
            }
            if (!authorizationError)
            {
                throw new QueryException(errors);
            }
        }
    }
}
