﻿using HotChocolate.Execution;
using HotChocolate.Resolvers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Utils.Loggers
{
    public class Headers
    {
        public string Parametros { get; set; }
        public string FechaCliente { get; set; }
        public string IdUsuario { get; set; }
        public string IdSesion { get; set; }
        public string Canal { get; set; }
        public string Ip { get; set; }
        public string Navegador { get; set; }
        public string VersionNavegador { get; set; }
        public string SistemaOperativo { get; set; }
        [JsonIgnore]
        public string Id { get; set; }
        [JsonIgnore]
        public string Servicio { get; set; }

        public string HeaderQuery(IHttpContextAccessor _ContextHTTP, IQueryContext _ContextQuery, string Tipo)
        {
            var Datos = _ContextHTTP.HttpContext.Request.Headers.ToList();
            var Header = (Headers)Datos;
            var Query = _ContextQuery.Request.Query.ToString().TrimStart('{').TrimEnd('}').Replace("\n", "").Replace(" ", "").Replace("}", "").Replace(")", "");
            string[] DatosQuery = Query.Split('{', '(');
            Header.Servicio = DatosQuery[0];
            Header.Id = _ContextHTTP.HttpContext.TraceIdentifier.Split(":")[0];

            if (DatosQuery.Length > 2 && DatosQuery.Length < 5)
            {
                Header.Parametros = DatosQuery[1].Insert(0, "{").Insert(DatosQuery[1].Length + 1, "}");
            }
            if (DatosQuery.Length == 5)
            {
                Header.Servicio = DatosQuery[1];
                Header.Parametros = DatosQuery[3].Insert(0, "{").Insert(DatosQuery[3].Length + 1, "}");
            }

            string header = ("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "][" + Header.Id + "] " +
                Tipo + " " + Header.Servicio + " " + JsonConvert.SerializeObject(Header));
            return header;
        }

        public string HeaderResolver(IHttpContextAccessor _ContextHTTP, IResolverContext _ContextResolver, string Tipo)
        {
            var Datos = _ContextHTTP.HttpContext.Request.Headers.ToList();
            var Header = (Headers)Datos;
            Header.Servicio = _ContextResolver.Path.Name.Value;
            Header.Id = _ContextHTTP.HttpContext.TraceIdentifier.Split(":")[0];

            string header = ("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "][" + Header.Id + "] " +
                Tipo + " " + Header.Servicio + " " + JsonConvert.SerializeObject(Header, Newtonsoft.Json.Formatting.None,
                            new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore
                            }));
            return header;
        }

        public static explicit operator Headers(List<KeyValuePair<string, StringValues>> Value)
        {
            if (Value == null)
                return null;
            Headers Result = new Headers();
            try
            {
                foreach (var v in Value)
                {
                    switch (v.Key)
                    {
                        case "IdUsuario":
                            Result.IdUsuario = v.Value;
                            break;
                        case "IdSesion":
                            Result.IdSesion = v.Value;
                            break;
                        case "Canal":
                            Result.Canal = v.Value;
                            break;
                        case "Ip":
                            Result.Ip = v.Value;
                            break;
                        case "SistemaOperativo":
                            Result.SistemaOperativo = v.Value;
                            break;
                        case "Navegador":
                            Result.Navegador = v.Value;
                            break;
                        case "VersionNavegador":
                            Result.VersionNavegador = v.Value;
                            break;
                        case "FechaCliente":
                            Result.FechaCliente = v.Value;
                            break;
                        case "Parametros":
                            Result.Parametros = v.Value;
                            break;
                        case "Id":
                            Result.Id = v.Value;
                            break;
                        case "Servicio":
                            Result.Servicio = v.Value;
                            break;
                    }
                }
            }
            catch
            {
                Result = null;
            }
            return Result;
        }
    }
}
