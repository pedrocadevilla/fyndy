﻿using System;

namespace Common.Utils.Exceptions
{
    public class BusinessControllerException : Exception
    {

        public BusinessControllerException(string message, params object[] args)
        : base(string.Format(message, args))
        {
            //throw new CustomException("Exception with parameter value {0}, {1}", param[0], param[1])
        }

    }
}
