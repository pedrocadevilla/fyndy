﻿
using Common.Utils.Redirecccionamiento;

using HotChocolate;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

using System;
using System.Threading.Tasks;

namespace Common.Utils.Error
{
    public class ErrorFilter : IErrorFilter
    {
        private IConfiguration _Configuration;
        private readonly IHttpContextAccessor _Accessor;

        public ErrorFilter(IConfiguration Config, IHttpContextAccessor contextAccessor)
        {
            _Configuration = Config;
            _Accessor = contextAccessor;
        }

        public IError OnError(IError error)
        {
            if (error.Exception != null)
            {
                Exception ex;
                if (error.Exception.InnerException != null)
                {
                    ex = error.Exception.InnerException;
                }
                else
                {
                    ex = error.Exception;
                }

                if (ex is AggregateException)
                {
                    ex = ex.InnerException;
                }

                switch (ex.GetType().Name)
                {
                    /*
                    case "DbUpdateException":
                        return ErrorBuilder.New()
                        .SetMessage("Actualización de datos fallida")
                        .SetCode("007")
                        .Build();
                    case "DbUpdateConcurrencyException":
                        return ErrorBuilder.New()
                        .SetMessage("Error de acceso concurrente")
                        .SetCode("007")
                        .Build();
                    case "SqlException":
                        Calls cliente = new Calls(_Configuration);
                        Task<string> respuesta = cliente.PostRestAsync("Notificaciones", "notification", _Configuration.GetSection("FallaInterna").Get<string>(), _Accessor.HttpContext.Request.Headers);
                        return ErrorBuilder.New()
                        .SetMessage("Hay problemas con la base de datos")
                        .SetCode("008")
                        .Build();
                        */
                    case "BusinessException":
                        return ErrorBuilder.New()
                        .SetMessage(_Configuration.GetValue<string>("BusinessCodes:" + ex.Message))
                        .SetCode(error.Exception.Message)
                        .Build();
                    case "BusinessParameterException":
                        return ErrorBuilder.New()
                        .SetMessage(error.Exception.Message.Substring(0, ex.Message.LastIndexOf('$')))
                        .SetCode(error.Exception.Message.Substring(ex.Message.LastIndexOf('$') + 1))
                        .Build();
                    case "FaultException":
                        Calls cliente2 = new Calls(_Configuration);
                        // TODO: NOTIFICACION DE FALLA DE SERVICIO
                        try
                        {
                            Task<string> respuesta2 = cliente2.PostRestAsync("Notificaciones", "notification", _Configuration.GetSection("FallaComunicacion").Get<string>(), _Accessor.HttpContext.Request.Headers);
                        }
                        catch (Exception postEx)
                        {
                            // TODO: HELP
                        }
                        return ErrorBuilder.New()
                        .SetMessage("Error de comunicacion SOAP")
                        .SetCode("010")
                        .Build();
                    default:
                        return ErrorBuilder.New()
                        .SetMessage(ex.Message)
                        .SetCode(error.Code)
                        .Build();
                }
            }
            return error;
        }
    }
}
