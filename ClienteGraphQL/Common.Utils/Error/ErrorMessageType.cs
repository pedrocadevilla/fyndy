﻿using HotChocolate.Types;

namespace Common.Utils.QueryTypes
{
    public class ErrorMessageType : ObjectType<ErrorMessage>
    {

    }
    public class ErrorMessage
    {
        public int Codigo { get; set; }
        public string Mensaje { get; set; }
    }
}
