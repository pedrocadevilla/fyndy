﻿using System.Collections.Generic;

namespace Common.Utils.List
{
    /// <summary>
    /// Clase que contiene Extensiones con respecto a listas o colecciones
    /// </summary>
    public static class CollectionExtensions
    {
        /// <summary>
        /// Retorna si un objeto pertenece o no a una colecion de Elementos.
        /// Es la pregunta inversa de 
        /// </summary>
        /// <typeparam name="T">Tipo de objeto a verificar</typeparam>
        /// <param name="value">Elemento a verificar la existencia</param>
        /// <param name="elements">Coleccion de objetos donde se verificara la existencia</param>
        /// <returns>true si el valor esta dentro de la coleccion, false si no esta contenida o si la coleccion es una o no tiene elementos</returns>
        public static bool IsIn<T>(this T value, ICollection<T> elements) {

            if (elements == null || elements.Count == 0) {
                return false;
            }

            return elements.Contains(value);
            
        }


    }
}
