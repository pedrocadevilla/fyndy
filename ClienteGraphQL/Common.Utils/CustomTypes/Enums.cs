﻿namespace Common.Utils.CustomTypes
{
    /// <summary>
    /// enum EstadosUsuario es una utilidad que facilita la identificion de los estado del usuario 
    /// NUEVO: Deprecado
    /// POR_ACTIVAR: Deprecado
    /// ACTIVO: Usuario que puede realizar transacciones en la plataforma
    /// BLOQUEADO: Usuario activo bloqueado por un periodo de tiempo este estado se actualiza de forma automatica 
    /// BAJA: Usuario dado de baja en sistema no puede realizar ningun tipo de transaccion, 
    /// es necesario que RRHH solicite la baja y Soporte lo procese para que quede de baja en Ldap
    /// SUSPENDIDO: Usuario activo bloqueado por un periodo de tiempo,
    /// este estado se actualiza de forma automatica en funcion del registro de suspencion
    /// CARGADO: Este usuario está en sistema, una vez que soporte lo active y le genere una contraseña
    /// temporal, el mismo se podrá activar y su estado cambiará a estado Activo
    /// NO_APLICA: Deprecado
    /// </summary>

    public enum EstadosUsuario
    {
        NUEVO = 1,
        POR_ACTIVAR = 2,
        ACTIVO = 3,
        BLOQUEADO = 4,
        BAJA = 5,
        SUSPENDIDO = 6,
        CARGADO = 7      
    }

    /// <summary>
    /// enum TipoReferencia
    /// MENU: Apunta a una aplicación que nace de una opción de un menú.
    /// BANDEJA: Apunta hacia una bandeja.
    /// ENLACE: Apunta hacia una URL general.
    /// TABLERO: Apunta a una opción dentro de otro tablero.
    /// </summary>
     public enum TipoReferencia
    {
        MENU = 1,
        BANDEJA = 2,
        ENLACE = 3,
        TABLERO = 4,
    }

    /// <summary>
    /// enum TipoModulo
    /// MODULO: El módulo está pensado como una opción dentro de un tablero
    /// OPCION: El módulo está pensado para ser una opción de menú.
    /// MODULO_APLICACION: El módulo está pensado en ser un enlace hacia una aplicación.
    /// </summary>
    // [Flags]
    public enum TipoModulo
    {
        MODULO = 1,
        OPCION = 2,
        MODULO_APLICACION = 3
    }

    /// <summary>
    /// enum EstadoEnum  
    /// BORRADO : regsitro borrado
    /// INACTIVO: registro inactivo
    /// ACTIVO: registro activo
    /// </summary>
    // [Flags]
    public enum EstadoEnum
    {
        BORRADO = -1,
        INACTIVO = 0,
        ACTIVO = 1,
    }

    /// <summary>
    /// enum EstadoEnum  
    /// PENDIENTE : actividad que fue asignada a una cola o persona y esta pendiente para su ejecucion 
    /// ASIGNADA: este estado esta pendiente de eleiminar y sera reemplazado por una columan ASIGNADA(true o false)
    /// indica si fue asignada especificamente al usaurio
    /// EN_PROCESO: la actividad esta siendo procesada por el usuario
    /// CERRADA: la actividad fue cerrada de forma normal
    /// CERRADA_POR_ABANDONO: la actvidad fue cerrada por abandono
    /// </summary>
    public enum EstadoActividad
    {
        PENDIENTE = 1,
        ASIGNADA = 2, 
        EN_PROCESO = 3,
        CERRADA = 4,
        CERRADA_POR_ABANDONO = 5
    }



    /// <summary>
    /// enum EstadoEnum  
    /// PENDIENTE : actividad que fue asignada a una cola o persona y esta pendiente para su ejecucion 
    /// ASIGNADA: este estado esta pendiente de eleiminar y sera reemplazado por una columan ASIGNADA(true o false)
    /// indica si fue asignada especificamente al usaurio
    /// EN_PROCESO: la actividad esta siendo procesada por el usuario
    /// CERRADA: la actividad fue cerrada de forma normal
    /// CERRADA_POR_ABANDONO: la actvidad fue cerrada por abandono
    /// </summary>
    public enum EstadoEvento
    {
        SIN_PROCESAR = 0,
        PROCESADO = 1
    }





    /// <summary>
    /// enum EstadoInstancia para manejar la informacion de los estados de la instacia  
    /// CREADA : la instancia esta creada en la tarea de inicio y la tarea primera tarea del negocio no se ha procesado 
    /// EN_PROCESO: La instancia esta siendo procesada es decir tiene almentos una tarea no cerrada diferente a la de fin de instancia
    /// CERRADA: todas las tareas de la instancia han sido cerradas satisfactoriamente
    /// </summary>
    public enum EstadoInstancia
    {
        CREADA = 1,
        EN_PROCESO = 2,
        CERRADA = 3
    }
}
