﻿using HotChocolate.Types;
using System;
using System.Globalization;

namespace Common.Utils.CustomTypes
{

    /// <summary>
    /// Esta clase es usada para gestionar los campos tipo fecha y hora en API de Hotchocolate
    /// </summary>
    public sealed class FechaHoraType
        : DateTimeTypeBase
    {

        /// <summary>
        /// Variable _utcFormat para gestionar la fecha en el formato UTC
        /// </summary>
        private const string _utcFormat = "dd/MM/yyyy HH\\:mm\\:ss";

        /// <summary>
        ///  Variable _localFormat para gestionar la fecha en el formato UTC
        /// </summary>
        private const string _localFormat = "dd/MM/yyyy HH\\:mm\\:ss";

        /// <summary>
        /// construtor de la clase FechaHoraType
        /// </summary>
        public FechaHoraType()
                : base("Fecha")
        {
            Description = "Fecha Formato Workflow";//TypeResources.DateTimeType_Description;
        }

        /// <summary>
        /// sobre escribe el metodo de tipo local
        /// </summary>
        public override Type ClrType => typeof(DateTimeOffset);

        /// <summary>
        /// Sobrescritura del metodo de serealización de la conversion a string de la fecha hora utc
        /// </summary>
        /// <param name="value">fecha hora utc (DateTime) que se desea serealizar</param>
        /// <returns>La fecha hora utc en formato string  "dd/MM/yyyy HH\\:mm\\:ss"</returns>
        protected override string Serialize(DateTime value)
        {
            if (value.Kind == DateTimeKind.Utc)
            {
                return value.ToString(
                    _utcFormat,
                    CultureInfo.InvariantCulture);
            }

            return value.ToString(
                _localFormat,
                CultureInfo.InvariantCulture);
        }


        /// <summary>
        /// Sobrescritura del metodo de serealización de la conversion a string de la fecha hora local 
        /// </summary>
        /// <param name="value">fecha hora local (DateTimeOffset) que se desea serealizar</param>
        /// <returns>La fecha hora local en formato string  "dd/MM/yyyy HH\\:mm\\:ss"</returns>
        protected override string Serialize(DateTimeOffset value)
        {
            if (value.Offset == TimeSpan.Zero)
            {
                return value.ToString(
                    _utcFormat,
                    CultureInfo.InvariantCulture);
            }

            return value.ToString(
                _localFormat,
                CultureInfo.InvariantCulture);
        }




        /// <summary>
        /// Sobrescritura del metodo de Deserealización de la conversion de la fecha hora a local
        /// </summary>
        /// <param name="serialized">fecha hora serializada en (string o DateTimeOffset) </param>
        /// <param name="obj">Fecha en formato DateTimeOffset</param>
        /// <returns></returns>
        public override bool TryDeserialize(object serialized, out object obj)
        {
            if (serialized is null)
            {
                obj = null;
                return true;
            }

            if (serialized is string s
                && TryDeserializeFromString(s, out object d))
            {
                obj = d;
                return true;
            }

            if (serialized is DateTimeOffset)
            {
                obj = serialized;
                return true;
            }

            if (serialized is DateTime dt)
            {
                obj = new DateTimeOffset(
                    dt.ToUniversalTime(),
                    TimeSpan.Zero);
                return true;
            }

            obj = null;
            return false;
        }



        /// <summary>
        /// Sobrescritura del metodo de Deserealización de la conversion de la fecha a utc
        /// </summary>
        /// <param name="serialized">fecha hora serializada en string</param>
        /// <param name="obj">Fecha en formato DateTime</param>
        /// <returns></returns>

        protected override bool TryDeserializeFromString(
            string serialized,
            out object obj)
        {
            if (serialized != null
                && serialized.EndsWith("Z")
                && DateTime.TryParse(
                    serialized,
                    CultureInfo.InvariantCulture,
                    DateTimeStyles.AssumeUniversal,
                    out DateTime zuluTime))
            {
                obj = new DateTimeOffset(
                    zuluTime.ToUniversalTime(),
                    TimeSpan.Zero);
                return true;
            }

            if (DateTimeOffset.TryParse(
                serialized,
                out DateTimeOffset dateTime))
            {
                obj = dateTime;
                return true;
            }

            obj = null;
            return false;
        }
    }

    /// <summary>
    /// Esta clase es usada para gestionar los campos tipo fecha en API de Hotchocolate
    /// </summary>

    public sealed class FechaType
          : DateTimeTypeBase
    {
        /// <summary>
        /// Variable _utcFormat para gestionar la fecha en el formato UTC
        /// </summary>
        private const string _utcFormat = "dd/MM/yyyy";


        /// <summary>
        ///  Variable _localFormat para gestionar la fecha en el formato local
        /// </summary>
        private const string _localFormat = "dd/MM/yyyy";
        
        /// <summary>
        /// construtor de la clase FechaType
        /// </summary>
        public FechaType()
                : base("SoloFecha")
        {
            Description = "Fecha personalizada";//TypeResources.DateTimeType_Description;
        }

        /// <summary>
        /// sobre escribe el metodo de tipo local
        /// </summary>  
        public override Type ClrType => typeof(DateTimeOffset);


        /// <summary>
        /// Sobrescritura del metodo de serealización de la conversion a string de la fecha utc
        /// </summary>
        /// <param name="value">fecha hora utc (DateTime) que se desea serealizar</param>
        /// <returns>La fecha utc en formato string  "dd/MM/yyyy"</returns>
        protected override string Serialize(DateTime value)
        {
            if (value.Kind == DateTimeKind.Utc)
            {
                return value.ToString(
                    _utcFormat,
                    CultureInfo.InvariantCulture);
            }

            return value.ToString(
                _localFormat,
                CultureInfo.InvariantCulture);
        }


        /// <summary>
        /// Sobrescritura del metodo de serealización de la conversion a string de la fecha local 
        /// </summary>
        /// <param name="value">fecha hora local (DateTimeOffset) que se desea serealizar</param>
        /// <returns>La fecha local en formato string  "dd/MM/yyyy"</returns>
        protected override string Serialize(DateTimeOffset value)
        {
            if (value.Offset == TimeSpan.Zero)
            {
                return value.ToString(
                    _utcFormat,
                    CultureInfo.InvariantCulture);
            }

            return value.ToString(
                _localFormat,
                CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Sobrescritura del metodo de Deserealización de la conversion de la fecha a local
        /// </summary>
        /// <param name="serialized">fecha serializada en (string o DateTimeOffset) </param>
        /// <param name="obj">Fecha en formato DateTimeOffset</param>
        /// <returns></returns>
        public override bool TryDeserialize(object serialized, out object obj)
        {
            if (serialized is null)
            {
                obj = null;
                return true;
            }

            if (serialized is string s
                && TryDeserializeFromString(s, out object d))
            {
                obj = d;
                return true;
            }

            if (serialized is DateTimeOffset)
            {
                obj = serialized;
                return true;
            }

            if (serialized is DateTime dt)
            {
                obj = new DateTimeOffset(
                    dt.ToUniversalTime(),
                    TimeSpan.Zero);
                return true;
            }

            obj = null;
            return false;
        }

        /// <summary>
        /// Sobrescritura del metodo de Deserealización de la conversion de la fecha a utc
        /// </summary>
        /// <param name="serialized">fecha hora serializada en string</param>
        /// <param name="obj">Fecha en formato DateTime</param>
        /// <returns></returns>
        protected override bool TryDeserializeFromString(
            string serialized,
            out object obj)
        {
            if (serialized != null
                && serialized.EndsWith("Z")
                && DateTime.TryParse(
                    serialized,
                    CultureInfo.InvariantCulture,
                    DateTimeStyles.AssumeUniversal,
                    out DateTime zuluTime))
            {
                obj = new DateTimeOffset(
                    zuluTime.ToUniversalTime(),
                    TimeSpan.Zero);
                return true;
            }

            if (DateTimeOffset.TryParse(
                serialized,
                out DateTimeOffset dateTime))
            {
                obj = dateTime;
                return true;
            }

            obj = null;
            return false;
        }
    }
}
