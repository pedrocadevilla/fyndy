﻿using HotChocolate.Language;
using HotChocolate.Types;
using System;
using System.Globalization;

namespace Common.Utils.CustomTypes
{
    /// <summary>
    /// taken from https://github.com/ChilliCream/hotchocolate/issues/1441
    /// </summary>
    public class TimeSpanType : ScalarType
    {

        public TimeSpanType() : base("TimeSpan")
        {
        }

        public override bool IsInstanceOfType(IValueNode literal)
        {
            if (literal == null) {
                throw new ArgumentNullException(nameof(literal));
            }

            return literal is StringValueNode || literal is NullValueNode;
        }

        public override object ParseLiteral(IValueNode literal)
        {
            if (literal == null) {
                throw new ArgumentNullException(nameof(literal));
            }

            if (literal is NullValueNode) {
                return null;
            }

            if (literal is StringValueNode stringLiteral) {
                return TimeSpan.Parse(stringLiteral.Value, CultureInfo.InvariantCulture);
            }

            throw new ArgumentException(
                "The TimeSpanType can only parse string literals.",
                nameof(literal));
        }

        public override IValueNode ParseValue(object value)
        {
            if (value == null) {
                return new NullValueNode(null);
            }

            if (value is TimeSpan t) {
                return new StringValueNode(null, t.ToString("c", CultureInfo.InvariantCulture), false);
            }

            throw new ArgumentException(
                "The specified value has to be a TimeSpan in order to be parsed by the TimeSpanType.");
        }

        public override object Serialize(object value)
        {
            if (value == null) {
                return null;
            }

            if (value is TimeSpan t) {
                return t.ToString("c", CultureInfo.InvariantCulture);
            }

            throw new ArgumentException(
                "The specified value cannot be serialized by the TimeSpanType.");
        }

        public override bool TryDeserialize(object serialized, out object value)
        {
            if (serialized is null) {
                value = null;
                return true;
            }

            if (serialized is string s) {
                var result = TimeSpan.TryParse(s, out var ts);
                value = ts;
                return result;
            }

            value = null;
            return false;
        }

        public override Type ClrType => typeof(TimeSpan);
    }

    public class HourMinuteTimeSpanType : ScalarType
    {

        private readonly string _HourMinuteFormat = @"hh\:mm";

        public HourMinuteTimeSpanType() : base("HourMinuteTimeSpan")
        {
        }

        public override bool IsInstanceOfType(IValueNode literal)
        {
            if (literal == null) {
                throw new ArgumentNullException(nameof(literal));
            }

            return literal is StringValueNode || literal is NullValueNode;
        }

        public override object ParseLiteral(IValueNode literal)
        {
            if (literal == null) {
                throw new ArgumentNullException(nameof(literal));
            }

            if (literal is NullValueNode) {
                return null;
            }

            if (literal is StringValueNode stringLiteral) {
                return TimeSpan.ParseExact(stringLiteral.Value, _HourMinuteFormat, CultureInfo.InvariantCulture);
                //return TimeSpan.Parse(stringLiteral.Value, CultureInfo.InvariantCulture);
            }

            throw new ArgumentException(
                "The TimeSpanType can only parse string literals.",
                nameof(literal));
        }

        public override IValueNode ParseValue(object value)
        {
            if (value == null) {
                return new NullValueNode(null);
            }

            if (value is TimeSpan t) {

                return new StringValueNode(null, t.ToString(_HourMinuteFormat, CultureInfo.InvariantCulture), false);
                //return new StringValueNode(null, t.ToString("c", CultureInfo.InvariantCulture), false);
            }

            throw new ArgumentException(
                "The specified value has to be a TimeSpan in order to be parsed by the TimeSpanType.");
        }

        public override object Serialize(object value)
        {
            if (value == null) {
                return null;
            }

            if (value is TimeSpan t) {
                return t.ToString(_HourMinuteFormat, CultureInfo.InvariantCulture);
                //return t.ToString("c", CultureInfo.InvariantCulture);
            }

            throw new ArgumentException(
                "The specified value cannot be serialized by the TimeSpanType.");
        }

        public override bool TryDeserialize(object serialized, out object value)
        {
            if (serialized is null) {
                value = null;
                return true;
            }

            if (serialized is string s) {
                var result = TimeSpan.TryParseExact(s, _HourMinuteFormat, CultureInfo.InvariantCulture, out var ts);
                //var result = TimeSpan.TryParse(s, out var ts);
                value = ts;
                return result;
            }

            value = null;
            return false;
        }

        public override Type ClrType => typeof(TimeSpan);
    }

    //public sealed class HourMinuteType : TimeSpanType {
    //    // private const string _utcFormat = "yyyy-MM-ddTHH\\:mm\\:ss.fffZ";
    //    private const string _format = "HH\\:mm";
    //    public HourMinuteType() : base("HourMinute") {
    //        Description = "Solo Horas y minutos";//TypeResources.DateTimeType_Description;
    //    }

    //    public override Type ClrType => typeof(TimeSpan);

    //    protected override string Serialize(TimeSpan value)
    //    {
    //        return value.ToString( _format, CultureInfo.InvariantCulture);
    //    }

    //    public override bool TryDeserialize(object serialized, out object value)
    //    {
    //        if (serialized is null) {
    //            value = null;
    //            return true;
    //        }

    //        if (serialized is string s
    //            && TryDeserializeFromString(s, out object d)) {
    //            value = d;
    //            return true;
    //        }

    //        if (serialized is TimeSpan) {
    //            value = serialized;
    //            return true;
    //        }

    //        value = null;
    //        return false;
    //    }

    //    protected override bool TryDeserializeFromString(
    //        string serialized,
    //        out object obj)
    //    {
    //        if (serialized != null
    //            && TimeSpan.TryParse(
    //                serialized,
    //                out TimeSpan ParsedTimeSpan)) {
    //            obj = ParsedTimeSpan;
    //            return true;
    //        }

    //        obj = null;
    //        return false;
    //    }
    //}
}
