﻿using Common.Utils.Formats.String.Encode.UTF8.RSA;
using Common.Utils.Redirecccionamiento;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

using Newtonsoft.Json;

using System.Security.Cryptography;
using System.Threading.Tasks;

namespace Common.Utils.Encryption.RSA
{
    public class RSAUtils
    {
        private static string DefaultKeyContainerName
        {
            get { return "ContainerPrueba"; }
        }

        private static int DefaultKeySize
        {
            get { return 1024; }
        }

        public static bool DefaultDoOAEPPadding
        {
            get { return false; }
        }

        private static RSAKey ClavePublica { get; set; }

        public static RSACryptoServiceProvider GenerateCryptoProvider(string KeyContainerName = null)
        {
            KeyContainerName = KeyContainerName ?? DefaultKeyContainerName;
            CspParameters CspParam = new CspParameters() { KeyContainerName = KeyContainerName, };
            RSACryptoServiceProvider Provider = new RSACryptoServiceProvider(DefaultKeySize, CspParam);
            return Provider;
        }

        public static RSACryptoServiceProvider GenerateCryptoProvider(RSAParameters Parameters)
        {
            RSACryptoServiceProvider Provider = new RSACryptoServiceProvider(DefaultKeySize);
            Provider.ImportParameters(Parameters);
            return Provider;
        }

        private static bool DeleteKey(RSACryptoServiceProvider Provider)
        {
            Provider.PersistKeyInCsp = false;
            Provider.Clear();
            return true;
        }

        /// <summary>
        /// Obtener la clave pública que proviene del servicio ldapRest
        /// </summary>
        /// <param name="config"></param>
        /// <param name="contextAccessor"></param>
        /// <returns>Objeto que contiene los datos de la clave pública</returns>
        public static async Task<ClavePublica> ObtenerClavePublica(IConfiguration config, IHttpContextAccessor contextAccessor)
        {
            Calls cliente = new Calls(config);

            string cadenaRespuesta = await cliente.PostRestAsync("LDAP",
                "clavepublica", "", contextAccessor.HttpContext.Request.Headers);
            ResultRest<ClavePublica> clave = JsonConvert.DeserializeObject<ResultRest<ClavePublica>>(cadenaRespuesta);
            return clave.Result;
        }

        /// <summary>
        /// Encriptar una cadena de texto asimétricamente utilizando la clave pública 
        /// obtenida desde el servicio LDAP
        /// </summary>
        /// <param name="config"></param>
        /// <param name="contextAccessor"></param>
        /// <param name="pass"></param>
        /// <returns>Cadena encriptada en formato base 64</returns>
        public static async Task<string> EncriptarPassword(IConfiguration config, IHttpContextAccessor contextAccessor, string pass)
        {
            using (RSACryptoServiceProvider rsaCrypto = new RSACryptoServiceProvider())
            {
                ClavePublica clavePublica = await ObtenerClavePublica(config, contextAccessor);

                rsaCrypto.ImportParameters(new RSAKey()
                {
                    Modulus = clavePublica.Modulus,
                    Exponent = clavePublica.Exponent
                }.AsRSAParameter());

                return pass.FromUTF8ToRSABase64With(rsaCrypto);
            }
        }

    }
    /// <summary>
    /// Clase contenedora para recibir la respuesta del servicio de clave pública
    /// </summary>
    public class ClavePublica
    {
        public string Modulus { get; set; }
        public string Exponent { get; set; }
        public string P { get; set; }
        public string Q { get; set; }
        public string DP { get; set; }
        public string DQ { get; set; }
        public string InverseQ { get; set; }
        public string D { get; set; }
        public string Modulus_Hex { get; set; }
        public string Exponent_Hex { get; set; }
        public string P_Hex { get; set; }
        public string Q_Hex { get; set; }
        public string DP_Hex { get; set; }
        public string DQ_Hex { get; set; }
        public string InverseQ_Hex { get; set; }
        public string D_Hex { get; set; }

    }

    public static class RSAServiceProviderExtensions
    {
        public static RSAKey getPublicKey(this RSACryptoServiceProvider Provider)
        {
            return (RSAKey)Provider.ExportParameters(false);
        }

        public static RSAKey getPrivateKey(this RSACryptoServiceProvider Provider)
        {
            return (RSAKey)Provider.ExportParameters(true);
        }
    }

}