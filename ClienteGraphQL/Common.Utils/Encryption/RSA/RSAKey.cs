﻿using Common.Utils.Formats.String.Encode.Base64;
using Common.Utils.Formats.String.Encode.Hexadecimal;
using Common.Utils.Formats.XML;
using System.Security.Cryptography;

namespace Common.Utils.Encryption.RSA
{
    public class RSAKey
    {
        public string Modulus { get; set; }
        public string Exponent { get; set; }
        public string P { get; set; }
        public string Q { get; set; }
        public string DP { get; set; }
        public string DQ { get; set; }
        public string InverseQ { get; set; }
        public string D { get; set; }
        public string Modulus_Hex { get { return Modulus?.FromBase64ToByteArray().GetHexString().Replace("-", string.Empty); } set { } }
        public string Exponent_Hex { get { return Exponent?.FromBase64ToByteArray().GetHexString().Replace("-", string.Empty); } set { } }
        public string P_Hex { get { return P?.FromBase64ToByteArray().GetHexString().Replace("-", string.Empty); } set { } }
        public string Q_Hex { get { return Q?.FromBase64ToByteArray().GetHexString().Replace("-", string.Empty); } set { } }
        public string DP_Hex { get { return DP?.FromBase64ToByteArray().GetHexString().Replace("-", string.Empty); } set { } }
        public string DQ_Hex { get { return DQ?.FromBase64ToByteArray().GetHexString().Replace("-", string.Empty); } set { } }
        public string InverseQ_Hex { get { return InverseQ?.FromBase64ToByteArray().GetHexString().Replace("-", string.Empty); } set { } }
        public string D_Hex { get { return D?.FromBase64ToByteArray().GetHexString().Replace("-", string.Empty); } set { } }

        public static explicit operator RSAKey(RSAParameters Value)
        {
            RSAKey NewInstance = new RSAKey()
            {
                D = Value.D?.GetBase64String(),
                DP = Value.DP?.GetBase64String(),
                DQ = Value.DQ?.GetBase64String(),
                Exponent = Value.Exponent?.GetBase64String(),
                InverseQ = Value.InverseQ?.GetBase64String(),
                Modulus = Value.Modulus?.GetBase64String(),
                P = Value.P?.GetBase64String(),
                Q = Value.Q?.GetBase64String()
            };

            return NewInstance;
        }

        public static explicit operator RSAKey(string XMLValue)
        {

            if (string.IsNullOrWhiteSpace(XMLValue)) {
                return null;
            }

            RSAKey NewInstance = XmlSerialzeUtils.GetObject<RSAKey>(XMLValue);

            return NewInstance;
        }

        public RSAParameters AsRSAParameter()
        {
            RSAParameters NewInstance = new RSAParameters();
            NewInstance.Modulus = string.IsNullOrEmpty(Modulus) ? null : Modulus.FromBase64ToByteArray();
            NewInstance.Exponent = string.IsNullOrEmpty(Exponent) ? null : Exponent.FromBase64ToByteArray();
            NewInstance.P = string.IsNullOrEmpty(P) ? null : P.FromBase64ToByteArray();
            NewInstance.Q = string.IsNullOrEmpty(Q) ? null : Q.FromBase64ToByteArray();
            NewInstance.DP = string.IsNullOrEmpty(DP) ? null : DP.FromBase64ToByteArray();
            NewInstance.DQ = string.IsNullOrEmpty(DQ) ? null : DQ.FromBase64ToByteArray();
            NewInstance.InverseQ = string.IsNullOrEmpty(InverseQ) ? null : InverseQ.FromBase64ToByteArray();
            NewInstance.D = string.IsNullOrEmpty(D) ? null : D.FromBase64ToByteArray();
            return NewInstance;
        }
    }
}