# Implementacion de Roles de Aplicacion SqlServer+EntityFramework

Un rol de aplicacion es un conjunto de permisos y restricciones que se adquieren cuando se conectan mediante una aplicacion. Conceptualmente seria un "usuario" de una aplicacion.

Por tanto, un usuario puede solo tener permiso de conexion a la base de datos, pero, si se aplica el rol de aplicacion, el usuario toma los permisos del rol y podra realizar cualquier operacion que tenga permitida el rol.

## Configuracion inicial

se debe configurar el Rol de Aplicacion y los permisos en la seguridad de la base de datos correspondiente

### Configuracion del Rol de Aplicacion en SQL Server - SQL Management Studio

En el apartado de `Roles de Aplicacion` dentro de `Roles` (apartado de `seguridad` de cada base de datos) se agrega un rol de aplicacion; se coloca el nombre, la contraseña, y, en la seccion `elementos protegibles` se configura a que elementos pueden ser accedidos atraves de este rol.

A cada uno de los `elementos protegibles` del rol,se le deben conceder sus permisos especificos; por ejemplo a que tabla se le dan permisos de seleccion, insercion, eliminacion o actualizacion(?)(UPDATE)

### Prueba de Concepto

Teniendo una base de datos con al menos 2 tablas (ejemplo "TABLA_PADRE" y "TABLA_HIJA") con una cantidad de registros para visualizar

Se crea un rol de aplicacion que tenga permisos de seleccion sobre "TABLA_PADRE".

Se ejecuta el siguiente script de base de datos:
```sql
USE NOMBRE_BASE_DATOS
DECLARE @cookie varbinary(8000); 
SELECT USER_NAME();  
EXEC sys.sp_setapprole 'NOMBRE_DE_ROL', 'CONTRASENIA_DE_ROL', @fCreateCookie = true, @cookie = @cookie OUTPUT;
SELECT USER_NAME();  
SELECT * FROM TABLA_PADRE;
SELECT * FROM TABLA_HIJA;
EXEC sys.sp_unsetapprole @cookie;
SELECT USER_NAME();  
```

El resultado de esta operacion deberia dar el resultado de la seleccion de la tabla "TABLA_PADRE" pero obtenerse un error con el intento de seleccion a la "TABLA_HIJA" denegando el permiso SELECT sobre ese eobjeto.

> En cuanto a los resultados del `SELECT USER_NAME()` debio ser el nombre del usuario en el primero y en el ultimo, y, el nombre del rol en el segundo.

En Esencia, la prueba del rol de aplicacion consta de las siguiente lineas:

* la declaracion de la variable que almacenara la cookie:
```sql
DECLARE @cookie varbinary(8000); 
```
* la asuncion de los permisos:
```sql
EXEC sys.sp_setapprole 'NOMBRE_DE_ROL', 'CONTRASENIA_DE_ROL', @fCreateCookie = true, @cookie = @cookie OUTPUT;
```
> se realiza atraves de el procedimiento almacenado `sp_setapprole` que recibe el nombre del rol de la aplicacion, la contraseña, si se requiere devolucion de cookie (`true`: si en este caso) y el parametro de salida, la cookie.

* el retiro de el rol de aplicacion a la conexion actual:

```sql
EXEC sys.sp_unsetapprole @cookie;
```
> se hace mediante la cookie generada por el procedimiento `sp_setapprole`, dando por finalizado el uso de el rol.

## Implementacion con EntityFramework

En cuanto a la implementacion en la capa de persistencia se debe agregar el llamado de los procedimiento de asuncion y retiro de el rol de aplicacion ocurriria en los momentos de creacion y cierre del contexto, es decir, en el constructores del contextos y en la llamada del `Dispose()` del contexto.

En el CommonUtils se crearon las clases de metodos de extension `DBContextApplicationRolesExtension` y la clase `ApplicationRoleParams` que carga los datos de rol de aplicacion.

### Configuracion necesaria

En el `appsettings.json` se debe contener la informacion de la siguiente manera:

```js
{
    //...
    "ApplicationRole": {
        "User": "NOMBRE_ROL",
        "Password": "CONTRASEÑA_ROL"
    },
    //...
}
```

### Carga de Configuracion

En el `Startup.cs` en el metodo `ConfigureServices` se agrega la inyeccion del servicio:

```csharp
public class Startup {    
    // ...
    public void ConfigureServices(IServiceCollection services) {
        //...
        services.AddTransient<ApplicationRoleParams>();
        //...
        services.AddSingleton<IConfiguration>(Configuration);
        //...
    }
    //...
}
```

> se debe tener en cuenta que la clase necesita tener la configuracion del proyecto instanciada en el servicio.

### Modificacion de DbContext

La clase de contexto (que hereda de `DbContext`) se implmenta el codigo de la implementacion del rol de aplicacion:

* Agregando la variable que contendra el Token/Cookie

```csharp
public partial class CENTRALContext : DbContext {
    //...
    /// <summary>
    /// Alamcena el Token/cookie del rol de aplicacion ejecutado
    /// </summary>
    byte[] cookie;
    //...
}
```
* Modificando el constructor que recibe las opciones.
```csharp
using Common.Utils.ApplicationRoles;
//...
public partial class CENTRALContext : DbContext {
    //...
    public CENTRALContext(DbContextOptions<CENTRALContext> options, ApplicationRoleParams appRoleParams) : base(options) {
        cookie = (this as DbContext).SetAppRole(appRoleParams);
    }
    //...
}
```
> El metodo de extension `SetAppRole` se implementa de forma generica hacia el tipo DbContext, y se encarga de abrir la conexion y ejecutar el procedimiento almacenado del rol de aplicacion

> Recuerde que cualquier metodo de extension debe tener su using respectiva

* Y sobreescribiendo el metodo `Dispose`.

```csharp
using Common.Utils.ApplicationRoles;
//...
public partial class CENTRALContext : DbContext {
    //...
    public override void Dispose() {
        if( cookie != null ) {
            (this as DbContext).UnSetAppRole(cookie);
        }
        base.Dispose();
    }
    //...
}
```

> se verifica que la cookie no este nula para desactiva el rol de aplicacion.

----------------------------------

> queda por verificar la colision de constructores; si es Database First y se generan constructores en el Scaffold, habria que eliminarlos del codigo generado.

> puede atacarse tambien como parte de la calse parcial, pero el constructor generado se deberia eliminar.? Meh
