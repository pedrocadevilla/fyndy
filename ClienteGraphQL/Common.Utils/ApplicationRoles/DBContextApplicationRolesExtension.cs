﻿using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;

namespace Common.Utils.ApplicationRoles
{
    /// <summary>
    /// Metodos de Extension para la aplicacion de un Contexto de Base de datos (solo SQL SERVER)
    /// <a>https://docs.microsoft.com/en-us/sql/relational-databases/system-stored-procedures/sp-setapprole-transact-sql?view=sql-server-ver15</a>
    /// </summary>
    public static class DBContextApplicationRolesExtension
    {
        /// <summary>
        /// Abre la conexion a Base de datos del contexto y ejecuta el rol de aplicacion con el usuario y contraseña contenidos en appRoleParam
        /// </summary>
        /// <param name="context">contexto de base de datos</param>
        /// <param name="appRoleParams">Parametros de Rol de aplicacion</param>
        /// <returns>el token/cookie que debe ser enviado en el momento de retirar el Rol de aplicacion</returns>
        public static byte[] SetAppRole(this DbContext context, ApplicationRoleParams appRoleParams) {
            /// TODO: QUE PASA SI NO PUEDE ABRIR LA CONEXION, O SI YA ESTA ABIERTA
            context.Database.OpenConnection();
            return appRoleParams.ApplyAplicationRoleTo(context);
        }

        /// <summary>
        /// Abre la conexion a Base de datos del contexto y ejecuta el rol de aplicacion con el usuario y contraseña recibidos
        /// </summary>
        /// <param name="context">Contexto de base de datos</param>
        /// <param name="approle">Nombre del Rol de aplicacion a ejecutar</param>
        /// <param name="password">Contraseña del Rol de aplicacion a ejecutar</param>
        /// <returns></returns>
        public static byte[] SetAppRole(this DbContext context, string approle, string password) {
            context.Database.OpenConnection();
            var cmd = context.Database.GetDbConnection().CreateCommand();
            cmd.CommandText = "sp_setapprole";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            // Parametros
            cmd.Parameters.Add(new SqlParameter("@rolename", approle));
            cmd.Parameters.Add(new SqlParameter("@password", password));
            cmd.Parameters.Add(new SqlParameter("@fCreateCookie", 1));
            var pCookieId = new SqlParameter("@cookie", System.Data.SqlDbType.VarBinary);
            pCookieId.Size = 8000;
            pCookieId.Direction = System.Data.ParameterDirection.Output; // OUT DEL STORED PROCEDURE
            cmd.Parameters.Add(pCookieId);

            // Ejecucion
            cmd.ExecuteNonQuery();
            return (byte[]) pCookieId.Value;
        }

        /// <summary>
        /// Retira el rol le aplicacion del contexto dado su token/cookie
        /// </summary>
        /// <param name="context">Contexto de base de datos</param>
        /// <param name="cookie">Token/cookie del rol de aplicacion aplicado. Se obtiene de la ejecucion de SetAppRole.</param>
        public static void UnSetAppRole(this DbContext context, byte[] cookie)
        {
            var cmd = context.Database.GetDbConnection().CreateCommand();
            cmd.CommandText = "sp_unsetapprole";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            var pCookieId = new SqlParameter("@cookie",
            System.Data.SqlDbType.VarBinary);
            pCookieId.Size = 8000;
            pCookieId.Value = cookie;
            cmd.Parameters.Add(pCookieId);
            cmd.ExecuteNonQuery();

        }
    }
}
