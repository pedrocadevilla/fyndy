﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;

namespace Common.Utils.ApplicationRoles
{

    /// <summary>
    /// Clase que representa los datos de un rol de aplicación.
    /// Dado que la contraseña debe ser guardada en plano en la configuracion se coloca como protegida en la clase.
    /// </summary>
    public class ApplicationRoleParams
    {
        /// <summary>
        /// Nombre del rol de aplicacion
        /// </summary>
        protected string User;

        /// <summary>
        /// Contraseña del rol de aplicacion
        /// </summary>
        protected string Password;

        // TODO: REVISAR SI ES NECESARIO RECIBIR COMO PARAMETRO EL NOMBRE DE LA SECCION
        /// <summary>
        /// Asigna el usuario y la contraseña ubicando los valores "ApplicationRole:User y "ApplicationRole:Password"
        /// </summary>
        /// <param name="configuration">La configuracion del servicio </param>
        public ApplicationRoleParams(IConfiguration configuration)
        {
            // SE OBTIENE EL SECTOR DE LA CONFIGURACION QUE CONTIENE LA CLAVE PRIVADA
            IConfigurationSection AppRoleSector = configuration.GetSection("ApplicationRole");
            // SE VERIFICA QUE EXISTA LA CLAVE PRIVADA EN LA CONFIGURACION
            bool IsAppRoleSectorConfigured = AppRoleSector.Exists();

            if (! IsAppRoleSectorConfigured) {
                throw new ArgumentException("No se ha configuerado el rol de aplicacion");
            }

            //Obtiene el usuario y la contraseña
            User = configuration["ApplicationRole:User"];
            Password = configuration["ApplicationRole:Password"];

        }

        /// <summary>
        /// Abre la conexion a Base de Datos y obtiene el token/cookie de haber aplicado el rol de aplicacion correspondiente
        /// </summary>
        /// <param name="context">DbContext a ser aplicado el reol de aplicacion, debe tener la conexion ya abierta</param>
        /// <returns>Vector de bytes identificador de la ejecucion del rol de aplicacion para luego ser retirado</returns>
        public byte[] ApplyAplicationRoleTo(DbContext context) {
            byte[] cookie = context.SetAppRole(this.User, this.Password);
            return cookie;

        }

    }
}
