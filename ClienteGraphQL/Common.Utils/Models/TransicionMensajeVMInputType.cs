﻿using HotChocolate.Types;
namespace Common.Utils.Models
{
    public class TransicionMensajeVMInputType : InputObjectType<TransicionMensajeVM>
    {
        protected override void Configure(IInputObjectTypeDescriptor<TransicionMensajeVM> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Name("TransicionMensajeVMInput");

            // descriptor.Field(x => x.Id).Ignore();
            descriptor.Field(x => x.Transicion).Type<NonNullType<LongType>>().Description("Identificador Transicion"); 
            descriptor.Field(x => x.Mensajes).Type<NonNullType<ListType<LongType>>>().Description("Lista Mensajes para procesar la Transicion"); 


        }
    }
}

