﻿using HotChocolate.Types;
namespace Common.Utils.Models
{
    public class ProcessActivityInputType : InputObjectType<ProcessActivity>
    {
        protected override void Configure(IInputObjectTypeDescriptor<ProcessActivity> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Name("ProcessActivityInput");

            // descriptor.Field(x => x.Id).Ignore();
            descriptor.Field(x => x.ActividadId).Type<NonNullType<LongType>>().Description("Identificador Actividad"); 
descriptor.Field(x => x.Condicion).Type<NonNullType<StringType>>().Description("Condicion para procesar la Actividad"); 
descriptor.Field(x => x.UsuarioId).Type<NonNullType<LongType>>().Description("Identificador Usuario que procesa la Actividad"); 
descriptor.Field(x => x.TransicionMensajes).Type<ListType<TransicionMensajeVMInputType>>().Description("Identificador Actividad"); 


        }
    }
}

