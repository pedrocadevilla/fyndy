﻿using System.Collections.Generic;

namespace Common.Utils.Models
{
    public class ProcessActivity
    {
        public long ActividadId { get; set; }
        public string Condicion { get; set; }
        public List<TransicionMensajeVM> TransicionMensajes { get; set; }
        public long? UsuarioId { get; set; }

        public ProcessActivity(long actId, string cond, List<TransicionMensajeVM> lista, long? Usuarioid)
        {
            ActividadId = actId;
            Condicion = cond;
            TransicionMensajes = lista;
            UsuarioId = Usuarioid;



        }
        public ProcessActivity()
        {
        }
    }
}
