﻿using System.Collections.Generic;

namespace Common.Utils.Models
{
    public class ProcesarActividadResponse
    {
        public List<ProcesarActividad> procesarActividad { get; set; }
    }
}
