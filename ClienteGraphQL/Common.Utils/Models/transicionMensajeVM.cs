﻿using System.Collections.Generic;

namespace Common.Utils.Models
{
    /// <summary>
    /// Vista de transición mensaje
    /// </summary>
    public class TransicionMensajeVM
    {
        public long Transicion { get; set; }
        public List<long> Mensajes { get; set; }

        public TransicionMensajeVM()
        {
        }

    }
}
