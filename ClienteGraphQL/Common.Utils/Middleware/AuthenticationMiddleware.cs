﻿using Common.Utils.Exceptions;
using Common.Utils.JWT;

using HotChocolate.Resolvers;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Utils.Middleware
{
    public class AuthenticationMiddleware
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly FieldDelegate _next;
        private readonly IConfiguration _configuration;


        public AuthenticationMiddleware(FieldDelegate next, IHttpContextAccessor contextAccessor, IConfiguration configuration)
        {
            _contextAccessor = contextAccessor;
            _next = next;
            _configuration = configuration;
        }

        /// <summary>
        /// Funcion del middleware que es llamada.
        /// </summary>
        /// <param name="context">Instancia del contexto del middleware. (generalmente inyectado)</param>
        /// <returns>La instacia de la tarea ejecutada. (en este caso, ya culminada)</returns>
        public async Task Invoke(IMiddlewareContext context)
        {

            string token = _contextAccessor.HttpContext.Request.Headers.FirstOrDefault(x => x.Key == "Authorization").Value;

            // solo se aplica si el usuario tiene un token válido
            if (!string.IsNullOrWhiteSpace(token) && !token.Equals("null"))
            {

                token = token.Split(" ")[1];
                ////
                string tokenActualizado = "";
                JwtToken jwtToken = new JwtToken(_configuration);

                // solo se aplica si el usuario tiene un token válido
                var handler = new JwtSecurityTokenHandler();
                //var jsonToken = handler.ReadToken(token);
                var tokenS = handler.ReadToken(token) as JwtSecurityToken;

                string origen = _contextAccessor.HttpContext.Request.Path.Value;

                // si la operación es "prevenirAbandonoActividad" no se refresca el token 
                // de lo contrario si el token está vencido se puede refrescar
                if (tokenS.ValidTo < DateTime.UtcNow)
                {
                    //throw new BusinessException("ID17");
                    // si el llamado no es desde el playground
                    //if (origen != "/")
                    //{
                    //    throw new BusinessException("ID17");
                    //}

                    // realizar la validación
                    try
                    {

                        // actualizar el token desde el servicio de autenticacion
                        // si el la operación es prevenirAbandonoActividad no se 
                        // extiende el tiempo de vida de la sesión y se usa solo en esta ocasión
                        bool nonce = false;
                        if (origen.CompareTo("prevenirAbandonoActividad") != 0)
                        {
                            nonce = true;
                        }

                        tokenActualizado = await jwtToken.RefrescarTokenAsync(token, nonce);
                        // reglas para validar el token nuevo
                        var validationParams = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidateAudience = true,
                            ValidateLifetime = true,
                            ValidateIssuerSigningKey = true,
                            ValidIssuer = _configuration["Jwt:Issuer"],
                            ValidAudience = _configuration["Jwt:Issuer"],
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]))
                        };

                        handler.ValidateToken(tokenActualizado, validationParams, out SecurityToken validToken);

                        // si el token es válido se retorna el token en los headers del navegador
                        if (validToken.ValidTo > DateTime.UtcNow)
                        {
                            // si el token es de un solo uso no se agrega a las cabeceras de respuesta
                            if (!nonce)
                            {
                                _contextAccessor.HttpContext.Response.Headers.Add("Token", tokenActualizado);
                            }
                        }
                        else
                        {
                            throw new BusinessException("ID17");
                        }
                    }
                    catch (Exception)
                    {
                        throw new BusinessException("ID17");
                    }
                }
                else
                {
                    // si el token no está vencido se revisa si la sesión está activa, en caso negativo
                    // se rechaza la petición
                    //    Calls cliente = new Calls(_configuration, token);

                    //    string query = @"query verificarSesionActiva {
                    //                                  sesionValida
                    //                                }";

                    //    SessionResponse respuesta = await cliente.QueryAsync<SessionResponse, object>(query, "verificarSesionActiva", null, "GraphAuth");
                    //    if (!respuesta.SesionValida)
                    //    {
                    //        throw new BusinessException("ID17");
                    //    }
                    await _next.Invoke(context);
                }
            }
            else
            {
                throw new BusinessException("ID17");
            }
        }
    }
    class SessionResponse
    {
        public bool SesionValida { get; set; }
    }
}
