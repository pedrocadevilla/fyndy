﻿using Common.Utils.Encryption.RSA;
using Common.Utils.Formats.String.Encode.UTF8.RSA;
using Common.Utils.Windows.Authentication;
using HotChocolate.Execution;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Win32.SafeHandles;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Threading.Tasks;

namespace Common.Utils.Middelware
{
    /// <summary>
    /// Clase que contiene el middleware para impersonalizar las operaciones al servicio
    /// Se debe agregar pipeline del execucion del QueryExcecutionBuilder del HotChocolate
    /// </summary>
    /// <remarks>
    /// Debe agregarse mediante el .Use<ImpersonatedMiddleware>() durante la creacion del schema
    /// </remarks>
    /// <remarks>
    /// Deben enviarse el usuario y la contraseña en los claims del usuario "Token1" y "Token2" respectivamente
    /// </remarks>
    /// <remarks>
    /// Se espera que el archivo de configuracion contenga en el Clave-Valor "Domain" el dominio del directorio de windows.
    /// </remarks>
    /// <remarks>
    /// Se espera que el archivo de configuracion contenga en el Clave-Valor "CheckImpersonation" se configure un bool; True si se debe realizar impersonalizacion, false para hacer corto al siguiente contexto.
    /// </remarks>
    /// <remarks>
    /// Se espera que el archivo de configuracion contenga en el Clave-Valor "ImpersonationKey" se configure la clave privada RSA que desencripta la contraseña; debe contener internamente los campo Modulus, Exponent, P, Q, DP, DQ, InverseQ y D; todos en base64.
    /// </remarks>
    public class ImpersonatedMiddleware
    {

        /// <summary>
        /// Contiene la cadena de caracteres que identifica el sector que contiene la configuracion relacionada con el Middleware de impersonacion.
        /// </summary>
        public static string CONFIG_SECTOR = "ImpersonationMiddleware";
        /// <summary>
        /// Contiene la cadena de caracteres que identifica el sector que contiene la clave privada para desencriptar la contraseña del usuario.
        /// </summary>
        public static string CONFIG_KEY = "ImpersonationKey";
        /// <summary>
        /// Contiene la cadena de caracteres que identifica el sector que contiene el dominio de windows del usuario.
        /// </summary>
        public static string CONFIG_DOMAIN = "Domain";
        /// <summary>
        /// Contiene la cadena de caracteres que identifica el usuario dentro del token jwt
        /// </summary>
        public static string TOKEN_USER = "Token1";
        /// <summary>
        /// Contiene la cadena de caracteres que identifica la contraseña encriptada en base 64 del usuario dentro del token jwt
        /// </summary>
        public static string TOKEN_PASS = "Token2";
        /// <summary>
        /// Contiene la cadena de caracteres que identifica la configuracion de uso o no de impersonacion
        /// </summary>
        public static string CONFIG_CHECK = "CheckImpersonation";

        /// <summary>
        /// Representa el metodo a ser llamado como siguiente capa del middleware.
        /// </summary>
        protected readonly QueryDelegate next;
        /// <summary>
        /// Representa el contexto en el cual fue llamado el middleware. Deberia ser inyectado.
        /// </summary>
        protected readonly IHttpContextAccessor contextAccessor;

        /// <summary>
        /// Representa la instancia del logger
        /// </summary>
        protected readonly ILogger logger;

        /// <summary>
        /// Representa la configuracion del servicio. Deberia ser inyectado.
        /// </summary>
        private readonly IConfiguration configuration;
        public ImpersonatedMiddleware(QueryDelegate next, IHttpContextAccessor contextAccessor, IConfiguration configuration, ILogger<ImpersonatedMiddleware> logger )
        {
            this.next = next;
            this.contextAccessor = contextAccessor;
            this.configuration = configuration;
            this.logger = logger;
        }

        /// <summary>
        /// Funcion del middleware que es llamada.
        /// </summary>
        /// <param name="context">Instancia del contexto del middleware. (generalmente inyectado)</param>
        /// <returns>La instacia de la tarea ejecutada. (en este caso, ya culminada)</returns>
        public async Task Invoke(IQueryContext context)
        {
            if (context == null) {
                return;
            }

            // SE OBTIENE EL SECTOR DE CONFIGURACION ESPECIFICA PARA EL MIDDLEWARE
            IConfigurationSection ConfigurationSector = configuration.GetSection(CONFIG_SECTOR);

            // SE OBTIENE LA CONFIGURACION SI SE DEBE IMPERSONALIZAR
            bool CheckImpersonation = ConfigurationSector.GetValue<bool>(CONFIG_CHECK);

            // SI ES FALSO, PASA LA INVOCACION DEL SIGUIENTE CONTEXTO
            if (!CheckImpersonation) {
                await next.Invoke(context);
                return;
            }

            // SE OBTIENE EL TOKEN1 (EL USUARIO) DEL TOKEN
            string token1 = contextAccessor.HttpContext.User.FindFirst(TOKEN_USER)?.Value;
            // SE OBTIENE EL TOKEN2 (LA CONTRASEÑA) DEL TOKEN
            string token2 = contextAccessor.HttpContext.User.FindFirst(TOKEN_PASS)?.Value;

            // SE OBTIENE EL SECTOR DE LA CONFIGURACION QUE CONTIENE LA CLAVE PRIVADA
            IConfigurationSection ImpersonationSector = ConfigurationSector.GetSection(CONFIG_KEY);
            // SE VERIFICA QUE EXISTA LA CLAVE PRIVADA EN LA CONFIGURACION
            bool ImpersonationKeyConfigured = ImpersonationSector.Exists();

            // SI NO EXISTE EL USUARIO EN EL TOKEN
            // O SI NO EXISTE LA CONTRASEÑA EN EL TOKEN
            // O SI NO ESTA CONFIGURADA LA CLAVE PRIVADA EN EL ARCHIVO
            // ----------------------------------------------------------
            // ∴ INVOCA EL SIGUIENTE CONTEXTO SIN IMPERSONALIZAR
            if (string.IsNullOrWhiteSpace(token1) || string.IsNullOrWhiteSpace(token2) || !ImpersonationKeyConfigured) {
                logger.LogWarning("token1: {0} token2: {1} ImpersonationKeyConfigured: {2}", token1, token2, ImpersonationKeyConfigured);
                // TODO: MEJORAR RESPUESTA DE REBOTE
                return;
            }

            // OBJETO QUE CONTENDRA LA CLAVE PRIVADA
            RSAKey key = new RSAKey();
            // SE TRANSFIEREN LOS DATOS DEL SECTOR AL OBJETO
            ImpersonationSector.Bind(key);
            // SE CREA EL CSP A PARTIR DE LA CLAVE PRIVADA
            RSACryptoServiceProvider sp = RSAUtils.GenerateCryptoProvider(key.AsRSAParameter());

            // SE OBTIENE EL DOMINIO
            string domain = ConfigurationSector[CONFIG_DOMAIN];

            // OBTIENE EL TOKEN DE IMPERSONALIZACION
            SafeAccessTokenHandle safeAccessTokenHandle = AuthenticationUtils.Authenticate(token1, token2.FromRSABase64ToUTF8With(sp), domain);

            // SE OBTIENE LA TAREA QUE SE VA CORRER IMPERSONALIZADA
            Task TareaImpersonada = WindowsIdentity.RunImpersonated(
                safeAccessTokenHandle,
                async () => {
                    // DENTRO DE ESTE BLOQUE TODO SE CORRERA IMPERSONALIZADO
                    logger.LogInformation("During impersonation: " + WindowsIdentity.GetCurrent().Name + " ");
                    logger.LogTrace("PreInvoke");
                    // SE INVOCA AL SIGUIENTE EJECUCION
                    await next.Invoke(context);
                    logger.LogTrace("PostInvoke");
                }
            );
            // SE ESPERA POR LA EJECUCION DE LA TAREA PERSONALIZADA
            await TareaImpersonada;
            // SE RETORNO LA TAREA YA EJECUTADA (TOTAL PIDE UNA TAREA DE RETORNO)
        }
    }
}
