
# Implementacion de Middleware de impersonacion

> TODO: AGREGAR NOTA SOBRE PERMISOS, GRUPOS Y USUARIOS EN BASE DE DATOS

## Lineas Generales:

Este middleware para HotChocolate de impersonalizacion hace uso de:

* Encriptacion y desencriptacion en base a RSA
* Autenticacion mediante llamadas a la api de windows (`advapi32.dll` y `kernel32.dll`)
* Recepcion de parametros mediante obtencion de valores de claims de un usuario.

Se espera que el usuario y la contraseña se encuentren dentro de los claims del usuario en variables identificadas por `Token1` y `Token2` respectivamente; la contraseña este cifrada por la clave publica RSA y que lo necesario este definido en la configuracion del proyecto ejecutable.

## Configuracion inicial

Las confguraciones de este middleware deben estar dentro de un sector llamado `ImpersonationMiddleware` en `appsettings.json`

### Clave de desencriptacion

Dentro del sector `ImpersonationMiddleware` se agrega una configuracion bajo el identificador `ImpersonationKey` de acuerdo a la siguiente estructura:

```js
{
    //...
    "ImpersonationMiddleware": {
        //...
        "ImpersonationKey": {
            "Modulus": "k/UCWoJSPpPJyT9MQZfdXsbykRuab0oo23hRUfrrpXeYMdeCDlYs9J+wNXgnrsRkvb1/1JMkbQSUhtNpDSEttYt6uB68Gwq6pphICKBynfaeVw0zr9o1JZvZMqwXjBF9Ts/5DIGwQTkPeshUwLDJUYp0W6Dx8Rq9Ktcub8Vj+G8=",
            "Exponent": "AQAB",
            "P": "vkd+bHSOSKpp0cNCRSyJLCAqjYKTodzaj1lbeYy2smzZcjEaM5U5taYnXIsFMJxXo1dgrmTSNKeylboy3Fk4wQ==",
            "Q": "xw9iELekhXCJVKVH+jpdnQwEveGjZm39v5oJlzcJXoRGmT70qhiz6TikpaeuzQ7aF+bdxzJEji2BmrMwcpDNLw==",
            "DP": "A/96j4rPZoV2HbstjEiIRU4Tts90jKUYToTBIEetmwggdX7EnyrWgJOSJiahoEjYqozuchuaMxMG9JBp+ylAwQ==",
            "DQ": "nvhIBrXCK9btta2xR8Ko1Cyu9L1n8vtQNF3d3udTEips/yFp3xWPSGe2BZ1eWeDMh14WK2iXmzv4TwVfYX8GfQ==",
            "InverseQ": "FveGWQOfFMaKvrfoPzMMy+/sprNHo6fb7zKLlrm7Wso0a9vd3ZlPrN0PWUZ5zKx9oWWHj6VBxeptlztrIp9noA==",
            "D": "F15w5I5wjWK2pzB2C7wqokVxBZCjDVCHcIFUnXVbfSiNDijtk/RyKExalAS/hnU4O4HFQXQ2IiPsOPlbk8/O51VlmYbH8yneyC8Zr4fbcQS0DZR+f6TuyWPLvh76QXPqsEOpfB+d9coaRgM/AzDSoWU536nmhoBblRO/Kl1CI0E="
        },
        //...
    }
    //...
}
```

siendo obligatorios los campos `Modulus`,`Exponent`, `P`, `Q`, `DP`, `DQ`, `InverseQ` y `D`; dado que son necesarios para la desencriptacion de la contraseña.

### Modo Desarrollo

Dentro del sector `ImpersonationMiddleware` se agrega una configuracion bajo el identificador `CheckImpersonation` que contenga un valor booleano; su valor significara si el ambiente que ejecuta el proyecto debe o no realizar impersonalizacion del mismo.

```js
{
    //...
    "ImpersonationMiddleware": {
        //...
        "CheckImpersonation":  false,
        //...
    }
}
```

Significando que si `CheckImpersonation` es `false`, el middleware no verificara usuario y contraseña, ni realizara la impersonalizacion; sino que se ejecutara el siguiente proceso.

En caso de ser `true`, se verificara que exista que el usuario, la contraseña y la clave de desencriptacion existan; si alguno de estos no contiene valor, no ejecutara el siguiente contexto.

### Configuracion del Dominio

Dentro del sector `ImpersonationMiddleware` se agrega una configuracion bajo el identificador `Domain` que contenga un valor del dominio de windows que va a ser usado durante la autenticacion.

```js
{
    //...
    "ImpersonationMiddleware": {
        //...
        "Domain" : "dominio.url.com.uy",
        //...
    }
    //...
}
```

## Marca de uso del Middleware

En la clase `Startup` del proyecto, donde se define el esquema a ser usado por el GraphQL (en el metodo `ConfigureServices`); se modifica el _pipeline_ de ejecucion.

Para eso se debe usar el metodo `AddGraphQL` que recibe un `Action<IQueryExecutionBuilder>`, quedando el codigo de la siguiente manera

```csharp
public class Startup {
    //...
    public void ConfigureServices(IServiceCollection services) {
        // ...
        services.AddGraphQL(
                sp => SchemaBuilder.New()/*...*/.Create(),
                builder => builder
                    .Use<ImpersonatedMiddleware>() // Use de la Impersonalizacion
                    .UseDefaultPipeline() // continuar con el fluyo del middleware
            );
        // ...

    }
}
```

como este componente es dependiente, debe agregarse la inyeccion de la configuracion y el contexto en el mismo metodo `ConfigureServices` de la clase `Startup`

```csharp
public class Startup {
    //...
    public void ConfigureServices(IServiceCollection services) {
        //...
        services.AddSingleton<IConfiguration>(Configuration);
        services.AddHttpContextAccessor();
        //...
    }
    //...
}
```

## Notas

En caso de no enviarse la configuracion necesaria del token, el servicio no contu


