﻿using Common.Utils.Exceptions;

using Microsoft.AspNetCore.Http;

using System.Linq;

namespace Common.Utils.JWT
{
    public static class JwtIdentity
    {
        /// <summary>
        /// Obtener el Id del usuario actual a partir del token que se pasa por cabecera
        /// </summary>
        /// <param name="contextAccessor">Objeto que contiene la información del request</param>
        /// <returns>El Id del usuario autenticado</returns>
        public static long ObtenerUsuarioId(IHttpContextAccessor contextAccessor)
        {
            long usuarioId = long.Parse(contextAccessor.HttpContext.User.Claims.FirstOrDefault(x => x.Type == "id").Value);
            if (usuarioId <= 0)
            {
                throw new BusinessException("ID17");
            }

            return usuarioId;

        }
        public static string ObtenerLogin(IHttpContextAccessor contextAccessor)
        {
            string login = contextAccessor.HttpContext.User.Claims.FirstOrDefault(x => x.Type == "token1").Value;
            if (string.IsNullOrEmpty(login))
            {
                throw new BusinessException("ID17");
            }

            return login;

        }

        public static string ObtenerToken2(IHttpContextAccessor contextAccessor)
        {
            string token2 = contextAccessor.HttpContext.User.Claims.FirstOrDefault(x => x.Type == "token2").Value;
            if (string.IsNullOrEmpty(token2)) {
                throw new BusinessException("ID17");
            }

            return token2;
        }

        public static string ObtenerTokenSesion(IHttpContextAccessor contextAccessor)
        {
            string token = contextAccessor.HttpContext.Request.Headers.FirstOrDefault(x => x.Key == "Authorization").Value;
            if (string.IsNullOrEmpty(token)) {
                throw new BusinessException("ID17");
            }
            return token.Split(" ")[1];
            ;
        }

    }
}
