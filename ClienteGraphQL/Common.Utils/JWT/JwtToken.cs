﻿using Common.Utils.Exceptions;
using Common.Utils.Redirecccionamiento;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;

using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Common.Utils.JWT
{
    public class JwtToken
    {
        private readonly IConfiguration _configuration;

        public JwtToken()
        {
        }

        public JwtToken(IConfiguration configuration)
        {
            _configuration = configuration;

        }

        public async Task<string> RefrescarTokenAsync(string token, bool nonce, int intentos = 1)
        {

            // si el tiempo de expiración está en el pasado se refresca el token
            //if (token.ValidTo < DateTime.UtcNow && fechaExpiracion >= DateTime.Now)
            //{

            Calls cliente = new Calls(_configuration, token);

            string mutacion = @"
                mutation refrescarToken($nonce: boolean) {
                    refrescarToken(nonce: $nonce)
                }";
            Object parametros = new
            {
                nonce
            };
            try
            {
                return await cliente.MutationAsync<string, Object>(mutacion, "refrescarToken", parametros, "GraphAuth");

            }
            catch (BusinessException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                if (intentos < 3)
                {
                    return await RefrescarTokenAsync(token, nonce, ++intentos);
                }
                throw e;
            }

        }

        public async Task<string> CerrarSesionesAsync(string token)
        {

            Calls cliente = new Calls(_configuration, token);

            string mutacion = @"mutation cerrarSesionesInactivas {
                                                  cerrarSesionesInactivas
                                                }";
            string resultado = await cliente.MutationAsync<string, Object>(mutacion, "cerrarSesionesInactivas", new { token = "" }, "GraphAuth");
            return resultado;
            // string res = resultado.Result;

        }
        public IEnumerable<Claim> GetTokenClaims(string token)
        {

            var stream = token;
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadJwtToken(stream);

            //string nombre = jsonToken.Claims.Where(x => x.Type == "nombre").FirstOrDefault().Value;


            //jsonToken.ToString();

            //DatosToken datos = new DatosToken();
            return jsonToken.Claims;
        }

        public JwtSecurityToken GetTokenJWT(IHeaderDictionary headers)
        {
            if (headers.TryGetValue("Authorization", out StringValues headerValues))
            {
                string token = headerValues.ToString();
                token = token.Substring(token.IndexOf(' ') + 1);

                var handler = new JwtSecurityTokenHandler();
                var tokenS = handler.ReadToken(token) as JwtSecurityToken;
                return tokenS;
            }
            return null;
        }
    }
}
