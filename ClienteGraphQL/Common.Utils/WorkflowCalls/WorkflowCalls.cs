﻿using Common.Utils.Exceptions;
using Common.Utils.Models;
using Common.Utils.Redirecccionamiento;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Common.Utils.WorkflowCalls
{
    public class WorkflowCalls
    {
        private readonly IConfiguration _Configuration;
        private readonly string _token;
        public WorkflowCalls(IConfiguration configuration, string token = null)
        {
            _Configuration = configuration;
            _token = token;

        }

        public async Task<List<ProcesarActividad>> procesarActividadAsync(ProcessActivity processActivity)
        {

            List<ProcesarActividad> act = null;


            string token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJEQU5JRUwiLCJqdGkiOiIwM2FmNTQ1ZS1hMTU0LTQxZmEtYmI3Ni1jYjdhODBmMDQzYTUiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOlsiUHJjQWdyZWdhclByb2Nlc29FamVjdWNpb24iLCJQcmNCdXNjYXJQcm9jZXNvRWplY3VjaW9uIiwiUHJjQ29kaWdvRXNjcml0dXJhIiwiUHJjQ29kaWdvTGVjdHVyYSIsIlByY0NvZGlnb1ZlcnNpb25Fc2NyaXR1cmEiLCJQcmNDb2RpZ29WZXJzaW9uTGVjdHVyYSIsIlByY0RldGFsbGVQcm9jZXNvRWplY3VjaW9uIiwiUHJjTm9tYnJlTGVjdHVyYSIsIlByY1RpcG9MZWN0dXJhIiwiQXJlYUlkTGVjdHVyYSIsIkFyZWFOb21icmVMZWN0dXJhIiwiQXJlYUNvZGlnb0xlY3R1cmEiLCJBcmVhRXN0YWRvTGVjdHVyYSIsIkFyZWFEZXNjcmlwY2lvbkxlY3R1cmEiLCJBcmVhRmVjaGFDcmVhY2lvbkxlY3R1cmEiLCJBcmVhU2VjdG9yZXNMZWN0dXJhIiwiQXJlYVRhcmVhc0xlY3R1cmEiLCJBcmVhUmVzcG9uc2FibGVzTGVjdHVyYSIsIkFyZWFVbmlkYWRMZWN0dXJhIiwiQXJlYVNlY3RvcmVzTGVjdHVyYSIsIkNwb0lkTGVjdHVyYSIsIkNwb05vbWJyZUxlY3R1cmEiLCJDcG9EZXNjcmlwY2lvbkxlY3R1cmEiLCJDcG9GZWNoYUNyZWFjaW9uTGVjdHVyYSIsIkNwb05vbWJyZVRhYmxhTGVjdHVyYSIsIkNwb0NvbXBvbmVudGVzTGVjdHVyYSIsIkN0ZUlkTGVjdHVyYSIsIkN0ZUV0aXF1ZXRhTGVjdHVyYSIsIkN0ZURlc2NyaXBjaW9uTGVjdHVyYSIsIkN0ZVRhcmVhTGVjdHVyYSIsIkN0ZUNhbXBvTGVjdHVyYSIsIkNuZElkTGVjdHVyYSIsIkNuZE5vbWJyZUxlY3R1cmEiLCJDbmRDb2RpZ29MZWN0dXJhIiwiQ25kRmVjaGFDcmVhY2lvbkxlY3R1cmEiLCJDbmREZXNjcmlwY2lvbkxlY3R1cmEiLCJDbmREb2N1bWVudGFjaW9uTGVjdHVyYSIsIkNuZEVzdGFkb0xlY3R1cmEiLCJDbmRSZWdleHBMZWN0dXJhIiwiQ25kVGFyZWFMZWN0dXJhIiwiQ25kVHJhbnNpY2lvbmVzTGVjdHVyYSIsIkRwY0lkTGVjdHVyYSIsIkRwY05vbWJyZUxlY3R1cmEiLCJEcGNEZXNjcmlwY2lvbkxlY3R1cmEiLCJEcGNDb2RpZ29MZWN0dXJhIiwiRHBjRmVjaGFDcmVhY2lvbkxlY3R1cmEiLCJEcGNFc3RhZG9MZWN0dXJhIiwiRHBjUGFkcmVMZWN0dXJhIiwiRHBjRGVwZW5kZW5jaWFzTGVjdHVyYSIsIkRwY1VuaWRhZGVzTGVjdHVyYSIsIk1kbElkTGVjdHVyYSIsIk1kbE5vbWJyZUxlY3R1cmEiLCJNZGxEZXNjcmlwY2lvbkxlY3R1cmEiLCJNZGxFdGlxdWV0YUxlY3R1cmEiLCJNZGxGb25kb0xlY3R1cmEiLCJNZGxJY29ub0xlY3R1cmEiLCJNZGxVcmxMZWN0dXJhIiwiTWRsVmVudGFuYUxlY3R1cmEiLCJNZGxGZWNoYUNyZWFjaW9uTGVjdHVyYSIsIk1kbFZpc2libGVMZWN0dXJhIiwiTWRsRXN0YWRvTGVjdHVyYSIsIlBtdElkTGVjdHVyYSIsIlBtdE5vbWJyZUxlY3R1cmEiLCJQbXRWYWxvckxlY3R1cmEiLCJQbXRSZWdsYUxlY3R1cmEiLCJQbXRUaXBvUGFyYW1ldHJvTGVjdHVyYSIsIlBtc0lkTGVjdHVyYSIsIlBtc1NpZ25vTGVjdHVyYSIsIlBtc0ZlY2hhQ3JlYWNpb25MZWN0dXJhIiwiUGx0Y0lkTGVjdHVyYSIsIlBsdGNOb21icmVMZWN0dXJhIiwiUGx0Y0Rlc2NyaXBjaW9uTGVjdHVyYSIsIlBsdGNGZWNoYUNyZWFjaW9uTGVjdHVyYSIsIlBsdGNEb2N1bWVudGFjaW9uTGVjdHVyYSIsIlBsdGNFc3RhZG9MZWN0dXJhIiwiUHJjSWRMZWN0dXJhIiwiUHJjQ29kaWdvTGVjdHVyYSIsIlByY05vbWJyZUxlY3R1cmEiLCJQcmNEZXNjcmlwY2lvbkxlY3R1cmEiLCJQcmNGZWNoYUNyZWFjaW9uTGVjdHVyYSIsIlByY1ZlcnNpb25MZWN0dXJhIiwiUHJjQ2xhc2lmaWNhY2lvbkxlY3R1cmEiLCJQcmNQcmlvcmlkYWRMZWN0dXJhIiwiUHJjRHVyYWNpb25MZWN0dXJhIiwiUHJjQ29zdG9MZWN0dXJhIiwiUHJjRG9jdW1lbnRhY2lvbkxlY3R1cmEiLCJQcmNEZXNjcmlwY2lvblZlcnNpb25MZWN0dXJhIiwiUHJjRXN0YWRvTGVjdHVyYSIsIlByY1RpcG9MZWN0dXJhIiwiUHJjVGFyZWFzTGVjdHVyYSIsIlJ0cklkTGVjdHVyYSIsIlJ0ck5vbWJyZUxlY3R1cmEiLCJSdHJGb3JtdWxhTGVjdHVyYSIsIlJ0clRhcmVhTGVjdHVyYSIsIlJ0clBhcmFtZXRyb3NMZWN0dXJhIiwiUnRzSWRMZWN0dXJhIiwiUnRzTm9tYnJlTGVjdHVyYSIsIlJ0c0Rlc2NyaXBjaW9uTGVjdHVyYSIsIlJ0c0RvY3VtZW50YWNpb25MZWN0dXJhIiwiUnRzRXN0YWRvTGVjdHVyYSIsIlJ0c1RyYW5zaWNpb25lc0xlY3R1cmEiLCJSb2xJZExlY3R1cmEiLCJSb2xOb21icmVMZWN0dXJhIiwiUm9sRGVzY3JpcGNpb25MZWN0dXJhIiwiUm9sQ2xhc2lmaWNhY2lvbkxlY3R1cmEiLCJSb2xGZWNoYUNyZWFjaW9uTGVjdHVyYSIsIlJvbEVzdGFkb0xlY3R1cmEiLCJTY3RJZExlY3R1cmEiLCJTY3RDb2RpZ29MZWN0dXJhIiwiU2N0Tm9tYnJlTGVjdHVyYSIsIlNjdERlc2NyaXBjaW9uTGVjdHVyYSIsIlNjdEZlY2hhQ3JlYWNpb25MZWN0dXJhIiwiU2N0RXN0YWRvTGVjdHVyYSIsIlNjdFJlc3BvbnNhYmxlc0xlY3R1cmEiLCJTY3RBcmVhc0xlY3R1cmEiLCJUcmFJZExlY3R1cmEiLCJUcmFOb21icmVMZWN0dXJhIiwiVHJhRGVzY3JpcGNpb25MZWN0dXJhIiwiVHJhRmVjaGFDcmVhY2lvbkxlY3R1cmEiLCJUcmFDbGFzaWZpY2FjaW9uTGVjdHVyYSIsIlRyYVByaW9yaWRhZExlY3R1cmEiLCJUcmFFc0luaWNpYWxMZWN0dXJhIiwiVHJhRW5CYW5kZWphTGVjdHVyYSIsIlRyYUR1cmFjaW9uTGVjdHVyYSIsIlRyYUNvc3RvTGVjdHVyYSIsIlRyYURvY3VtZW50YWNpb25MZWN0dXJhIiwiVHJhQ29kaWdvTGVjdHVyYSIsIlRyYUVzdGFkb0xlY3R1cmEiLCJUcmFVcmxMZWN0dXJhIiwiVHJhSWNvbm9MZWN0dXJhIiwiVHJhUG9saXRpY2FMZWN0dXJhIiwiVHJhQ29uZGljaW9uZXNMZWN0dXJhIiwiVHJhUHJvY2Vzb0xlY3R1cmEiLCJUcmFBdXRvckxlY3R1cmEiLCJUcmFDb21wb25lbnRlc0xlY3R1cmEiLCJUcmFSZWdsYXNMZWN0dXJhIiwiVHB0b0lkTGVjdHVyYSIsIlRwdG9Ob21icmVMZWN0dXJhIiwiVHB0b1BhcmFtZXRyb3NMZWN0dXJhIiwiVHByY0lkTGVjdHVyYSIsIlRwcmNOb21icmVMZWN0dXJhIiwiVHByY0Rlc2NyaXBjaW9uTGVjdHVyYSIsIlRwcmNGZWNoYUNyZWFjaW9uTGVjdHVyYSIsIlRwcmNQcm9jZXNvc0xlY3R1cmEiLCJUcm5JZExlY3R1cmEiLCJUcm5Ob21icmVMZWN0dXJhIiwiVHJuRGVzY3JpcGNpb25MZWN0dXJhIiwiVHJuQ29kaWdvTGVjdHVyYSIsIlRybkZlY2hhQ3JlYWNpb25MZWN0dXJhIiwiVHJuRG9jdW1lbnRhY2lvbkxlY3R1cmEiLCJUcm5Fc3RhZG9MZWN0dXJhIiwiVHJuRW52aW9MZWN0dXJhIiwiVHJuUmVnbGFMZWN0dXJhIiwiVHJuQ29uZGljaW9uTGVjdHVyYSIsIlRyblRhcmVhTGVjdHVyYSIsIlRybkRlc3Rpbm9MZWN0dXJhIiwiVW5kSWRMZWN0dXJhIiwiVW5kQ29kaWdvTGVjdHVyYSIsIlVuZE5vbWJyZUxlY3R1cmEiLCJVbmREZXNjcmlwY2lvbkxlY3R1cmEiLCJVbmRGZWNoYUNyZWFjaW9uTGVjdHVyYSIsIlVuZEVzdGFkb0xlY3R1cmEiLCJVbmREZXBlbmRlbmNpYUxlY3R1cmEiLCJVbmRSZXNwb25zYWJsZXNMZWN0dXJhIiwiVW5kQXJlYXNMZWN0dXJhIiwiVXNySWRMZWN0dXJhIiwiVXNyQ2VkdWxhTGVjdHVyYSIsIlVzckxvZ2luTGVjdHVyYSIsIlVzclBhc3N3b3JkTGVjdHVyYSIsIlVzck5vbWJyZUxlY3R1cmEiLCJVc3JTZWd1bmRvTm9tYnJlTGVjdHVyYSIsIlVzckFwZWxsaWRvTGVjdHVyYSIsIlVzclNlZ3VuZG9BcGVsbGlkb0xlY3R1cmEiLCJVc3JSaWZMZWN0dXJhIiwiVXNyVGVsZlBlcnNvbmFsTGVjdHVyYSIsIlVzckVtYWlsTGVjdHVyYSIsIlVzckZheExlY3R1cmEiLCJVc3JEaXJEb21pY2lsaW9MZWN0dXJhIiwiVXNyRGlyVHJhYmFqb0xlY3R1cmEiLCJVc3JEZXNjcmlwY2lvbkxlY3R1cmEiLCJVc3JDbGFzaWZpY2FjaW9uTGVjdHVyYSIsIlVzckR1cmFjaW9uQ2xhdmVMZWN0dXJhIiwiVXNyRXN0YWRvTGVjdHVyYSIsIlVzck1vZHVsb3NMZWN0dXJhIiwiVXNyVGFibGVyb3NMZWN0dXJhIiwiVXNyUm9sZXNMZWN0dXJhIiwiVXNyRGVwZW5kZW5jaWFMZWN0dXJhIiwiVXNyUGVybWlzb3NMZWN0dXJhIl0sImV4cCI6MTYwMzQ2Nzg4MCwiaXNzIjoiaHR0cHM6Ly9sb2NhbGhvc3Q6NTAwMS8iLCJhdWQiOiJodHRwczovL2xvY2FsaG9zdDo1MDAxLyIsImlkIjo5LCJzZXNpb25JZCI6MTAwMDIsIm1vZHVsb0lkIjowLCJub21icmUiOiJEQU5JRUwiLCJhcGVsbGlkbyI6IlJVSVoifQ.T8UHhpUaniL71VUAaiU5VWuBBkF-3tbS0xjTgTMR0G8";

            Calls llamada = new Calls(_Configuration,token);

             ProcesarActividadResponse cadenaRespuesta;
             string query = @"mutation procesarActividad($actividadId: Long!,$condicion: String!,$transicionMensajes: [TransicionMensajeVMInput],$usuarioId: Long!) {
                                          procesarActividad(
                                          actividadId:$actividadId, 
                                          condicion: $condicion,
                                          usuarioId: $usuarioId,
                                          transicionMensajes: $transicionMensajes,
                                          ) {
                                            id
                                          }
                                       }";


            object parametro = new
            {

                actividadId = processActivity.ActividadId,
                condicion= processActivity.Condicion,
                transicionMensajes= processActivity.TransicionMensajes,
                usuarioId = processActivity.UsuarioId


            };
            string parametros = JsonConvert.SerializeObject(parametro);
          
            try
            {

                cadenaRespuesta = await llamada.MutationAsync<ProcesarActividadResponse, Object>(query, "", parametro, "Workflow-Backend");

            }
            catch (BusinessException e)
            {
                throw e;
            }






            //Deserialización de la respuesta para tratar los resultados

            if (cadenaRespuesta.procesarActividad != null)
            {
                act = cadenaRespuesta.procesarActividad;   
}
            else
                throw new BusinessException("ID7");


    
            return act; 
        }



    }
}
