﻿namespace Common.Utils.General
{
    public class DatosMensaje
    {
        public string Titulo { get; set; }
        public string Mensaje { get; set; }
        public string Correo { get; set; }      
        public string FechaRequest { get; set; }
        public string HoraRequest { get; set; }
        public string Telefono { get; set; }
       


    }
}
