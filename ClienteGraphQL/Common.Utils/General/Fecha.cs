﻿using Microsoft.Extensions.Configuration;

using System;

namespace Common.Utils.General
{
    public static class Fecha
    {
        public static bool EnRango(DateTime? baseInicio, DateTime? baseFin, DateTime inicio, DateTime fin)
        {
            if ((inicio >= baseInicio && inicio <= baseFin) || (fin >= baseInicio && fin <= baseFin))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Método para obtener la fecha actual agregándole la cantidad de minutos y remover los segundos
        /// </summary>
        /// <param name="minutos">Minutos a agregar a la hora actual</param>
        /// <returns>Tiempo sin segundos</returns>
        public static DateTime ObtenerExpiracionToken(IConfiguration config)
        {
            ;
            DateTime fecha = DateTime.UtcNow;

            //return fecha.Date.Add(new TimeSpan(fecha.Hour, fecha.Minute + 1 + minutos, 0));
            return fecha.AddMinutes(Convert.ToInt32(config["Jwt:TokenLife"]));
        }
        public static DateTime ObtenerExpiracionTokenCliente(IConfiguration config)
        {
            ;
            DateTime fecha = DateTime.UtcNow;

            //return fecha.Date.Add(new TimeSpan(fecha.Hour, fecha.Minute + 1 + minutos, 0));
            return fecha.AddMinutes(Convert.ToInt32(config["Jwt:TokenClientLife"]));
        }
        public static DateTime ObtenerExpiracionSesion(IConfiguration config)
        {
            ;
            DateTime fecha = DateTime.Now;

            //return fecha.Date.Add(new TimeSpan(fecha.Hour, fecha.Minute + 1 + minutos, 0));
            return fecha.AddMinutes(Convert.ToInt32(config["SessionLife"]));
        }

    }
}
