﻿using System;

namespace Common.Utils.Formats.String.Encode.Hexadecimal
{
    public static class StringHexExtensions
    {
        public static string GetHexString(this byte[] Value)
        {
            return BitConverter.ToString(Value);
        }
    }
}
