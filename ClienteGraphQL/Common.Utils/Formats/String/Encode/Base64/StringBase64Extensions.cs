﻿namespace Common.Utils.Formats.String.Encode.Base64
{
    public static class StringBase64Extensions
    {
        public static byte[] FromBase64ToByteArray(this string Value)
        {
            return System.Convert.FromBase64String(Value);
        }

        public static string GetBase64String(this byte[] Value)
        {
            return System.Convert.ToBase64String(Value);
        }
    }

}
