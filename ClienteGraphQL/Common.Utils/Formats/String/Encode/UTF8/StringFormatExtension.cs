﻿using System.Text.RegularExpressions;

namespace Common.Utils.Formats.String.Encode.UTF8
{

    /// <summary>
    /// Metodos de extension para formatos de strings
    /// </summary>
    public static class StringFormatExtension {

        /// <summary>
        /// Expresion regular para identicar no-letras (que no estan entre la 'a' y la 'z' o 'A' o 'Z')
        /// </summary>
        private static Regex _nonLetters = new Regex("[^a-zA-Z]");

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Value"></param>
        /// <returns>el valor original pero solo las letras entre la 'a' y la 'z' o entre la 'A' y 'Z'</returns>
        public static string OnlyLetters(this string Value) {

            // si es nulo vacio o espacios en blanco... pues... no tiene letras...
            if (string.IsNullOrWhiteSpace(Value)) {
                return null;
            }

            return _nonLetters.Replace(Value, string.Empty);

        }

    }

}
