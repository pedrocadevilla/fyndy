﻿using Common.Utils.Encryption.RSA;
using Common.Utils.Formats.String.Encode.Base64;
using System.Security.Cryptography;

namespace Common.Utils.Formats.String.Encode.UTF8.RSA
{
    public static class UTF8RSAEncryptExtensions
    {
        public static string FromUTF8ToRSABase64With(this string Value, RSACryptoServiceProvider ServiceProvider)
        {
            byte[] Value_ByteArray = Value.FromUTF8StringToByteArray();
            byte[] Value_Encrypted = ServiceProvider.Encrypt(Value_ByteArray, RSAUtils.DefaultDoOAEPPadding);
            string Value_toReturn = Value_Encrypted.GetBase64String();
            return Value_toReturn;
        }

        public static string FromRSABase64ToUTF8With(this string Value_Base64, RSACryptoServiceProvider ServiceProvider)
        {
            byte[] Value_ByteArray = Value_Base64.FromBase64ToByteArray();
            byte[] Value_Decrypted = ServiceProvider.Decrypt(Value_ByteArray, RSAUtils.DefaultDoOAEPPadding);
            string Value_toReturn = Value_Decrypted.GetUTF8String();
            return Value_toReturn;
        }
    }
}
