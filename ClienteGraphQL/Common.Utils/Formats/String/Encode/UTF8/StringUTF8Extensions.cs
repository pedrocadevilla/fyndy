﻿using System.Text;

namespace Common.Utils.Formats.String.Encode.UTF8
{
    public static class StringUTF8Extensions
    {
        public static byte[] FromUTF8StringToByteArray(this string Value)
        {
            return Encoding.UTF8.GetBytes(Value);
        }

        public static string GetUTF8String(this byte[] Value)
        {
            return Encoding.UTF8.GetString(Value);
        }
    }
}
