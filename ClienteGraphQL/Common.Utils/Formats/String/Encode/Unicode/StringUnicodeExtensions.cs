﻿using System.Text;

namespace Common.Utils.Formats.String.Encode.Unicode
{
    public static class StringUnicodeExtensions
    {
        public static byte[] FromUnicodeStringToByteArray(this string Value)
        {
            return new UnicodeEncoding().GetBytes(Value);
        }

        public static string GetUnicodeString(this byte[] Value)
        {
            return new UnicodeEncoding().GetString(Value);
        }
    }
}
