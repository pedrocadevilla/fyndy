﻿using Common.Utils.Encryption.RSA;
using Common.Utils.Formats.String.Encode.Base64;
using System.Security.Cryptography;

namespace Common.Utils.Formats.String.Encode.Unicode.RSA
{
    public static class UnicodeRSAEncryptExtensions
    {
        public static string FromUnicodeToRSABase64With(this string Value, RSACryptoServiceProvider ServiceProvider)
        {
            byte[] Value_ByteArray = Value.FromUnicodeStringToByteArray();
            byte[] Value_Encrypted = ServiceProvider.Encrypt(Value_ByteArray, RSAUtils.DefaultDoOAEPPadding);
            string Value_toReturn = Value_Encrypted.GetBase64String();
            return Value_toReturn;
        }

        public static string FromRSABase64ToUnicodeWith(this string Value_Base64, RSACryptoServiceProvider ServiceProvider)
        {
            byte[] Value_ByteArray = Value_Base64.FromBase64ToByteArray();
            byte[] Value_Decrypted = ServiceProvider.Encrypt(Value_ByteArray, RSAUtils.DefaultDoOAEPPadding);
            string Value_toReturn = Value_Decrypted.GetUnicodeString();
            return Value_toReturn;
        }
    }
}
