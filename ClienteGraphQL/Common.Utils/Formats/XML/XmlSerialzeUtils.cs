﻿using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Common.Utils.Formats.XML
{
    public static class XmlSerialzeUtils
    {

        public static T GetObject<T>(this string Xml)
        {
            XDocument AuxiliarDocument = XDocument.Parse(Xml, LoadOptions.None);
                        
            XmlSerializer Deserializer = new XmlSerializer(typeof(T));
            XmlReader reader = AuxiliarDocument.Root.CreateReader();
            T NewInstance = (T)Deserializer.Deserialize(reader);

            return NewInstance;
        }

        public static string GetXmlString<T>(this T Object)
        {
            XmlSerializerNamespaces Namespaces = new XmlSerializerNamespaces();
            XmlWriterSettings Settings = new XmlWriterSettings() { OmitXmlDeclaration = true, Indent = true };

            StringBuilder sb = new StringBuilder();
            XmlWriter xmlwriter = XmlWriter.Create(sb, Settings);

            XmlSerializer Serializer = new XmlSerializer(typeof(T));

            Serializer.Serialize(
                    xmlwriter,
                    Object,
                    Namespaces
                );

            return sb.ToString();
        }

        public static bool IsXmlString(this string Value) {

            Stream stream = new MemoryStream(Encoding.UTF8.GetBytes(Value));
            
            XmlReaderSettings settings = new XmlReaderSettings {
                CheckCharacters = true,
                ConformanceLevel = ConformanceLevel.Document,
                DtdProcessing = DtdProcessing.Ignore,
                IgnoreComments = true,
                IgnoreProcessingInstructions = true,
                IgnoreWhitespace = true,
                ValidationFlags = XmlSchemaValidationFlags.None,
                ValidationType = ValidationType.None,
            };
            bool isValid;

            using (XmlReader xmlReader = XmlReader.Create(stream, settings)) {
                try {
                    while (xmlReader.Read()) {
                        ; // This space intentionally left blank
                    }
                    isValid = true;
                } catch (XmlException) {
                    isValid = false;
                }
            }
            return isValid;
            
        }

    }
}
