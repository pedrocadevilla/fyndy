﻿using System.Threading.Tasks;

using Cliente.Core;
using Cliente.Core.Repositories;
using Cliente.Persistence.Repositories;
using Microsoft.Extensions.Configuration;

namespace Cliente.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly FYNDDYContext _Context;
        private readonly IConfiguration _Configuration;

        public IAccionRepository Accion { get; private set; }
        public IActividadEconomicaRepository ActividadEconomica { get; private set; }
        public IActividadesRepository Actividades { get; private set; }
        public IActividadesPersonaRepository ActividadesPersona { get; private set; }
        public ICategoriaRepository Categoria { get; private set; }
        public ICategoriaEmpresaRepository CategoriaEmpresa { get; private set; }
        public ICiudadRepository Ciudad { get; private set; }
        public ICodigoPostalRepository CodigoPostal { get; private set; }
        public IComentariosEmpresaRepository ComentariosEmpresa { get; private set; }
        public IDatosContactoRepository DatosContacto { get; private set; }
        public IDireccionRepository Direccion { get; private set; }
        public IEmpresaRepository Empresa { get; private set; }
        public IEstadisticasRepository Estadisticas { get; private set; }
        public IEstadisticasCategoriasRepository EstadisticasCategorias { get; private set; }
        public IEstadisticasEmpresaRepository EstadisticasEmpresa { get; private set; }
        public IEstadisticasProductoRepository EstadisticasProducto { get; private set; }
        public IEstadisticasProductoEmpresaRepository EstadisticasProductoEmpresa { get; private set; }
        public IEstadisticasRubroRepository EstadisticasRubro { get; private set; }
        public IEstadisticasRubroEmpresaRepository EstadisticasRubroEmpresa { get; private set; }
        public IEstadoCivilRepository EstadoCivil { get; private set; }
        public IGeneroRepository Genero { get; private set; }
        public IHorarioRepository Horario { get; private set; }
        public IHorarioContactoRepository HorarioContacto { get; private set; }
        public IImagenesEmpresaRepository ImagenesEmpresa { get; private set; }
        public ILocalidadRepository Localidad { get; private set; }
        public IPaisRepository Pais { get; private set; }
        public IPalabrasRepository Palabras { get; private set; }
        public IParametrosRepository Parametros { get; private set; }
        public IParametrosAccionRepository ParametrosAccion { get; private set; }
        public IPermutacionesRepository Permutaciones { get; private set; }
        public IPermutacionesProductoEmpresaRepository PermutacionesProductoEmpresa { get; private set; }
        public IPlanEmpresaRepository PlanEmpresa { get; private set; }
        public IPlanesRepository Planes { get; private set; }
        public IProductoRepository Producto { get; private set; }
        public IProductoEmpresaRepository ProductoEmpresa { get; private set; }
        public IRedSocialRepository RedSocial { get; private set; }
        public IRedSocialEmpresaRepository RedSocialEmpresa { get; private set; }
        public IRedSocialUsuarioRepository RedSocialUsuario { get; private set; }
        public IRubroRepository Rubro { get; private set; }
        public IRubroEmpresaRepository RubroEmpresa { get; private set; }
        public ISesionRepository Sesion { get; private set; }
        public ITipoEmpresaRepository TipoEmpresa { get; private set; }
        public ITipoPlanRepository TipoPlan { get; private set; }
        public ITipoUsuarioRepository TipoUsuario { get; private set; }
        public IUsuarioRepository Usuario { get; private set; }
        public IVisitaRepository Visita { get; private set; }

        public UnitOfWork(FYNDDYContext Context){
            _Context = Context;

            Accion = new AccionRepository(_Context);
            ActividadEconomica = new ActividadEconomicaRepository(_Context);
            Actividades = new ActividadesRepository(_Context);
            ActividadesPersona = new ActividadesPersonaRepository(_Context);
            Categoria = new CategoriaRepository(_Context);
            CategoriaEmpresa = new CategoriaEmpresaRepository(_Context);
            Ciudad = new CiudadRepository(_Context);
            CodigoPostal = new CodigoPostalRepository(_Context);
            ComentariosEmpresa = new ComentariosEmpresaRepository(_Context);
            DatosContacto = new DatosContactoRepository(_Context);
            Direccion = new DireccionRepository(_Context);
            Empresa = new EmpresaRepository(_Context);
            Estadisticas = new EstadisticasRepository(_Context);
            EstadisticasCategorias = new EstadisticasCategoriasRepository(_Context);
            EstadisticasEmpresa = new EstadisticasEmpresaRepository(_Context);
            EstadisticasProducto = new EstadisticasProductoRepository(_Context);
            EstadisticasProductoEmpresa = new EstadisticasProductoEmpresaRepository(_Context);
            EstadisticasRubro = new EstadisticasRubroRepository(_Context);
            EstadisticasRubroEmpresa = new EstadisticasRubroEmpresaRepository(_Context);
            EstadoCivil = new EstadoCivilRepository(_Context);
            Genero = new GeneroRepository(_Context);
            Horario = new HorarioRepository(_Context);
            HorarioContacto = new HorarioContactoRepository(_Context);
            ImagenesEmpresa = new ImagenesEmpresaRepository(_Context);
            Localidad = new LocalidadRepository(_Context);
            Pais = new PaisRepository(_Context);
            Palabras = new PalabrasRepository(_Context);
            Parametros = new ParametrosRepository(_Context);
            ParametrosAccion = new ParametrosAccionRepository(_Context);
            Permutaciones = new PermutacionesRepository(_Context);
            PermutacionesProductoEmpresa = new PermutacionesProductoEmpresaRepository(_Context);
            PlanEmpresa = new PlanEmpresaRepository(_Context);
            Planes = new PlanesRepository(_Context);
            Producto = new ProductoRepository(_Context);
            ProductoEmpresa = new ProductoEmpresaRepository(_Context, _Configuration);
            RedSocial = new RedSocialRepository(_Context);
            RedSocialEmpresa = new RedSocialEmpresaRepository(_Context);
            RedSocialUsuario = new RedSocialUsuarioRepository(_Context);
            Rubro = new RubroRepository(_Context);
            RubroEmpresa = new RubroEmpresaRepository(_Context);
            Sesion = new SesionRepository(_Context);
            TipoEmpresa = new TipoEmpresaRepository(_Context);
            TipoPlan = new TipoPlanRepository(_Context);
            TipoUsuario = new TipoUsuarioRepository(_Context);
            Usuario = new UsuarioRepository(_Context);
            Visita = new VisitaRepository(_Context);
        }

        public async Task<int> Complete()
        {
            return await _Context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _Context.Dispose();
        }
    }
}
