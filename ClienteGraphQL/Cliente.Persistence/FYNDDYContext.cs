﻿using Cliente.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace Cliente.Persistence
{
    public partial class FYNDDYContext : DbContext
    {
        public FYNDDYContext()
        {
        }

        public FYNDDYContext(DbContextOptions<FYNDDYContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Accion> Accion { get; set; }
        public virtual DbSet<ActividadEconomica> ActividadEconomica { get; set; }
        public virtual DbSet<Actividades> Actividades { get; set; }
        public virtual DbSet<ActividadesEconomicasPersona> ActividadesEconomicasPersona { get; set; }
        public virtual DbSet<ActividadesPersona> ActividadesPersona { get; set; }
        public virtual DbSet<Categoria> Categoria { get; set; }
        public virtual DbSet<CategoriaEmpresa> CategoriaEmpresa { get; set; }
        public virtual DbSet<Ciudad> Ciudad { get; set; }
        public virtual DbSet<CodigoPostal> CodigoPostal { get; set; }
        public virtual DbSet<ComentariosEmpresa> ComentariosEmpresa { get; set; }
        public virtual DbSet<CondicionSocial> CondicionSocial { get; set; }
        public virtual DbSet<DatosContacto> DatosContacto { get; set; }
        public virtual DbSet<Direccion> Direccion { get; set; }
        public virtual DbSet<Empresa> Empresa { get; set; }
        public virtual DbSet<Estadisticas> Estadisticas { get; set; }
        public virtual DbSet<EstadisticasCategorias> EstadisticasCategorias { get; set; }
        public virtual DbSet<EstadisticasCategoriasEmpresa> EstadisticasCategoriasEmpresa { get; set; }
        public virtual DbSet<EstadisticasEmpresa> EstadisticasEmpresa { get; set; }
        public virtual DbSet<EstadisticasProducto> EstadisticasProducto { get; set; }
        public virtual DbSet<EstadisticasProductoEmpresa> EstadisticasProductoEmpresa { get; set; }
        public virtual DbSet<EstadisticasRubro> EstadisticasRubro { get; set; }
        public virtual DbSet<EstadisticasRubroEmpresa> EstadisticasRubroEmpresa { get; set; }
        public virtual DbSet<EstadoCivil> EstadoCivil { get; set; }
        public virtual DbSet<Genero> Genero { get; set; }
        public virtual DbSet<Horario> Horario { get; set; }
        public virtual DbSet<HorarioContacto> HorarioContacto { get; set; }
        public virtual DbSet<ImagenesEmpresa> ImagenesEmpresa { get; set; }
        public virtual DbSet<Localidad> Localidad { get; set; }
        public virtual DbSet<Pais> Pais { get; set; }
        public virtual DbSet<Palabras> Palabras { get; set; }
        public virtual DbSet<Parametros> Parametros { get; set; }
        public virtual DbSet<ParametrosAccion> ParametrosAccion { get; set; }
        public virtual DbSet<Permutaciones> Permutaciones { get; set; }
        public virtual DbSet<PermutacionesProductoEmpresa> PermutacionesProductoEmpresa { get; set; }
        public virtual DbSet<PlanEmpresa> PlanEmpresa { get; set; }
        public virtual DbSet<Planes> Planes { get; set; }
        public virtual DbSet<Producto> Producto { get; set; }
        public virtual DbSet<ProductoEmpresa> ProductoEmpresa { get; set; }
        public virtual DbSet<RedSocial> RedSocial { get; set; }
        public virtual DbSet<RedSocialEmpresa> RedSocialEmpresa { get; set; }
        public virtual DbSet<RedSocialUsuario> RedSocialUsuario { get; set; }
        public virtual DbSet<Rubro> Rubro { get; set; }
        public virtual DbSet<RubroEmpresa> RubroEmpresa { get; set; }
        public virtual DbSet<Sesion> Sesion { get; set; }
        public virtual DbSet<TipoEmpresa> TipoEmpresa { get; set; }
        public virtual DbSet<TipoPlan> TipoPlan { get; set; }
        public virtual DbSet<TipoUsuario> TipoUsuario { get; set; }
        public virtual DbSet<Usuario> Usuario { get; set; }
        public virtual DbSet<Visita> Visita { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=tcp:fynddy.database.windows.net,1433;Initial Catalog=FYNDDY;Persist Security Info=False;User ID=spyglass;Password=fyndy2020.;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            }
            optionsBuilder.UseLazyLoadingProxies().UseSqlServer("Server=tcp:fynddy.database.windows.net,1433;Initial Catalog=FYNDDY;Persist Security Info=False;User ID=spyglass;Password=fyndy2020.;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Accion>(entity =>
            {
                entity.Property(e => e.Descripcion).IsUnicode(false);

                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<ActividadEconomica>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<Actividades>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<ActividadesEconomicasPersona>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.ActividadEconomica)
                    .WithMany(p => p.ActividadesEconomicasPersona)
                    .HasForeignKey(d => d.ActividadEconomicaId)
                    .HasConstraintName("FK_REFERENCE_67");

                entity.HasOne(d => d.Usuario)
                    .WithMany(p => p.ActividadesEconomicasPersona)
                    .HasForeignKey(d => d.UsuarioId)
                    .HasConstraintName("FK_REFERENCE_68");
            });

            modelBuilder.Entity<ActividadesPersona>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.Actividad)
                    .WithMany(p => p.ActividadesPersona)
                    .HasForeignKey(d => d.ActividadId)
                    .HasConstraintName("FK_REFERENCE_70");

                entity.HasOne(d => d.Usuario)
                    .WithMany(p => p.ActividadesPersona)
                    .HasForeignKey(d => d.UsuarioId)
                    .HasConstraintName("FK_REFERENCE_69");
            });

            modelBuilder.Entity<Categoria>(entity =>
            {
                entity.Property(e => e.Nombre).IsUnicode(false);

                entity.Property(e => e.Usuario).IsUnicode(false);
            });

            modelBuilder.Entity<CategoriaEmpresa>(entity =>
            {
                entity.HasOne(d => d.Categoria)
                    .WithMany(p => p.CategoriaEmpresa)
                    .HasForeignKey(d => d.CategoriaId)
                    .HasConstraintName("FK_REFERENCE_29");

                entity.HasOne(d => d.Empresa)
                    .WithMany(p => p.CategoriaEmpresa)
                    .HasForeignKey(d => d.EmpresaId)
                    .HasConstraintName("FK_REFERENCE_28");
            });

            modelBuilder.Entity<Ciudad>(entity =>
            {
                entity.Property(e => e.Nombre).IsUnicode(false);

                entity.HasOne(d => d.Localidad)
                    .WithMany(p => p.Ciudad)
                    .HasForeignKey(d => d.LocalidadId)
                    .HasConstraintName("FK_REFERENCE_24");
            });

            modelBuilder.Entity<CodigoPostal>(entity =>
            {
                entity.Property(e => e.Nombre).IsUnicode(false);

                entity.HasOne(d => d.Ciudad)
                    .WithMany(p => p.CodigoPostal)
                    .HasForeignKey(d => d.CiudadId)
                    .HasConstraintName("FK_REFERENCE_26");
            });

            modelBuilder.Entity<ComentariosEmpresa>(entity =>
            {
                entity.Property(e => e.Comentarios).IsUnicode(false);

                entity.HasOne(d => d.Empresa)
                    .WithMany(p => p.ComentariosEmpresa)
                    .HasForeignKey(d => d.EmpresaId)
                    .HasConstraintName("FK_REFERENCE_46");

                entity.HasOne(d => d.TipoUsuario)
                    .WithMany(p => p.ComentariosEmpresa)
                    .HasForeignKey(d => d.TipoUsuarioId)
                    .HasConstraintName("FK_REFERENCE_76");

                entity.HasOne(d => d.Usuario)
                    .WithMany(p => p.ComentariosEmpresa)
                    .HasForeignKey(d => d.UsuarioId)
                    .HasConstraintName("FK_REFERENCE_72");
            });

            modelBuilder.Entity<CondicionSocial>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<DatosContacto>(entity =>
            {
                entity.Property(e => e.Contacto).IsUnicode(false);

                entity.HasOne(d => d.Empresa)
                    .WithMany(p => p.DatosContacto)
                    .HasForeignKey(d => d.EmpresaId)
                    .HasConstraintName("FK_REFERENCE_53");
            });

            modelBuilder.Entity<Direccion>(entity =>
            {
                entity.Property(e => e.InformacionAdicional).IsUnicode(false);

                entity.HasOne(d => d.Ciudad)
                    .WithMany(p => p.Direccion)
                    .HasForeignKey(d => d.CiudadId)
                    .HasConstraintName("FK_REFERENCE_51");

                entity.HasOne(d => d.CodigoPostal)
                    .WithMany(p => p.Direccion)
                    .HasForeignKey(d => d.CodigoPostalId)
                    .HasConstraintName("FK_REFERENCE_17");

                entity.HasOne(d => d.Empresa)
                    .WithMany(p => p.Direccion)
                    .HasForeignKey(d => d.EmpresaId)
                    .HasConstraintName("FK_REFERENCE_27");

                entity.HasOne(d => d.Localidad)
                    .WithMany(p => p.Direccion)
                    .HasForeignKey(d => d.LocalidadId)
                    .HasConstraintName("FK_REFERENCE_50");

                entity.HasOne(d => d.Pais)
                    .WithMany(p => p.Direccion)
                    .HasForeignKey(d => d.PaisId)
                    .HasConstraintName("FK_REFERENCE_49");
            });

            modelBuilder.Entity<Empresa>(entity =>
            {
                entity.Property(e => e.Contraseña).IsUnicode(false);

                entity.Property(e => e.Correo).IsUnicode(false);

                entity.Property(e => e.Descripcion).IsUnicode(false);

                entity.Property(e => e.FotoPerfil).IsUnicode(false);

                entity.Property(e => e.Latitud).IsUnicode(false);

                entity.Property(e => e.Longuitud).IsUnicode(false);

                entity.Property(e => e.Nombre).IsUnicode(false);

                entity.Property(e => e.PalabrasClave).IsUnicode(false);

                entity.HasOne(d => d.TipoEmpresa)
                    .WithMany(p => p.Empresa)
                    .HasForeignKey(d => d.TipoEmpresaId)
                    .HasConstraintName("FK_REFERENCE_71");
            });

            modelBuilder.Entity<Estadisticas>(entity =>
            {
                entity.Property(e => e.Descripcion).IsUnicode(false);

                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<EstadisticasCategorias>(entity =>
            {
                entity.Property(e => e.Valor).IsUnicode(false);

                entity.HasOne(d => d.Categoria)
                    .WithMany(p => p.EstadisticasCategorias)
                    .HasForeignKey(d => d.CategoriaId)
                    .HasConstraintName("FK_REFERENCE_40");

                entity.HasOne(d => d.Estadistica)
                    .WithMany(p => p.EstadisticasCategorias)
                    .HasForeignKey(d => d.EstadisticaId)
                    .HasConstraintName("FK_REFERENCE_57");
            });

            modelBuilder.Entity<EstadisticasCategoriasEmpresa>(entity =>
            {
                entity.Property(e => e.Valor).IsUnicode(false);

                entity.HasOne(d => d.Categoria)
                    .WithMany(p => p.EstadisticasCategoriasEmpresa)
                    .HasForeignKey(d => d.CategoriaId)
                    .HasConstraintName("FK_REFERENCE_43");

                entity.HasOne(d => d.Estadistica)
                    .WithMany(p => p.EstadisticasCategoriasEmpresa)
                    .HasForeignKey(d => d.EstadisticaId)
                    .HasConstraintName("FK_REFERENCE_56");
            });

            modelBuilder.Entity<EstadisticasEmpresa>(entity =>
            {
                entity.Property(e => e.Valor).IsUnicode(false);

                entity.HasOne(d => d.Empresa)
                    .WithMany(p => p.EstadisticasEmpresa)
                    .HasForeignKey(d => d.EmpresaId)
                    .HasConstraintName("FK_REFERENCE_54");

                entity.HasOne(d => d.Estadistica)
                    .WithMany(p => p.EstadisticasEmpresa)
                    .HasForeignKey(d => d.EstadisticaId)
                    .HasConstraintName("FK_REFERENCE_55");
            });

            modelBuilder.Entity<EstadisticasProducto>(entity =>
            {
                entity.Property(e => e.Valor).IsUnicode(false);

                entity.HasOne(d => d.Estadistica)
                    .WithMany(p => p.EstadisticasProducto)
                    .HasForeignKey(d => d.EstadisticaId)
                    .HasConstraintName("FK_REFERENCE_59");

                entity.HasOne(d => d.Producto)
                    .WithMany(p => p.EstadisticasProducto)
                    .HasForeignKey(d => d.ProductoId)
                    .HasConstraintName("FK_REFERENCE_42");
            });

            modelBuilder.Entity<EstadisticasProductoEmpresa>(entity =>
            {
                entity.Property(e => e.Valor).IsUnicode(false);

                entity.HasOne(d => d.Estadistica)
                    .WithMany(p => p.EstadisticasProductoEmpresa)
                    .HasForeignKey(d => d.EstadisticaId)
                    .HasConstraintName("FK_REFERENCE_60");

                entity.HasOne(d => d.Producto)
                    .WithMany(p => p.EstadisticasProductoEmpresa)
                    .HasForeignKey(d => d.ProductoId)
                    .HasConstraintName("FK_REFERENCE_44");
            });

            modelBuilder.Entity<EstadisticasRubro>(entity =>
            {
                entity.Property(e => e.Valor).IsUnicode(false);

                entity.HasOne(d => d.Estadistica)
                    .WithMany(p => p.EstadisticasRubro)
                    .HasForeignKey(d => d.EstadisticaId)
                    .HasConstraintName("FK_REFERENCE_58");

                entity.HasOne(d => d.Rubro)
                    .WithMany(p => p.EstadisticasRubro)
                    .HasForeignKey(d => d.RubroId)
                    .HasConstraintName("FK_REFERENCE_41");
            });

            modelBuilder.Entity<EstadisticasRubroEmpresa>(entity =>
            {
                entity.Property(e => e.Valor).IsUnicode(false);

                entity.HasOne(d => d.Estadistica)
                    .WithMany(p => p.EstadisticasRubroEmpresa)
                    .HasForeignKey(d => d.EstadisticaId)
                    .HasConstraintName("FK_REFERENCE_61");

                entity.HasOne(d => d.Rubro)
                    .WithMany(p => p.EstadisticasRubroEmpresa)
                    .HasForeignKey(d => d.RubroId)
                    .HasConstraintName("FK_REFERENCE_45");
            });

            modelBuilder.Entity<EstadoCivil>(entity =>
            {
                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<Genero>(entity =>
            {
                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<Horario>(entity =>
            {
                entity.Property(e => e.Horario1).IsUnicode(false);
            });

            modelBuilder.Entity<HorarioContacto>(entity =>
            {
                entity.HasOne(d => d.Empresa)
                    .WithMany(p => p.HorarioContacto)
                    .HasForeignKey(d => d.EmpresaId)
                    .HasConstraintName("FK_REFERENCE_52");

                entity.HasOne(d => d.Horario)
                    .WithMany(p => p.HorarioContacto)
                    .HasForeignKey(d => d.HorarioId)
                    .HasConstraintName("FK_REFERENCE_86");
            });

            modelBuilder.Entity<ImagenesEmpresa>(entity =>
            {
                entity.Property(e => e.Imagen).IsUnicode(false);

                entity.HasOne(d => d.Empresa)
                    .WithMany(p => p.ImagenesEmpresa)
                    .HasForeignKey(d => d.EmpresaId)
                    .HasConstraintName("FK_REFERENCE_48");
            });

            modelBuilder.Entity<Localidad>(entity =>
            {
                entity.Property(e => e.Nombre).IsUnicode(false);

                entity.HasOne(d => d.Pais)
                    .WithMany(p => p.Localidad)
                    .HasForeignKey(d => d.PaisId)
                    .HasConstraintName("FK_REFERENCE_25");
            });

            modelBuilder.Entity<Pais>(entity =>
            {
                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<Palabras>(entity =>
            {
                entity.Property(e => e.Palabra).IsUnicode(false);

                entity.HasOne(d => d.Empresa)
                    .WithMany(p => p.Palabras)
                    .HasForeignKey(d => d.EmpresaId)
                    .HasConstraintName("FK_REFERENCE_75");

                entity.HasOne(d => d.Producto)
                    .WithMany(p => p.Palabras)
                    .HasForeignKey(d => d.ProductoId)
                    .HasConstraintName("FK_REFERENCE_74");
            });

            modelBuilder.Entity<Parametros>(entity =>
            {
                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<ParametrosAccion>(entity =>
            {
                entity.Property(e => e.Valor).IsUnicode(false);

                entity.HasOne(d => d.Accion)
                    .WithMany(p => p.ParametrosAccion)
                    .HasForeignKey(d => d.AccionId)
                    .HasConstraintName("FK_REFERENCE_88");

                entity.HasOne(d => d.Parametros)
                    .WithMany(p => p.ParametrosAccion)
                    .HasForeignKey(d => d.ParametrosId)
                    .HasConstraintName("FK_REFERENCE_89");
            });

            modelBuilder.Entity<Permutaciones>(entity =>
            {
                entity.Property(e => e.Frase).IsUnicode(false);
            });

            modelBuilder.Entity<PermutacionesProductoEmpresa>(entity =>
            {
                entity.HasOne(d => d.Empresa)
                    .WithMany(p => p.PermutacionesProductoEmpresa)
                    .HasForeignKey(d => d.EmpresaId)
                    .HasConstraintName("FK_REFERENCE_47");

                entity.HasOne(d => d.Permutacion)
                    .WithMany(p => p.PermutacionesProductoEmpresa)
                    .HasForeignKey(d => d.PermutacionId)
                    .HasConstraintName("FK_REFERENCE_37");

                entity.HasOne(d => d.Producto)
                    .WithMany(p => p.PermutacionesProductoEmpresa)
                    .HasForeignKey(d => d.ProductoId)
                    .HasConstraintName("FK_REFERENCE_38");
            });

            modelBuilder.Entity<PlanEmpresa>(entity =>
            {
                entity.HasOne(d => d.Empresa)
                    .WithMany(p => p.PlanEmpresa)
                    .HasForeignKey(d => d.EmpresaId)
                    .HasConstraintName("FK_REFERENCE_36");

                entity.HasOne(d => d.Plan)
                    .WithMany(p => p.PlanEmpresa)
                    .HasForeignKey(d => d.PlanId)
                    .HasConstraintName("FK_REFERENCE_35");
            });

            modelBuilder.Entity<Planes>(entity =>
            {
                entity.Property(e => e.Nombre).IsUnicode(false);

                entity.HasOne(d => d.TipoPlan)
                    .WithMany(p => p.Planes)
                    .HasForeignKey(d => d.TipoPlanId)
                    .HasConstraintName("FK_REFERENCE_39");
            });

            modelBuilder.Entity<Producto>(entity =>
            {
                entity.Property(e => e.Nombre).IsUnicode(false);

                entity.Property(e => e.PalabrasClave).IsUnicode(false);

                entity.Property(e => e.Usuario).IsUnicode(false);

                entity.HasOne(d => d.Rubro)
                    .WithMany(p => p.Producto)
                    .HasForeignKey(d => d.RubroId)
                    .HasConstraintName("FK_REFERENCE_22");
            });

            modelBuilder.Entity<ProductoEmpresa>(entity =>
            {
                entity.HasOne(d => d.Empresa)
                    .WithMany(p => p.ProductoEmpresa)
                    .HasForeignKey(d => d.EmpresaId)
                    .HasConstraintName("FK_REFERENCE_34");

                entity.HasOne(d => d.Producto)
                    .WithMany(p => p.ProductoEmpresa)
                    .HasForeignKey(d => d.ProductoId)
                    .HasConstraintName("FK_REFERENCE_33");

                entity.HasOne(d => d.RubroEmpresa)
                    .WithMany(p => p.ProductoEmpresa)
                    .HasForeignKey(d => d.RubroEmpresaId)
                    .HasConstraintName("FK_REFERENCE_32");
            });

            modelBuilder.Entity<RedSocial>(entity =>
            {
                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<RedSocialEmpresa>(entity =>
            {
                entity.HasOne(d => d.Empresa)
                    .WithMany(p => p.RedSocialEmpresa)
                    .HasForeignKey(d => d.EmpresaId)
                    .HasConstraintName("FK_REFERENCE_6");

                entity.HasOne(d => d.RedSocial)
                    .WithMany(p => p.RedSocialEmpresa)
                    .HasForeignKey(d => d.RedSocialId)
                    .HasConstraintName("FK_REFERENCE_5");
            });

            modelBuilder.Entity<RedSocialUsuario>(entity =>
            {
                entity.Property(e => e.Link).IsUnicode(false);

                entity.HasOne(d => d.RedSocial)
                    .WithMany(p => p.RedSocialUsuario)
                    .HasForeignKey(d => d.RedSocialId)
                    .HasConstraintName("FK_REFERENCE_4");

                entity.HasOne(d => d.Usuario)
                    .WithMany(p => p.RedSocialUsuario)
                    .HasForeignKey(d => d.UsuarioId)
                    .HasConstraintName("FK_REFERENCE_3");
            });

            modelBuilder.Entity<Rubro>(entity =>
            {
                entity.Property(e => e.Nombre).IsUnicode(false);

                entity.Property(e => e.Usuario).IsUnicode(false);

                entity.HasOne(d => d.Categoria)
                    .WithMany(p => p.Rubro)
                    .HasForeignKey(d => d.CategoriaId)
                    .HasConstraintName("FK_REFERENCE_21");
            });

            modelBuilder.Entity<RubroEmpresa>(entity =>
            {
                entity.HasOne(d => d.CategoriaEmpresa)
                    .WithMany(p => p.RubroEmpresa)
                    .HasForeignKey(d => d.CategoriaEmpresaId)
                    .HasConstraintName("FK_REFERENCE_30");

                entity.HasOne(d => d.Rubro)
                    .WithMany(p => p.RubroEmpresa)
                    .HasForeignKey(d => d.RubroId)
                    .HasConstraintName("FK_REFERENCE_31");
            });

            modelBuilder.Entity<Sesion>(entity =>
            {
                entity.Property(e => e.Token).IsUnicode(false);

                entity.HasOne(d => d.Empresa)
                    .WithMany(p => p.Sesion)
                    .HasForeignKey(d => d.EmpresaId)
                    .HasConstraintName("FK_REFERENCE_78");

                entity.HasOne(d => d.Usuario)
                    .WithMany(p => p.Sesion)
                    .HasForeignKey(d => d.UsuarioId)
                    .HasConstraintName("FK_REFERENCE_77");
            });

            modelBuilder.Entity<TipoEmpresa>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<TipoPlan>(entity =>
            {
                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<TipoUsuario>(entity =>
            {
                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.Property(e => e.Contraseña).IsUnicode(false);

                entity.Property(e => e.Correo).IsUnicode(false);

                entity.Property(e => e.FotoPerfil).IsUnicode(false);

                entity.Property(e => e.PrimerApellido).IsUnicode(false);

                entity.Property(e => e.PrimerNombre).IsUnicode(false);

                entity.Property(e => e.SegundoApellido).IsUnicode(false);

                entity.Property(e => e.SegundoNombre).IsUnicode(false);

                entity.HasOne(d => d.Ciudad)
                    .WithMany(p => p.Usuario)
                    .HasForeignKey(d => d.CiudadId)
                    .HasConstraintName("FK_REFERENCE_65");

                entity.HasOne(d => d.CodigoPostal)
                    .WithMany(p => p.Usuario)
                    .HasForeignKey(d => d.CodigoPostalId)
                    .HasConstraintName("FK_REFERENCE_66");

                entity.HasOne(d => d.CondicionSocial)
                    .WithMany(p => p.Usuario)
                    .HasForeignKey(d => d.CondicionSocialId)
                    .HasConstraintName("FK_REFERENCE_62");

                entity.HasOne(d => d.EstadoCivil)
                    .WithMany(p => p.Usuario)
                    .HasForeignKey(d => d.EstadoCivilId)
                    .HasConstraintName("FK_REFERENCE_2");

                entity.HasOne(d => d.Genero)
                    .WithMany(p => p.Usuario)
                    .HasForeignKey(d => d.GeneroId)
                    .HasConstraintName("FK_REFERENCE_1");

                entity.HasOne(d => d.Localidad)
                    .WithMany(p => p.Usuario)
                    .HasForeignKey(d => d.LocalidadId)
                    .HasConstraintName("FK_REFERENCE_64");

                entity.HasOne(d => d.Pais)
                    .WithMany(p => p.Usuario)
                    .HasForeignKey(d => d.PaisId)
                    .HasConstraintName("FK_REFERENCE_63");
            });

            modelBuilder.Entity<Visita>(entity =>
            {
                entity.HasOne(d => d.Accion)
                    .WithMany(p => p.Visita)
                    .HasForeignKey(d => d.AccionId)
                    .HasConstraintName("FK_REFERENCE_87");

                entity.HasOne(d => d.Ciudad)
                    .WithMany(p => p.Visita)
                    .HasForeignKey(d => d.CiudadId)
                    .HasConstraintName("FK_REFERENCE_85");

                entity.HasOne(d => d.Empresa)
                    .WithMany(p => p.Visita)
                    .HasForeignKey(d => d.EmpresaId)
                    .HasConstraintName("FK_REFERENCE_81");

                entity.HasOne(d => d.Localidad)
                    .WithMany(p => p.Visita)
                    .HasForeignKey(d => d.LocalidadId)
                    .HasConstraintName("FK_REFERENCE_84");

                entity.HasOne(d => d.Pais)
                    .WithMany(p => p.Visita)
                    .HasForeignKey(d => d.PaisId)
                    .HasConstraintName("FK_REFERENCE_83");

                entity.HasOne(d => d.Usuario)
                    .WithMany(p => p.Visita)
                    .HasForeignKey(d => d.UsuarioId)
                    .HasConstraintName("FK_REFERENCE_80");
            });
        }
    }
}
