﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace Cliente.Persistence.Repositories
{
    class HorarioContactoRepository : Repository<HorarioContacto>, IHorarioContactoRepository
    {
        public HorarioContactoRepository(DbContext context) : base(context)
        { }

        public FYNDDYContext FYNDDYContext
        {
            get { return Context as FYNDDYContext; }
        }

        public async Task<IEnumerable<HorarioContacto>> ObtenerHorariosXEliminar(long empresaId, long[] horarioId)
        {
            IEnumerable<HorarioContacto> horarios = await FYNDDYContext.HorarioContacto
                .Where(x => !horarioId.Contains(x.HorarioId ?? default(long)))
                .ToListAsync();
            return horarios;
        }

        public async Task<HorarioContacto> ObtenerXIdHorario(long empresaId, long horarioId)
        {
            HorarioContacto horario = await FYNDDYContext.HorarioContacto
                .Where(x => x.EmpresaId == empresaId && x.HorarioId == horarioId)
                .FirstOrDefaultAsync();
            return horario;
        }
    }
}