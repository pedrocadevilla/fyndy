﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;

namespace Cliente.Persistence.Repositories
{
    class PaisRepository : Repository<Pais>, IPaisRepository
    {
        public PaisRepository(DbContext context) : base(context)
        { }
    }
}