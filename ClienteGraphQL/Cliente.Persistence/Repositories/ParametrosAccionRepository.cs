﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;

namespace Cliente.Persistence.Repositories
{
    class ParametrosAccionRepository : Repository<ParametrosAccion>, IParametrosAccionRepository
    {
        public ParametrosAccionRepository(DbContext context) : base(context)
        { }
    }
}