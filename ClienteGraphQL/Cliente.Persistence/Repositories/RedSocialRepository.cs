﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;

namespace Cliente.Persistence.Repositories
{
    class RedSocialRepository : Repository<RedSocial>, IRedSocialRepository
    {
        public RedSocialRepository(DbContext context) : base(context)
        { }
    }
}