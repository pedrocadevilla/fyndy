﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;

namespace Cliente.Persistence.Repositories
{
    class EstadisticasRubroRepository : Repository<EstadisticasRubro>, IEstadisticasRubroRepository
    {
        public EstadisticasRubroRepository(DbContext context) : base(context)
        { }
    }
}