﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using Cliente.Core.Clases;
using Microsoft.Extensions.Configuration;

namespace Cliente.Persistence.Repositories
{
    class ProductoEmpresaRepository : Repository<ProductoEmpresa>, IProductoEmpresaRepository
    {
        public ProductoEmpresaRepository(DbContext context, IConfiguration _configuration) : base(context)
        {
            Config = _configuration;
        }

        public FYNDDYContext FYNDDYContext
        {
            get { return Context as FYNDDYContext; }
        }

        public IConfiguration Config { get; private set; }

        public async Task<List<ProductoEmpresa>> busquedaProductos(List<ElementoEncontrado> Elementos, long? filtroId, long? tipoFiltroId)
        {
            List<ProductoEmpresa> lista = new List<ProductoEmpresa>();
            foreach (ElementoEncontrado elemento in Elementos)
            {
                if(elemento.Empresa != null)
                {
                    ProductoEmpresa Empresa = null;
                    Empresa EmpresaSinProducto = null;
                    if (filtroId != 0)
                    {
                        if(tipoFiltroId == 1)
                        {
                            Empresa = await FYNDDYContext.ProductoEmpresa
                            .Where(x => x.EmpresaId == elemento.Empresa && x.Empresa.Direccion.FirstOrDefault().PaisId == filtroId)
                            .FirstOrDefaultAsync();
                        }
                        if (tipoFiltroId == 2)
                        {
                            Empresa = await FYNDDYContext.ProductoEmpresa
                            .Where(x => x.EmpresaId == elemento.Empresa && x.Empresa.Direccion.FirstOrDefault().LocalidadId == filtroId)
                            .FirstOrDefaultAsync();
                        }
                        if (tipoFiltroId == 3)
                        {
                            Empresa = await FYNDDYContext.ProductoEmpresa
                            .Where(x => x.EmpresaId == elemento.Empresa && x.Empresa.Direccion.FirstOrDefault().CiudadId == filtroId)
                            .FirstOrDefaultAsync();
                        }
                    }
                    else
                    {
                        Empresa = await FYNDDYContext.ProductoEmpresa
                        .Where(x => x.EmpresaId == elemento.Empresa)
                        .FirstOrDefaultAsync();
                    }
                    if(Empresa != null)
                    {
                        Empresa.ProductoId = null;
                        Empresa.Producto = null;
                        lista.Add(Empresa);
                    }
                    else
                    {
                        if (filtroId != null)
                        {
                            if (tipoFiltroId == 1)
                            {
                                EmpresaSinProducto = await FYNDDYContext.Empresa
                                .Where(x => x.Id == elemento.Empresa && x.Direccion.FirstOrDefault().PaisId == filtroId)
                                .FirstOrDefaultAsync();
                            }
                            if (tipoFiltroId == 2)
                            {
                                EmpresaSinProducto = await FYNDDYContext.Empresa
                                .Where(x => x.Id == elemento.Empresa && x.Direccion.FirstOrDefault().LocalidadId == filtroId)
                                .FirstOrDefaultAsync();
                            }
                            if (tipoFiltroId == 3)
                            {
                                EmpresaSinProducto = await FYNDDYContext.Empresa
                                .Where(x => x.Id == elemento.Empresa && x.Direccion.FirstOrDefault().CiudadId == filtroId)
                                .FirstOrDefaultAsync();
                            }
                        }
                        else
                        {
                            EmpresaSinProducto = await FYNDDYContext.Empresa
                            .Where(x => x.Id == elemento.Empresa)
                            .FirstOrDefaultAsync();
                        }
                        if (EmpresaSinProducto != null)
                        {
                            Empresa = new ProductoEmpresa();
                            Empresa.Empresa = EmpresaSinProducto;
                            Empresa.EmpresaId = EmpresaSinProducto.Id;
                            Empresa.ProductoId = null;
                            Empresa.Producto = null;
                            lista.Add(Empresa);
                        }
                    }
                }
                else
                {
                    List<ProductoEmpresa> listaProducto = null;
                    if (filtroId != 0)
                    {
                        if (tipoFiltroId == 1)
                        {
                            listaProducto = await FYNDDYContext.ProductoEmpresa
                            .Where(x => x.ProductoId == elemento.Producto && x.Empresa.Direccion.FirstOrDefault().PaisId == filtroId)
                            .OrderBy(x => x.Producto.Nombre)
                            .ToListAsync();
                        }
                        if (tipoFiltroId == 2)
                        {
                            listaProducto = await FYNDDYContext.ProductoEmpresa
                            .Where(x => x.ProductoId == elemento.Producto && x.Empresa.Direccion.FirstOrDefault().LocalidadId == filtroId)
                            .OrderBy(x => x.Producto.Nombre)
                            .ToListAsync();
                        }
                        if (tipoFiltroId == 3)
                        {
                            listaProducto = await FYNDDYContext.ProductoEmpresa
                            .Where(x => x.ProductoId == elemento.Producto && x.Empresa.Direccion.FirstOrDefault().CiudadId == filtroId)
                            .OrderBy(x => x.Producto.Nombre)
                            .ToListAsync();
                        }
                        
                    }
                    else
                    {
                        listaProducto = await FYNDDYContext.ProductoEmpresa
                        .Where(x => x.ProductoId == elemento.Producto)
                        .OrderBy(x => x.Producto.Nombre)
                        .ToListAsync();
                    }
                    lista.AddRange(listaProducto);
                }
            }
            return lista.Distinct().ToList();
        }

        public async Task<List<ProductoEmpresa>> busquedaProductosXCategoriaAsync(List<long> rubros)
        {
            List<ProductoEmpresa> listaProducto = await FYNDDYContext.ProductoEmpresa
            .Where(x => rubros.Contains(x.RubroEmpresaId ?? -1))
            .OrderBy(x => x.Producto.Nombre)
            .ToListAsync();
            return listaProducto;
        }

        public async Task<List<ProductoEmpresa>> busquedaProductosXEmpresaAsync(long empresaId)
        {
            Estadisticas estadisticaProductos = await FYNDDYContext.Estadisticas
                .Where(x => x.Nombre == Config.GetSection("EstadisticaNombre:BusquedaProducto").Get<string>())
                .FirstOrDefaultAsync();
            List<EstadisticasProductoEmpresa> categoriaEstadistica = await FYNDDYContext.EstadisticasProductoEmpresa
                .Where(x => x.EstadisticaId == estadisticaProductos.Id)
                .OrderByDescending(x => x.Valor)
                .Take(3)
                .ToListAsync();
            IEnumerable<long?> listaProducto = categoriaEstadistica.Select(x => x.ProductoId).ToList();
            List<ProductoEmpresa> productos = await FYNDDYContext.ProductoEmpresa
                .Where(x => x.EmpresaId == empresaId && listaProducto.Contains(x.ProductoId))
                .OrderBy(x => x.Producto.Nombre)
                .ToListAsync();
            return productos;
        }
    }
}