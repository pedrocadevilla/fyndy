﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;

namespace Cliente.Persistence.Repositories
{
    class PlanesRepository : Repository<Planes>, IPlanesRepository
    {
        public PlanesRepository(DbContext context) : base(context)
        { }
    }
}