﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;

namespace Cliente.Persistence.Repositories
{
    class PlanEmpresaRepository : Repository<PlanEmpresa>, IPlanEmpresaRepository
    {
        public PlanEmpresaRepository(DbContext context) : base(context)
        { }
    }
}