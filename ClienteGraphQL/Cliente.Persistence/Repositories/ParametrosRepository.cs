﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;

namespace Cliente.Persistence.Repositories
{
    class ParametrosRepository : Repository<Parametros>, IParametrosRepository
    {
        public ParametrosRepository(DbContext context) : base(context)
        { }
    }
}