﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;

namespace Cliente.Persistence.Repositories
{
    class TipoPlanRepository : Repository<TipoPlan>, ITipoPlanRepository
    {
        public TipoPlanRepository(DbContext context) : base(context)
        { }
    }
}