﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;
using System.Threading.Tasks;
using System.Linq;

namespace Cliente.Persistence.Repositories
{
    class EstadisticasEmpresaRepository : Repository<EstadisticasEmpresa>, IEstadisticasEmpresaRepository
    {
        public EstadisticasEmpresaRepository(DbContext context) : base(context)
        { }

        public FYNDDYContext FYNDDYContext
        {
            get { return Context as FYNDDYContext; }
        }

        public async Task<EstadisticasEmpresa> obtenerEstadisticaEmpresaXId(long empresaId, long estadisticaId)
        {
            EstadisticasEmpresa categoriaEStadistica = await FYNDDYContext.EstadisticasEmpresa
                .Where(x => x.EstadisticaId == estadisticaId && x.EmpresaId == empresaId)
                .FirstOrDefaultAsync();
            return categoriaEStadistica;
        }
    }
}