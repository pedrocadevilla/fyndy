﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;

namespace Cliente.Persistence.Repositories
{
    class ActividadesPersonaRepository : Repository<ActividadesPersona>, IActividadesPersonaRepository
    {
        public ActividadesPersonaRepository(DbContext context) : base(context)
        { }
    }
}