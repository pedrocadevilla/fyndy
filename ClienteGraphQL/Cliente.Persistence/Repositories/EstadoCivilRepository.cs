﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Common.Utils.Repository;
using Cliente.Core.Repositories;

namespace Cliente.Persistence.Repositories
{
    class EstadoCivilRepository : Repository<EstadoCivil>, IEstadoCivilRepository
    {
        public EstadoCivilRepository(DbContext context) : base(context)
        { }

    }
}