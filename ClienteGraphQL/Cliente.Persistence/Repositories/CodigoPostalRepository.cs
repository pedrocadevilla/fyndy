﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;

namespace Cliente.Persistence.Repositories
{
    class CodigoPostalRepository : Repository<CodigoPostal>, ICodigoPostalRepository
    {
        public CodigoPostalRepository(DbContext context) : base(context)
        { }
    }
}