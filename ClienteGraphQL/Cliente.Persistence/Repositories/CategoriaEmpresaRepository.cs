﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;

namespace Cliente.Persistence.Repositories
{
    class CategoriaEmpresaRepository : Repository<CategoriaEmpresa>, ICategoriaEmpresaRepository
    {
        public CategoriaEmpresaRepository(DbContext context) : base(context)
        { }

        public FYNDDYContext FYNDDYContext
        {
            get { return Context as FYNDDYContext; }
        }

        public async Task<CategoriaEmpresa> obtenerCategoriaEmpresaAsync(long? categoriaEmpresa, long? empresaId)
        {
            var categoria = await FYNDDYContext.CategoriaEmpresa
                .Where(x => x.CategoriaId == categoriaEmpresa && x.EmpresaId == empresaId)
                .FirstOrDefaultAsync();
            return categoria;
        }

        public async Task<List<CategoriaEmpresa>> obtenerListaCategoriaEmpresaAsync(long? categoriaEmpresa)
        {
            var categoria = await FYNDDYContext.CategoriaEmpresa
                .Where(x => x.CategoriaId == categoriaEmpresa)
                .ToListAsync();
            return categoria;
        }
    }
}