﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;

namespace Cliente.Persistence.Repositories
{
    class CiudadRepository : Repository<Ciudad>, ICiudadRepository
    {
        public CiudadRepository(DbContext context) : base(context)
        { }
    }
}