﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;

namespace Cliente.Persistence.Repositories
{
    class ActividadesRepository : Repository<Actividades>, IActividadesRepository
    {
        public ActividadesRepository(DbContext context) : base(context)
        { }
    }
}