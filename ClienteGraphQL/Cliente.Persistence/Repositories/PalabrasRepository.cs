﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace Cliente.Persistence.Repositories
{
    class PalabrasRepository : Repository<Palabras>, IPalabrasRepository
    {
        public PalabrasRepository(DbContext context) : base(context)
        { }

        public FYNDDYContext FYNDDYContext
        {
            get { return Context as FYNDDYContext; }
        }

        public async Task<List<Palabras>> BuscarPalabrasProducto()
        {
            List<Palabras> palabras = await FYNDDYContext.Palabras
                .Where(x => x.ProductoId != null)
                .ToListAsync();
            return palabras;
        }

        public async Task<List<Palabras>> BuscarPalabrasEmpresa()
        {
            List<Palabras> palabras = await FYNDDYContext.Palabras
                .Where(x => x.EmpresaId != null)
                .ToListAsync();
            return palabras;
        }
    }
}