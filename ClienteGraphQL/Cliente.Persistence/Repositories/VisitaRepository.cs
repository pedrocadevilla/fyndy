﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;

namespace Cliente.Persistence.Repositories
{
    class VisitaRepository : Repository<Visita>, IVisitaRepository
    {
        public VisitaRepository(DbContext context) : base(context)
        { }
    }
}