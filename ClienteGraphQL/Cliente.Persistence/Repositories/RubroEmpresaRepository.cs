﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;

namespace Cliente.Persistence.Repositories
{
    class RubroEmpresaRepository : Repository<RubroEmpresa>, IRubroEmpresaRepository
    {
        public RubroEmpresaRepository(DbContext context) : base(context)
        { }

        public FYNDDYContext FYNDDYContext
        {
            get { return Context as FYNDDYContext; }
        }

        public async Task<RubroEmpresa> ObtenerRubroEmpresaAsync(long? rubroId, long? categoriaId)
        {
            var rubro = await FYNDDYContext.RubroEmpresa
                .Where(x => x.RubroId == rubroId && x.CategoriaEmpresaId == categoriaId)
                .FirstOrDefaultAsync();
            return rubro;
        }

        public async Task<List<RubroEmpresa>> ObtenerListaRubroEmpresaAsync(List<long> lista)
        {
            var rubro = await FYNDDYContext.RubroEmpresa
                .Where(x => lista.Contains(x.CategoriaEmpresaId ?? -1))
                .ToListAsync();
            return rubro;
        }
    }
}