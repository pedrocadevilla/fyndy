﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;
using System.Threading.Tasks;
using System.Linq;
using Common.Utils.Exceptions;

namespace Cliente.Persistence.Repositories
{
    class UsuarioRepository : Repository<Usuario>, IUsuarioRepository
    {
        public UsuarioRepository(DbContext context) : base(context)
        { }

        public FYNDDYContext FYNDDYContext
        {
            get { return Context as FYNDDYContext; }
        }

        public async Task<Usuario> inicioSesion(string correo, string password)
        {
            var usuario = await FYNDDYContext.Usuario
                .Where(x => x.Correo == correo && x.Contraseña == password)
                .FirstOrDefaultAsync();
            if(usuario != null)
            {
                return usuario;
            }

            return null;
        }

        public async Task<Usuario> usuarioExiste(string correo)
        {
            var usuario = await FYNDDYContext.Usuario
                .Where(x => x.Correo == correo)
                .FirstOrDefaultAsync();
            if (usuario != null)
            {
                throw new BusinessException("1");
            }

            return null;
        }
    }
}