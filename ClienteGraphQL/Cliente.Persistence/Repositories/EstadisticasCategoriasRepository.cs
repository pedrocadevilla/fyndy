﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace Cliente.Persistence.Repositories
{
    class EstadisticasCategoriasRepository : Repository<EstadisticasCategorias>, IEstadisticasCategoriasRepository
    {
        public EstadisticasCategoriasRepository(DbContext context) : base(context)
        { }

        public FYNDDYContext FYNDDYContext
        {
            get { return Context as FYNDDYContext; }
        }

        public async Task<List<Categoria>> obtenerPrincipalesCategoriasAsync()
        {
            Estadisticas estadistica = await FYNDDYContext.Estadisticas
                .Where(x => x.Nombre == "Busqueda Categoria")
                .FirstOrDefaultAsync();
            List<EstadisticasCategorias> categoriaEStadistica = await FYNDDYContext.EstadisticasCategorias
                .Where(x => x.EstadisticaId == estadistica.Id)
                .OrderByDescending(x => int.Parse(x.Valor))
                .ToListAsync();
            List<Categoria> lista = new List<Categoria>();
            for (int i = 0; i < 8; i++)
            {
                Categoria cat = await FYNDDYContext.Categoria.Where(x => x.Id == categoriaEStadistica[i].CategoriaId).FirstOrDefaultAsync();
                lista.Add(cat);
            }
            return lista;
        }

        public async Task<EstadisticasCategorias> obtenerEstadisticaCategoriaXId(long categoriaId, long estadisticaId)
        {
            EstadisticasCategorias categoriaEStadistica = await FYNDDYContext.EstadisticasCategorias
                .Where(x => x.EstadisticaId == estadisticaId && x.CategoriaId == categoriaId)
                .FirstOrDefaultAsync();
            return categoriaEStadistica;
        }
    }
}