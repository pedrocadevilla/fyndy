﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;
using System.Threading.Tasks;
using System.Linq;

namespace Cliente.Persistence.Repositories
{
    class TipoUsuarioRepository : Repository<TipoUsuario>, ITipoUsuarioRepository
    {
        public TipoUsuarioRepository(DbContext context) : base(context)
        { }

        public FYNDDYContext FYNDDYContext
        {
            get { return Context as FYNDDYContext; }
        }

        public async Task<TipoUsuario> ObtenerTipoUsuario(string nombre)
        {
            var tipoUsuario = await FYNDDYContext.TipoUsuario
                .Where(x => x.Nombre == nombre)
                .FirstOrDefaultAsync();
            return tipoUsuario;
        }
    }
}