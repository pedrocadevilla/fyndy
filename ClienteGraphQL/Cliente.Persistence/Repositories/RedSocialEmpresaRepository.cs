﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;

namespace Cliente.Persistence.Repositories
{
    class RedSocialEmpresaRepository : Repository<RedSocialEmpresa>, IRedSocialEmpresaRepository
    {
        public RedSocialEmpresaRepository(DbContext context) : base(context)
        { }
    }
}