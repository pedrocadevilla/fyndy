﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;
using System.Threading.Tasks;
using System.Linq;
using Common.Utils.Exceptions;
using System.Collections.Generic;

namespace Cliente.Persistence.Repositories
{
    class EmpresaRepository : Repository<Empresa>, IEmpresaRepository
    {
        public EmpresaRepository(DbContext context) : base(context)
        { }

        public FYNDDYContext FYNDDYContext
        {
            get { return Context as FYNDDYContext; }
        }

        public async Task<Empresa> inicioSesion(string correo, string password)
        {
            var empresa = await FYNDDYContext.Empresa
                .Where(x => x.Correo == correo && x.Contraseña == password)
                .FirstOrDefaultAsync();

            if (empresa != null)
            {
                return empresa;
            }

            return null;
        }

        public async Task<Empresa> empresaExiste(string correo)
        {
            var Empresa = await FYNDDYContext.Empresa
                .Where(x => x.Correo == correo)
                .FirstOrDefaultAsync();
            if (Empresa != null)
            {
                throw new BusinessException("1");
            }

            return null;
        }

        public async Task<List<Empresa>> principalesEmpresasAsync()
        {
            List<Empresa> IdsEmpresa = await FYNDDYContext.Empresa
                .OrderBy(x => x.PonderacionTotal)
                .Take(3)
                .ToListAsync();

            return IdsEmpresa;
        }
    }
}