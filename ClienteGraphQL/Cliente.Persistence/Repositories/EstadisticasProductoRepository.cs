﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;

namespace Cliente.Persistence.Repositories
{
    class EstadisticasProductoRepository : Repository<EstadisticasProducto>, IEstadisticasProductoRepository
    {
        public EstadisticasProductoRepository(DbContext context) : base(context)
        { }
    }
}