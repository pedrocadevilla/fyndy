﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;

namespace Cliente.Persistence.Repositories
{
    class ActividadEconomicaRepository : Repository<ActividadEconomica>, IActividadEconomicaRepository
    {
        public ActividadEconomicaRepository(DbContext context) : base(context)
        { }
    }
}