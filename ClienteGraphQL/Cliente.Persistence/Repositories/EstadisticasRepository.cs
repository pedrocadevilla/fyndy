﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;
using System.Threading.Tasks;
using System.Linq;

namespace Cliente.Persistence.Repositories
{
    class EstadisticasRepository : Repository<Estadisticas>, IEstadisticasRepository
    {
        public EstadisticasRepository(DbContext context) : base(context)
        { }

        public FYNDDYContext FYNDDYContext
        {
            get { return Context as FYNDDYContext; }
        }

        public async Task<long> obtenerEstatadisticaXNombre(string nombre)
        {
            Estadisticas estadistica = await FYNDDYContext.Estadisticas
                .Where(x => x.Nombre == nombre)
                .FirstOrDefaultAsync();
            return estadistica.Id;
        }
    }
}