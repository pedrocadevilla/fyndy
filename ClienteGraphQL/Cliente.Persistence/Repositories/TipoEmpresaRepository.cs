﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;

namespace Cliente.Persistence.Repositories
{
    class TipoEmpresaRepository : Repository<TipoEmpresa>, ITipoEmpresaRepository
    {
        public TipoEmpresaRepository(DbContext context) : base(context)
        { }
    }
}