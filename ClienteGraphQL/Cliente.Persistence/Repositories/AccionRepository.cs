﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;

namespace Cliente.Persistence.Repositories
{
    class AccionRepository : Repository<Accion>, IAccionRepository
    {
        public AccionRepository(DbContext context) : base(context)
        { }
    }
}