﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;

namespace Cliente.Persistence.Repositories
{
    class DireccionRepository : Repository<Direccion>, IDireccionRepository
    {
        public DireccionRepository(DbContext context) : base(context)
        { }
    }
}