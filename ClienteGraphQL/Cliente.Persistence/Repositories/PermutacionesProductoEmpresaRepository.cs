﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;

namespace Cliente.Persistence.Repositories
{
    class PermutacionesProductoEmpresaRepository : Repository<PermutacionesProductoEmpresa>, IPermutacionesProductoEmpresaRepository
    {
        public PermutacionesProductoEmpresaRepository(DbContext context) : base(context)
        { }

        public FYNDDYContext FYNDDYContext
        {
            get { return Context as FYNDDYContext; }
        }
    }
}