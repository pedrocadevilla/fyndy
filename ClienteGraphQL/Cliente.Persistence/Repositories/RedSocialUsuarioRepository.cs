﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;

namespace Cliente.Persistence.Repositories
{
    class RedSocialUsuarioRepository : Repository<RedSocialUsuario>, IRedSocialUsuarioRepository
    {
        public RedSocialUsuarioRepository(DbContext context) : base(context)
        { }
    }
}