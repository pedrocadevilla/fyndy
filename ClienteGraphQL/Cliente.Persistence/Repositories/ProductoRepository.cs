﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;

namespace Cliente.Persistence.Repositories
{
    class ProductoRepository : Repository<Producto>, IProductoRepository
    {
        public ProductoRepository(DbContext context) : base(context)
        { }
    }
}