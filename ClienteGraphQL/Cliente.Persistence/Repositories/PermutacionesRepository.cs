﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using Cliente.Core.Clases;

namespace Cliente.Persistence.Repositories
{
    class PermutacionesRepository : Repository<Permutaciones>, IPermutacionesRepository
    {
        public PermutacionesRepository(DbContext context) : base(context)
        { }

        public FYNDDYContext FYNDDYContext
        {
            get { return Context as FYNDDYContext; }
        }

        public async Task<bool> ExistePermutacion(string Frase)
        {
            var existe = await FYNDDYContext.Permutaciones
                .Where(x => x.Frase == Frase)
                .FirstOrDefaultAsync();

            if (existe != null)
            {
                return true;
            }
            return false;
        }

        public async Task<long> IdPermutacion(string Frase)
        {
            var permutacion = await FYNDDYContext.Permutaciones
                .Where(x => x.Frase == Frase)
                .FirstOrDefaultAsync();

            return permutacion.Id;
        }
    
        public async Task<List<long>> IdPermutacionBusqueda(List<string> Palabras)
        {
            int longuitud = Palabras.Count();
            List<long> listaPermutaciones = new List<long>();
            foreach (string p in Palabras)
            {
                Permutaciones permutacion = await FYNDDYContext.Permutaciones
                .Where(x => x.Frase == p)
                .FirstOrDefaultAsync();

                if (permutacion != null)
                {
                    listaPermutaciones.Add(permutacion.Id);
                }
                else
                {
                    permutacion = await FYNDDYContext.Permutaciones
                    .Where(x => x.Frase.Contains(p))
                    .FirstOrDefaultAsync();

                    if (permutacion != null)
                    {
                        listaPermutaciones.Add(permutacion.Id);
                    }
                }
            }

            return listaPermutaciones;
        }


        public async Task<List<ElementoEncontrado>> busquedaProductosId(List<long> Permutaciones)
        {
            List<ElementoEncontrado> ListadoId = new List<ElementoEncontrado>();
            ElementoEncontrado elemento = new ElementoEncontrado(null, null);
            foreach (long per in Permutaciones)
            {
                List<PermutacionesProductoEmpresa> IdsEmpresa = await FYNDDYContext.PermutacionesProductoEmpresa
                .Where(x => x.PermutacionId == per && x.EmpresaId != null)
                .ToListAsync();
                if (IdsEmpresa.Count > 0) {
                    List<ElementoEncontrado> Empresas = IdsEmpresa.Select(x => new ElementoEncontrado(null, x.EmpresaId)).ToList();
                    ListadoId.AddRange(Empresas);
                }
                List<PermutacionesProductoEmpresa> IdsProducto = await FYNDDYContext.PermutacionesProductoEmpresa
                .Where(x => x.PermutacionId == per && x.ProductoId != null)
                .ToListAsync();
                if (IdsProducto.Count > 0)
                {
                    List<ElementoEncontrado> Productos = IdsProducto.Select(x => new ElementoEncontrado(x.ProductoId, null)).ToList();
                    ListadoId.AddRange(Productos);
                }
            }
            return ListadoId.Distinct().ToList();
        }
    }
}