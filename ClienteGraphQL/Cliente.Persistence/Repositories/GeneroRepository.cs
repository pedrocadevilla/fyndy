﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Common.Utils.Repository;
using Cliente.Core.Repositories;

namespace Cliente.Persistence.Repositories
{
    class GeneroRepository : Repository<Genero>, IGeneroRepository
    {
        public GeneroRepository(DbContext context) : base(context)
        { }
    }
}