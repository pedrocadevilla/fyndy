﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace Cliente.Persistence.Repositories
{
    class ComentariosEmpresaRepository : Repository<ComentariosEmpresa>, IComentariosEmpresaRepository
    {
        public ComentariosEmpresaRepository(DbContext context) : base(context)
        { }

        public FYNDDYContext FYNDDYContext
        {
            get { return Context as FYNDDYContext; }
        }

        public async Task<List<ComentariosEmpresa>> ObtenerComentariosEmpresaXId(long id)
        {
            List<ComentariosEmpresa> comentarios = await FYNDDYContext.ComentariosEmpresa
                .Where(x => x.FechaEliminacion == null && x.EmpresaId == id)
                .OrderByDescending(x => x.Fecha)
                .ToListAsync();
            return comentarios;
        }
    }
}