﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;

namespace Cliente.Persistence.Repositories
{
    class DatosContactoRepository : Repository<DatosContacto>, IDatosContactoRepository
    {
        public DatosContactoRepository(DbContext context) : base(context)
        { }
    }
}