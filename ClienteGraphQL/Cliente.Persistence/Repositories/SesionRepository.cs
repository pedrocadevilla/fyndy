﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;

namespace Cliente.Persistence.Repositories
{
    class SesionRepository : Repository<Sesion>, ISesionRepository
    {
        public SesionRepository(DbContext context) : base(context)
        { }
    }
}