﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;

namespace Cliente.Persistence.Repositories
{
    class RubroRepository : Repository<Rubro>, IRubroRepository
    {
        public RubroRepository(DbContext context) : base(context)
        { }
    }
}