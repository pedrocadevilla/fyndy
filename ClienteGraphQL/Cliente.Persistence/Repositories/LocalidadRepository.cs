﻿using Microsoft.EntityFrameworkCore;
using Cliente.Core.Models;
using Cliente.Core.Repositories;
using Common.Utils.Repository;

namespace Cliente.Persistence.Repositories
{
    class LocalidadRepository : Repository<Localidad>, ILocalidadRepository
    {
        public LocalidadRepository(DbContext context) : base(context)
        { }
    }
}