using Common.Utils.Error;
using Cliente.Api.GraphQL.Mutations;
using Cliente.Api.GraphQL.Queries;
using Cliente.Core;
using Cliente.Persistence;
using Common.Utils.Logging;
using HotChocolate;
using HotChocolate.AspNetCore;
using HotChocolate.Execution;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Common.Utils.Redirecccionamiento;
using Cliente.Api.GraphQL.MutationsType;
using Cliente.Api.GraphQL.QueryTypes;
using Common.Utils.CustomTypes;
using Microsoft.AspNetCore.Diagnostics;

namespace Cliente.Api
{
    public class Startup
    {
        public IConfiguration _Configuration { get; }

        public Startup(IConfiguration Configuration)
        {
            _Configuration = Configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<FYNDDYContext>(options =>
            options.UseSqlServer(_Configuration.GetConnectionString("DefaultConnection")),
                ServiceLifetime.Transient
            );

            services.AddScoped<Calls>();
            services.AddTransient<Query>();
            services.AddTransient<Mutation>();
            services.AddDiagnosticObserver<DiagnosticObserver>();

            services.AddGraphQL(
                SchemaBuilder.New()
                .AddQueryType<QueryType>()
                .AddMutationType<MutationType>()
                .AddType<FechaHoraType>()
                .Create(), builder => builder
                .UseDefaultPipeline());

            services.AddCors(options => {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .WithExposedHeaders("Token")
                    .Build());
            });

            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddSingleton<IConfiguration>(_Configuration);
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddErrorFilter<ErrorFilter>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors("CorsPolicy");

            app.UseExceptionHandler(a => a.Run(async context =>
            {
                var exceptionHandlerPathFeature = context.Features.Get<IExceptionHandlerPathFeature>();
                var exception = exceptionHandlerPathFeature.Error;
            }));

            app.UseGraphQL();
            app.UsePlayground();

            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("Service running");
            });
        }
    }
}