﻿using Cliente.Api.GraphQL.Resolvers;
using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.QueryTypes
{
    public class ProductoEmpresaType : ObjectType<ProductoEmpresa>
    {
        protected override void Configure(IObjectTypeDescriptor<ProductoEmpresa> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Description("ProductoEmpresa de la BD");

            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.RubroEmpresaId);
            descriptor.Field(x => x.ProductoId);
            descriptor.Field<CategoriaEmpresaResolver>(x => x.ObtenerProductoAsync(default));
            descriptor.Field<CategoriaEmpresaResolver>(x => x.ObtenerEmpresaAsync(default));
        }
    }
}
