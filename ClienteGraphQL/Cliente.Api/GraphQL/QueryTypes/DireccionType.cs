﻿using Cliente.Api.GraphQL.Resolvers;
using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.QueryTypes
{
    public class DireccionType : ObjectType<Direccion>
    {
        protected override void Configure(IObjectTypeDescriptor<Direccion> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Description("Direccion de la BD");

            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.InformacionAdicional);
            descriptor.Field(x => x.CodigoPostalId);
            descriptor.Field(x => x.EmpresaId);
            descriptor.Field(x => x.CiudadId);
            descriptor.Field(x => x.LocalidadId);
            descriptor.Field(x => x.Primaria);
            descriptor.Field<DireccionResolver>(x => x.ObtenerCodigoPostalAsync(default));

        }
    }
}
