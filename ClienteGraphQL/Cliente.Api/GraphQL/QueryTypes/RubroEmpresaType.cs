﻿using Cliente.Api.GraphQL.Resolvers;
using Cliente.Core.Models;
using HotChocolate.Types;
using HotChocolate.Types.Relay;

namespace Cliente.Api.GraphQL.QueryTypes
{
    public class RubroEmpresaType : ObjectType<RubroEmpresa>
    {
        protected override void Configure(IObjectTypeDescriptor<RubroEmpresa> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Description("RubroEmpresa de la BD");

            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.CategoriaEmpresaId);
            descriptor.Field(x => x.RubroId);
            descriptor.Field<CategoriaEmpresaResolver>(x => x.ObtenerRubroAsync(default));
            descriptor.Field(x => x.ProductoEmpresa)
                .Type<ListType<ProductoEmpresaType>>()
                .UsePaging<ProductoEmpresaType>()
                .UseFiltering()
                .UseSorting();
        }
    }
}
