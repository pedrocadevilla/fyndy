﻿using Cliente.Api.GraphQL.Resolvers;
using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.QueryTypes
{
    public class ComentariosEmpresaType : ObjectType<ComentariosEmpresa>
    {
        protected override void Configure(IObjectTypeDescriptor<ComentariosEmpresa> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Description("ComentariosEmpresa de la BD");

            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.EmpresaId);
            descriptor.Field(x => x.Clasificacion);
            descriptor.Field(x => x.Comentarios);
            descriptor.Field(x => x.FechaEliminacion);
            descriptor.Field(x => x.Fecha);
            descriptor.Field(x => x.UsuarioId);
            descriptor.Field<ComentariosEmpresaResovler>(x => x.obtenerUsuarioAsync(default));
            descriptor.Field<ComentariosEmpresaResovler>(x => x.obtenerEmpresaAsync(default));
            descriptor.Field<ComentariosEmpresaResovler>(x => x.obtenerTipoUsuarioAsync(default));
        }
    }
}
