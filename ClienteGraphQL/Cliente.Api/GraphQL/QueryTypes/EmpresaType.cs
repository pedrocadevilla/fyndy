﻿using Cliente.Api.GraphQL.Resolvers;
using Cliente.Core.Models;
using Common.Utils.CustomTypes;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.QueryTypes
{
    public class EmpresaType : ObjectType<Empresa>
    {
        protected override void Configure(IObjectTypeDescriptor<Empresa> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Description("Empresa de la BD");

            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.Contraseña);
            descriptor.Field(x => x.Correo);
            descriptor.Field(x => x.Descripcion);
            descriptor.Field(x => x.Direccion);
            descriptor.Field(x => x.FotoPerfil);
            descriptor.Field(x => x.Latitud);
            descriptor.Field(x => x.Longuitud);
            descriptor.Field(x => x.Nombre);
            descriptor.Field(x => x.PonderacionTotal);
            descriptor.Field(x => x.PalabrasClave);
            descriptor.Field(x => x.CantidadTrabajadores);
            descriptor.Field(x => x.InicioOperaciones).Type<FechaHoraType>();
            descriptor.Field<EmpresaResolver>(x => x.ObtenerPlanes(default));
            descriptor.Field<EmpresaResolver>(x => x.ObtenerContacto(default));
            descriptor.Field<EmpresaResolver>(x => x.ObtenerHorario(default));
            descriptor.Field<EmpresaResolver>(x => x.ObtenerImagenes(default));
            descriptor.Field<EmpresaResolver>(x => x.ObtenerRedSocial(default));
            descriptor.Field<EmpresaResolver>(x => x.ObtenerTipoEmpresa(default));
        }
    }
}
