﻿using Cliente.Api.GraphQL.Resolvers;
using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.QueryTypes
{
    public class CiudadType : ObjectType<Ciudad>
    {
        protected override void Configure(IObjectTypeDescriptor<Ciudad> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Description("Ciudad de la BD");

            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.Nombre);
            descriptor.Field<DireccionResolver>(x => x.ObtenerLocalidadAsync(default));

        }
    }
}
