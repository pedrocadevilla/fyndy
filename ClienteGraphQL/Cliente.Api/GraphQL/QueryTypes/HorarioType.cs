﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.QueryTypes
{
    public class HorarioType : ObjectType<Horario>
    {
        protected override void Configure(IObjectTypeDescriptor<Horario> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Description("Horario de la BD");

            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.Horario1);
        }
    }
}
