﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.QueryTypes
{
    public class ParametrosType : ObjectType<Parametros>
    {
        protected override void Configure(IObjectTypeDescriptor<Parametros> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Description("Parametros de la BD");

            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.Nombre);
        }
    }
}
