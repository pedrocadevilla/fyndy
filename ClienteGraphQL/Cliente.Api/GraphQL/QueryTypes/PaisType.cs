﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.QueryTypes
{
    public class PaisType : ObjectType<Pais>
    {
        protected override void Configure(IObjectTypeDescriptor<Pais> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Description("Pais de la BD");

            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.Nombre);
        }
    }
}
