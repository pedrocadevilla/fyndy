﻿using Cliente.Api.GraphQL.Resolvers;
using Cliente.Core.Models;
using HotChocolate.Types;
using HotChocolate.Types.Relay;

namespace Cliente.Api.GraphQL.QueryTypes
{
    public class CategoriaEmpresaType : ObjectType<CategoriaEmpresa>
    {
        protected override void Configure(IObjectTypeDescriptor<CategoriaEmpresa> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Description("CategoriaEmpresa de la BD");

            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.CategoriaId);
            descriptor.Field(x => x.EmpresaId);
            descriptor.Field<CategoriaEmpresaResolver>(x => x.ObtenerCategoriaAsync(default));
            descriptor.Field(x => x.RubroEmpresa)
                .Type<ListType<RubroEmpresaType>>()
                .UsePaging<RubroEmpresaType>()
                .UseFiltering()
                .UseSorting();
        }
    }
}
