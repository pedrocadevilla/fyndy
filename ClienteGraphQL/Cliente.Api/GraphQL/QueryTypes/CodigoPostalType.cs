﻿using Cliente.Api.GraphQL.Resolvers;
using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.QueryTypes
{
    public class CodigoPostalType : ObjectType<CodigoPostal>
    {
        protected override void Configure(IObjectTypeDescriptor<CodigoPostal> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Description("CodigoPostal de la BD");

            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.Nombre);
            descriptor.Field<DireccionResolver>(x => x.ObtenerCiudadAsync(default));
        }
    }
}
