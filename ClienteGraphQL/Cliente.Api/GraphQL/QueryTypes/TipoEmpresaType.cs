﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.QueryTypes
{
    public class TipoEmpresaType : ObjectType<TipoEmpresa>
    {
        protected override void Configure(IObjectTypeDescriptor<TipoEmpresa> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Description("TipoEmpresa de la BD");

            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.Nombre);
        }
    }
}
