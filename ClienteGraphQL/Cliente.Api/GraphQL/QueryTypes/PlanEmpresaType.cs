﻿using Cliente.Api.GraphQL.Resolvers;
using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.QueryTypes
{
    public class PlanEmpresaType : ObjectType<PlanEmpresa>
    {
        protected override void Configure(IObjectTypeDescriptor<PlanEmpresa> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Description("ParametroEmpresa de la BD");

            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.EmpresaId);
            descriptor.Field(x => x.FechaFin);
            descriptor.Field(x => x.FechaInicio);
            descriptor.Field(x => x.PlanId);
            descriptor.Field<PlanEmpresaResolver>(x => x.ObtenerPlanesAsync(default));
        }
    }
}
