﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.QueryTypes
{
    public class CategoriaType : ObjectType<Categoria>
    {
        protected override void Configure(IObjectTypeDescriptor<Categoria> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Description("Categoria de la BD");

            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.Nombre);
        }
    }
}
