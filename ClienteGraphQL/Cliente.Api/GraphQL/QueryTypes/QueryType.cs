﻿using Cliente.Api.GraphQL.Queries;
using HotChocolate.Types;
using HotChocolate.Types.Relay;

namespace Cliente.Api.GraphQL.QueryTypes
{
    public class QueryType : ObjectType<Query>
    {
        protected override void Configure(IObjectTypeDescriptor<Query> descriptor)
        {
            descriptor.Name("Consultas");
            descriptor.Description("Conjunto de elementos acerca de los cuales se puede obtener informaciòn");

            descriptor.Field(x => x.ObtenerComentariosEmpresaAsync(default))
                .Argument("id", x => x.Type<NonNullType<IdType>>())
                .Type<ListType<ComentariosEmpresaType>>()
                .Description("Mostrar todas los comentarios de una empresa dado su id.");

            descriptor.Field(x => x.ObtenerComentariosXIdAsync(default))
                .Argument("id", x => x.Type<NonNullType<IdType>>())
                .Type<ComentariosEmpresaType>()
                .Description("Mostrar un comentarios de una empresa dado su id.");

            descriptor.Field(x => x.ObtenerCategoriasAsync())
                .Type<ListType<CategoriaType>>()
                .Description("Mostrar todas las categorias.");

            descriptor.Field(x => x.ObtenerCategoriasPrincipalesAsync())
                .Type<ListType<CategoriaType>>()
                .UsePaging<CategoriaType>()
                .Description("Mostrar las categorias principales.");

            descriptor.Field(x => x.ObtenerEmpresasPrincipalesAsync())
                .Type<ListType<EmpresaType>>()
                .Description("Mostrar las empresas principales.");

            descriptor.Field(x => x.ObtenerEmpresaXIdAsync(default, default, default))
                .Argument("id", x => x.Type<NonNullType<IdType>>())
                .Argument("frase", x => x.Type<StringType>())
                .Argument("productoId", x => x.Type<NonNullType<IdType>>())
                .Type<EmpresaType>()
                .Description("Mostrar empresa dado id.");

            descriptor.Field(x => x.ObtenerEmpresaAsync())
                .Type<ListType<EmpresaType>>()
                .Description("Mostrar todas las empresas.");

            descriptor.Field(x => x.ObtenerTodosProductoAsync())
                .Type<ListType<ProductoType>>()
                .Description("Mostrar todas los productos.");

            descriptor.Field(x => x.ConsultarProductosEmpresa(default, default, default))
                .Argument("empresaId", x => x.Type<NonNullType<IdType>>())
                .Argument("categoriaId", x => x.Type<IdType>())
                .Argument("rubroId", x => x.Type<IdType>())
                .Type<ListType<CategoriaEmpresaType>>()
                .UsePaging<CategoriaEmpresaType>()
                .UseFiltering()
                .UseSorting()
                .Description("Mostrar todas los productos.");

            descriptor.Field(x => x.ConsultarProductosPrincipalesEmpresaMotorAsync(default))
                .Argument("empresaId", x => x.Type<NonNullType<IdType>>())
                .Type<ListType<ProductoEmpresaType>>()
                .UsePaging<ProductoEmpresaType>()
                .UseFiltering()
                .UseSorting()
                .Description("Mostrar productos principales de una Empresa.");

            descriptor.Field(x => x.ConsultarProductosEmpresaMotorAsync(default, default, default))
                .Argument("palabra", x => x.Type<NonNullType<StringType>>())
                .Argument("filtroId", x => x.Type<IdType>())
                .Argument("tipoFiltro", x => x.Type<IdType>())
                .Type<ListType<ProductoEmpresaType>>()
                .UsePaging<ProductoEmpresaType>()
                .UseFiltering()
                .UseSorting()
                .Description("Mostrar todas los productos segun palabra.");

            descriptor.Field(x => x.ConsultarProductosCategoriaMotorAsync(default))
                .Argument("categoriaId", x => x.Type<NonNullType<LongType>>())
                .Type<ListType<ProductoEmpresaType>>()
                .UsePaging<ProductoEmpresaType>()
                .UseFiltering()
                .UseSorting()
                .Description("Mostrar todas los productos segun categoria.");

            descriptor.Field(x => x.ObtenerTodosRubrosAsync())
                .Type<ListType<RubroType>>()
                .Description("Mostrar todos los rubros.");

            descriptor.Field(x => x.ObtenerHorariosAsync())
                .Type<ListType<HorarioType>>()
                .Description("Mostrar todos los rubros.");

            descriptor.Field(x => x.ObtenerTipoEmpresaAsync())
                .Type<ListType<TipoEmpresaType>>()
                .Description("Mostrar todos los rubros.");

            descriptor.Field(x => x.ObtenerRedSocialAsync())
                .Type<ListType<RedSocialType>>()
                .Description("Mostrar todos los rubros.");

            descriptor.Field(x => x.ObtenerGenerosAsync())
                .Type<ListType<GeneroType>>()
                .Description("Mostrar todos los rubros.");

            descriptor.Field(x => x.ObtenerEstadosCivilesAsync())
                .Type<ListType<EstadoCivilType>>()
                .Description("Mostrar todos los rubros.");

            descriptor.Field(x => x.ObtenerRubroIdAsync(default))
                .Argument("id", x => x.Type<NonNullType<IdType>>())
                .Type<RubroType>()
                .Description("Mostrar un rubro, dado un id");

            descriptor.Field(x => x.ObtenerCategoriaIdAsync(default))
                .Argument("id", x => x.Type<NonNullType<IdType>>())
                .Type<CategoriaType>()
                .Description("Mostrar una categoria, dado un id");

            descriptor.Field(x => x.ObtenerProductoIdAsync(default))
                .Argument("id", x => x.Type<NonNullType<IdType>>())
                .Type<ProductoType>()
                .Description("Mostrar un producto, dado un id");

            descriptor.Field(x => x.ObtenerProductoEmpresaXIdAsync(default))
                .Argument("id", x => x.Type<NonNullType<IdType>>())
                .Type<ProductoEmpresaType>()
                .Description("Mostrar un producto, dado un id");

            descriptor.Field(x => x.ObtenerRubrosAsync(default))
                .Argument("id", x => x.Type<NonNullType<IdType>>())
                .Type<ListType<RubroType>>()
                .Description("Mostrar todas los rubros, dado un id de categoria");

            descriptor.Field(x => x.ObtenerProductoAsync(default))
                .Argument("id", x => x.Type<NonNullType<IdType>>())
                .Type<ListType<ProductoType>>()
                .Description("Mostrar todas los rubros, dado un id de categoria");

            descriptor.Field(x => x.ObtenerPaisesAsync())
                .Type<ListType<PaisType>>()
                .Description("Mostrar todos los paises.");

            descriptor.Field(x => x.ObtenerLocalidadesAsync(default))
                .Argument("id", x => x.Type<NonNullType<IdType>>())
                .Type<ListType<LocalidadType>>()
                .Description("Mostrar todas las localidades, dado un id de pais");

            descriptor.Field(x => x.ObtenerCiudadesAsync(default))
                .Argument("id", x => x.Type<NonNullType<IdType>>())
                .Type<ListType<CiudadType>>()
                .Description("Mostrar todas las ciudades, dado un id de localidad");

            descriptor.Field(x => x.ObtenerCodigosPostalesAsync(default))
                .Argument("id", x => x.Type<NonNullType<IdType>>())
                .Type<ListType<CodigoPostalType>>()
                .Description("Mostrar todos los codigos postales, dado un id de ciudad");

            descriptor.Field(x => x.ObtenerRedesSocialesAsync())
                .Type<ListType<RedSocialType>>()
                .Description("Mostrar todas las redes sociales.");

            descriptor.Field(x => x.ObtenerGenerosAsync())
                .Type<ListType<GeneroType>>()
                .Description("Mostrar todos los generos.");

            descriptor.Field(x => x.ObtenerEstadosCivilesAsync())
                .Type<ListType<EstadoCivilType>>()
                .Description("Mostrar todos los estados civiles.");

            descriptor.Field(x => x.InicioEmpresa(default, default))
                .Argument("correo", x => x.Type<NonNullType<StringType>>())
                .Argument("password", x => x.Type<NonNullType<StringType>>())
                .Type<EmpresaType>()
                .Description("Comprobar datos inicio empresa");

            descriptor.Field(x => x.InicioUsuario(default, default))
                .Argument("correo", x => x.Type<NonNullType<StringType>>())
                .Argument("password", x => x.Type<NonNullType<StringType>>())
                .Type<UsuarioType>()
                .Description("Comprobar datos inicio usuario");
        }
    }
}
