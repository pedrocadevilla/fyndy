﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.QueryTypes
{
    public class HorarioContactoType : ObjectType<HorarioContacto>
    {
        protected override void Configure(IObjectTypeDescriptor<HorarioContacto> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Description("HorarioContacto de la BD");

            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.Horario);
            descriptor.Field(x => x.EmpresaId);
        }
    }
}
