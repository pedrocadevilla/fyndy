﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.QueryTypes
{
    public class DatosContactoType : ObjectType<DatosContacto>
    {
        protected override void Configure(IObjectTypeDescriptor<DatosContacto> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Description("DatosContacto de la BD");

            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.Contacto);
            descriptor.Field(x => x.EmpresaId);
        }
    }
}
