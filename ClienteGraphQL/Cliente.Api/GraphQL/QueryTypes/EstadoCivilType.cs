﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.QueryTypes
{
    public class EstadoCivilType : ObjectType<EstadoCivil>
    {
        protected override void Configure(IObjectTypeDescriptor<EstadoCivil> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Description("EstadoCivil de la BD");

            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.Nombre);
        }
    }
}
