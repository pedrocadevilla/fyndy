﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.QueryTypes
{
    public class RedSocialType : ObjectType<RedSocial>
    {
        protected override void Configure(IObjectTypeDescriptor<RedSocial> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Description("RedSocial de la BD");

            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.Nombre);
            
        }
    }
}
