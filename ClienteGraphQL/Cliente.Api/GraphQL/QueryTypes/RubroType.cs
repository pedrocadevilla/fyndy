﻿using Cliente.Api.GraphQL.Resolvers;
using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.QueryTypes
{
    public class RubroType : ObjectType<Rubro>
    {
        protected override void Configure(IObjectTypeDescriptor<Rubro> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Description("Rubro de la BD");

            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.Nombre);
            descriptor.Field<ProductoResolver>(x => x.ObtenerCategoriaAsync(default));
        }
    }
}
