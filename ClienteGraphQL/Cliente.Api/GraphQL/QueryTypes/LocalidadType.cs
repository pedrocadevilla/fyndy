﻿using Cliente.Api.GraphQL.Resolvers;
using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.QueryTypes
{
    public class LocalidadType : ObjectType<Localidad>
    {
        protected override void Configure(IObjectTypeDescriptor<Localidad> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Description("Localidad de la BD");

            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.Nombre);
            descriptor.Field<DireccionResolver>(x => x.ObtenerPaisAsync(default));
        }
    }
}
