﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.QueryTypes
{
    public class UsuarioType : ObjectType<Usuario>
    {
        protected override void Configure(IObjectTypeDescriptor<Usuario> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Description("Usuario de la BD");

            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.Contraseña);
            descriptor.Field(x => x.Correo);
            descriptor.Field(x => x.EstadoCivilId);
            descriptor.Field(x => x.FechaNacimiento);
            descriptor.Field(x => x.FotoPerfil);
            descriptor.Field(x => x.GeneroId);
            descriptor.Field(x => x.PrimerApellido);
            descriptor.Field(x => x.PrimerNombre);
            descriptor.Field(x => x.SegundoApellido);
            descriptor.Field(x => x.SegundoNombre);
            descriptor.Field(x => x.Tipo);
        }
    }
}
