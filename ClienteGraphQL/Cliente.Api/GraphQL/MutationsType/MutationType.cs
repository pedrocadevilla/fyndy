﻿using Cliente.Api.GraphQL.InputTypes;
using Cliente.Api.GraphQL.Mutations;
using Cliente.Api.GraphQL.QueryTypes;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.MutationsType
{
    public class MutationType : ObjectType<Mutation>
    {
        protected override void Configure(IObjectTypeDescriptor<Mutation> descriptor)
        {
            descriptor.Name("Mutaciones");
            descriptor.Description("Operaciones para guardar, modificar y Delete registros");

            descriptor.Field(t => t.CrearComentario(default, default))
                .Type<NonNullType<ComentariosEmpresaType>>()
                .Type<NonNullType<StringType>>()
                .Argument("comentario", a => a.Type<NonNullType<ComentariosEmpresaInputType>>())
                .Description("Permite crear una nueva comentario. Recibe como parámetro un objeto de tipo ComentarioInput");

            descriptor.Field(t => t.ModificaComentarioAsync(default, default))
                .Type<NonNullType<ComentariosEmpresaType>>()
                .Argument("id", a => a.Type<NonNullType<IdType>>())
                .Argument("comentario", a => a.Type<NonNullType<ComentariosEmpresaInputType>>())
                .Description("Permite modificar una comentario. Recibe como parámetro un objeto de tipo ComentarioInput y un id");

            descriptor.Field(t => t.EliminarComentarioAsync(default))
                .Type<NonNullType<StringType>>()
                .Argument("id", a => a.Type<NonNullType<IdType>>())
                .Description("Permite eliminar una comentario. Recibe como parámetro un ID de objeto");

            descriptor.Field(t => t.ModificarPalabraEmpresaAsync(default, default, default))
                .Type<NonNullType<EmpresaType>>()
                .Argument("id", a => a.Type<NonNullType<IdType>>())
                .Argument("palabra", a => a.Type<NonNullType<StringType>>())
                .Argument("palabraEditada", a => a.Type<StringType>())
                .Description("Permite modificar una palabra clave de empresa o eliminarla de acuerdo a sus parametros");

            descriptor.Field(t => t.CrearCategoriaAsync(default))
                .Type<NonNullType<CategoriaType>>()
                .Argument("categoria", a => a.Type<NonNullType<CategoriaInputType>>())
                .Description("Permite crear una nueva categoria. Recibe como parámetro un objeto de tipo CategoriaInput");

            descriptor.Field(t => t.ModificarCategoriaAsync(default, default))
                .Type<NonNullType<CategoriaType>>()
                .Argument("id", a => a.Type<NonNullType<IdType>>())
                .Argument("categoria", a => a.Type<NonNullType<CategoriaInputType>>())
                .Description("Permite modificar una categoria. Recibe como parámetro un objeto de tipo CategoriaInput y un id");

            descriptor.Field(t => t.EliminarCategoriaAsync(default))
                .Type<NonNullType<StringType>>()
                .Argument("id", a => a.Type<NonNullType<IdType>>())
                .Description("Permite eliminar una categoria. Recibe como parámetro un ID de objeto");

            descriptor.Field(t => t.CrearRubroAsync(default))
                .Type<NonNullType<RubroType>>()
                .Argument("rubro", a => a.Type<NonNullType<RubroInputType>>())
                .Description("Permite crear un nuevo rubro. Recibe como parámetro un objeto de tipo RubroInput");

            descriptor.Field(t => t.ModificarRubroAsync(default, default))
                .Type<NonNullType<RubroType>>()
                .Argument("id", a => a.Type<NonNullType<IdType>>())
                .Argument("rubro", a => a.Type<NonNullType<RubroInputType>>())
                .Description("Permite modificar un rubro. Recibe como parámetro un objeto de tipo RubroINput y un id");

            descriptor.Field(t => t.EliminarRubroAsync(default))
                .Type<NonNullType<StringType>>()
                .Argument("id", a => a.Type<NonNullType<IdType>>())
                .Description("Permite eliminar un rubro. Recibe como parámetro un ID de objeto");

            descriptor.Field(t => t.CrearProductoAsync(default))
                .Type<NonNullType<ProductoType>>()
                .Argument("producto", a => a.Type<NonNullType<ProductoInputType>>())
                .Description("Permite crear un nuevo producto. Recibe como parámetro un objeto de tipo ProductoInput");

            descriptor.Field(t => t.ModificarProductoAsync(default, default))
                .Type<NonNullType<ProductoType>>()
                .Argument("id", a => a.Type<NonNullType<IdType>>())
                .Argument("producto", a => a.Type<NonNullType<ProductoInputType>>())
                .Description("Permite modificar un producto. Recibe como parámetro un objeto de tipo ProductoInput y un id");

            descriptor.Field(t => t.EliminarProductoAsync(default))
                .Type<NonNullType<StringType>>()
                .Argument("id", a => a.Type<NonNullType<IdType>>())
                .Description("Permite eliminar un producto. Recibe como parámetro un ID de objeto");

            descriptor.Field(t => t.CrearPaisAsync(default))
                .Type<NonNullType<PaisType>>()
                .Argument("pais", a => a.Type<NonNullType<PaisInputType>>())
                .Description("Permite crear un nuevo pais. Recibe como parámetro un objeto de tipo PaisInput");

            descriptor.Field(t => t.CrearLocalidadAsync(default))
                .Type<NonNullType<LocalidadType>>()
                .Argument("localidad", a => a.Type<NonNullType<LocalidadInputType>>())
                .Description("Permite crear una nueva localidad. Recibe como parámetro un objeto de tipo LocalidadInput");

            descriptor.Field(t => t.CrearCiudadAsync(default))
                .Type<NonNullType<CiudadType>>()
                .Argument("ciudad", a => a.Type<NonNullType<CiudadInputType>>())
                .Description("Permite crear una nueva ciudad. Recibe como parámetro un objeto de tipo CiudadInput");

            descriptor.Field(t => t.CrearCodigoPostalAsync(default))
                .Type<NonNullType<CodigoPostalType>>()
                .Argument("codigo", a => a.Type<NonNullType<CodigoPostalInputType>>())
                .Description("Permite crear un nuevo codigo postal. Recibe como parámetro un objeto de tipo CodigoPostalInput");

            descriptor.Field(t => t.CrearRedSocialAsync(default))
                .Type<NonNullType<RedSocialType>>()
                .Argument("red", a => a.Type<NonNullType<RedSocialInputType>>())
                .Description("Permite crear una nueva red social. Recibe como parámetro un objeto de tipo RedSocialInput");

            descriptor.Field(t => t.CrearGeneroAsync(default))
                .Type<NonNullType<GeneroType>>()
                .Argument("genero", a => a.Type<NonNullType<GeneroInputType>>())
                .Description("Permite crear un nuevo genero. Recibe como parámetro un objeto de tipo GeneroInput");

            descriptor.Field(t => t.CrearEstadoCivilAsync(default))
                .Type<NonNullType<EstadoCivilType>>()
                .Argument("estado", a => a.Type<NonNullType<EstadoCivilInputType>>())
                .Description("Permite crear un nuevo estado. Recibe como parámetro un objeto de tipo EstadoInput");

            descriptor.Field(t => t.CrearDireccionAsync(default))
                .Type<NonNullType<DireccionType>>()
                .Argument("direccion", a => a.Type<NonNullType<DireccionInputType>>())
                .Description("Permite crear una direccion. Recibe como parámetro un objeto de tipo DireccionInput");

            descriptor.Field(t => t.CrearEmpresaAsync(default, default))
                .Type<NonNullType<EmpresaType>>()
                .Argument("empresa", a => a.Type<NonNullType<EmpresaInputType>>())
                .Argument("direccion", a => a.Type<NonNullType<DireccionInputType>>())
                .Description("Permite crear una empresa. Recibe como parámetro un objeto de tipo EmpresaInput");

            descriptor.Field(t => t.CrearUsuarioAsync(default))
                .Type<NonNullType<UsuarioType>>()
                .Argument("usuario", a => a.Type<NonNullType<UsuarioInputType>>())
                .Description("Permite crear un usuario. Recibe como parámetro un objeto de tipo UsuarioInput");

            descriptor.Field(t => t.CrearPermutacionesAsync())
                .Type<NonNullType<BooleanType>>()
                .Description("Permite crear las permutacones de los productos.");

            descriptor.Field(t => t.CrearProductoEmpresaAsync(default, default, default))
                .Type<NonNullType<ListType<ProductoEmpresaType>>>()
                .Argument("producto", a => a.Type<NonNullType<ListType<ProductoEmpresaInputType>>>())
                .Argument("rubro", a => a.Type<NonNullType<RubroEmpresaInputType>>())
                .Argument("categoria", a => a.Type<NonNullType<CategoriaEmpresaInputType>>())
                .Description("Permite asociar un producto a una empresa. Recibe como parámetro un objeto de tipo ProductoEmpresaInput");

            descriptor.Field(t => t.ModificarDireccionAsync(default, default))
                .Type<NonNullType<DireccionType>>()
                .Argument("id", a => a.Type<NonNullType<IdType>>())
                .Argument("direccion", a => a.Type<NonNullType<DireccionInputType>>())
                .Description("Permite modificar una direccion. Recibe como parámetro un objeto de tipo DireccionInput");

            descriptor.Field(t => t.ModificarEmpresaAsync(default, default))
                .Type<NonNullType<EmpresaType>>()
                .Argument("id", a => a.Type<NonNullType<IdType>>())
                .Argument("empresa", a => a.Type<NonNullType<EmpresaInputType>>())
                .Description("Permite modificar una empresa. Recibe como parámetro un objeto de tipo EmpresaInput");

            descriptor.Field(t => t.ModificarHorarioEmpresaAsync(default, default))
                .Type<NonNullType<HorarioContactoType>>()
                .Argument("empresaId", a => a.Type<NonNullType<IdType>>())
                .Argument("horarioId", a => a.Type<NonNullType<ListType<LongType>>>())
                .Description("Permite modificar una empresa. Recibe como parámetro un objeto de tipo EmpresaInput");

            descriptor.Field(t => t.ModificarContactoEmpresaAsync(default, default))
                .Type<NonNullType<DatosContactoType>>()
                .Argument("contacto", a => a.Type<NonNullType<DatosContactoInputType>>())
                .Argument("contactoId", a => a.Type<LongType>())
                .Description("Permite modificar una empresa. Recibe como parámetro un objeto de tipo EmpresaInput");

            descriptor.Field(t => t.ModificarRedSocialEmpresaAsync(default, default))
                .Type<NonNullType<DatosContactoType>>()
                .Argument("redSocial", a => a.Type<NonNullType<RedSocialEmpresaInputType>>())
                .Argument("redSocialId", a => a.Type<LongType>())
                .Description("Permite modificar una empresa. Recibe como parámetro un objeto de tipo EmpresaInput");

            descriptor.Field(t => t.ModificarUsuarioAsync(default, default))
                .Type<NonNullType<UsuarioType>>()
                .Argument("id", a => a.Type<NonNullType<IdType>>())
                .Argument("usuario", a => a.Type<NonNullType<UsuarioInputType>>())
                .Description("Permite modificar un usuario. Recibe como parámetro un objeto de tipo UsuarioInput");

            descriptor.Field(t => t.ModificarProductoEmpresaAsync(default, default, default, default))
                .Type<NonNullType<ProductoEmpresaType>>()
                .Argument("id", a => a.Type<NonNullType<IdType>>())
                .Argument("producto", a => a.Type<NonNullType<ProductoEmpresaInputType>>())
                .Argument("rubro", a => a.Type<NonNullType<RubroEmpresaInputType>>())
                .Argument("categoria", a => a.Type<NonNullType<CategoriaEmpresaInputType>>())
                .Description("Permite modificar un producto asociado a una empresa. Recibe como parámetro un objeto de tipo ProductoEmpresaInput");

            descriptor.Field(t => t.EliminarProductoEmpresaAsync(default))
                .Type<NonNullType<StringType>>()
                .Argument("id", a => a.Type<NonNullType<IdType>>())
                .Description("Permite eliminar un producto asociado a una empresa. Recibe como parámetro un ID de objeto");

        }
    }
}