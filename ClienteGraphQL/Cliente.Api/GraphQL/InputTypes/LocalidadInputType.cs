﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.InputTypes
{
    public class LocalidadInputType : InputObjectType<Localidad>
    {
        protected override void Configure(IInputObjectTypeDescriptor<Localidad> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Name("LocalidadInput");

            descriptor.Field(x => x.Nombre);
            descriptor.Field(x => x.PaisId);
        }
    }
}
