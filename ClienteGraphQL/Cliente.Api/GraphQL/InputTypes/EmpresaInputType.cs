﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.InputTypes
{
    public class EmpresaInputType : InputObjectType<Empresa>
    {
        protected override void Configure(IInputObjectTypeDescriptor<Empresa> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Name("EmpresaInput");

            descriptor.Field(x => x.Contraseña);
            descriptor.Field(x => x.Correo);
            descriptor.Field(x => x.Descripcion);
            descriptor.Field(x => x.Direccion);
            descriptor.Field(x => x.FotoPerfil);
            descriptor.Field(x => x.Latitud);
            descriptor.Field(x => x.Longuitud);
            descriptor.Field(x => x.Nombre);
            descriptor.Field(x => x.PonderacionTotal);
            descriptor.Field(x => x.PalabrasClave);
            descriptor.Field(x => x.HorarioContacto);
            descriptor.Field(x => x.DatosContacto);
            descriptor.Field(x => x.TipoEmpresaId);
            descriptor.Field(x => x.CantidadTrabajadores);
            descriptor.Field(x => x.InicioOperaciones);
        }
    }
}
