﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.InputTypes
{
    public class RedSocialEmpresaInputType : InputObjectType<RedSocialEmpresa>
    {
        protected override void Configure(IInputObjectTypeDescriptor<RedSocialEmpresa> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Name("RedSocialEmpresaInput");

            descriptor.Field(x => x.RedSocialId);
            descriptor.Field(x => x.EmpresaId);
            descriptor.Field(x => x.Link);
        }
    }
}
