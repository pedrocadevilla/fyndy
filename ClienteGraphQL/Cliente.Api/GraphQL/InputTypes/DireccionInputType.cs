﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.InputTypes
{
    public class DireccionInputType : InputObjectType<Direccion>
    {
        protected override void Configure(IInputObjectTypeDescriptor<Direccion> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Name("DireccionInput");

            descriptor.Field(x => x.InformacionAdicional);
            descriptor.Field(x => x.CodigoPostalId);
            descriptor.Field(x => x.EmpresaId);
            descriptor.Field(x => x.CiudadId);
            descriptor.Field(x => x.LocalidadId);
            descriptor.Field(x => x.Primaria);
            descriptor.Field(x => x.PaisId);
            descriptor.Field(x => x.LocalidadId);
        }
    }
}
