﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.InputTypes
{
    public class EstadoCivilInputType : InputObjectType<EstadoCivil>
    {
        protected override void Configure(IInputObjectTypeDescriptor<EstadoCivil> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Name("EstadoCivilInput");

            descriptor.Field(x => x.Nombre);
        }
    }
}
