﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.InputTypes
{
    public class CiudadInputType : InputObjectType<Ciudad>
    {
        protected override void Configure(IInputObjectTypeDescriptor<Ciudad> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Name("CiudadInput");

            descriptor.Field(x => x.Nombre);
            descriptor.Field(x => x.LocalidadId);
        }
    }
}
