﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.InputTypes
{
    public class RubroEmpresaInputType : InputObjectType<RubroEmpresa>
    {
        protected override void Configure(IInputObjectTypeDescriptor<RubroEmpresa> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Name("RubroEmpresaInput");

            descriptor.Field(x => x.CategoriaEmpresaId);
            descriptor.Field(x => x.RubroId);

        }
    }
}
