﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.InputTypes
{
    public class ProductoInputType : InputObjectType<Producto>
    {
        protected override void Configure(IInputObjectTypeDescriptor<Producto> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Name("ProductoInput");

            descriptor.Field(x => x.Nombre);
            descriptor.Field(x => x.RubroId);
            descriptor.Field(x => x.PalabrasClave);
        }
    }
}
