﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.InputTypes
{
    public class RubroInputType : InputObjectType<Rubro>
    {
        protected override void Configure(IInputObjectTypeDescriptor<Rubro> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Name("RubroInput");

            descriptor.Field(x => x.Nombre);
            descriptor.Field(x => x.CategoriaId);

        }
    }
}
