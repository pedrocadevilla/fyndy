﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.InputTypes
{
    public class ProductoEmpresaInputType : InputObjectType<ProductoEmpresa>
    {
        protected override void Configure(IInputObjectTypeDescriptor<ProductoEmpresa> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Name("ProductoEmpresaInput");

            descriptor.Field(x => x.RubroEmpresaId);
            descriptor.Field(x => x.ProductoId);
        }
    }
}
