﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.InputTypes
{
    public class GeneroInputType : InputObjectType<Genero>
    {
        protected override void Configure(IInputObjectTypeDescriptor<Genero> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Name("GeneroInput");

            descriptor.Field(x => x.Nombre);
        }
    }
}
