﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.InputTypes
{
    public class PaisInputType : InputObjectType<Pais>
    {
        protected override void Configure(IInputObjectTypeDescriptor<Pais> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Name("PiasInput");

            descriptor.Field(x => x.Nombre);
        }
    }
}
