﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.InputTypes
{
    public class RedSocialInputType : InputObjectType<RedSocial>
    {
        protected override void Configure(IInputObjectTypeDescriptor<RedSocial> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Name("RedSocialInput");

            descriptor.Field(x => x.Nombre);
        }
    }
}
