﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.InputTypes
{
    public class CategoriaInputType : InputObjectType<Categoria>
    {
        protected override void Configure(IInputObjectTypeDescriptor<Categoria> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Name("CategoriaInput");

            descriptor.Field(x => x.Nombre);
        }
    }
}
