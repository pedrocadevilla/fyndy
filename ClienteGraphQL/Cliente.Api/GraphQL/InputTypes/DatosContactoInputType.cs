﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.InputTypes
{
    public class DatosContactoInputType : InputObjectType<DatosContacto>
    {
        protected override void Configure(IInputObjectTypeDescriptor<DatosContacto> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Name("DatosContactoInput");

            descriptor.Field(x => x.Contacto);
            descriptor.Field(x => x.EmpresaId);
        }
    }
}
