﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.InputTypes
{
    public class CategoriaEmpresaInputType : InputObjectType<CategoriaEmpresa>
    {
        protected override void Configure(IInputObjectTypeDescriptor<CategoriaEmpresa> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Name("CategoriaEmpresaInput");

            descriptor.Field(x => x.CategoriaId);
            descriptor.Field(x => x.EmpresaId);
        }
    }
}
