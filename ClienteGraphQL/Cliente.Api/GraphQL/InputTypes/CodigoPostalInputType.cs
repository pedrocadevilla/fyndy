﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.InputTypes
{
    public class CodigoPostalInputType : InputObjectType<CodigoPostal>
    {
        protected override void Configure(IInputObjectTypeDescriptor<CodigoPostal> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Name("CodigoPostalInput");

            descriptor.Field(x => x.Nombre);
            descriptor.Field(x => x.CiudadId);
        }
    }
}
