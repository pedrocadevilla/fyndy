﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.InputTypes
{
    public class HorarioContactoInputType : InputObjectType<HorarioContacto>
    {
        protected override void Configure(IInputObjectTypeDescriptor<HorarioContacto> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Name("HorarioContactoInput");

            descriptor.Field(x => x.Horario);
            descriptor.Field(x => x.EmpresaId);
        }
    }
}
