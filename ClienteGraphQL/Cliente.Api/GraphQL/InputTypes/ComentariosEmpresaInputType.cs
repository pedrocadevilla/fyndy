﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.InputTypes
{
    public class ComentariosEmpresaInputType : InputObjectType<ComentariosEmpresa>
    {
        protected override void Configure(IInputObjectTypeDescriptor<ComentariosEmpresa> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Name("ComentariosEmpresaInput");

            descriptor.Field(x => x.EmpresaId);
            descriptor.Field(x => x.Clasificacion);
            descriptor.Field(x => x.Comentarios);
            descriptor.Field(x => x.UsuarioId);
        }
    }
}
