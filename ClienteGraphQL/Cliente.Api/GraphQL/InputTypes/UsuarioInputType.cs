﻿using Cliente.Core.Models;
using HotChocolate.Types;

namespace Cliente.Api.GraphQL.InputTypes
{
    public class UsuarioInputType : InputObjectType<Usuario>
    {
        protected override void Configure(IInputObjectTypeDescriptor<Usuario> descriptor)
        {
            descriptor.BindFields(BindingBehavior.Explicit);
            descriptor.Name("UsuarioInput");

            descriptor.Field(x => x.Contraseña);
            descriptor.Field(x => x.Correo);
            descriptor.Field(x => x.EstadoCivilId);
            descriptor.Field(x => x.FechaNacimiento);
            descriptor.Field(x => x.FotoPerfil);
            descriptor.Field(x => x.GeneroId);
            descriptor.Field(x => x.PrimerApellido);
            descriptor.Field(x => x.PrimerNombre);
            descriptor.Field(x => x.SegundoApellido);
            descriptor.Field(x => x.SegundoNombre);
            descriptor.Field(x => x.Tipo);
        }
    }
}
