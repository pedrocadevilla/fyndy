﻿using Cliente.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cliente.Api.GraphQL.Queries
{
    public partial class Query
    {
        public async Task<List<Categoria>> ObtenerCategoriasPrincipalesAsync()
        {
            return await _unitOfWork.EstadisticasCategorias.obtenerPrincipalesCategoriasAsync();
        }

        public async Task<List<Empresa>> ObtenerEmpresasPrincipalesAsync()
        {
            return await _unitOfWork.Empresa.principalesEmpresasAsync();
        }
    }
}
