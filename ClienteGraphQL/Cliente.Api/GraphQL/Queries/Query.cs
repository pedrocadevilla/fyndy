﻿using Cliente.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;

namespace Cliente.Api.GraphQL.Queries
{
    public partial class Query
    {
        private readonly IUnitOfWork _unitOfWork;
        private IConfiguration _Configuration;
        private readonly IHttpContextAccessor _context;

        public Query(IUnitOfWork unitOfWork, IHttpContextAccessor context, IConfiguration Configuration)
        {
            _unitOfWork = unitOfWork;
            _Configuration = Configuration
                ?? throw new ArgumentNullException(nameof(unitOfWork));
            _context = context;
        }
    }
}
