﻿using Cliente.Core.Models;
using System.Threading.Tasks;

namespace Cliente.Api.GraphQL.Queries
{
    public partial class Query
    {
        public async Task<Empresa> InicioEmpresa(string correo, string password)
        {
            return await _unitOfWork.Empresa.inicioSesion(correo, password);
        }

        public async Task<Usuario> InicioUsuario(string correo, string password)
        {
            return await _unitOfWork.Usuario.inicioSesion(correo, password);
        }
    }
}
