﻿using Cliente.Core.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cliente.Api.GraphQL.Queries
{
    public partial class Query
    {
        public List<CategoriaEmpresa> ConsultarProductosEmpresa(long empresaId, long? categoriaId, long? rubroId)
        {
            if(categoriaId != null)
            {
                if(rubroId != null)
                {
                    return _unitOfWork.CategoriaEmpresa.QueryAll().Where(x => x.EmpresaId == empresaId && x.CategoriaId == categoriaId && x.RubroEmpresa.First().RubroId == rubroId).ToList();
                }
                else
                {
                    return _unitOfWork.CategoriaEmpresa.QueryAll().Where(x => x.EmpresaId == empresaId && x.CategoriaId == categoriaId).ToList();
                }
            }
            else
            {
                return _unitOfWork.CategoriaEmpresa.QueryAll().Where(x => x.EmpresaId == empresaId).ToList();
            }
        }

        public async Task<Empresa> ObtenerEmpresaXIdAsync(long id, string frase, long productoId)
        {

            return await _unitOfWork.Empresa.GetAsync(id);
        }
    }
}
