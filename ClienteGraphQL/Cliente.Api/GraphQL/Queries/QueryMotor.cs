﻿using Cliente.Api.Utilidades;
using Cliente.Core.Clases;
using Cliente.Core.Models;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cliente.Api.GraphQL.Queries
{
    public partial class Query
    {
        public async Task<IEnumerable<ProductoEmpresa>> ConsultarProductosEmpresaMotorAsync(string palabra, long? filtroId, long? tipoFiltro)
        {
            Variaciones ClaseVariaciones = new Variaciones();
            if (palabra == "" || palabra == null) return await _unitOfWork.ProductoEmpresa.GetAllAsync();
            string[] palabras = palabra.ToUpper().Split(" ");
            List<string> palabrasRestringidas = _Configuration.GetSection("List:PalabrasRestringidas").Get<List<string>>();
            List<string> palabrasFiltradas = palabras.Where(x => !palabrasRestringidas.Contains(x)).ToList();
            List<string> palabrasVariaciones = palabras.Where(x => !palabrasRestringidas.Contains(x)).ToList();
            do
            {
                int cont = 2;
                do
                {
                    ClaseVariaciones.VariacionesSinRepeticion(ref palabrasFiltradas, palabrasVariaciones, cont, 0);
                    cont++;
                } while (cont <= palabrasVariaciones.Count);
                palabrasVariaciones.Remove(palabrasVariaciones.First());
            } while (palabrasVariaciones.Count > 0);
            palabrasFiltradas = palabrasFiltradas.OrderByDescending(x => x.Length).ToList();
            List<long> Permutaciones = await _unitOfWork.Permutaciones.IdPermutacionBusqueda(palabrasFiltradas);
            List<ElementoEncontrado> Productos = await _unitOfWork.Permutaciones.busquedaProductosId(Permutaciones);
            return await _unitOfWork.ProductoEmpresa.busquedaProductos(Productos, filtroId, tipoFiltro);
        }

        public async Task<IEnumerable<ProductoEmpresa>> ConsultarProductosCategoriaMotorAsync(long categoriaId)
        {
            long id = categoriaId;
            long estadisticaId = await _unitOfWork.Estadisticas.obtenerEstatadisticaXNombre(_Configuration.GetSection("EstadisticaNombre:BusquedaCategoria").Get<string>());
            EstadisticasCategorias estadisticaCategoria = await _unitOfWork.EstadisticasCategorias.obtenerEstadisticaCategoriaXId(categoriaId, estadisticaId);
            long valor = long.Parse(estadisticaCategoria.Valor);
            valor = valor + 1;
            string val = (valor).ToString();
            estadisticaCategoria.Valor = val;
            await _unitOfWork.Complete();
            List<CategoriaEmpresa> categoriaEmpresa = await _unitOfWork.CategoriaEmpresa.obtenerListaCategoriaEmpresaAsync(categoriaId);
            List<long> listaCategoria = categoriaEmpresa.Select(x => x.Id).ToList();
            List<RubroEmpresa> rubrosEmpresa = await _unitOfWork.RubroEmpresa.ObtenerListaRubroEmpresaAsync(listaCategoria);
            List<long> listaRubro = rubrosEmpresa.Select(x => x.Id).ToList();
            
            return await _unitOfWork.ProductoEmpresa.busquedaProductosXCategoriaAsync(listaRubro);
        }

        public async Task<IEnumerable<ProductoEmpresa>> ConsultarProductosPrincipalesEmpresaMotorAsync(long empresaId)
        {
            return await _unitOfWork.ProductoEmpresa.busquedaProductosXEmpresaAsync(empresaId);
        }
    }
}
