﻿using Cliente.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cliente.Api.GraphQL.Queries
{
    public partial class Query
    {
        public async Task<List<ComentariosEmpresa>> ObtenerComentariosEmpresaAsync(long id)
        {
            return await _unitOfWork.ComentariosEmpresa.ObtenerComentariosEmpresaXId(id);
        }

        public async Task<ComentariosEmpresa> ObtenerComentariosXIdAsync(long id)
        {
            return await _unitOfWork.ComentariosEmpresa.GetAsync(id);
        }
    }
}
