﻿using Cliente.Core.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cliente.Api.GraphQL.Queries
{
    public partial class Query
    {
        public async Task<IEnumerable<Pais>> ObtenerPaisesAsync()
        {
            return await _unitOfWork.Pais.GetAllAsync();
        }

        public async Task<IEnumerable<Localidad>> ObtenerLocalidadesAsync(long id)
        {
            return await _unitOfWork.Localidad.FindAsync(x => x.PaisId == id);
        }

        public async Task<IEnumerable<Ciudad>> ObtenerCiudadesAsync(long id)
        {
            return await _unitOfWork.Ciudad.FindAsync(x => x.LocalidadId == id);
        }
    }
}
