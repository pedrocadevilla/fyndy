﻿using Cliente.Core.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cliente.Api.GraphQL.Queries
{
    public partial class Query
    {
        public async Task<IEnumerable<Categoria>> ObtenerCategoriasAsync()
        {
            IEnumerable<Categoria> lista = await _unitOfWork.Categoria.FindAsync(x => x.FechaEliminacion == null);
            return lista.OrderBy(x => x.Nombre);
        }

        public async Task<Categoria> ObtenerCategoriaIdAsync(long id)
        {
            return await _unitOfWork.Categoria.GetAsync(id);
        }

        public async Task<IEnumerable<Rubro>> ObtenerRubrosAsync(long id)
        {
            IEnumerable<Rubro> lista = await _unitOfWork.Rubro.FindAsync(x => x.CategoriaId == id);
            return lista.OrderBy(x => x.Nombre);
        }

        public async Task<IEnumerable<Empresa>> ObtenerEmpresaAsync()
        {
            IEnumerable<Empresa> lista = await _unitOfWork.Empresa.GetAllAsync();
            return lista.OrderBy(x => x.Nombre);
        }

        public async Task<ProductoEmpresa> ObtenerProductoEmpresaXIdAsync(long id)
        {
            return await _unitOfWork.ProductoEmpresa.GetAsync(id);
        }

        public async Task<Rubro> ObtenerRubroIdAsync(long id)
        {
            return await _unitOfWork.Rubro.GetAsync(id);
        }

        public async Task<IEnumerable<Producto>> ObtenerProductoAsync(long id)
        {
            IEnumerable<Producto> lista = await _unitOfWork.Producto.FindAsync(x => x.RubroId == id);
            return lista.OrderBy(x => x.Nombre);
        }

        public async Task<Producto> ObtenerProductoIdAsync(long id)
        {
            return await _unitOfWork.Producto.GetAsync(id);
        }

        public async Task<IEnumerable<Rubro>> ObtenerTodosRubrosAsync()
        {
            IEnumerable<Rubro> lista = await _unitOfWork.Rubro.FindAsync(x => x.FechaEliminacion == null);
            return lista.OrderBy(x => x.Nombre);
        }

        public async Task<IEnumerable<Producto>> ObtenerTodosProductoAsync()
        {
            IEnumerable<Producto> lista = await _unitOfWork.Producto.FindAsync(x => x.FechaEliminacion == null);
            return lista.OrderBy(x => x.Nombre);
        }

        public async Task<IEnumerable<CodigoPostal>> ObtenerCodigosPostalesAsync(long id)
        {
            return await _unitOfWork.CodigoPostal.FindAsync(x => x.CiudadId == id);
        }

        public async Task<IEnumerable<RedSocial>> ObtenerRedesSocialesAsync()
        {
            return await _unitOfWork.RedSocial.GetAllAsync();
        }

        public async Task<IEnumerable<Genero>> ObtenerGenerosAsync()
        {
            return await _unitOfWork.Genero.GetAllAsync();
        }

        public async Task<IEnumerable<EstadoCivil>> ObtenerEstadosCivilesAsync()
        {
            return await _unitOfWork.EstadoCivil.GetAllAsync();
        }

        public async Task<IEnumerable<Horario>> ObtenerHorariosAsync()
        {
            return await _unitOfWork.Horario.GetAllAsync();
        }

        public async Task<IEnumerable<TipoEmpresa>> ObtenerTipoEmpresaAsync()
        {
            return await _unitOfWork.TipoEmpresa.GetAllAsync();
        }

        public async Task<IEnumerable<RedSocial>> ObtenerRedSocialAsync()
        {
            return await _unitOfWork.RedSocial.GetAllAsync();
        }
    }
}
