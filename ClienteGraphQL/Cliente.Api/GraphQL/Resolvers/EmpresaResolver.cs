﻿using Cliente.Core;
using Cliente.Core.Models;
using HotChocolate;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cliente.Api.GraphQL.Resolvers
{
    public class EmpresaResolver
    {
        protected readonly IUnitOfWork _unitOfWork;

        public EmpresaResolver(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<PlanEmpresa>> ObtenerPlanes([Parent]Empresa Empresa)
        {
            return await _unitOfWork.PlanEmpresa.FindAsync(x => x.EmpresaId == Empresa.Id);
        }

        public async Task<IEnumerable<HorarioContacto>> ObtenerHorario([Parent]Empresa Empresa)
        {
            return await _unitOfWork.HorarioContacto.FindAsync(x => x.EmpresaId == Empresa.Id);
        }

        public async Task<IEnumerable<DatosContacto>> ObtenerContacto([Parent]Empresa Empresa)
        {
            return await _unitOfWork.DatosContacto.FindAsync(x => x.EmpresaId == Empresa.Id);
        }

        public async Task<IEnumerable<ImagenesEmpresa>> ObtenerImagenes([Parent]Empresa Empresa)
        {
            return await _unitOfWork.ImagenesEmpresa.FindAsync(x => x.EmpresaId == Empresa.Id);
        }

        public async Task<IEnumerable<RedSocialEmpresa>> ObtenerRedSocial([Parent]Empresa Empresa)
        {
            return await _unitOfWork.RedSocialEmpresa.FindAsync(x => x.EmpresaId == Empresa.Id);
        }

        public async Task<TipoEmpresa> ObtenerTipoEmpresa([Parent]Empresa Empresa)
        {
            return await _unitOfWork.TipoEmpresa.GetAsync(Empresa.TipoEmpresaId ?? default(long));
        }
    }
}
