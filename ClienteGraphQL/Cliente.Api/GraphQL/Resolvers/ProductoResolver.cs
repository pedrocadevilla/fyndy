﻿using Cliente.Core;
using Cliente.Core.Models;
using HotChocolate;
using System.Threading.Tasks;

namespace Cliente.Api.GraphQL.Resolvers
{
    public class ProductoResolver
    {
        protected readonly IUnitOfWork _unitOfWork;

        public ProductoResolver(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Producto> ObtenerProductoAsync([Parent]ProductoEmpresa producto)
        {
            long id = producto.ProductoId ?? default(long);
            return await _unitOfWork.Producto.GetAsync(id);
        }

        public async Task<Rubro> ObtenerRubroAsync([Parent]Producto producto)
        {
            long id = producto.RubroId ?? default(long);
            return await _unitOfWork.Rubro.GetAsync(id);
        }

        public async Task<Categoria> ObtenerCategoriaAsync([Parent]Rubro rubro)
        {
            long id = rubro.CategoriaId ?? default(long);
            return await _unitOfWork.Categoria.GetAsync(id);
        }
    }
}
