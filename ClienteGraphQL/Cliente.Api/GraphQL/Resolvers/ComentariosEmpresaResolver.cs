﻿using Cliente.Core;
using Cliente.Core.Models;
using HotChocolate;
using System.Threading.Tasks;

namespace Cliente.Api.GraphQL.Resolvers
{
    public class ComentariosEmpresaResovler
    {
        protected readonly IUnitOfWork _unitOfWork;

        public ComentariosEmpresaResovler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Usuario> obtenerUsuarioAsync([Parent]ComentariosEmpresa comentariosEmpresa)
        {
            if(comentariosEmpresa.UsuarioId != null)
            {
                long id = comentariosEmpresa.UsuarioId ?? default(long);
                return await _unitOfWork.Usuario.GetAsync(id);
            }
            return null;
        }

        public async Task<Empresa> obtenerEmpresaAsync([Parent]ComentariosEmpresa comentariosEmpresa)
        {
            if (comentariosEmpresa.UsuarioId != null)
            {
                long id = comentariosEmpresa.UsuarioId ?? default(long);
                return await _unitOfWork.Empresa.GetAsync(id);
            }
            return null;
        }

        public async Task<Empresa> obtenerTipoUsuarioAsync([Parent]ComentariosEmpresa comentariosEmpresa)
        {
            long id = comentariosEmpresa.TipoUsuarioId ?? default(long);
            return await _unitOfWork.Empresa.GetAsync(id);
        }
    }
}
