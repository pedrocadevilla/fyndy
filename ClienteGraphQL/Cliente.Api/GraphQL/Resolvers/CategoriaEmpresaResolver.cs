﻿using Cliente.Core;
using Cliente.Core.Models;
using HotChocolate;
using System.Threading.Tasks;

namespace Cliente.Api.GraphQL.Resolvers
{
    public class CategoriaEmpresaResolver
    {
        protected readonly IUnitOfWork _unitOfWork;

        public CategoriaEmpresaResolver(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Categoria> ObtenerCategoriaAsync([Parent]CategoriaEmpresa categoriaEmpresa)
        {
            long id = categoriaEmpresa.CategoriaId ?? default(long);
            return await _unitOfWork.Categoria.GetAsync(id);
        }

        public async Task<Empresa> ObtenerEmpresaAsync([Parent]ProductoEmpresa productoEmpresa)
        {
            long id = productoEmpresa.EmpresaId ?? default(long);
            return await _unitOfWork.Empresa.GetAsync(id);
        }

        public async Task<Rubro> ObtenerRubroAsync([Parent]RubroEmpresa rubroEmpresa)
        {
            long id = rubroEmpresa.RubroId ?? default(long);
            return await _unitOfWork.Rubro.GetAsync(id);
        }

        public async Task<Producto> ObtenerProductoAsync([Parent]ProductoEmpresa productoEmpresa)
        {
            long id = productoEmpresa.ProductoId ?? default(long);
            return await _unitOfWork.Producto.GetAsync(id);
        }
    }
}
