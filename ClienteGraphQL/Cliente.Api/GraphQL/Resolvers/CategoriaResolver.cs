﻿using Cliente.Core;
using Cliente.Core.Models;
using HotChocolate;
using System.Threading.Tasks;

namespace Cliente.Api.GraphQL.Resolvers
{
    public class CategoriaResolver
    {
        protected readonly IUnitOfWork _unitOfWork;

        public CategoriaResolver(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<EstadisticasCategorias> ObtenerEstadisticasCategoriaAsync([Parent]Categoria categoria)
        {
            long id = categoria.Id;
            return await _unitOfWork.EstadisticasCategorias.GetAsync(id);
        }

    }
}
