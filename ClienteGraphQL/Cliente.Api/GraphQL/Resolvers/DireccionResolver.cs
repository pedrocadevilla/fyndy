﻿using Cliente.Core;
using Cliente.Core.Models;
using HotChocolate;
using System.Threading.Tasks;

namespace Cliente.Api.GraphQL.Resolvers
{
    public class DireccionResolver
    {
        protected readonly IUnitOfWork _unitOfWork;

        public DireccionResolver(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<CodigoPostal> ObtenerCodigoPostalAsync([Parent]Direccion direccion)
        {
            long id = direccion.CodigoPostalId ?? default(long);
            return await _unitOfWork.CodigoPostal.GetAsync(id);
        }

        public async Task<Ciudad> ObtenerCiudadAsync([Parent]CodigoPostal codigo)
        {
            long id = codigo.CiudadId ?? default(long);
            return await _unitOfWork.Ciudad.GetAsync(id);
        }

        public async Task<Localidad> ObtenerLocalidadAsync([Parent]Ciudad ciudad)
        {
            long id = ciudad.LocalidadId ?? default(long);
            return await _unitOfWork.Localidad.GetAsync(id);
        }

        public async Task<Pais> ObtenerPaisAsync([Parent]Localidad localidad)
        {
            long id = localidad.PaisId ?? default(long);
            return await _unitOfWork.Pais.GetAsync(id);
        }
    }
}
