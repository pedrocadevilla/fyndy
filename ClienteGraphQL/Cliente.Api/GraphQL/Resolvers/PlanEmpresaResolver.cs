﻿using Cliente.Core;
using Cliente.Core.Models;
using HotChocolate;
using System.Threading.Tasks;

namespace Cliente.Api.GraphQL.Resolvers
{
    public class PlanEmpresaResolver
    {
        protected readonly IUnitOfWork _unitOfWork;

        public PlanEmpresaResolver(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Planes> ObtenerPlanesAsync([Parent]PlanEmpresa PlanesEmpresa)
        {
            long id = PlanesEmpresa.PlanId ?? default(long);
            return await _unitOfWork.Planes.GetAsync(id);
        }

    }
}
