﻿using Cliente.Core.Models;
using System.Threading.Tasks;

namespace Cliente.Api.GraphQL.Mutations
{
    public partial class Mutation
    {
        public async Task<Pais> CrearPaisAsync(Pais pais)
        {
            _unitOfWork.Pais.Add(pais);
            await _unitOfWork.Complete();
            return pais;
        }

        public async Task<Localidad> CrearLocalidadAsync(Localidad localidad)
        {
            _unitOfWork.Localidad.Add(localidad);
            await _unitOfWork.Complete();
            return localidad;
        }

        public async Task<Ciudad> CrearCiudadAsync(Ciudad ciudad)
        {
            _unitOfWork.Ciudad.Add(ciudad);
            await _unitOfWork.Complete();
            return ciudad;
        }

        public async Task<CodigoPostal> CrearCodigoPostalAsync(CodigoPostal codigo)
        {
            _unitOfWork.CodigoPostal.Add(codigo);
            await _unitOfWork.Complete();
            return codigo;
        }

        public async Task<RedSocial> CrearRedSocialAsync(RedSocial red)
        {
            _unitOfWork.RedSocial.Add(red);
            await _unitOfWork.Complete();
            return red;
        }

        public async Task<Genero> CrearGeneroAsync(Genero genero)
        {
            _unitOfWork.Genero.Add(genero);
            await _unitOfWork.Complete();
            return genero;
        }

        public async Task<EstadoCivil> CrearEstadoCivilAsync(EstadoCivil estado)
        {
            _unitOfWork.EstadoCivil.Add(estado);
            await _unitOfWork.Complete();
            return estado;
        }
    }
}