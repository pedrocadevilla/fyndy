﻿using Cliente.Core;
using Microsoft.Extensions.Configuration;
using System;


namespace Cliente.Api.GraphQL.Mutations
{
    public partial class Mutation
    {
        private readonly IUnitOfWork _unitOfWork;
        private IConfiguration _Configuration;

        public Mutation(IUnitOfWork unitOfWork, IConfiguration Configuration)
        {
            _Configuration = Configuration;
            _unitOfWork = unitOfWork
                ?? throw new ArgumentNullException(nameof(unitOfWork));
        }
    }
}