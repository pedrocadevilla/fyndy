﻿using Cliente.Core.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cliente.Api.GraphQL.Mutations
{
    public partial class Mutation
    {
        public async Task<Empresa> CrearEmpresaAsync(Empresa empresa, Direccion direccion)
        {
            var empresaExiste = await _unitOfWork.Empresa.empresaExiste(empresa.Correo);
            if (empresaExiste == null)
            {
                _unitOfWork.Empresa.Add(empresa);
                await _unitOfWork.Complete();
                Direccion direccionEmpresa = direccion;
                direccionEmpresa.EmpresaId = empresa.Id;
                direccionEmpresa.Primaria = true;
                _unitOfWork.Direccion.Add(direccionEmpresa);
                await _unitOfWork.Complete();
                return empresa;
            }
            return null;
        }

        public async Task<Empresa> ModificarPalabraEmpresaAsync(long id, string palabra, string palabraEditada)
        {
            Empresa empresaActual = await _unitOfWork.Empresa.GetAsync(id);
            if (empresaActual.PalabrasClave != null)
            {
                if(String.IsNullOrEmpty(palabraEditada)) {
                    string[] palabras = empresaActual.PalabrasClave.Split(',');
                    string palabrasClave = "";
                    foreach(string pal in palabras)
                    {
                        if (!pal.Equals(palabra))
                            palabrasClave += pal.ToUpper() + ",";
                    }
                    empresaActual.PalabrasClave = palabrasClave.Remove(palabrasClave.Length-1);
                    await _unitOfWork.Complete();
                }
                else
                {
                    empresaActual.PalabrasClave = empresaActual.PalabrasClave.Replace(palabra, palabraEditada.ToUpper());
                    await _unitOfWork.Complete();
                }
            }

            if (empresaActual == null)
            {
                throw new ArgumentException("Elemento no encontrado en la base de datos");
            }


            return empresaActual;
        }

        public async Task<Empresa> ModificarEmpresaAsync(long id, Empresa empresa)
        {
            Empresa empresaActual = await _unitOfWork.Empresa.GetAsync(id);
            empresa.Id = empresaActual.Id;
            if (empresa.PalabrasClave != null && empresaActual.PalabrasClave != null)
            {
                string[] vecPalabras = empresa.PalabrasClave.Split(',');
                empresa.PalabrasClave = empresaActual.PalabrasClave;
                if (vecPalabras.Length > 1)
                {
                    foreach(string palabra in vecPalabras)
                    {
                        if (!empresaActual.PalabrasClave.Contains(palabra))
                        {
                            empresa.PalabrasClave += ',' + palabra.ToUpper();
                        }
                    }
                }
                else
                {
                    if (!empresaActual.PalabrasClave.Contains(vecPalabras[0]))
                    {
                        empresa.PalabrasClave += ',' + vecPalabras[0].ToUpper();
                    }
                }
            }
            else
            {
                if(empresaActual.PalabrasClave != null)
                    empresa.PalabrasClave = empresaActual.PalabrasClave.ToUpper();
            }

            if (empresaActual == null)
            {
                throw new ArgumentException("Elemento no encontrado en la base de datos");
            }

            var empresaNueva = JsonConvert.SerializeObject(empresa, Newtonsoft.Json.Formatting.None,
                                                new JsonSerializerSettings
                                                { NullValueHandling = NullValueHandling.Ignore });

            JsonConvert.PopulateObject(empresaNueva, empresaActual);
            
            await _unitOfWork.Complete();
            return empresaActual;
        }

        public async Task<bool> ModificarHorarioEmpresaAsync(long empresaId, long[] horarioId)
        {
            IEnumerable<HorarioContacto> horarioXEliminar = await _unitOfWork.HorarioContacto.ObtenerHorariosXEliminar(empresaId, horarioId);
            if(horarioXEliminar != null)_unitOfWork.HorarioContacto.RemoveRange(horarioXEliminar);
            foreach(long id in horarioId)
            {
                HorarioContacto horario = await _unitOfWork.HorarioContacto.ObtenerXIdHorario(empresaId, id);
                if(horario == null)
                {
                    HorarioContacto nuevo = new HorarioContacto();
                    nuevo.EmpresaId = empresaId;
                    nuevo.HorarioId = id;
                    _unitOfWork.HorarioContacto.Add(nuevo);
                }
            }
            await _unitOfWork.Complete();
            return true;
        }

        public async Task<DatosContacto> ModificarContactoEmpresaAsync(DatosContacto contacto, long? contactoId)
        {
            if (contactoId != null)
            {
                DatosContacto contactoActual = await _unitOfWork.DatosContacto.GetAsync(contactoId ?? 0);
                contacto.Id = contactoActual.Id;

                var contactoNuevo = JsonConvert.SerializeObject(contacto, Newtonsoft.Json.Formatting.None,
                                                new JsonSerializerSettings
                                                { NullValueHandling = NullValueHandling.Ignore });

                JsonConvert.PopulateObject(contactoNuevo, contactoActual);
                await _unitOfWork.Complete();
                return contactoActual;
            }
            else
            {
                _unitOfWork.DatosContacto.Add(contacto);
                await _unitOfWork.Complete();
                return contacto;
            }
        }

        public async Task<RedSocialEmpresa> ModificarRedSocialEmpresaAsync(RedSocialEmpresa redSocial, long? redSocialId)
        {
            if (redSocialId != null)
            {
                RedSocialEmpresa redSocialActual = await _unitOfWork.RedSocialEmpresa.GetAsync(redSocialId ?? 0);
                redSocial.Id = redSocialActual.Id;

                var redSocialNueva = JsonConvert.SerializeObject(redSocial, Newtonsoft.Json.Formatting.None,
                                                new JsonSerializerSettings
                                                { NullValueHandling = NullValueHandling.Ignore });

                JsonConvert.PopulateObject(redSocialNueva, redSocialActual);
                await _unitOfWork.Complete();
                return redSocialActual;
            }
            else
            {
                _unitOfWork.RedSocialEmpresa.Add(redSocial);
                await _unitOfWork.Complete();
                return redSocial;
            }
        }
    }
}