﻿using Cliente.Core.Models;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace Cliente.Api.GraphQL.Mutations
{
    public partial class Mutation
    {
        public async Task<Usuario> CrearUsuarioAsync(Usuario usuario)
        {
            var usuarioExiste = await _unitOfWork.Usuario.usuarioExiste(usuario.Correo);
            if(usuarioExiste == null)
            {
                _unitOfWork.Usuario.Add(usuario);
                await _unitOfWork.Complete();
                return usuario;
            }
            return null;
        }

        public async Task<Usuario> ModificarUsuarioAsync(long id, Usuario usuario)
        {
            Usuario usuarioActual = await _unitOfWork.Usuario.GetAsync(id);
            usuario.Id = usuarioActual.Id;
            if (usuarioActual == null)
            {
                throw new ArgumentException("Elemento no encontrado en la base de datos");
            }

            var usuarioNueva = JsonConvert.SerializeObject(usuario, Newtonsoft.Json.Formatting.None,
                                                new JsonSerializerSettings
                                                { NullValueHandling = NullValueHandling.Ignore });

            JsonConvert.PopulateObject(usuarioNueva, usuarioActual);
            
            await _unitOfWork.Complete();
            return usuarioActual;
        }
    }
}