﻿using Cliente.Api.Utilidades;
using Cliente.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cliente.Api.GraphQL.Mutations
{
    public partial class Mutation
    {
        public async Task<Boolean> CrearPermutacionesAsync()
        {
            List<Palabras> PalabrasProducto = await _unitOfWork.Palabras.BuscarPalabrasProducto();
            List<Palabras> PalabrasEmpresa = await _unitOfWork.Palabras.BuscarPalabrasEmpresa();
            List<string> Palabras = new List<string>();
            List<string> Variaciones = new List<string>();
            Variaciones ClaseVariaciones = new Variaciones();
            int cont, index = 0;
            long EmpresaId = 0, ProductoId = 0;

            foreach (Palabras Palabra in PalabrasProducto)
            {
                if (index == 0) ProductoId = Palabra.ProductoId ?? default(long);
                index++;
                if (Palabra.ProductoId.Equals(ProductoId) && index < PalabrasProducto.Count)
                {
                    Palabras.Add(Palabra.Palabra);
                    Variaciones.Add(Palabra.Palabra);
                }
                else
                {
                    if (index < PalabrasProducto.Count)
                    {
                        cont = 2;
                        do
                        {
                            ClaseVariaciones.VariacionesSinRepeticion(ref Palabras, Variaciones, cont, 0);
                            cont++;
                        } while (cont <= Variaciones.Count);

                        foreach (var Frase in Palabras)
                        {
                            PermutacionesProductoEmpresa PermutacionesProducto = new PermutacionesProductoEmpresa();
                            PermutacionesProducto.ProductoId = ProductoId;
                            var containInterno = await _unitOfWork.Permutaciones.ExistePermutacion(Frase);
                            if (containInterno != true)
                            {
                                Permutaciones Permutacion = new Permutaciones();
                                Permutacion.Frase = Frase;
                                _unitOfWork.Permutaciones.Add(Permutacion);
                                await _unitOfWork.Complete();
                                PermutacionesProducto.PermutacionId = Permutacion.Id;
                                _unitOfWork.PermutacionesProductoEmpresa.Add(PermutacionesProducto);
                                await _unitOfWork.Complete();
                            }
                            else
                            {
                                var IdPermutacion = await _unitOfWork.Permutaciones.IdPermutacion(Frase);
                                PermutacionesProducto.PermutacionId = IdPermutacion;
                                _unitOfWork.PermutacionesProductoEmpresa.Add(PermutacionesProducto);
                                await _unitOfWork.Complete();
                            }
                        }
                        Palabras.Clear();
                        Variaciones.Clear();
                        ProductoId = Palabra.ProductoId ?? default(long);
                        Palabras.Add(Palabra.Palabra);
                        Variaciones.Add(Palabra.Palabra);
                    }
                    else
                    {
                        Palabras.Add(Palabra.Palabra);
                        Variaciones.Add(Palabra.Palabra);
                        cont = 2;
                        do
                        {
                            ClaseVariaciones.VariacionesSinRepeticion(ref Palabras, Variaciones, cont, 0);
                            cont++;
                        } while (cont <= Variaciones.Count);

                        foreach (var Frase in Palabras)
                        {
                            PermutacionesProductoEmpresa PermutacionesProducto = new PermutacionesProductoEmpresa();
                            PermutacionesProducto.ProductoId = ProductoId;
                            var containInterno = await _unitOfWork.Permutaciones.ExistePermutacion(Frase);
                            if (containInterno != true)
                            {
                                Permutaciones Permutacion = new Permutaciones();
                                Permutacion.Frase = Frase;
                                _unitOfWork.Permutaciones.Add(Permutacion);
                                await _unitOfWork.Complete();
                                PermutacionesProducto.PermutacionId = Permutacion.Id;
                                _unitOfWork.PermutacionesProductoEmpresa.Add(PermutacionesProducto);
                                await _unitOfWork.Complete();
                            }
                            else
                            {
                                var IdPermutacion = await _unitOfWork.Permutaciones.IdPermutacion(Frase);
                                PermutacionesProducto.PermutacionId = IdPermutacion;
                                _unitOfWork.PermutacionesProductoEmpresa.Add(PermutacionesProducto);
                                await _unitOfWork.Complete();
                            }
                        }
                    }
                }
            }

            index = 0;
            Palabras.Clear();
            Variaciones.Clear();

            foreach (var Palabra in PalabrasEmpresa)
            {
                if (index == 0) EmpresaId = Palabra.EmpresaId ?? default(long);
                index++;

                if (Palabra.EmpresaId.Equals(EmpresaId) && index < PalabrasEmpresa.Count)
                {
                    Palabras.Add(Palabra.Palabra);
                    Variaciones.Add(Palabra.Palabra);
                }
                else
                {
                    if (index < PalabrasEmpresa.Count)
                    {
                        cont = 2;
                        do
                        {
                            ClaseVariaciones.VariacionesSinRepeticion(ref Palabras, Variaciones, cont, 0);
                            cont++;
                        } while (cont <= Variaciones.Count);

                        foreach (var Frase in Palabras)
                        {
                            PermutacionesProductoEmpresa PermutacionesEmpresa = new PermutacionesProductoEmpresa();
                            PermutacionesEmpresa.EmpresaId = EmpresaId;
                            var containInterno = await _unitOfWork.Permutaciones.ExistePermutacion(Frase);
                            if (containInterno != true)
                            {
                                Permutaciones Permutacion = new Permutaciones();
                                Permutacion.Frase = Frase;
                                _unitOfWork.Permutaciones.Add(Permutacion);
                                await _unitOfWork.Complete();
                                PermutacionesEmpresa.PermutacionId = Permutacion.Id;
                                _unitOfWork.PermutacionesProductoEmpresa.Add(PermutacionesEmpresa);
                                await _unitOfWork.Complete();
                            }
                            else
                            {
                                var IdPermutacion = await _unitOfWork.Permutaciones.IdPermutacion(Frase);
                                PermutacionesEmpresa.PermutacionId = IdPermutacion;
                                _unitOfWork.PermutacionesProductoEmpresa.Add(PermutacionesEmpresa);
                                await _unitOfWork.Complete();
                            }
                        }
                        Palabras.Clear();
                        Variaciones.Clear();
                        EmpresaId = Palabra.EmpresaId ?? default(long);
                        Palabras.Add(Palabra.Palabra);
                        Variaciones.Add(Palabra.Palabra);
                    }
                    else
                    {
                        Palabras.Add(Palabra.Palabra);
                        Variaciones.Add(Palabra.Palabra);
                        cont = 2;
                        do
                        {
                            ClaseVariaciones.VariacionesSinRepeticion(ref Palabras, Variaciones, cont, 0);
                            cont++;
                        } while (cont <= Variaciones.Count);

                        foreach (var Frase in Palabras)
                        {
                            PermutacionesProductoEmpresa PermutacionesEmpresa = new PermutacionesProductoEmpresa();
                            PermutacionesEmpresa.EmpresaId = EmpresaId;
                            var containInterno = await _unitOfWork.Permutaciones.ExistePermutacion(Frase);
                            if (containInterno != true)
                            {
                                Permutaciones Permutacion = new Permutaciones();
                                Permutacion.Frase = Frase;
                                _unitOfWork.Permutaciones.Add(Permutacion);
                                await _unitOfWork.Complete();
                                PermutacionesEmpresa.PermutacionId = Permutacion.Id;
                                _unitOfWork.PermutacionesProductoEmpresa.Add(PermutacionesEmpresa);
                                await _unitOfWork.Complete();
                            }
                            else
                            {
                                var IdPermutacion = await _unitOfWork.Permutaciones.IdPermutacion(Frase);
                                PermutacionesEmpresa.PermutacionId = IdPermutacion;
                                _unitOfWork.PermutacionesProductoEmpresa.Add(PermutacionesEmpresa);
                                await _unitOfWork.Complete();
                            }
                        }
                    }
                }
            }
            return true;
        }
    }
}