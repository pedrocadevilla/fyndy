﻿using Cliente.Core.Models;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace Cliente.Api.GraphQL.Mutations
{
    public partial class Mutation
    {
        public async Task<Rubro> CrearRubroAsync(Rubro rubro)
        {
            rubro.FechaCreacion = DateTime.Now;
            _unitOfWork.Rubro.Add(rubro);
            await _unitOfWork.Complete();
            return rubro;
        }

        public async Task<Rubro> ModificarRubroAsync(long id, Rubro rubro)
        {
            Rubro rubroActual = await _unitOfWork.Rubro.GetAsync(id);
            rubro.Id = rubroActual.Id;
            if (rubroActual == null)
            {
                throw new ArgumentException("Elemento no encontrado en la base de datos");
            }

            var rubroNueva = JsonConvert.SerializeObject(rubro, Newtonsoft.Json.Formatting.None,
                                                new JsonSerializerSettings
                                                { NullValueHandling = NullValueHandling.Ignore });

            JsonConvert.PopulateObject(rubroNueva, rubroActual);

            await _unitOfWork.Complete();
            return rubroActual;
        }

        public async Task<string> EliminarRubroAsync(long id)
        {
            Rubro rubro = new Rubro();
            Rubro rubroActual = await _unitOfWork.Rubro.GetAsync(id);
            if (rubroActual == null)
            {
                throw new ArgumentException("Elemento no encontrado en la base de datos");
            }
            rubro.Id = rubroActual.Id;
            rubro.FechaEliminacion = DateTime.Now;

            var rubroNueva = JsonConvert.SerializeObject(rubro, Newtonsoft.Json.Formatting.None,
                                                new JsonSerializerSettings
                                                { NullValueHandling = NullValueHandling.Ignore });

            JsonConvert.PopulateObject(rubroNueva, rubroActual);

            await _unitOfWork.Complete();
            return "Registro Eliminado.";
        }
    }
}