﻿using Cliente.Core.Models;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace Cliente.Api.GraphQL.Mutations
{
    public partial class Mutation
    {
        public async Task<Producto> CrearProductoAsync(Producto producto)
        {
            if (producto.Nombre.Contains(","))
            {
                var productos = producto.Nombre.Split(",");
                foreach(string p in productos)
                {
                    Producto prod = new Producto();
                    prod.Nombre = p;
                    prod.RubroId = producto.RubroId;
                    prod.FechaCreacion = DateTime.Now;
                    _unitOfWork.Producto.Add(producto);
                    await _unitOfWork.Complete();
                }
                await _unitOfWork.Complete();
                return producto;
            }
            else
            {
                producto.FechaCreacion = DateTime.Now;
                _unitOfWork.Producto.Add(producto);
                await _unitOfWork.Complete();
                return producto;
            }
            
        }

        public async Task<Producto> ModificarProductoAsync(long id, Producto producto)
        {
            Producto productoActual = await _unitOfWork.Producto.GetAsync(id);
            producto.Id = productoActual.Id;
            if (productoActual == null)
            {
                throw new ArgumentException("Elemento no encontrado en la base de datos");
            }

            var productoNueva = JsonConvert.SerializeObject(producto, Newtonsoft.Json.Formatting.None,
                                                new JsonSerializerSettings
                                                { NullValueHandling = NullValueHandling.Ignore });

            JsonConvert.PopulateObject(productoNueva, productoActual);

            await _unitOfWork.Complete();
            return productoActual;
        }

        public async Task<string> EliminarProductoAsync(long id)
        {
            Producto producto = new Producto();
            Producto productoActual = await _unitOfWork.Producto.GetAsync(id);
            if (productoActual == null)
            {
                throw new ArgumentException("Elemento no encontrado en la base de datos");
            }
            producto.Id = productoActual.Id;
            producto.FechaEliminacion = DateTime.Now;
            
            var productoNueva = JsonConvert.SerializeObject(producto, Newtonsoft.Json.Formatting.None,
                                                new JsonSerializerSettings
                                                { NullValueHandling = NullValueHandling.Ignore });

            JsonConvert.PopulateObject(productoNueva, productoActual);

            await _unitOfWork.Complete();
            return "Registro Eliminado.";
        }
    }
}