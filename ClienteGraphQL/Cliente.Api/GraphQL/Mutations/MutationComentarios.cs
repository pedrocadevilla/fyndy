﻿using Cliente.Core.Models;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace Cliente.Api.GraphQL.Mutations
{
    public partial class Mutation
    {
        public async Task<ComentariosEmpresa> CrearComentario(ComentariosEmpresa comentario, string tipo)
        {
            TipoUsuario tipoId = await _unitOfWork.TipoUsuario.ObtenerTipoUsuario(tipo);
            comentario.TipoUsuarioId = tipoId.Id;
            comentario.Fecha = DateTime.Now;
            _unitOfWork.ComentariosEmpresa.Add(comentario);
            await _unitOfWork.Complete();
            return comentario;
        }

        public async Task<ComentariosEmpresa> ModificaComentarioAsync(long id, ComentariosEmpresa comentario)
        {
            ComentariosEmpresa comentarioActual = await _unitOfWork.ComentariosEmpresa.GetAsync(id);
            if (comentarioActual == null)
            {
                throw new ArgumentException("Elemento no encontrado en la base de datos");
            }
            comentario.Id = comentarioActual.Id;

            var comentarioNuevo = JsonConvert.SerializeObject(comentario, Newtonsoft.Json.Formatting.None,
                                                new JsonSerializerSettings
                                                { NullValueHandling = NullValueHandling.Ignore });

            JsonConvert.PopulateObject(comentarioNuevo, comentarioActual);

            await _unitOfWork.Complete();
            return comentarioActual;
        }

        public async Task<string> EliminarComentarioAsync(long id)
        {
            ComentariosEmpresa comentario = new ComentariosEmpresa();
            ComentariosEmpresa comentarioActual = await _unitOfWork.ComentariosEmpresa.GetAsync(id);
            if (comentarioActual == null)
            {
                throw new ArgumentException("Elemento no encontrado en la base de datos");
            }
            comentario.Id = comentarioActual.Id;
            comentario.FechaEliminacion = DateTime.Now;

            var categoriaNueva = JsonConvert.SerializeObject(comentario, Newtonsoft.Json.Formatting.None,
                                                new JsonSerializerSettings
                                                { NullValueHandling = NullValueHandling.Ignore });

            JsonConvert.PopulateObject(categoriaNueva, comentarioActual);

            await _unitOfWork.Complete();
            return "Registro Eliminado.";
        }
    }
}