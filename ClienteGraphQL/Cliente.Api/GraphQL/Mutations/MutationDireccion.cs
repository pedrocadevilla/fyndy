﻿using Cliente.Core.Models;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace Cliente.Api.GraphQL.Mutations
{
    public partial class Mutation
    {
        public async Task<Direccion> CrearDireccionAsync(Direccion direccion)
        {
            _unitOfWork.Direccion.Add(direccion);
            await _unitOfWork.Complete();
            return direccion;
        }

        public async Task<Direccion> ModificarDireccionAsync(long id, Direccion direccion)
        {
            Direccion direccionActual = await _unitOfWork.Direccion.GetAsync(id);
            direccion.Id = direccionActual.Id;
            if (direccionActual == null)
            {
                throw new ArgumentException("Elemento no encontrado en la base de datos");
            }

            var direccionNueva = JsonConvert.SerializeObject(direccion, Newtonsoft.Json.Formatting.None,
                                                new JsonSerializerSettings
                                                { NullValueHandling = NullValueHandling.Ignore });

            JsonConvert.PopulateObject(direccionNueva, direccionActual);
            
            await _unitOfWork.Complete();
            return direccionActual;
        }
    }
}