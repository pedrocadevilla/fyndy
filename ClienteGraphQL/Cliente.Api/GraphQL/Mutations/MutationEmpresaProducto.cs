﻿using Cliente.Core.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cliente.Api.GraphQL.Mutations
{
    public partial class Mutation
    {
        public async Task<IEnumerable<ProductoEmpresa>> CrearProductoEmpresaAsync(IEnumerable<ProductoEmpresa> producto, RubroEmpresa rubro, CategoriaEmpresa categoria)
        {
            var categoriaExiste = await _unitOfWork.CategoriaEmpresa.obtenerCategoriaEmpresaAsync(categoria.CategoriaId, categoria.EmpresaId);
            if(categoriaExiste == null)
            {
                _unitOfWork.CategoriaEmpresa.Add(categoria);
                await _unitOfWork.Complete();
                rubro.CategoriaEmpresaId = categoria.Id;
                _unitOfWork.RubroEmpresa.Add(rubro);
                await _unitOfWork.Complete();
                Parallel.ForEach(producto, (x) => { x.RubroEmpresaId = rubro.Id; x.EmpresaId = categoria.EmpresaId; x.FechaCreacion = DateTime.Now;  });
                _unitOfWork.ProductoEmpresa.AddRange(producto);
                await _unitOfWork.Complete();
                foreach(ProductoEmpresa p in producto)
                {
                    EstadisticasProductoEmpresa estadistica = new EstadisticasProductoEmpresa();
                    estadistica.EstadisticaId = _Configuration.GetSection("EstadisticaId:BusquedaProductos").Get<long>();
                    estadistica.ProductoId = p.Id;
                    estadistica.Valor = "0";
                    _unitOfWork.EstadisticasProductoEmpresa.Add(estadistica);
                    await _unitOfWork.Complete();
                }
                return producto;
                
            }
            else
            {
                var rubroExiste = await _unitOfWork.RubroEmpresa.ObtenerRubroEmpresaAsync(rubro.RubroId, categoriaExiste.Id);
                if (rubroExiste == null)
                {
                    rubro.CategoriaEmpresaId = categoriaExiste.Id;
                    _unitOfWork.RubroEmpresa.Add(rubro);
                    await _unitOfWork.Complete();
                    Parallel.ForEach(producto, (x) => { x.RubroEmpresaId = rubro.Id; x.FechaCreacion = DateTime.Now; });
                    _unitOfWork.ProductoEmpresa.AddRange(producto);
                    await _unitOfWork.Complete();
                    return producto;
                }
                else
                {
                    Parallel.ForEach(producto, (x) => { x.RubroEmpresaId = rubroExiste.Id; x.EmpresaId = categoriaExiste.EmpresaId; x.FechaCreacion = DateTime.Now; });
                    var productosEmpresa = await _unitOfWork.ProductoEmpresa.FindAsync(x => x.RubroEmpresaId == rubroExiste.Id);
                    var productosId = productosEmpresa.Select(x => x.ProductoId);
                    var productosFinales = producto.Where(x => !productosId.Contains(x.ProductoId));
                    _unitOfWork.ProductoEmpresa.AddRange(productosFinales);
                    await _unitOfWork.Complete();
                    return producto;
                }
            }
            
        }

        public async Task<IEnumerable<ProductoEmpresa>> ModificarProductoEmpresaAsync(long id, IEnumerable<ProductoEmpresa> producto, RubroEmpresa rubro, CategoriaEmpresa categoria)
        {
            ProductoEmpresa productoEliminar = await _unitOfWork.ProductoEmpresa.GetAsync(id);
            _unitOfWork.ProductoEmpresa.Remove(productoEliminar);
            await _unitOfWork.Complete();
            var productoModificado = await CrearProductoEmpresaAsync(producto, rubro, categoria);
            return productoModificado;
        }

        public async Task<string> EliminarProductoEmpresaAsync(long id)
        {
            ProductoEmpresa producto = await _unitOfWork.ProductoEmpresa.GetAsync(id);
            if (producto != null) {
                _unitOfWork.ProductoEmpresa.Remove(producto);
                await _unitOfWork.Complete();
                return "Registro Eliminado.";
            }
            else
            {
                return "Registro no encontrado.";
            }
        }
    }
}