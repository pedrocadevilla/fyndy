﻿using Cliente.Core.Models;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace Cliente.Api.GraphQL.Mutations
{
    public partial class Mutation
    {
        public async Task<Categoria> CrearCategoriaAsync(Categoria categoria)
        {
            categoria.FechaCreacion = DateTime.Now;
            _unitOfWork.Categoria.Add(categoria);
            await _unitOfWork.Complete();
            return categoria;
        }

        public async Task<Categoria> ModificarCategoriaAsync(long id, Categoria categoria)
        {
            Categoria categoriaActual = await _unitOfWork.Categoria.GetAsync(id);
            categoria.Id = categoriaActual.Id;
            if (categoriaActual == null)
            {
                throw new ArgumentException("Elemento no encontrado en la base de datos");
            }

            var categoriaNueva = JsonConvert.SerializeObject(categoria, Newtonsoft.Json.Formatting.None,
                                                new JsonSerializerSettings
                                                { NullValueHandling = NullValueHandling.Ignore });

            JsonConvert.PopulateObject(categoriaNueva, categoriaActual);

            await _unitOfWork.Complete();
            return categoriaActual;
        }

        public async Task<string> EliminarCategoriaAsync(long id)
        {
            Categoria categoria = new Categoria();
            Categoria categoriaActual = await _unitOfWork.Categoria.GetAsync(id);
            if (categoriaActual == null)
            {
                throw new ArgumentException("Elemento no encontrado en la base de datos");
            }
            categoria.Id = categoriaActual.Id;
            categoria.FechaEliminacion = DateTime.Now;

            var categoriaNueva = JsonConvert.SerializeObject(categoria, Newtonsoft.Json.Formatting.None,
                                                new JsonSerializerSettings
                                                { NullValueHandling = NullValueHandling.Ignore });

            JsonConvert.PopulateObject(categoriaNueva, categoriaActual);

            await _unitOfWork.Complete();
            return "Registro Eliminado.";
        }
    }
}