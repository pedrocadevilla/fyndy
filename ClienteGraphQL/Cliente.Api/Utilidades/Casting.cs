﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cliente.Api.Utilidades
{
    class Casting
    {
        private IConfiguration _Configuration;

        public Casting(IConfiguration Config)
        {
            _Configuration = Config;
        }

        public U Convertir<T, U>(T objeto) where U : new()
        {
            U ObjetoFinal = new U();
            Type Propiedades = objeto.GetType();
            List<string> CamposRestringidos = _Configuration.GetSection("List:CamposRestringidos").Get<List<string>>();
            var Campos = Propiedades.GetProperties().Where(x => !x.PropertyType.ToString().Contains("ICollection") && !CamposRestringidos.Contains(x.Name) && !x.PropertyType.ToString().Contains("Cliente.Core.Models") && objeto.GetType().GetProperty(x.Name).GetValue(objeto, null)?.ToString() != null).ToList();
            var NombresCampos = Campos.Select(x => x.Name);
            var CamposFinal = ObjetoFinal.GetType().GetProperties().Where(x => NombresCampos.Contains(x.Name)).ToList();
            CamposFinal.ForEach(x => ObjetoFinal.GetType().GetProperty(x.Name).SetValue(ObjetoFinal, objeto.GetType().GetProperty(x.Name).GetValue(objeto, null)));
            return ObjetoFinal;
        }
    }
}
