﻿using System.Collections.Generic;

namespace Cliente.Api.Utilidades
{
    class Variaciones
    {
        public Variaciones(){
            
        }

        public void VariacionesSinRepeticion(ref List<string> Original, List<string> Variaciones, int size, int pos)
        {
            int band = 0;
            string temp = "";
            if (pos == size)
            {
                temp = "";
                foreach (var Palabra in Variaciones)
                {
                    if (temp != "" && band < size)
                    {
                        temp = temp + ' ' + Palabra;
                        band++;
                    }
                    else if (band < size)
                    {
                        temp = Palabra;
                        band++;
                    }
                }
                if (!Original.Contains(temp))
                {
                    Original.Add(temp);
                }
            }
            else
            {
                for (int i = pos; i < Variaciones.Count; i++)
                {
                    VariacionesSinRepeticion(ref Original, Variaciones, size, pos + 1);
                }
            }
        }
    }
}
