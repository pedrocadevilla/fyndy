﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Cliente.Api.Utilidades
{
    class Comparador
    {
        private IConfiguration _Configuration;

        public Comparador(IConfiguration Config)
        {
            _Configuration = Config;
        }

        public IEnumerable<PropertyInfo> CompararTablasAsync<T, U>(T objetoActual, U objetoComparador)
        {
            Type actual = objetoActual.GetType();
            List<string> CamposRestringidos = _Configuration.GetSection("List:CamposRestringidosComparar").Get<List<string>>();
            var camposComparadores = actual.GetProperties().Where(x => !x.PropertyType.ToString().Contains("ICollection") && !CamposRestringidos.Contains(x.Name) && !x.PropertyType.ToString().Contains("Cliente.Core.Models") && objetoActual.GetType().GetProperty(x.Name).GetValue(objetoActual, null)?.ToString() != null);
            var camposDistintos = camposComparadores.Where(x => objetoComparador.GetType().GetProperty(x.Name).GetValue(objetoComparador, null)?.ToString() != objetoActual.GetType().GetProperty(x.Name).GetValue(objetoActual, null)?.ToString());

            return camposDistintos;
        }
    }
}
