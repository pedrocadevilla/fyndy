function getLocation() {
    if (navigator.geolocation && sacarLocalStorage("ubicacionPais") == null) {
        navigator.geolocation.getCurrentPosition(solicitarPaisGeoCode);
    } else {
        //
    }
}

function validarMultiple(elem) {
    var opcion = elem;
    if (opcion.selectedOptions.length > 1 || (opcion.selectedOptions.length >= 1 && opcion.options[0].selected === false)) {
        if (opcion.options[1].selected == true && sacarLocalStorage("seleccion") === "0") {
            selectAll(elem);
        }
        if (opcion.options[1].selected == false && sacarLocalStorage("seleccion") === "1") {
            selectAll(elem);
        }
        $('select').formSelect();
        opcion.options[0].selected = false;
    } else {
        opcion.options[0].selected = true;
    }
}

function selectAll(elem) {
    var opciones = elem.options;
    if (sacarLocalStorage("seleccion") === "0") {
        for (var i=0; i < opciones.length; i++) {
            opciones[i].selected = true;
        }
        almacenarLocalStorage("seleccion", 1);
        opciones[0].selected = false;
    } else {
        for (var i=0; i < opciones.length; i++) {
            opciones[i].selected = false;
        }
        almacenarLocalStorage("seleccion", 0);
        opciones[0].selected = true;
    }
}

function solicitarPaisGeoCode(position) {
    var createUrl = 'https://api.bigdatacloud.net/data/reverse-geocode-client?latitude='+position.coords.latitude+'&longitude='+position.coords.longitude+'&localityLanguage=es';
    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: createUrl,
        dataType: "json",
        success: function(data, textStatus, jqXHR) {
            if (data.countryName != "") {
                almacenarLocalStorage("ubicacionPais", data.countryName)
            } else {
                almacenarLocalStorage("ubicacionPais", "")
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            mensajeError()
        }
    });
}

function resizebase64(base64, maxWidth, maxHeight, id) {
    // Create original image
    var img = new Image();
    img.src = base64;
    img.onload = function() {
        // Max size for thumbnail
        if (typeof(maxWidth) === 'undefined') maxWidth = 500;
        if (typeof(maxHeight) === 'undefined') maxHeight = 500;

        // Create and initialize two canvas
        var canvas = document.createElement("canvas");
        var ctx = canvas.getContext("2d");
        var canvasCopy = document.createElement("canvas");
        var copyContext = canvasCopy.getContext("2d");
        // Determine new ratio based on max size
        var ratio = 1;
        if (img.width > maxWidth)
            ratio = maxWidth / img.width;
        else if (img.height > maxHeight)
            ratio = maxHeight / img.height;

        // Draw original image in second canvas
        canvasCopy.width = img.width;
        canvasCopy.height = img.height;
        copyContext.drawImage(img, 0, 0);

        // Copy and resize second canvas to first canvas
        canvas.width = img.width * ratio;
        canvas.height = img.height * ratio;
        ctx.drawImage(canvasCopy, 0, 0, canvasCopy.width, canvasCopy.height, 0, 0, canvas.width, canvas.height);
        almacenarLocalStorage(id, canvas.toDataURL("image/jpeg", 0.5));
        return canvas.toDataURL();
    };

}

function cargarImagenPerfil(){
    $('#imgPerfilInput').trigger('click');
}

function cargarImagenCarrete(){
    $('#imgCarreteInput').trigger('click');
}

function borrarLocalStorage(key) {
    var valoutput;
    if (typeof(window.localStorage) != 'undefined') {
        valoutput = window.localStorage.removeItem(key);
    } else {
        throw "window.localStorage, not defined";
    }
    return valoutput;
}

function busquedaLista(idLista, input) {
    filter = input.value.toUpperCase();
    ul = document.getElementById(idLista);
    li = ul.getElementsByTagName('li');

    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("span")[0];
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}

function modalUbicacion(option) {
    if (option.value == 1) {
        almacenarLocalStorage("tipoFiltro", 1);
        $('#modalFiltros').modal('open');
    }
    if (option.value == 2) {
        almacenarLocalStorage("tipoFiltro", 2);
        $("#estado").removeAttr("style");
        $('#modalFiltros').modal('open');
    }
    if (option.value == 3) {
        almacenarLocalStorage("tipoFiltro", 3);
        $("#estado").removeAttr("style");
        $("#ciudad").removeAttr("style");
        $('#modalFiltros').modal('open');
    }
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function busquedaListaProductosEmpresa(input) {
    filter = input.value.toUpperCase();
    var datos = [];
    var data = JSON.parse(sacarLocalStorage("datosProductosEmpresa"));
    $("#listaProductosRegistrados").html("");
    if (data.consultarProductosEmpresa.nodes.length > 0) {
        var obj = data.consultarProductosEmpresa.nodes;
        obj.forEach(function(categoria) {
            var obj2 = categoria.rubroEmpresa.nodes;
            obj2.forEach(function(rubro) {
                var obj3 = rubro.productoEmpresa.nodes;
                obj3.forEach(function(producto) {
                    if (producto.obtenerProducto.nombre.toUpperCase().indexOf(filter) > -1) {
                        datos.push([producto.obtenerProducto.nombre, producto.id]);
                    }
                });
            });

        });
    }
    almacenarLocalStorage("prueba", JSON.stringify(datos))
    $("#paginationContainer").html();
    $('#paginationContainer').pagination({
        dataSource: datos,
        pageSize: 9,
        callback: function(data, pagination) {
            var html = templateRegistradosBusquedaEmpresa(data);
            $("#listaProductosRegistrados").html(html);
        }
    });
    $('#paginationContainer').addHook('afterPreviousOnClick', function() {
        $('html, body').animate({ scrollTop: 0 }, 500, 'swing');
    });
    $('#paginationContainer').addHook('afterPageOnClick', function() {
        $('html, body').animate({ scrollTop: 0 }, 500, 'swing');
    });
    $('#paginationContainer').addHook('afterNextOnClick', function() {
        $('html, body').animate({ scrollTop: 0 }, 500, 'swing');
    });
}

function templateRegistradosBusquedaEmpresa(data) {
    var html = "";
    var i = 0;
    data.forEach(function(producto) {
        html += '<li class="collection-item"><div><span>' + data[i][0] + '</span><a href="#!" class="secondary-content"><i class="material-icons pointer botonEliminar" onclick="eliminarProducto(' + data[i][1] + ')">delete</i></a></div></li>'
        i++;
    });
    return html;
}

function titleCase(str) {
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
        splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    return splitStr.join(' ');
}

function cerrarSesion() {
    limpiarTodasLocalStorage();
    window.location = "index.html";
}

function limpiarCache() {
    var usuario, empresa, empresaId, palabra, ubicacion;
    ubicacion = sacarLocalStorage("ubicacionPais");
    if (sacarLocalStorage("nombreUsuario") != null) {
        usuario = sacarLocalStorage("nombreUsuario");
        usuarioId = sacarLocalStorage("usuarioId");
    }
    if (sacarLocalStorage("nombreEmpresa") != null) {
        empresa = sacarLocalStorage("nombreEmpresa");
        empresaId = sacarLocalStorage("empresaId");
    }
    palabra = sacarLocalStorage("palabraIndex");
    limpiarTodasLocalStorage();
    if (usuario != null) {
        almacenarLocalStorage("nombreUsuario", usuario);
        almacenarLocalStorage("usuarioId", usuarioId);
    }
    if (empresa != null) {
        almacenarLocalStorage("nombreEmpresa", empresa);
        almacenarLocalStorage("empresaId", empresaId);
    }
    almacenarLocalStorage("palabraIndex", palabra);
    almacenarLocalStorage("ubicacionPais", ubicacion);
}

function limpiarCacheBusqueda() {
    var usuario, empresa, empresaId, palabra, ubicacion;
    ubicacion = sacarLocalStorage("ubicacionPais");
    if (sacarLocalStorage("nombreUsuario") != null) {
        usuario = sacarLocalStorage("nombreUsuario");
        usuarioId = sacarLocalStorage("usuarioId");
    }
    if (sacarLocalStorage("nombreEmpresa") != null) {
        empresa = sacarLocalStorage("nombreEmpresa");
        empresaId = sacarLocalStorage("empresaId");
    }
    idProducto = sacarLocalStorage("idProducto");
    idEmpresa = sacarLocalStorage("idEmpresa");
    palabra = sacarLocalStorage("palabraIndex");
    limpiarTodasLocalStorage();
    if (usuario != null) {
        almacenarLocalStorage("nombreUsuario", usuario);
        almacenarLocalStorage("usuarioId", usuarioId);
    }
    if (empresa != null) {
        almacenarLocalStorage("nombreEmpresa", empresa);
        almacenarLocalStorage("empresaId", empresaId);
    }
    almacenarLocalStorage("palabraIndex", palabra);
    almacenarLocalStorage("idProducto", idProducto);
    almacenarLocalStorage("idEmpresa", idEmpresa);
    almacenarLocalStorage("ubicacionPais", ubicacion);
}

function headerIndex() {
    if (sacarLocalStorage("nombreEmpresa") == null && sacarLocalStorage("nombreUsuario") == null) {
        $("#headerAdd").prepend('<nav class="navFyndy" role="navigation"><div class="nav-wrapper container"><ul class="right hide-on-med-and-down"><li><a href="seleccionRegistro.html" class="opcionSUpDerecha lime-text text-darken-4">Registrate</a></li><li><a href="inicioSesion.html" class="opcionSUpDerecha lime-text text-darken-4">Ingresa</a></li></ul><ul id="nav-mobile" class="sidenav lime-text text-darken-4"><li><a href="seleccionRegistro.html">Registrate</a></li><li><a href="inicioSesion.html">Ingresa</a></li></ul><a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a></div></nav>');
    } else {
        if (sacarLocalStorage("nombreUsuario") != null) {
            $("#headerAdd").prepend('<nav class="navFyndy" role="navigation"><div class="nav-wrapper container"><ul class="right hide-on-med-and-down"><li><a href="perfilUsuario.html" class="opcionSUpDerecha lime-text text-darken-4">' + sacarLocalStorage("nombreUsuario") + '</a></li><li><a onclick="cerrarSesion()" class="opcionSUpDerecha lime-text text-darken-4">Salir</a></li></ul><ul id="nav-mobile" class="sidenav lime-text text-darken-4"><li><a href="perfilUsuario.html" class="opcionSUpDerecha lime-text text-darken-4">' + sacarLocalStorage("nombreUsuario") + '</a></li><li><a onclick="cerrarSesion()" class="opcionSUpDerecha lime-text text-darken-4">Salir</a></li></ul><a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a></div></nav>');
        } else {
            $("#headerAdd").prepend('<nav class="navFyndy" role="navigation"> <div class="nav-wrapper container"> <ul class="right hide-on-med-and-down"> <li><a href="perfilEmpresa.html" class="opcionSUpDerecha lime-text text-darken-4">' + sacarLocalStorage("nombreEmpresa") + '</a></li> <li><a href="#" data-target="menuAdminProd" class="dropdown-trigger opcionSUpDerecha lime-text text-darken-4">Adm. Producto</a> <ul id="menuAdminProd" class="dropdown-content"> <li><a href="verProductos.html">Ver</a></li> <li><a href="registrarProductos.html">Registrar</a></li></ul></li><li><a onclick="cerrarSesion()" class="opcionSUpDerecha lime-text text-darken-4">Salir</a></li></ul> <ul id="nav-mobile" class="sidenav lime-text text-darken-4"> <li><a href="perfilEmpresa.html" class="opcionSUpDerecha lime-text text-darken-4">' + sacarLocalStorage("nombreEmpresa") + '</a></li> <li><a href="agregarProductos.html" class="opcionSUpDerecha lime-text text-darken-4">Adm. Producto</a></li> </ul><a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a> </div></nav>');
        }
    }
    $('.dropdown-trigger').dropdown();
}

function header() {
    if (sacarLocalStorage("nombreEmpresa") == null && sacarLocalStorage("nombreUsuario") == null) {
        $("#headerAdd").prepend('<nav class="navFyndyGeneral" role="navigation" id="navPrincipal"><div class="nav-wrapper container"><ul class="right hide-on-med-and-down"><li><a href="seleccionRegistro.html" class="opcionSUpDerechaInterna">Registrate</a></li><li><a href="inicioSesion.html" class="opcionSUpDerechaInterna">Ingresa</a></li><li><a href="index.html" class="opcionSUpDerechaInterna">Inicio</a></li></ul><ul id="nav-mobile" class="sidenav lime-text text-darken-4"><li><a href="seleccionRegistro.html">Registrate</a></li><li><a href="inicioSesion.html">Ingresa</a></li></ul><a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a></div></nav>');
    } else {
        if (sacarLocalStorage("nombreUsuario") != null) {
            $("#headerAdd").prepend('<nav class="navFyndyGeneral" role="navigation"><div class="nav-wrapper container"><ul class="right hide-on-med-and-down"><li><a href="perfilUsuario.html" class="opcionSUpDerechaInterna">' + sacarLocalStorage("nombreUsuario") + '</a></li><li><a href="index.html" class="opcionSUpDerechaInterna">Inicio</a></li><li><a onclick="cerrarSesion()" class="opcionSUpDerechaInterna">Salir</a></li></ul><ul id="nav-mobile" class="sidenav lime-text text-darken-4"><li><a href="perfilUsuario.html.html">' + sacarLocalStorage("nombreUsuario") + '</a></li><li><a href="index.html">Inicio</a></li><li><a onclick="cerrarSesion()">Salir</a></li></ul><a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a></div></nav>');
        } else {
            $("#headerAdd").prepend('<nav class="navFyndyGeneral" role="navigation"> <div class="nav-wrapper container"> <ul class="right hide-on-med-and-down"> <li><a href="perfilEmpresa.html" class="opcionSUpDerechaInterna">' + sacarLocalStorage("nombreEmpresa") + '</a></li> <li><a href="#" data-target="menuAdminProd" class="dropdown-trigger opcionSUpDerechaInterna">Adm. Producto</a> <ul id="menuAdminProd" class="dropdown-content"> <li><a href="verProductos.html">Ver</a></li> <li><a href="registrarProductos.html">Registrar</a></li> </ul> </li> <li><a href="index.html" class="opcionSUpDerechaInterna">Inicio</a></li> <li><a onclick="cerrarSesion()" class="opcionSUpDerechaInterna">Salir</a></li> </ul> <ul id="nav-mobile" class="sidenav lime-text text-darken-4"> <li><a href="perfilEmpresa.html.html">' + sacarLocalStorage("nombreEmpresa") + '</a></li> <li><a href="agregarProductos.html" class="opcionSUpDerecha lime-text text-darken-4">Adm. Producto</a></li> <li><a href="index.html">Inicio</a></li> <li><a onclick="cerrarSesion()">Salir</a></li> </ul><a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a> </div></nav>');
        }
    }
    $('.dropdown-trigger').dropdown();
}

function ponerLoader() {
    $("body").removeClass("loaded");
}

function quitarLoader() {
    $('body').addClass('loaded');
}

function shadowOn() {
    $('#buscador').addClass('buscadorShadow');
}

function shadowOff() {
    $('#buscador').removeClass('buscadorShadow');
}

function validarEnter(event) {
    if (event.keyCode == 13) busquedaEmpresa();
}

function validarPalabraClave(event) {
    if (event.keyCode == 13) modificarPalabras();
}

function validarPalabraClaveEditada(event, palabra) {
    if (event.keyCode == 13) editWord(palabra);
}

function validarEnterIndex(event) {
    if (event.keyCode == 13) {
        almacenarLocalStorage("palabraIndex", $("#search").val());
        window.location = "busquedaProductos.html";
    }
}

function buscarProductoIndex() {
    almacenarLocalStorage("palabraIndex", $("#search").val());
    window.location = "busquedaProductos.html";
}

function busquedaCategoriasIndex(elem) {
    almacenarLocalStorage("categoriaIndex", elem.id);
    window.location = "busquedaProductos.html";
}

function busquedaCategoriasDetalle(elem) {
    almacenarLocalStorage("categoriaIndex", elem.id);
    window.location = "busquedaProductos.html";
}

function buscarDetalleEmpresa(idEmpresa, idProducto) {
    if(idProducto != null) {
        almacenarLocalStorage("idProducto", idProducto);
    }else{
        almacenarLocalStorage("idProducto", 0);
    }
    almacenarLocalStorage("idEmpresa", idEmpresa);
    window.location = "detalleEmpresa.html";
}

function limpiarTodasLocalStorage() {
    if (typeof(window.localStorage) != 'undefined') {
        window.localStorage.clear();
        window.sessionStorage.clear();
    } else {
        throw "window.localStorage, not defined";
    }
}

function almacenarLocalStorage(key, valor) {

    if (typeof(window.localStorage) != 'undefined') {
        window.localStorage.setItem(key, valor);
    } else {
        throw "window.localStorage, no definido";
    }

}

function sacarLocalStorage(codigo) {
    var llave;
    if (typeof(window.localStorage) != 'undefined') {
        llave = window.localStorage.getItem(codigo);
    } else {
        throw "window.localStorage, no definido";
    }
    return llave;
}

function abrir(elem) {
    var instance = M.Autocomplete.getInstance(elem);
    instance.open();
}

function gql(query) {
    return new Promise((resolve, reject) => {
        axios({
                url: 'https://fynddy.azurewebsites.net/',
                method: 'POST',
                data: { query },
            })
            .then((res) => resolve(res['data']))
            .catch((err) => reject(err))
    })
}

function mensajeError() {
    quitarLoader();
    swal({
        title: '¡Atención!',
        text: '¡Ha ocurrido un error intente nuevamente!',
        type: 'warning',
        confirmButtonColor: '#4CAF50',
        confirmButtonText: 'Aceptar'
    });
}

function mensajeErrorControlado(err) {
    quitarLoader();
    swal({
        title: "¡Atención!",
        text: err,
        type: "warning",
        confirmButtonColor: "#007b03",
        confirmButtonText: "Aceptar",
        allowOutsideClick: false,
        allowEscapeKey: false
    });
}

function mensajeErrorCatch(err) {
    quitarLoader();
    swal({
        title: "¡Atención!",
        text: err,
        type: "warning",
        confirmButtonColor: "#007b03",
        confirmButtonText: "Aceptar",
        allowOutsideClick: false,
        allowEscapeKey: false
    });
}

function mensajeLlenarCampos() {
    swal({
        title: '¡Atención!',
        text: '¡Debe llenar todos los campos para poder continuar!',
        type: 'warning',
        confirmButtonColor: '#4CAF50',
        confirmButtonText: 'Aceptar'
    });
}

function mensajeIniciarSesion() {
    swal({
        title: '¡Atención!',
        text: '¡Debe iniciar sesion para poder enviar un comentario!',
        type: 'warning',
        confirmButtonColor: '#4CAF50',
        confirmButtonText: 'Aceptar'
    });
}

function mensajeProcesado() {
    quitarLoader();
    swal({
        title: "Procesada",
        text: "Su solicitud ha sido procesada con exito.",
        type: "success",
        showCancelButton: false,
        confirmButtonColor: "#007b03",
        confirmButtonText: "Aceptar"
    });
}

function mensajeInvalido() {
    swal({
        title: "¡Atención!",
        text: "¡Datos invalidos!",
        type: "warning",
        confirmButtonColor: "#007b03",
        confirmButtonText: "Aceptar",
        allowOutsideClick: false,
        allowEscapeKey: false
    });
}

function mensajeContraseña() {
    swal({
        title: "¡Atención!",
        text: "¡Las contraseñas ingresadas no coinciden!",
        type: "warning",
        confirmButtonColor: "#007b03",
        confirmButtonText: "Aceptar",
        allowOutsideClick: false,
        allowEscapeKey: false
    });
}

function mensajeURLInvalido() {
    swal({
        title: "¡Atención!",
        text: "¡La url ingresada como dirección no es valida!",
        type: "warning",
        confirmButtonColor: "#007b03",
        confirmButtonText: "Aceptar",
        allowOutsideClick: false,
        allowEscapeKey: false
    });
}

function mensajeRegistro(val) {
    quitarLoader();
    swal({
        title: "Procesada",
        text: "Su registro ha sido procesado con exito.",
        type: "success",
        showCancelButton: false,
        confirmButtonColor: "#007b03",
        confirmButtonText: "Aceptar",
        closeOnCancel: true
    }).then(function() {
        if (val == 0)
            window.location = "perfilEmpresa.html";
        else
            window.location = "perfilUsuario.html";
    });
}

