function buscarPrincipalesProductosEmpresa() {
    gql('query {consultarProductosPrincipalesEmpresaMotor(empresaId: ' + sacarLocalStorage("idEmpresa") + '){nodes{id,obtenerProducto{id,nombre}obtenerEmpresa{id,nombre}}}}')
        .then(function(data) {
            if (data.data != undefined) {
                var obj = data.data.consultarProductosPrincipalesEmpresaMotor;
                $("#listaEmpresasDetalle").html("");
                if (data.data.consultarProductosPrincipalesEmpresaMotor.nodes.length > 0) {
                    var obj = data.data.consultarProductosPrincipalesEmpresaMotor.nodes;
                    obj.forEach(function(producto) {
                        $("#listaEmpresasDetalle").append('<div class="col s12 m4 l4"> <div class="listaEmpresas z-depth-1 pointer"><div class="row mp0"><img class="imagenProducto" src="images/2.jpg"></div><div class="row "><span>' + producto.obtenerProducto.nombre + '</span></div> </div></div>');
                    });
                    buscarProductos();
                } else {
                    $("#listaEmpresasDetalle").append('<div class="col s12 m12 l12"><h4>No hay resultados disponibles.</h4></div>');
                }
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function buscarDetalleEmpresa() {
    gql('query{ obtenerEmpresaXId(id: ' + sacarLocalStorage("idEmpresa") + ', frase: "' + $("#search").val() + '", productoId: ' + sacarLocalStorage("idProducto") + '){ descripcion, fotoPerfil, nombre, obtenerHorario{ horario{ horario1, id } }, obtenerContacto{ contacto, id }, obtenerImagenes{ imagen }, obtenerRedSocial{ link } }}')
        .then(function(data) {
            if (data.data != undefined) {
                if (data.data.obtenerEmpresaXId != undefined) {
                    var obj = data.data.obtenerEmpresaXId;
                    $("#nombre").html(obj.nombre);
                    if (obj.fotoPerfil != null) {
                        $("#fotoPerfil").html("");
                        $("#fotoPerfil").html('<img class="responsive-img fotoPerfil" src="' + obj.fotoPerfil + '">');
                    }
                    if (obj.descripcion != null) {
                        $("#descripcion").html("");
                        $("#descripcion").html(obj.descripcion);
                    }
                    if (obj.obtenerHorario.length > 0 || obj.obtenerRedSocial.length > 0) {
                        $("#horario").html("");
                        if (obj.obtenerHorario.length > 0) {
                            var listaHorario = obj.obtenerHorario.horario;
                            listaHorario.forEach(function(lista) {
                                $("#horario").append(lista.horario);
                            });
                        }
                        if (obj.obtenerRedSocial.length > 0) {
                            var listaRedSocial = obj.obtenerRedSocial;
                            listaRedSocial.forEach(function(lista) {
                                $("#horario").append(lista.link);
                            });
                        }
                    }
                    if (obj.obtenerContacto.length > 0) {
                        $("#contacto").html("");
                        var listaContacto = obj.obtenerContacto;
                        listaContacto.forEach(function(lista) {
                            $("#contacto").append(lista.contacto);
                        });
                    }
                    if (obj.obtenerImagenes.length > 0) {
                        $("#imagenes").html("");
                        var listaImagen = obj.obtenerImagenes;
                        listaImagen.forEach(function(lista) {
                            $("#imagenes").append('<a class="carousel-item"><img src="' + lista.imagen + '"></a>');
                        });
                    }
                    buscarProductosEmpresa();
                }
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function buscarProductosEmpresa() {
    gql('query consulta{consultarProductosEmpresa(empresaId: ' + sacarLocalStorage("idEmpresa") + ', categoriaId: null, rubroId: null){nodes{obtenerCategoria{nombre}categoriaId,rubroEmpresa{nodes{obtenerRubro{nombre}rubroId,productoEmpresa{nodes{obtenerProducto{nombre}id}}}}}}}')
        .then(function(data) {
            if (data.data != undefined) {
                $("#listaProducto").html("");
                var datos = [];
                if (data.data.consultarProductosEmpresa.nodes.length > 0) {
                    var obj = data.data.consultarProductosEmpresa.nodes;
                    obj.forEach(function(categoria) {
                        var obj2 = categoria.rubroEmpresa.nodes;
                        obj2.forEach(function(rubro) {
                            var obj3 = rubro.productoEmpresa.nodes;
                            obj3.forEach(function(producto) {
                                if (producto.obtenerProducto != null) {
                                    $("#listaProducto").append('<li class="collection-item"><div><span>' + producto.obtenerProducto.nombre + '</span></div></li>');
                                }
                            });
                        });
                    });
                }
            } else {
                document.getElementById("divListaProducto").style.display = "none";
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function consultaCategoriasEmpresa() {
    gql('query consulta{obtenerCategoriasPrincipales(first: 3){nodes{id,nombre}}}')
        .then(function(data) {
            if (data.data != undefined) {
                $("#categoriasPrincipalesEmpresa").html("");
                if (data.data.obtenerCategoriasPrincipales.nodes != undefined) {
                    var obj = data.data.obtenerCategoriasPrincipales.nodes;
                    obj.forEach(function(categoria) {
                        $("#categoriasPrincipalesEmpresa").append('<div class="row mp0"><div class="listaCategoriasEmpresa pointer row mp0"><a id="' + categoria.id + '" onclick="busquedaCategoriasIndex(this)" class="pointer"><img src="images/botones/' + categoria.id + '.svg" /></a><p>' + categoria.nombre + '</p></div>');
                    });
                }
                setTimeout(quitarLoader(), 4000);
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}