function consultaInicial() {
    gql('query consulta{obtenerEmpresasPrincipales{id,nombre}}')
        .then(function(data) {
            if (data.data != undefined) {
                $("#empresas").html("");
                if (data.data.obtenerEmpresasPrincipales != undefined) {
                    var obj = data.data.obtenerEmpresasPrincipales;
                    obj.forEach(function(empresa) {
                        $("#empresas").append('<div class="col s12 m4 l4"><div class="listaEmpresas pointer" onclick="buscarDetalleEmpresa(' + empresa.id + ')"><div class="row mp0"><img class="imagenProducto" src="images/2.jpg"></div><div class="row mp0"><i class="tiny material-icons">stars</i><i class="tiny material-icons">stars</i><i class="tiny material-icons">stars</i><i class="tiny material-icons">stars</i><i class="tiny material-icons">stars</i></div><div class="row mp0 tituloEmpresas"><span>' + empresa.nombre + '</span></div>');
                    });
                }
                consultaCategorias();
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function consultaCategorias() {
    gql('query consulta{obtenerCategoriasPrincipales{nodes{id,nombre}}}')
        .then(function(data) {
            if (data.data != undefined) {
                $("#categoriasPrincipales").html("");
                if (data.data.obtenerCategoriasPrincipales.nodes != undefined) {
                    var obj = data.data.obtenerCategoriasPrincipales.nodes;
                    obj.forEach(function(categoria) {
                        $("#categoriasPrincipales").append('<div class="col s6 m3 l3"><div class="listaCategorias"><a id="' + categoria.id + '" onclick="busquedaCategoriasIndex(this)" class="pointer"><img src="images/botones/' + categoria.id + '.svg" /></a><p>' + categoria.nombre + '</p></div>');
                    });
                }
                quitarLoader();
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function consultarPaises() {
    gql('query {obtenerPaises{id,nombre}}')
        .then(function(data) {
            if (data.data != undefined) {
                var pais = document.getElementById("filtroPais");
                while (pais.length > 1) {
                    pais.remove(1);
                }
                document.getElementById("filtroEstado").value = '0';
                document.getElementById("filtroEstado").disabled = true;
                document.getElementById("filtroCiudad").value = '0';
                document.getElementById("filtroCiudad").disabled = true;
                if (data.data.obtenerPaises.length > 0) {
                    var obj = data.data.obtenerPaises;
                    obj.forEach(function(lista) {
                        $("#filtroPais").append('<option value=' + lista.id + ' >' + lista.nombre + '</option>');
                    });
                    document.getElementById("filtroPais").value = '0';
                }
                $('select').formSelect();
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function consultarEstados(val) {
    document.getElementById("filtroCiudad").value = '0';
    document.getElementById("filtroCiudad").disabled = true;
    $('select').formSelect();
    document.getElementById("filtroEstado").disabled = false;
    gql('query {obtenerLocalidades(id: ' + val.value + '){id,nombre}}')
        .then(function(data) {
            if (data.data != undefined) {
                var localidad = document.getElementById("filtroEstado");
                while (localidad.length > 1) {
                    localidad.remove(1);
                }
                if (data.data.obtenerLocalidades.length > 0) {
                    var obj = data.data.obtenerLocalidades;
                    obj.forEach(function(lista) {
                        $("#filtroEstado").append('<option value=' + lista.id + ' >' + lista.nombre + '</option>');
                    });
                    document.getElementById("filtroEstado").value = '0';
                }
                $('#filtroEstado').formSelect();
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function consultarCiudades(val) {
    document.getElementById("filtroCiudad").disabled = false;
    gql('query {obtenerCiudades(id: ' + val.value + '){id,nombre}}')
        .then(function(data) {
            if (data.data != undefined) {
                var ciudad = document.getElementById("filtroCiudad");
                while (ciudad.length > 1) {
                    ciudad.remove(1);
                }
                if (data.data.obtenerCiudades.length > 0) {
                    var obj = data.data.obtenerCiudades;
                    obj.forEach(function(lista) {
                        $("#filtroCiudad").append('<option value=' + lista.id + ' >' + lista.nombre + '</option>');
                    });
                    document.getElementById("filtroCiudad").value = '0';
                }
                $('#filtroCiudad').formSelect();
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function guardarFiltros() {
    if ($("#filtroPais").val() != 0){
        almacenarLocalStorage("tipoFiltro", 1);
        almacenarLocalStorage("filtroId", $("#filtroPais").val());
    }
    if ($("#filtroEstado").val() != 0){
        almacenarLocalStorage("tipoFiltro", 2);
        almacenarLocalStorage("filtroId", $("#filtroEstado").val());
    }
    if ($("#filtroCiudad").val() != 0){
        almacenarLocalStorage("tipoFiltro", 3);
        almacenarLocalStorage("filtroId", $("#filtroCiudad").val());
    }
}

function busquedaEmpresa() {
    ponerLoader();
    almacenarLocalStorage("pagina", 1)
    var filtro = 0;
    var tipoFiltro = 0;
    if (sacarLocalStorage("tipoFiltro") != null){
        tipoFiltro = sacarLocalStorage("tipoFiltro");
        filtro = sacarLocalStorage("filtroId");
    }
    gql('query consulta{consultarProductosEmpresaMotor(palabra: "' + $("#search").val() + '", filtroId: "' + filtro + '", tipoFiltro: " ' + tipoFiltro + ' ", first:9){totalCount,pageInfo{endCursor,startCursor}edges{node{id,obtenerEmpresa{id,nombre,fotoPerfil}obtenerProducto{id,nombre}}}}}')
        .then(function(data) {
            if (data.data != undefined) {
                $("#empresaContainer").html("");
                $("#paginationContainer").html("");
                if (data.data.consultarProductosEmpresaMotor.edges.length > 0) {
                    almacenarLocalStorage("palabraIndex", $("#search").val());
                    almacenarLocalStorage("startCursor", data.data.consultarProductosEmpresaMotor.pageInfo.startCursor);
                    almacenarLocalStorage("endCursor", data.data.consultarProductosEmpresaMotor.pageInfo.endCursor);
                    var obj = data.data.consultarProductosEmpresaMotor.edges;
                    $('#paginationContainer').materializePagination({
                        lastPage:  Math.ceil(data.data.consultarProductosEmpresaMotor.totalCount/9),
                        firstPage:  1,
                        useUrlParameter: false,
                        onClickCallback: function(requestedPage){
                            var paginaActual = sacarLocalStorage("pagina");
                            almacenarLocalStorage("pagina", requestedPage);
                            if(requestedPage > paginaActual){
                                busquedaEmpresaPaginaSiguiente(sacarLocalStorage("endCursor"));
                            }
                            else{
                                if(requestedPage > 1)
                                    busquedaEmpresaPaginaAnterior(sacarLocalStorage("startCursor"));
                                else
                                    busquedaEmpresa();
                            }
                            almacenarLocalStorage("pagina", requestedPage);
                            $('html, body').animate({ scrollTop: 0 }, 500, 'swing');
                        }
                    });
                    var html = template(data.data.consultarProductosEmpresaMotor.edges);
                    $("#empresaContainer").html(html);
                    consultarPaises();
                    quitarLoader();
                } else {
                    $("#empresaContainer").append('<div class="col s12 m12 l12"><h4>No hay resultados disponibles.</h4></div>');
                    $('#paginationContainer').html("");
                    quitarLoader();
                }
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function busquedaEmpresaPaginaSiguiente(cursor) {
    ponerLoader();
    var filtro = 0;
    var tipoFiltro = 0;
    if (sacarLocalStorage("tipoFiltro") != null){
        tipoFiltro = sacarLocalStorage("tipoFiltro");
        filtro = sacarLocalStorage("filtroId");
    }
    gql('query consulta{consultarProductosEmpresaMotor(palabra: "' + $("#search").val() + '", filtroId: "' + filtro + '", tipoFiltro: "' + tipoFiltro + '", first:9, after: "' + cursor + '"){totalCount,pageInfo{endCursor,startCursor}edges{node{id,obtenerEmpresa{id,nombre,fotoPerfil}obtenerProducto{id,nombre}}}}}')
        .then(function(data) {
            if (data.data != undefined) {
                $("#empresaContainer").html("");
                if (data.data.consultarProductosEmpresaMotor.edges.length > 0) {
                    almacenarLocalStorage("palabraIndex", $("#search").val());
                    almacenarLocalStorage("startCursor", data.data.consultarProductosEmpresaMotor.pageInfo.startCursor);
                    almacenarLocalStorage("endCursor", data.data.consultarProductosEmpresaMotor.pageInfo.endCursor);
                    var obj = data.data.consultarProductosEmpresaMotor.edges;
                    var html = template(data.data.consultarProductosEmpresaMotor.edges);
                    $("#empresaContainer").html(html);
                    consultarPaises();
                    quitarLoader();
                } else {
                    $("#empresaContainer").append('<div class="col s12 m12 l12"><h4>No hay resultados disponibles.</h4></div>');
                    $('#paginationContainer').html("");
                    quitarLoader();
                }
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function busquedaEmpresaPaginaAnterior(cursor) {
    ponerLoader();
    var filtro = 0;
    var tipoFiltro = 0;
    if (sacarLocalStorage("tipoFiltro") != null){
        tipoFiltro = sacarLocalStorage("tipoFiltro");
        filtro = sacarLocalStorage("filtroId");
    }
    gql('query consulta{consultarProductosEmpresaMotor(palabra: "' + $("#search").val() + '", filtroId: "' + filtro + '", tipoFiltro: " ' + tipoFiltro + ' ", first:9, before: "' + cursor + '"){totalCount,pageInfo{endCursor,startCursor}edges{node{id,obtenerEmpresa{id,nombre,fotoPerfil}obtenerProducto{id,nombre}}}}}')
        .then(function(data) {
            if (data.data != undefined) {
                $("#empresaContainer").html("");
                if (data.data.consultarProductosEmpresaMotor.edges.length > 0) {
                    almacenarLocalStorage("palabraIndex", $("#search").val());
                    almacenarLocalStorage("startCursor", data.data.consultarProductosEmpresaMotor.pageInfo.startCursor);
                    almacenarLocalStorage("endCursor", data.data.consultarProductosEmpresaMotor.pageInfo.endCursor);
                    var obj = data.data.consultarProductosEmpresaMotor.edges;
                    var html = template(data.data.consultarProductosEmpresaMotor.edges);
                    $("#empresaContainer").html(html);
                    consultarPaises();
                    quitarLoader();
                } else {
                    $("#empresaContainer").append('<div class="col s12 m12 l12"><h4>No hay resultados disponibles.</h4></div>');
                    $('#paginationContainer').html("");
                    quitarLoader();
                }
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function busquedaCategorias(val) {
    gql('query consulta{consultarProductosCategoriaMotor(categoriaId: ' + val + '){edges{cursor node{id,obtenerEmpresa{id,nombre}obtenerProducto{nombre}}}}}')
        .then(function(data) {
            if (data.data != undefined) {
                if (document.getElementById('empresaContainer') != null) {
                    if (data.data.consultarProductosCategoriaMotor.edges.length > 0) {
                        var obj = data.data.consultarProductosCategoriaMotor.edges;
                        $("#empresaContainer").html("");
                        $('#paginationContainer').pagination({
                            dataSource: obj,
                            pageSize: 9,
                            callback: function(data, pagination) {
                                var html = template(data);
                                $("#empresaContainer").html(html);
                            }
                        });
                        $('#paginationContainer').addHook('afterPreviousOnClick', function() {
                            $('html, body').animate({ scrollTop: 0 }, 'fast');
                        });
                        $('#paginationContainer').addHook('afterPageOnClick', function() {
                            $('html, body').animate({ scrollTop: 0 }, 'fast');
                        });
                        $('#paginationContainer').addHook('afterNextOnClick', function() {
                            $('html, body').animate({ scrollTop: 0 }, 'fast');
                        });
                        quitarLoader();
                    } else {
                        $("#empresaContainer").append('<div class="col s12 m12 l12"><h4>No hay resultados disponibles.</h4></div>');
                        quitarLoader();
                    }
                }

            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function template(data) {
    var html = '';
    if(data.length > 2) html = '<div class="row flex">';
    var i = 0;
    var j = 0;
    data.forEach(function(empresa) {
        if(i == 3){
            if((data.length - j) > 3) html += '</div><div class="row flex">';
            else html += '</div><div class="row">'
            i = 0;
        }
        if (empresa.node.obtenerProducto != null) {
            if (empresa.node.obtenerEmpresa.fotoPerfil != null)
                html += '<div class="col s12 m4 l4"> <div class="listaEmpresas pointer" onclick="buscarDetalleEmpresa(' + empresa.node.obtenerEmpresa.id + ', ' + empresa.node.obtenerProducto.id + ')"> <img class="imagenProducto" src="' + empresa.node.obtenerEmpresa.fotoPerfil + '"> <div class="row"> <i class="tiny material-icons">stars</i><i class="tiny material-icons">stars</i> <i class="tiny material-icons">stars</i><i class="tiny material-icons">stars</i> <i class="tiny material-icons">stars</i> </div> <div class="row tituloEmpresas"> <span>' + empresa.node.obtenerEmpresa.nombre + '</span> </div> <div class="row"> <p>' + empresa.node.obtenerProducto.nombre + '</p> </div> </div></div>'
            else
                html += '<div class="col s12 m4 l4"> <div class="listaEmpresas pointer" onclick="buscarDetalleEmpresa(' + empresa.node.obtenerEmpresa.id + ', ' + empresa.node.obtenerProducto.id + ')"> <img class="imagenProducto" src="images/4.jpg"> <div class="row"> <i class="tiny material-icons">stars</i><i class="tiny material-icons">stars</i> <i class="tiny material-icons">stars</i><i class="tiny material-icons">stars</i> <i class="tiny material-icons">stars</i> </div> <div class="row tituloEmpresas"> <span>' + empresa.node.obtenerEmpresa.nombre + '</span> </div> <div class="row"> <p>' + empresa.node.obtenerProducto.nombre + '</p> </div> </div></div>'
        } else {
            if (empresa.node.obtenerEmpresa.fotoPerfil != null)
                html += '<div class="col s12 m4 l4"><div class="listaEmpresas pointer" onclick="buscarDetalleEmpresa(' + empresa.node.obtenerEmpresa.id + ', null)"><img class="imagenProducto" src="' + empresa.node.obtenerEmpresa.fotoPerfil + '"><div class="row"><i class="tiny material-icons">stars</i><i class="tiny material-icons">stars</i><i class="tiny material-icons">stars</i><i class="tiny material-icons">stars</i><i class="tiny material-icons">stars</i></div><div class="row tituloEmpresas"><span>' + empresa.node.obtenerEmpresa.nombre + '</span></div></div></div>'
            else
                html += '<div class="col s12 m4 l4"><div class="listaEmpresas pointer" onclick="buscarDetalleEmpresa(' + empresa.node.obtenerEmpresa.id + ', null)"><img class="imagenProducto" src="images/3.jpg"><div class="row"><i class="tiny material-icons">stars</i><i class="tiny material-icons">stars</i><i class="tiny material-icons">stars</i><i class="tiny material-icons">stars</i><i class="tiny material-icons">stars</i></div><div class="row tituloEmpresas"><span>' + empresa.node.obtenerEmpresa.nombre + '</span></div></div></div>'
        }
        i++;
        j++;
    });
    return html;
}