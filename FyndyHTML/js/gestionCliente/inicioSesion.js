function validar() {
    if ($("input[type='radio']:checked").length > 0 && $("#user").val() != "" && $("#password").val() != "") {
        if ($("input[type='radio']:checked")[0].id == "usuario") {
            gql('query {inicioUsuario(correo: "' + $("#user").val() + '", password: "' + $("#password").val() + '"){id, tipo, primerNombre, primerApellido}}')
                .then(function(data) {
                    if (data.data != undefined) {
                        if (data.data.inicioUsuario != null) {
                            almacenarLocalStorage("nombreUsuario", data.data.inicioUsuario.primerNombre + " " + data.data.inicioUsuario.primerApellido);
                            almacenarLocalStorage("usuarioId", data.data.inicioUsuario.id);
                            if (data.data.inicioUsuario.tipo == 0)
                                window.location = "perfilUsuario.html";
                            else
                                window.location = "crearAdministrador.html";
                        } else {
                            mensajeInvalido();
                        }
                    } else {
                        mensajeError();
                    }
                })
                .catch((err) => mensajeErrorCatch(err));
        } else {
            gql('query {inicioEmpresa(correo: "' + $("#user").val() + '", password: "' + $("#password").val() + '"){id, nombre}}')
                .then(function(data) {
                    if (data.data != undefined) {
                        if (data.data.inicioEmpresa != null) {
                            almacenarLocalStorage("nombreEmpresa", data.data.inicioEmpresa.nombre);
                            almacenarLocalStorage("empresaId", data.data.inicioEmpresa.id);
                            window.location = "perfilEmpresa.html";
                        } else {
                            mensajeInvalido();
                        }
                    } else {
                        mensajeError();
                    }
                })
                .catch((err) => mensajeErrorCatch(err));
        }
    }
    else{
        mensajeLlenarCampos();
    }
}