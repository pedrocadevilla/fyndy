function registrarEmpresa() {
    if ($("#nombre").val() != "" && $("#correo").val() != "" && $("#password").val() != "" && $("#passwordCheck").val() != "" && $("#direccionPais").val() != "" && $("#direccionCiudad").val() != "" && $("#direccionLocalidad").val() != "") {
        if ($("#password").val() == $("#passwordCheck").val()) {
            gql('mutation {crearEmpresa(empresa:{nombre: "' + $("#nombre").val() + '", contrase_a: "' + $("#password").val() + '", correo: "' + $("#correo").val() + '"}, direccion:{paisId: ' + $("#direccionPais").val() + ', ciudadId: ' + $("#direccionCiudad").val() + ', localidadId: ' + $("#direccionLocalidad").val() + '}){id, nombre}}')
                .then(function(data) {
                    if (data.data != undefined) {
                        almacenarLocalStorage("nombreEmpresa", data.data.crearEmpresa.nombre);
                        almacenarLocalStorage("empresaId", data.data.crearEmpresa.id);
                        mensajeRegistro(0);
                    } else {
                        if (data.errors != undefined) {
                            mensajeErrorControlado(data.errors[0].message);
                        } else {
                            mensajeError();
                        }
                    }
                })
                .catch((err) => mensajeErrorCatch(err));
        } else {
            mensajeContraseña();
        }
    } else {
        mensajeLlenarCampos();
    }
}

function consultarPaises() {
    gql('query {obtenerPaises{id,nombre}}')
        .then(function(data) {
            if (data.data != undefined) {
                var pais = document.getElementById("direccionPais");
                while (pais.length > 1) {
                    pais.remove(1);
                }
                if (data.data.obtenerPaises.length > 0) {
                    var obj = data.data.obtenerPaises;
                    obj.forEach(function(listaPaises) {
                        $("#direccionPais").append('<option value=' + listaPaises.id + ' >' + listaPaises.nombre + '</option>');
                    });
                    document.getElementById("direccionPais").value = '0';
                }
                $('#direccionPais').formSelect();
                $('#direccionLocalidad').formSelect();
                $('#direccionCiudad').formSelect();
                quitarLoader();
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function consultarLocalidad(pais) {
    document.getElementById("direccionLocalidad").disabled = false;
    gql('query {obtenerLocalidades(id:' + pais.value + '){id,nombre}}')
        .then(function(data) {
            if (data.data != undefined) {
                var localidad = document.getElementById("direccionLocalidad");
                while (localidad.length > 1) {
                    localidad.remove(1);
                }
                if (data.data.obtenerLocalidades.length > 0) {
                    var obj = data.data.obtenerLocalidades;
                    obj.forEach(function(listaLocalidad) {
                        $("#direccionLocalidad").append('<option value=' + listaLocalidad.id + ' >' + listaLocalidad.nombre + '</option>');
                    });
                    document.getElementById("direccionLocalidad").value = '0';
                }
                $('#direccionLocalidad').formSelect();
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function consultarCiudad(localidad) {
    document.getElementById("direccionCiudad").disabled = false;
    gql('query {obtenerCiudades(id:' + localidad.value + '){id,nombre}}')
        .then(function(data) {
            if (data.data != undefined) {
                var ciudad = document.getElementById("direccionCiudad");
                while (ciudad.length > 1) {
                    ciudad.remove(1);
                }
                if (data.data.obtenerCiudades.length > 0) {
                    var obj = data.data.obtenerCiudades;
                    obj.forEach(function(listaCiudad) {
                        $("#direccionCiudad").append('<option value=' + listaCiudad.id + ' >' + listaCiudad.nombre + '</option>');
                    });
                    document.getElementById("direccionCiudad").value = '0';
                }
                $('#direccionCiudad').formSelect();
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function registrarUsuario() {
    if ($("#nombres").val() != "" && $("#apellidos").val() != "" && $("#correo").val() != "" && $("#password").val() != "" && $("#passwordCheck").val() != "" && $("#fechaNacimiento").val() != "") {
        if ($("#password").val() == $("#passwordCheck").val()) {
            gql('mutation {crearUsuario(usuario:{nombres: "' + $("#nombres").val() + '", apellidos: "' + $("#apellidos").val() + '", contrase_a: "' + $("#password").val() + '", correo: "' + $("#correo").val() + '", fechaNacimiento: "' + $("#fechaNacimiento").val() + '", tipo: 0}){id, primerNombre, primerApellido}}')
                .then(function(data) {
                    if (data.data != undefined) {
                        almacenarLocalStorage("nombreUsuario", data.data.crearUsuario.primerNombre + " " + data.data.crearUsuario.primerApellido);
                        almacenarLocalStorage("usuarioId", data.data.crearUsuario.id);
                        mensajeRegistro(1);
                    } else {
                        if (data.errors != undefined) {
                            mensajeErrorControlado(data.errors[0].message);
                        } else {
                            mensajeError();
                        }
                    }
                })
                .catch(function(err) {
                    mensajeErrorCatch(err)
                })
        } else {
            mensajeContraseña();
        }
    } else {
        mensajeLlenarCampos();
    }
}