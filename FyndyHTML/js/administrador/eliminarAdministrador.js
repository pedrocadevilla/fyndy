function eliminarCategoria(id) {
    swal({
        text: "¿Esta seguro de eliminar el registro seleccionado?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#007b03",
        confirmButtonText: "Aceptar",
        allowOutsideClick: false,
        allowEscapeKey: false
    }).then(function() {
        gql('mutation {eliminarCategoria(id: ' + id + ')}')
            .then(function(data) {
                if (data.data != undefined) {
                    gql('query {obtenerCategorias{id,nombre}}')
                        .then(function(dataCategoria) {
                            if (dataCategoria.data != undefined) {
                                var cat = document.getElementById("categoriaRubro");
                                while (cat.length > 1) {
                                    cat.remove(1);
                                }
                                var cat2 = document.getElementById("categoriaProducto");
                                while (cat2.length > 1) {
                                    cat2.remove(1);
                                }
                                $("#listaCategoria").html("");
                                if (dataCategoria.data.obtenerCategorias.length > 0) {
                                    var obj = dataCategoria.data.obtenerCategorias;
                                    obj.forEach(function(categoria) {
                                        $("#categoriaRubro").append('<option value=' + categoria.id + ' >' + categoria.nombre + '</option>');
                                        $("#categoriaProducto").append('<option value=' + categoria.id + ' >' + categoria.nombre + '</option>');
                                        $("#listaCategoria").append('<li class="collection-item"><div>' + categoria.nombre + '<a class="secondary-content"><i data-target="modalEditarCategoria" class="material-icons modal-trigger pointer botonEditar" onclick="editarCategoria(' + categoria.id + ')">mode_edit</i><i class="material-icons pointer botonEliminar" onclick="eliminarCategoria(' + categoria.id + ')">delete</i></a></div></li>');
                                    });
                                    $('select').formSelect();
                                    document.getElementById("categoriaInput").value = "";
                                    mensajeProcesado();
                                }
                            } else {
                                mensajeError();
                            }
                        })
                        .catch((err) => mensajeErrorCatch(err));
                } else {
                    mensajeError();
                }
            })
            .catch((err) => mensajeErrorCatch(err));
    });
}

function eliminarRubro(id) {
    swal({
        text: "¿Esta seguro de eliminar el registro seleccionado?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#007b03",
        confirmButtonText: "Aceptar",
        allowOutsideClick: false,
        allowEscapeKey: false
    }).then(function() {
        gql('mutation {eliminarRubro(id: ' + id + ')}')
            .then(function(data) {
                if (data.data != undefined) {
                    gql('query {obtenerTodosRubros{id,nombre}}')
                        .then(function(dataRubro) {
                            if (dataRubro.data != undefined) {
                                $("#listaRubro").html("");
                                if (dataRubro.data.obtenerTodosRubros.length > 0) {
                                    var obj = dataRubro.data.obtenerTodosRubros;
                                    obj.forEach(function(rubro) {
                                        $("#listaRubro").append('<li class="collection-item"><div>' + rubro.nombre + '<a class="secondary-content"><i data-target="modalEditarRubro" class="material-icons modal-trigger pointer botonEditar" onclick="editarRubro(' + rubro.id + ')">mode_edit</i><i class="material-icons pointer botonEliminar" onclick="eliminarRubro(' + rubro.id + ')">delete</i></a></div></li>');
                                    });
                                }
                                mensajeProcesado();
                            } else {
                                mensajeError();
                            }
                        })
                        .catch((err) => mensajeErrorCatch(err));
                } else {
                    mensajeError();
                }
            })
            .catch((err) => mensajeErrorCatch(err));
    });
}

function eliminarProducto(id) {
    swal({
        text: "¿Esta seguro de eliminar el registro seleccionado?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#007b03",
        confirmButtonText: "Aceptar",
        allowOutsideClick: false,
        allowEscapeKey: false
    }).then(function() {
        gql('mutation {eliminarProducto(id: ' + id + ')}')
            .then(function(data) {
                if (data.data != undefined) {
                    gql('query {obtenerTodosProducto{id,nombre}}')
                        .then(function(dataProducto) {
                            if (dataProducto.data != undefined) {
                                $("#listaProducto").html("");
                                if (dataProducto.data.obtenerTodosProducto.length > 0) {
                                    var obj = dataProducto.data.obtenerTodosProducto;
                                    obj.forEach(function(producto) {
                                        $("#listaProducto").append('<li class="collection-item"><div class="row mp0">' + producto.nombre + '<a class="secondary-content"><i data-target="modalEditarProducto" class="material-icons modal-trigger pointer botonEditar" onclick="editarProducto(' + producto.id + ')">mode_edit</i><i class="material-icons pointer botonEliminar" onclick="eliminarProducto(' + producto.id + ')">delete</i></a></div></li>');
                                    });
                                }
                                mensajeProcesado();
                            } else {
                                mensajeError();
                            }
                        })
                        .catch((err) => mensajeErrorCatch(err));

                } else {
                    mensajeError();
                }
            })
            .catch((err) => mensajeErrorCatch(err));
    });
}