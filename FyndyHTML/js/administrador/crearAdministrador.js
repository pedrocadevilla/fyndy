function consultarDatos() {
    gql('query {obtenerCategorias{id,nombre}}')
        .then(function(data) {
            if (data != undefined) {
                var cat = document.getElementById("categoriaRubro");
                while (cat.length > 1) {
                    cat.remove(1);
                }
                var cat2 = document.getElementById("categoriaProducto");
                while (cat2.length > 1) {
                    cat2.remove(1);
                }
                $("#listaCategoria").html("");
                $("#listaRubro").html("");
                $("#listaProducto").html("");
                if (data.data.obtenerCategorias.length > 0) {
                    var obj = data.data.obtenerCategorias;
                    obj.forEach(function(categoria) {
                        $("#categoriaRubro").append('<option value=' + categoria.id + ' >' + categoria.nombre + '</option>');
                        $("#categoriaProducto").append('<option value=' + categoria.id + ' >' + categoria.nombre + '</option>');
                        $("#listaCategoria").append('<li class="collection-item"><div class="row mp0"><span>' + categoria.nombre + '</span><img src="images/BotonesSeleccion/edit.svg" class="logoVerAdministrador pointer" onclick="consultaEditarCategoria(' + categoria.id + ')" /><img src="images/BotonesSeleccion/delete.svg" class="logoVerAdministrador pointer" onclick="eliminarCategoria(' + categoria.id + ')" /></div></li>');
                    });
                    $('select').formSelect();
                }
                gql('query {obtenerTodosRubros{id,nombre}}')
                    .then(function(dataRubro) {
                        if (dataRubro.data != undefined) {
                            if (dataRubro.data.obtenerTodosRubros.length > 0) {
                                var obj = dataRubro.data.obtenerTodosRubros;
                                obj.forEach(function(rubro) {
                                    $("#listaRubro").append('<li class="collection-item"><div class="row mp0"><span>' + rubro.nombre + '</span><img src="images/BotonesSeleccion/edit.svg" class="logoVerAdministrador pointer" onclick="consultaEditarRubro(' + rubro.id + ')" /><img src="images/BotonesSeleccion/delete.svg" class="logoVerAdministrador pointer" onclick="eliminarRubro(' + rubro.id + ')" /></div></li>');
                                });
                            }
                            gql('query {obtenerTodosProducto{id,nombre}}')
                                .then(function(dataProducto) {
                                    if (dataProducto.data != undefined) {
                                        if (dataProducto.data.obtenerTodosProducto.length > 0) {
                                            var obj = dataProducto.data.obtenerTodosProducto;
                                            obj.forEach(function(producto) {
                                                $("#listaProducto").append('<li class="collection-item"><div class="row mp0"><span>' + producto.nombre + '</span><img src="images/BotonesSeleccion/edit.svg" class="logoVerAdministrador pointer" onclick="consultaEditarProducto(' + producto.id + ')" /><img src="images/BotonesSeleccion/delete.svg" class="logoVerAdministrador pointer" onclick="eliminarProducto(' + producto.id + ')" /></div></li>');
                                            });
                                            quitarLoader();
                                        }
                                    } else {
                                        mensajeError();
                                    }
                                    $('select').formSelect();
                                    quitarLoader();
                                })
                                .catch((err) => mensajeErrorCatch(err));
                        } else {
                            mensajeError();
                        }
                    })
                    .catch((err) => mensajeErrorCatch(err));
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function crearCategoria() {
    if ($("#categoriaInput").val() != "") {
        gql('mutation {crearCategoria(categoria: {nombre: "' + $("#categoriaInput").val() + '"}){id}}')
            .then(function(data) {
                if (data.data != undefined) {
                    gql('query {obtenerCategorias{id,nombre}}')
                        .then(function(data) {
                            if (data.data != undefined) {
                                var cat = document.getElementById("categoriaRubro");
                                while (cat.length > 1) {
                                    cat.remove(1);
                                }
                                var cat2 = document.getElementById("categoriaProducto");
                                while (cat2.length > 1) {
                                    cat2.remove(1);
                                }
                                $("#listaCategoria").html("");
                                if (data.data.obtenerCategorias.length > 0) {
                                    var obj = data.data.obtenerCategorias;
                                    obj.forEach(function(categoria) {
                                        $("#categoriaRubro").append('<option value=' + categoria.id + ' >' + categoria.nombre + '</option>');
                                        $("#categoriaProducto").append('<option value=' + categoria.id + ' >' + categoria.nombre + '</option>');
                                        $("#listaCategoria").append('<li class="collection-item"><div class="row mp0"><span>' + categoria.nombre + '</span><img src="images/BotonesSeleccion/edit.svg" class="logoVerAdministrador pointer" onclick="consultaEditarCategoria(' + categoria.id + ')" /><img src="images/BotonesSeleccion/delete.svg" class="logoVerAdministrador pointer" onclick="eliminarCategoria(' + categoria.id + ')" /></div></li>');
                                    });
                                    $('select').formSelect();
                                    document.getElementById("categoriaInput").value = "";
                                    mensajeProcesado();
                                }
                            } else {
                                mensajeError();
                            }
                        })
                        .catch((err) => mensajeErrorCatch(err));
                } else {
                    mensajeError();
                }
            })
            .catch((err) => mensajeErrorCatch(err));
    } else {
        mensajeLlenarCampos();
    }
}

function limpiarCategoria() {
    document.getElementById("categoriaInput").value = "";
}

function consultarRubro(categoriaId) {
    document.getElementById("rubroProducto").disabled = false;
    gql('query {obtenerRubros(id:' + categoriaId.value + '){id,nombre}}')
        .then(function(data) {
            if (data.data != undefined) {
                var rubro = document.getElementById("rubroProducto");
                while (rubro.length > 1) {
                    rubro.remove(1);
                }
                if (data.data.obtenerRubros.length > 0) {
                    var obj = data.data.obtenerRubros;
                    obj.forEach(function(listaRubro) {
                        $("#rubroProducto").append('<option value=' + listaRubro.id + ' >' + listaRubro.nombre + '</option>');
                    });
                    document.getElementById("rubroProducto").value = '0';
                }
                $('#rubroProducto').formSelect();
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function crearRubro() {
    if ($("#rubroInput").val() != "" && $("#categoriaRubro").val() != null) {
        gql('mutation {crearRubro(rubro: {nombre: "' + $("#rubroInput").val() + '", categoriaId: ' + $("#categoriaRubro").val() + '}){id}}')
            .then(function(data) {
                if (data.data != undefined) {
                    document.getElementById("categoriaRubro").value = '0';
                    document.getElementById("rubroInput").value = "";
                    $('select').formSelect();
                    if (data.data != undefined) {
                        gql('query {obtenerTodosRubros{id,nombre}}')
                            .then(function(dataRubro) {
                                if (dataRubro.data != undefined) {
                                    $("#listaRubro").html("");
                                    if (dataRubro.data.obtenerTodosRubros.length > 0) {
                                        var obj = dataRubro.data.obtenerTodosRubros;
                                        obj.forEach(function(rubro) {
                                            $("#listaRubro").append('<li class="collection-item"><div class="row mp0"><span>' + rubro.nombre + '</span><img src="images/BotonesSeleccion/edit.svg" class="logoVerAdministrador pointer" onclick="consultaEditarRubro(' + rubro.id + ')" /><img src="images/BotonesSeleccion/delete.svg" class="logoVerAdministrador pointer" onclick="eliminarRubro(' + rubro.id + ')" /></div></li>');
                                        });
                                    }
                                    mensajeProcesado();
                                } else {
                                    mensajeError();
                                }
                            })
                            .catch((err) => mensajeErrorCatch(err));
                    } else {
                        mensajeError();
                    }
                } else {
                    mensajeError();
                }
            })
            .catch((err) => mensajeErrorCatch(err));
    } else {
        mensajeLlenarCampos();
    }
}

function limpiarRubro() {
    document.getElementById("categoriaRubro").value = '0';
    document.getElementById("rubroInput").value = "";
    $('select').formSelect();
}

function crearProducto() {
    if ($("#productoInput").val() != "" && $("#categoriaProducto").val() != null && $("#rubroProducto").val() != null && $("#palabras").val() != null) {
        gql('mutation {crearProducto(producto: {nombre: "' + $("#productoInput").val() + '", rubroId: ' + $("#rubroProducto").val() + ', palabrasClave: "' + $("#palabras").val() + '"}){id}}')
            .then(function(data) {
                if (data.data != undefined) {
                    document.getElementById("categoriaProducto").value = '0';
                    document.getElementById("rubroProducto").value = '0';
                    document.getElementById("productoInput").value = "";
                    document.getElementById("palabras").value = "";
                    document.getElementById("rubroProducto").disabled = true;
                    $('select').formSelect();
                    gql('query {obtenerTodosProducto{id,nombre}}')
                        .then(function(dataProducto) {
                            if (dataProducto.data != undefined) {
                                $("#listaProducto").html("");
                                if (dataProducto.data.obtenerTodosProducto.length > 0) {
                                    var obj = dataProducto.data.obtenerTodosProducto;
                                    obj.forEach(function(producto) {
                                        $("#listaProducto").append('<li class="collection-item"><div class="row mp0"><span>' + producto.nombre + '</span><img src="images/BotonesSeleccion/edit.svg" class="logoVerAdministrador pointer" onclick="consultaEditarProducto(' + producto.id + ')" /><img src="images/BotonesSeleccion/delete.svg" class="logoVerAdministrador pointer" onclick="eliminarProducto(' + producto.id + ')" /></div></li>');
                                    });
                                }
                                mensajeProcesado();
                            } else {
                                mensajeError();
                            }
                        })
                        .catch((err) => mensajeErrorCatch(err));

                } else {
                    mensajeError();
                }
            })
            .catch((err) => mensajeErrorCatch(err));
    } else {
        mensajeLlenarCampos();
    }
}

function limpiarProducto() {
    document.getElementById("categoriaProducto").value = '0';
    document.getElementById("rubroProducto").value = '0';
    document.getElementById("productoInput").value = "";
    document.getElementById("palabras").value = "";
    document.getElementById("rubroProducto").disabled = true;
    $('select').formSelect();
}

function validarEnterCategoria(event) {
    if (event.keyCode == 13) crearCategoria();
}

function validarEnterCategoriaEditar(event) {
    if (event.keyCode == 13) editarCategoria();
}

function validarEnterRubro(event) {
    if (event.keyCode == 13) crearRubro();
}

function validarEnterRubroEditar(event) {
    if (event.keyCode == 13) editarRubro();
}

function validarEnterProducto(event) {
    if (event.keyCode == 13) crearProducto();
}

function validarEnterProductoEditar(event) {
    if (event.keyCode == 13) editarProducto();
}