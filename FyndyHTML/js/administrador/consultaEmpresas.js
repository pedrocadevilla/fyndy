function buscarEmpresas() {
    gql('query{obtenerEmpresa{id,nombre}}')
        .then(function(data) {
            if (data.data != undefined) {
                $("#bodyEmpresa").html("");
                var obj = data.data.obtenerEmpresa;
                var i = 0
                obj.forEach(function(producto) {
                    hilera = document.createElement("tr");
                    hilera.setAttribute("id", obj[i].id);
                    hilera.setAttribute("onclick", "tomarEmpresa(this)");
                    for (var j = 0; j < 2; j++) {
                        celda = document.createElement("td");
                        if (j == 0) {
                            textoCelda = document.createTextNode(obj[i].id);
                        }
                        if (j == 1) {
                            textoCelda = document.createTextNode(obj[i].nombre);
                        }
                        celda.appendChild(textoCelda);
                        hilera.appendChild(celda);
                    }
                    $("#bodyEmpresa").append(hilera);
                    i++;
                });
                $("#empresas").footable({
                    "state": {
                        "filtering": false
                    },
                    "filtering": {
                        "delay": -1
                    }
                });
                $('ul.tabs').tabs();
                quitarLoader();
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}