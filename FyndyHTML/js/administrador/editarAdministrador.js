function consultaEditarCategoria(id) {
    gql('query {obtenerCategoriaId(id: ' + id + '){nombre}}')
        .then(function(data) {
            if (data.data != undefined) {
                $("#categoriaEditar").val(data.data.obtenerCategoriaId.nombre);
                almacenarLocalStorage("categoriaId", id);
                document.getElementById("categoriaEditar").focus();
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function consultaEditarRubro(id) {
    gql('query {obtenerRubroId(id: ' + id + '){nombre,obtenerCategoria{id}}}')
        .then(function(data) {
            if (data.data != undefined) {
                $("#rubroEditar").val(data.data.obtenerRubroId.nombre);
                almacenarLocalStorage("rubroId", id);
                document.getElementById("rubroEditar").focus();
                var idCategoria = data.data.obtenerRubroId.obtenerCategoria.id;
                gql('query {obtenerCategorias{id,nombre}}')
                    .then(function(dataCategoria) {
                        if (dataCategoria.data != undefined) {
                            var cat = document.getElementById("categoriaRubroEditar");
                            while (cat.length > 1) {
                                cat.remove(1);
                            }
                            if (dataCategoria.data.obtenerCategorias.length > 0) {
                                var obj = dataCategoria.data.obtenerCategorias;
                                obj.forEach(function(categoria) {
                                    $("#categoriaRubroEditar").append('<option value=' + categoria.id + ' >' + categoria.nombre + '</option>');
                                });
                                document.getElementById("categoriaRubroEditar").value = idCategoria;
                                $('select').formSelect();
                            }
                        } else {
                            mensajeError();
                        }
                    })
                    .catch((err) => mensajeErrorCatch(err));
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function consultaEditarProducto(id) {
    gql('query {obtenerProductoId(id: ' + id + '){nombre,palabrasClave,obtenerRubro{id,obtenerCategoria{id}}}}')
        .then(function(data) {
            if (data.data != undefined) {
                $("#productoEditar").val(data.data.obtenerProductoId.nombre);
                almacenarLocalStorage("productoId", id);
                document.getElementById("productoEditar").focus();
                $("#palabrasEditar").val(data.data.obtenerProductoId.palabrasClave);
                document.getElementById("palabrasEditar").focus();
                var idCategoria = data.data.obtenerProductoId.obtenerRubro.obtenerCategoria.id;
                var idRubro = data.data.obtenerProductoId.obtenerRubro.id;
                gql('query {obtenerTodosRubros{id,nombre}}')
                    .then(function(dataRubro) {
                        if (dataRubro.data != undefined) {
                            var cat = document.getElementById("rubroProductoEditar");
                            while (cat.length > 1) {
                                cat.remove(1);
                            }
                            if (dataRubro.data.obtenerTodosRubros.length > 0) {
                                var obj = dataRubro.data.obtenerTodosRubros;
                                obj.forEach(function(categoria) {
                                    $("#rubroProductoEditar").append('<option value=' + categoria.id + ' >' + categoria.nombre + '</option>');
                                });
                                document.getElementById("rubroProductoEditar").value = idRubro;
                                gql('query {obtenerCategorias{id,nombre}}')
                                    .then(function(dataCategoria) {
                                        if (dataCategoria.data != undefined) {
                                            var cat2 = document.getElementById("categoriaProductoEditar");
                                            while (cat2.length > 1) {
                                                cat2.remove(1);
                                            }
                                            if (dataCategoria.data.obtenerCategorias.length > 0) {
                                                var obj = dataCategoria.data.obtenerCategorias;
                                                obj.forEach(function(categoria) {
                                                    $("#categoriaProductoEditar").append('<option value=' + categoria.id + ' >' + categoria.nombre + '</option>');
                                                });
                                                document.getElementById("categoriaProductoEditar").value = idCategoria;
                                                $('select').formSelect();
                                            }
                                        } else {
                                            mensajeError();
                                        }
                                    })
                                    .catch((err) => mensajeErrorCatch(err));
                            }
                        } else {
                            mensajeError();
                        }
                    })
                    .catch((err) => mensajeErrorCatch(err));
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function consultarRubroEditarProducto(rubroeditar) {
    document.getElementById("rubroProducto").disabled = false;
    gql('query {obtenerRubros(id:' + rubroeditar.value + '){id,nombre}}')
        .then(function(data) {
            if (data.data != undefined) {
                var rubro = document.getElementById("rubroProductoEditar");
                while (rubro.length > 1) {
                    rubro.remove(1);
                }
                if (data.data.obtenerRubros.length > 0) {
                    var obj = data.data.obtenerRubros;
                    obj.forEach(function(listaRubro) {
                        $("#rubroProductoEditar").append('<option value=' + listaRubro.id + ' >' + listaRubro.nombre + '</option>');
                    });
                    document.getElementById("rubroProductoEditar").value = '0';
                }
                $('#rubroProductoEditar').formSelect();
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function editarCategoria(){
	if ($("#categoriaEditar").val() != "") {
        gql('mutation {modificarCategoria(id: ' + sacarLocalStorage("categoriaId") + ', categoria: {nombre: "' + $("#categoriaEditar").val() + '"}){id}}')
            .then(function(data) {
                if (data.data != undefined) {
                    gql('query {obtenerCategorias{id,nombre}}')
                        .then(function(data) {
                            if (data.data != undefined) {
                                var cat = document.getElementById("categoriaRubro");
                                while (cat.length > 1) {
                                    cat.remove(1);
                                }
                                var cat2 = document.getElementById("categoriaProducto");
                                while (cat2.length > 1) {
                                    cat2.remove(1);
                                }
                                $("#listaCategoria").html("");
                                if (data.data.obtenerCategorias.length > 0) {
                                    var obj = data.data.obtenerCategorias;
                                    obj.forEach(function(categoria) {
                                        $("#categoriaRubro").append('<option value=' + categoria.id + ' >' + categoria.nombre + '</option>');
                                        $("#categoriaProducto").append('<option value=' + categoria.id + ' >' + categoria.nombre + '</option>');
                                        $("#listaCategoria").append('<li class="collection-item"><div><span>' + categoria.nombre + '</span><a class="secondary-content"><i data-target="modalEditarCategoria" class="material-icons modal-trigger pointer botonEditar" onclick="consultaEditarCategoria(' + categoria.id + ')">mode_edit</i><i class="material-icons pointer botonEliminar" onclick="eliminarCategoria(' + categoria.id + ')">delete</i></a></div></li>');
                                    });
                                    $('select').formSelect();
                                    document.getElementById("categoriaInput").value = "";
                                    mensajeProcesado()
                                }
                            } else {
                                mensajeError();
                            }
                        })
                        .catch((err) => mensajeErrorCatch(err));
                } else {
                    mensajeError()
                }
            })
            .catch((err) => mensajeErrorCatch(err));
    } else {
        mensajeLlenarCampos()
    }
}

function editarRubro() {
    if ($("#rubroEditar").val() != "" && $("#categoriaRubroEditar").val() != null) {
        gql('mutation {modificarRubro(id: ' + sacarLocalStorage("rubroId") + ', rubro: {nombre: "' + $("#rubroEditar").val() + '", categoriaId: ' + $("#categoriaRubroEditar").val() + '}){id}}')
            .then(function(data) {
                if (data.data != undefined) {
                    if (data.data != undefined) {
                        gql('query {obtenerTodosRubros{id,nombre}}')
                            .then(function(data) {
                                if (data.data != undefined) {
                                    $("#listaRubro").html("");
                                    if (data.data.obtenerTodosRubros.length > 0) {
                                        var obj = data.data.obtenerTodosRubros;
                                        obj.forEach(function(rubro) {
                                            $("#listaRubro").append('<li class="collection-item"><div><span>' + rubro.nombre + '</span><a class="secondary-content"><i data-target="modalEditarRubro" class="material-icons modal-trigger pointer botonEditar" onclick="consultaEditarRubro(' + rubro.id + ')">mode_edit</i><i class="material-icons pointer botonEliminar" onclick="eliminarRubro(' + rubro.id + ')">delete</i></a></div></li>');
                                        });
                                    }
                                    mensajeProcesado();
                                } else {
                                    mensajeError();
                                }
                            })
                            .catch((err) => mensajeErrorCatch(err));
                    } else {
                        mensajeError();
                    }
                } else {
                    mensajeError();
                }
            })
            .catch((err) => mensajeErrorCatch(err));
    } else {
        mensajeLlenarCampos();
    }
}

function editarProducto() {
    if ($("#productoEditar").val() != "" && $("#categoriaProductoEditar").val() != null && $("#rubroProductoEditar").val() != null &&  $("#palabrasEditar").val() != null) {
        gql('mutation {modificarProducto(id: ' + sacarLocalStorage("productoId") + ', producto: {nombre: "' + $("#productoEditar").val() + '", rubroId: ' + $("#rubroProductoEditar").val() + ', palabrasClave: "' + $("#palabrasEditar").val() + '"}){id}}')
            .then(function(data) {
                if (data.data != undefined) {
                    gql('query {obtenerTodosProducto{id,nombre}}')
                        .then(function(data) {
                            if (data.data != undefined) {
                                $("#listaProducto").html("");
                                if (data.data.obtenerTodosProducto.length > 0) {
                                    var obj = data.data.obtenerTodosProducto;
                                    obj.forEach(function(producto) {
                                        $("#listaProducto").append('<li class="collection-item"><div><span>' + producto.nombre + '</span><a class="secondary-content"><i data-target="modalEditarProducto" class="material-icons modal-trigger pointer botonEditar" onclick="consultaEditarProducto(' + producto.id + ')">mode_edit</i><i class="material-icons pointer botonEliminar" onclick="eliminarProducto(' + producto.id + ')">delete</i></a></div></li>');
                                    });
                                }
                                mensajeProcesado();
                            } else {
                                mensajeError();
                            }
                        })
                        .catch((err) => mensajeErrorCatch(err));

                } else {
                    mensajeError();
                }
            })
            .catch((err) => mensajeErrorCatch(err));
    } else {
        mensajeLlenarCampos();
    }
}