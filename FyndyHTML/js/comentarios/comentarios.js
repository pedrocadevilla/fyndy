var vec = [];

function mostrarComentarios() {
    var nombre = "";
    gql('query consulta{obtenerComentariosEmpresa(id: ' + sacarLocalStorage("idEmpresa") + '){id,comentarios,clasificacion,fecha,obtenerUsuario{primerNombre,primerApellido}}}')
        .then(function(data) {
            if (data.data != undefined) {
                $("#comentariosLista").html("");
                if (data.data.obtenerComentariosEmpresa != undefined && data.data.obtenerComentariosEmpresa.length > 0) {
                    var obj = data.data.obtenerComentariosEmpresa;
                    obj.forEach(function(comentario) {
                        if(comentario.obtenerTipoUsuario.nombre == "Empresa"){
                            nombre = comentario.obtenerEmpresa.nombre;
                        }else{
                            nombre = comentario.obtenerUsuario.primerNombre + ' ' + comentario.obtenerUsuario.primerApellido;
                        }
                        if (vec.includes(comentario.id))
                            $("#comentariosLista").prepend('<div class="comentario z-depth-1"><div class="row" style="background-color: white"><div class="col s12 m12 l12"><p class="left">' + nombre + '</p><p class="right ">' + comentario.fecha + '</p></div><div class="col s12 m12 l12"><p class="left">' + comentario.comentarios + '</p><a class="secondary-content"><i data-target="modalEditarComentario" class="material-icons modal-trigger pointer botonEditar" onclick="consultarEditarComentario(' + comentario.id + ')">mode_edit</i><i class="material-icons pointer botonEliminar" onclick="eliminarComentario(' + comentario.id + ')">delete</i></a></div></div></div>');
                        else
                            $("#comentariosLista").prepend('<div class="comentario z-depth-1"><div class="row" style="background-color: white"><div class="col s12 m12 l12"><p class="left">' + nombre + '</p><p class="right ">' + comentario.fecha + '</p></div><div class="col s12 m12 l12"><p class="left">' + comentario.comentarios + '</p></div></div></div>');
                    });
                }
                consultaCategorias();
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function consultarEditarComentario(id) {
    gql('query {obtenerComentariosXId(id: ' + id + '){comentarios}}')
        .then(function(data) {
            if (data.data != undefined) {
                $("#comentarioEditar").val(data.data.obtenerComentariosXId.comentarios);
                almacenarLocalStorage("comentarioId", id);
                document.getElementById("comentarioEditar").focus();
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function editarComentario() {
    if ($("#comentarioEditar").val() != "") {
        gql('mutation {modificarComentario(id: ' + sacarLocalStorage("comentarioId") + ', comentario: {comentarios: "' + $("#comentarioEditar").val() + '"}){id}}')
            .then(function(data) {
                if (data.data != undefined) {
                    mostrarComentarios();
                } else {
                    mensajeError()
                }
            })
            .catch((err) => mensajeErrorCatch(err));
    } else {
        mensajeLlenarCampos()
    }
}

function crearComentario() {
    var tipo = "";
    if (sacarLocalStorage("usuarioId") != null || sacarLocalStorage("empresaId") != null) {
        if(sacarLocalStorage("usuarioId") != null) tipo = "Usuario";
        if(sacarLocalStorage("empresaId") != null) tipo = "Empresa";
        if ($("#nota").val() != "") {
            gql('mutation {crearComentario(comentario: {comentarios: "' + $("#nota").val() + '", empresaId: ' + sacarLocalStorage("idEmpresa") + ', usuarioId: ' + sacarLocalStorage("usuarioId") + '}, tipo: "' + tipo + '"){id}}')
                .then(function(data) {
                    if (data.data != undefined) {
                        vec.push(data.data.crearComentario.id);
                        $("#nota").val("");
                        mostrarComentarios();
                    } else {
                        mensajeError();
                    }
                })
                .catch((err) => mensajeErrorCatch(err));
        } else {
            mensajeLlenarCampos();
        }
    } else {
        mensajeIniciarSesion();      
    }
}

function eliminarComentario(id) {
    swal({
        text: "¿Esta seguro de eliminar el comentario?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#007b03",
        confirmButtonText: "Aceptar",
        allowOutsideClick: false,
        allowEscapeKey: false
    }).then(function() {
        gql('mutation {eliminarComentario(id: ' + id + ')}')
            .then(function(data) {
                if (data.data != undefined) {
                    mostrarComentarios();
                } else {
                    mensajeError();
                }
            })
            .catch((err) => mensajeErrorCatch(err));
    });
}