function consultarProductosRegistradosEmpresa() {
    gql('query consulta{consultarProductosEmpresa(empresaId: ' + sacarLocalStorage("empresaId") + ', categoriaId: ' + $("#categoriaProducto").val() + ', rubroId: ' + $("#rubroProducto").val() + '){nodes{obtenerCategoria{nombre}categoriaId,rubroEmpresa{nodes{obtenerRubro{nombre}rubroId,productoEmpresa{nodes{obtenerProducto{nombre}id}}}}}}}')
        .then(function(data) {
            if (data.data != undefined) {
                $("#listaProductosRegistrados").html("");
                var datos = [];
                if (data.data.consultarProductosEmpresa.nodes.length > 0) {
                    var obj = data.data.consultarProductosEmpresa.nodes;
                    obj.forEach(function(categoria) {
                        var obj2 = categoria.rubroEmpresa.nodes;
                        obj2.forEach(function(rubro) {
                            var obj3 = rubro.productoEmpresa.nodes;
                            obj3.forEach(function(producto) {
                                datos.push([producto.obtenerProducto.nombre, producto.id]);
                            });
                        });
                    });
                }
                if ($("#categoriaProducto").val() == null) {
                    gql('query consultaEmpresa {consultarProductosEmpresa(empresaId: ' + sacarLocalStorage("empresaId") + ', categoriaId: null, rubroId: null) {nodes {obtenerCategoria {nombre}categoriaId}}}')
                        .then(function(data) {
                            var cat = document.getElementById("categoriaProducto");
                            while (cat.length > 1) {
                                cat.remove(1);
                            }
                            if (data.data.consultarProductosEmpresa.nodes.length > 0) {
                                var obj = data.data.consultarProductosEmpresa.nodes;
                                obj.forEach(function(categoria) {
                                    $("#categoriaProducto").append('<option value=' + categoria.categoriaId + ' >' + categoria.obtenerCategoria.nombre + '</option>');
                                });
                                $('select').formSelect();
                            }
                        })
                        .catch((err) => mensajeErrorCatch(err));
                }
                $('select').formSelect();
                $('#paginationContainer').pagination({
                    dataSource: datos.sort(),
                    pageSize: 9,
                    callback: function(data, pagination) {
                        var html = templateRegistradosEmpresa(data);
                        $("#listaProductosRegistrados").html(html);
                    }
                });
                $('#paginationContainer').addHook('afterPreviousOnClick', function() {
                    $('html, body').animate({ scrollTop: 0 }, 500, 'swing');
                });
                $('#paginationContainer').addHook('afterPageOnClick', function() {
                    $('html, body').animate({ scrollTop: 0 }, 500, 'swing');
                });
                $('#paginationContainer').addHook('afterNextOnClick', function() {
                    $('html, body').animate({ scrollTop: 0 }, 500, 'swing');
                });
                quitarLoader();
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function consultarRubro(categoriaId) {
    gql('query {obtenerRubros(id:' + categoriaId.value + '){id,nombre}}')
        .then(function(data) {
            if (data.data != undefined) {
                var rubro = document.getElementById("rubroProducto");
                while (rubro.length > 1) {
                    rubro.remove(1);
                }
                if (data.data.obtenerRubros.length > 0) {
                    document.getElementById("rubroProducto").disabled = false;
                    var obj = data.data.obtenerRubros;
                    obj.forEach(function(listaRubro) {
                        $("#rubroProducto").append('<option value=' + listaRubro.id + ' >' + listaRubro.nombre + '</option>');
                    });
                    document.getElementById("rubroProducto").value = '0';
                }
                consultarProductosRegistradosEmpresa();
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function templateRegistradosEmpresa(data) {
    var html = "";
    data.forEach(function(producto) {
        html += '<li class="collection-item"><div class="row mp0"><span>' + producto[0] + '</span><img src="images/BotonesSeleccion/delete.svg" class="logoVer pointer" onclick="eliminarProducto(' + producto[1] + ')" /></div></li>';
    });
    return html;
}

function estadoReinicio() {
    document.getElementById("categoriaProducto").value = '0';
    document.getElementById("rubroProducto").value = '0';
    document.getElementById("rubroProducto").disabled = true;
    consultarProductosRegistradosEmpresa();
    $('select').formSelect();
}

function eliminarProducto(id) {
    swal({
        text: "¿Esta seguro de eliminar el producto seleccionado?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#007b03",
        confirmButtonText: "Aceptar",
        allowOutsideClick: false,
        allowEscapeKey: false
    }).then(function() {
        ponerLoader();
        gql('mutation {eliminarProductoEmpresa(id: ' + id + ')}')
            .then(function(data) {
                if (data.data != undefined) {
                    consultarProductosRegistradosEmpresa();
                } else {
                    mensajeError();
                }
            })
            .catch((err) => mensajeErrorCatch(err));
    });
}