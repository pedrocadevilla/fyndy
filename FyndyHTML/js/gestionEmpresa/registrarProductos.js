function consultarDatos() {
    gql('query {obtenerCategorias{id,nombre}}')
        .then(function(data) {
            if (data.data != undefined) {
                var cat = document.getElementById("categoriaProducto");
                while (cat.length > 1) {
                    cat.remove(1);
                }
                $("#listaCategoria").html("");
                $("#listaRubro").html("");
                $("#listaProducto").html("");
                if (data.data.obtenerCategorias.length > 0) {
                    var obj = data.data.obtenerCategorias;
                    obj.forEach(function(categoria) {
                        $("#categoriaProducto").append('<option value=' + categoria.id + ' >' + categoria.nombre + '</option>');
                    });
                }
                $('select').formSelect();
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function limpiar() {
    document.getElementById("categoriaProducto").value = '0';
    document.getElementById("rubroProducto").value = '0';
    document.getElementById("productoInput").value = '0';
    document.getElementById("rubroProducto").disabled = true;
    document.getElementById("productoInput").disabled = true;
    $('select').formSelect();
}

function consultarRubro(categoriaId) {
    gql('query {obtenerRubros(id:' + categoriaId.value + '){id,nombre}}')
        .then(function(data) {
            if (data.data != undefined) {
                var rubro = document.getElementById("rubroProducto");
                while (rubro.length > 1) {
                    rubro.remove(1);
                }
                if (data.data.obtenerRubros.length > 0) {
                    document.getElementById("rubroProducto").disabled = false;
                    $("productoInput").attr("disabled", true);
                    var obj = data.data.obtenerRubros;
                    obj.forEach(function(listaRubro) {
                        $("#rubroProducto").append('<option value=' + listaRubro.id + ' >' + listaRubro.nombre + '</option>');
                    });
                    document.getElementById("rubroProducto").value = '0';
                }
                document.getElementById("productoInput").disabled = true;
                $('#rubroProducto').formSelect();
                var producto = document.getElementById("productoInput");
                while (producto.length > 1) {
                    producto.remove(1);
                }
                $('#productoInput').formSelect();
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function consultarProducto(rubroId) {
    gql('query {obtenerProducto(id:' + rubroId.value + '){id,nombre}}')
        .then(function(data) {
            if (data.data != undefined) {
                var producto = document.getElementById("productoInput");
                while (producto.length > 1) {
                    producto.remove(1);
                }
                if (data.data.obtenerProducto.length > 0) {
                    document.getElementById("productoInput").disabled = false;
                    var obj = data.data.obtenerProducto;
                    $("#productoInput").append('<option value="-1">Seleccionar Todos</option>');
                    obj.forEach(function(listaProducto) {
                        $("#productoInput").append('<option value=' + listaProducto.id + ' >' + listaProducto.nombre + '</option>');
                    });
                    document.getElementById("productoInput").value = '0';
                }
                $('#productoInput').formSelect();
                almacenarLocalStorage("seleccion", 0);
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function crearProducto() {
    if ($("#productoInput").val() != "" && $("#categoriaProducto").val() != null && $("#rubroProducto").val() != null) {
        var productos = $('#productoInput option:selected').map(function(i, el) {
            var result = {};
            if ($(el).val() != -1) {
                result['productoId'] = $(el).val();
                return result;
            }
        }).get();
        productos = JSON.stringify(productos);
        productos = productos.replace(/"/g, '')
        gql('mutation {crearProductoEmpresa(producto: ' + productos + ', categoria: {categoriaId: ' + $("#categoriaProducto").val() + ', empresaId: ' + sacarLocalStorage("empresaId") + '}, rubro: {rubroId:' + $("#rubroProducto").val() + '}){id}}')
            .then(function(data) {
                if (data.data != undefined) {
                    document.getElementById("categoriaProducto").value = '0';
                    document.getElementById("rubroProducto").value = '0';
                    document.getElementById("productoInput").value = '0';
                    document.getElementById("rubroProducto").disabled = true;
                    document.getElementById("productoInput").disabled = true;
                    $('select').formSelect();
                    mensajeProcesado();
                } else {
                    mensajeError();
                }
            })
            .catch((err) => mensajeErrorCatch(err));
    } else {
        mensajeLlenarCampos();
    }
}