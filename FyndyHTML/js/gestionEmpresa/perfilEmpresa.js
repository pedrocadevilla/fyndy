function preview_image(val) {
    let reader = new FileReader();
    reader.onload = function() {
        if (screen.width < 500) {
            ancho = (screen.width / 2) - 40;
        } else {
            ancho = (screen.width / 4) - 100;
        }
        let output = document.getElementById('modalImagen');
        output.src = reader.result;
        if (val == 1) {
            $('#modalImagen').attr("width", ancho);
            setTimeout(function() {
                let elem = document.getElementById("foto");
                elem.src = reader.result;
                resizebase64(reader.result, 900, 900, "fotoPerfil");
            }, 1000);
        }
        else{
            $('#modalImagen').attr("width", ancho);
            setTimeout(function() {
                let elem = document.getElementById("fotoCarrete");
                elem.src = reader.result;
                resizebase64(reader.result, 900, 900, "fotoCarrete");
            }, 1000);
        }
    }
    reader.readAsDataURL(event.target.files[0]);
}

function consultarEmpresa() {
    gql('query{ obtenerEmpresaXId(id: ' + sacarLocalStorage("empresaId") + ', frase: "", productoId: 0){ descripcion, fotoPerfil, nombre, palabrasClave, cantidadTrabajadores, inicioOperaciones, latitud, longuitud, obtenerHorario{ horario{ horario1, id } }, obtenerContacto{ contacto, id }, obtenerImagenes{ imagen }, obtenerRedSocial{ link }, obtenerTipoEmpresa{ nombre } }}')
        .then(function(data) {
            if (data.data != undefined) {
                if (data.data.obtenerEmpresaXId != undefined) {
                    let obj = data.data.obtenerEmpresaXId;
                    $("#nombre").html(obj.nombre);
                    if (obj.fotoPerfil != null) {
                        $("#fotoPerfil").html("");
                        $("#fotoPerfil").html('<img class="responsive-img fotoPerfil" src="' + obj.fotoPerfil + '"><a href="#modalImagen" class="modal-trigger pointer right"><img class="edit" src="images/BotonesSeleccion/edit.svg" /></a>');
                    }
                    if (obj.obtenerTipoEmpresa != null) {
                        $("#tipoEmpresaInfo").html("");
                        $("#tipoEmpresaInfo").html('<p>' + obj.obtenerTipoEmpresa.nombre + '</p><img onclick="editField(\'tipoEmpresa\')" class="edit pointer right" src="images/BotonesSeleccion/edit.svg" />');
                    }
                    if (obj.cantidadTrabajadores != null) {
                        $("#cantidadTrabajadoresInfo").html("");
                        $("#cantidadTrabajadoresInfo").html('<p>' + obj.cantidadTrabajadores + '</p><img onclick="editField(\'cantidadTrabajadores\')" class="edit pointer right" src="images/BotonesSeleccion/edit.svg" />');
                    }
                    if (obj.inicioOperaciones != null) {
                        $("#inicioOperacionesInfo").html("");
                        $("#inicioOperacionesInfo").html('<p>' + obj.inicioOperaciones.substr(0,10) + '</p><img onclick="editField(\'inicioOperaciones\')" class="edit pointer right" src="images/BotonesSeleccion/edit.svg" />');
                    }
                    if (obj.latitud != null) {
                        $("#latLongInfo").html("");
                        $("#latLongInfo").html('<p><a href="https://www.google.com/maps/@' + obj.latitud + ',' + obj.longuitud + ',19z?hl=es-ES">UBICACION</a></p><img onclick="editField(\'latLong\')" class="edit pointer right" src="images/BotonesSeleccion/edit.svg" />');
                    }
                    if (obj.descripcion != null) {
                        $("#descripcionInfo").html("");
                        $("#descripcionInfo").html('<p>' + obj.descripcion + '</p><img onclick="editField(\'descripcion\')" class="edit pointer right" src="images/BotonesSeleccion/edit.svg" />');
                    }
                    if (obj.obtenerHorario.length > 0) {
                        $("#horarioContactoInfo").html("");
                        let listaHorario = obj.obtenerHorario;
                        listaHorario.forEach(function(lista) {
                            $("#horarioContactoInfo").append('<p>' + lista.horario.horario1 + '</p>');
                        });
                        $("#horarioContactoInfo").append('<a onclick="editField(\'horarioContacto\')" class="pointer right"><img class="edit" src="images/BotonesSeleccion/edit.svg" /></a>');
                    }
                    if (obj.obtenerContacto.length > 0) {
                        $("#datoContactoInfo").html("");
                        let listaContacto = obj.obtenerContacto;
                        listaContacto.forEach(function(lista) {
                            $("#datoContactoInfo").append('<p>' + lista.contacto + '</p>');
                        });
                        $("#datoContactoInfo").append('<a onclick="editField(\'datoContacto\')" class="pointer right"><img class="edit" src="images/BotonesSeleccion/edit.svg" /></a>');
                    }
                    if (obj.obtenerRedSocial.length > 0) {
                        let listaRedSocial = obj.obteneobtenerRedSocialrContacto;
                        listaRedSocial.forEach(function(lista) {
                            $("#datoContactoInfo").append('<p>' + lista.link + '</p>');
                        });
                        $("#datoContactoInfo").append('<a onclick="editField(\'datoContacto\')" class="pointer right"><img class="edit" src="images/BotonesSeleccion/edit.svg" /></a>');
                    }
                    if (obj.obtenerImagenes.length > 0) {
                        $("#imagenes").html("");
                        let listaImagen = obj.obtenerImagenes;
                        listaImagen.forEach(function(lista) {
                            $("#imagenes").append('<a class="carousel-item"><img src="' + lista.imagen + '"></a>');
                        });
                    }
                    if (obj.palabrasClave != null) {
                        let palabras = obj.palabrasClave.split(',');
                        almacenarLocalStorage("vectorPalabras", palabras);
                        $("#listaPalabras").html("");
                        palabras.forEach(function(palabra) {
                            $("#listaPalabras").append('<li class="collection-item"><div class="row mp0"><span>' + palabra + '</span><img onclick="deleteWord(\'' + palabra + '\')" src="images/BotonesSeleccion/delete.svg" class="logoVer pointer"/><img onclick="showEditInput(\'' + palabra + '\')" src="images/BotonesSeleccion/edit.svg" class="logoVer pointer"/></div></li>');
                        });
                    }
                    quitarLoader();
                    consultarSelectHorario();
                }
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function editField(campo){
    $("#" + campo + "Edit").removeAttr("style");
    $("#" + campo).css("display", "none");
}

function cancelEdit(campo){
    $("#" + campo).removeAttr("style");
    $("#" + campo + "Edit").css("display", "none");
}

function finishEdit(campo, tipo){
    $("#" + campo + "Info").html("");
    if(tipo < 3)
        $("#" + campo + "Info").html('<p>' + $("#" + campo + "Text").val() + '</p><a onclick="editField(\'' + campo + '\')" class="pointer right"><img class="edit" src="images/BotonesSeleccion/edit.svg" /></a>');
    if(tipo === 3)
        $("#" + campo + "Info").html('<p><a href="https://www.google.com/maps/@' + sacarLocalStorage("lat") + ',' + sacarLocalStorage("long")  + ',19z?hl=es-ES">UBICACION</a></p><img onclick="editField(\'' + campo + '\')" class="edit pointer right" src="images/BotonesSeleccion/edit.svg" />');
    if(tipo === 4){
        let horarios =
        $('#horarioContactoText option:selected').map(function(i, el){
            var result = [];
            if ($(el).val() != -1) {
                result.push($(el)[0].innerHTML);
                return result;
            }
        }).get();
        if (horarios.length > 0) {
            horarios.forEach(function(lista) {
                $("#" + campo + "Info").append('<p>' + lista + '</p>');
            });
            $("#" + campo + "Info").append('<a onclick="editField(\'' + campo + '\')" class="pointer right"><img class="edit" src="images/BotonesSeleccion/edit.svg" /></a>');
        }
    }
    if(tipo === 5)
        $("#" + campo + "Info").html('<p>' + sacarLocalStorage("nombreTipo") + '</p><a onclick="editField(\'' + campo + '\')" class="pointer right"><img class="edit" src="images/BotonesSeleccion/edit.svg" /></a>');
    $("#" + campo).removeAttr("style");
    $("#" + campo + "Edit").css("display", "none");
}

function validarRadio(elem){
    if (elem.id === "telefonoRadio")
        $("#redSocialDiv").css("display", "none");
    if (elem.id === "redSocialRadio")
        $("#redSocialDiv").removeAttr("style");
}

function consultarSelectHorario() {
    gql('query {obtenerHorarios{id,horario1}}')
        .then(function(dataHorario) {
            if (dataHorario.data != undefined) {
                if (dataHorario.data.obtenerHorarios.length > 0) {
                    var obj = dataHorario.data.obtenerHorarios;
                    var select = document.getElementById("horarioContactoText");
                    while (select.length > 1) {
                        select.remove(1);
                    }
                    $("#horarioContactoText").append('<option value="-1">Seleccionar Todos</option>');
                    obj.forEach(function(horario) {
                        $("#horarioContactoText").append('<option value=' + horario.id + ' >' + horario.horario1 + '</option>');
                    });
                    almacenarLocalStorage("seleccion", 0);
                    consultarSelectTipoEmpresa();
                }
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function consultarSelectTipoEmpresa() {
    gql('query {obtenerTipoEmpresa{id,nombre}}')
        .then(function(dataTipoEmpresa) {
            if (dataTipoEmpresa.data != undefined) {
                if (dataTipoEmpresa.data.obtenerTipoEmpresa.length > 0) {
                    var obj = dataTipoEmpresa.data.obtenerTipoEmpresa;
                    var select = document.getElementById("tipoEmpresaText");
                    while (select.length > 1) {
                        select.remove(1);
                    }
                    obj.forEach(function(tipo) {
                        $("#tipoEmpresaText").append('<option value=' + tipo.id + ' >' + tipo.nombre + '</option>');
                    });
                    consultarSelectRedSocial();
                }
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function consultarSelectRedSocial() {
    gql('query {obtenerRedSocial{id,nombre}}')
        .then(function(dataRedSocial) {
            if (dataRedSocial.data != undefined) {
                if (dataRedSocial.data.obtenerRedSocial.length > 0) {
                    var obj = dataRedSocial.data.obtenerRedSocial;
                    var select = document.getElementById("redSocial");
                    while (select.length > 1) {
                        select.remove(1);
                    }
                    obj.forEach(function(tipo) {
                        $("#redSocial").append('<option value=' + tipo.id + ' >' + tipo.nombre + '</option>');
                    });
                    $('select').formSelect();
                }
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function modificaFotoPerfil() {
    if (sacarLocalStorage("fotoPerfil") != undefined) {
        gql('mutation {modificarEmpresa(id:' + sacarLocalStorage("empresaId") + ', empresa: {fotoPerfil: "' + sacarLocalStorage("fotoPerfil") + '" }){id}}')
            .then(function(data) {
                if (data.data != undefined) {
                    ponerLoader();
                    borrarLocalStorage("fotoPerfil");
                    consultarEmpresa();
                } else {
                    mensajeError();
                }
            })
            .catch((err) => mensajeErrorCatch(err));
    } else {
        mensajeLlenarCampos();
    }
}

function modificarTextValues(campo, tipo) {
    if ($("#" + campo + "Text").val() !== "") {
        let query = "";
        if(tipo === 1)
            query = 'mutation {modificarEmpresa(id:' + sacarLocalStorage("empresaId") + ', empresa: {' + campo + ': "' + $("#" + campo + "Text").val() + '" }){id}}';
        if(tipo === 2)
            query = 'mutation {modificarEmpresa(id:' + sacarLocalStorage("empresaId") + ', empresa: {' + campo + ': ' + $("#" + campo + "Text").val() + ' }){id}}';
        if(tipo === 3){
            let regexUrl = new RegExp("^https://www.google.com/maps/");
            let valuesUrl = regexUrl.exec($("#" + campo + "Text").val())
            if(valuesUrl !== null){
                let regex = new RegExp("(?<=@)(.*?)(?=z)");
                let values = regex.exec($("#" + campo + "Text").val());
                let lat = values[0].split(',')[0];
                let long = values[0].split(',')[1];
                almacenarLocalStorage("lat",lat);
                almacenarLocalStorage("lon",lon);
                query = 'mutation {modificarEmpresa(id:' + sacarLocalStorage("empresaId") + ', empresa: { latitud: "' + lat + '", longuitud: "' + long + '"}){id}}';
            }
            else{
                mensajeURLInvalido();
            }
        }
        if(tipo === 4){
            let horarios = $('#horarioContactoText option:selected').map(function(i, el) {
                var result = [];
                if ($(el).val() != -1) {
                    result.push($(el).val());
                    return result;
                }
            }).get();
            horarios = JSON.stringify(horarios);
            horarios = horarios.replace(/"/g, '');
            query = 'mutation {modificarHorarioEmpresa(empresaId: ' + sacarLocalStorage("empresaId") + ', horarioId: ' + horarios + '){}}';
        }
        if(tipo === 5){
            almacenarLocalStorage("nombreTipo", $("#" + campo + "Text option:selected").text());
            query = 'mutation {modificarEmpresa(id:' + sacarLocalStorage("empresaId") + ', empresa: {' + campo + 'Id: ' + $("#" + campo + "Text").val() + ' }){id}}';
        }
        if(tipo === 6){
            if(document.getElementById("telefonoRadio").checked === true){
                if (sacarLocalStorage("contactoId") != null)
                    query = 'mutation {modificarContactoEmpresa(contacto: { contacto:"' + $("#" + campo + "Text").val() + '", empresaId: ' + sacarLocalStorage("empresaId") + '}, contactoId: ' + sacarLocalStorage("contactoId") + '){id}}';
                else
                    query = 'mutation {modificarContactoEmpresa(contacto: { contacto:"' + $("#" + campo + "Text").val() + '", empresaId: ' + sacarLocalStorage("empresaId") + '}){id}}';
            }else{
                if (sacarLocalStorage("contactoId") != null)
                    query = 'mutation {modificarRedSocialEmpresa(redSocial: { link:"' + $("#" + campo + "Text").val() + '", empresaId: ' + sacarLocalStorage("empresaId") + ', redSocialId: ' + $("#redSocial").val() + '}, redSocialId: ' + sacarLocalStorage("contactoId") + '){id}}';
                else
                    query = 'mutation {modificarRedSocialEmpresa(redSocial: { link:"' + $("#" + campo + "Text").val() + '", empresaId: ' + sacarLocalStorage("empresaId") + ', redSocialId: ' + $("#redSocial").val() + '}){id}}';
            }
        }
        if(query != ""){
            gql(query)
            .then(function(data) {
                if (data.data != undefined) {
                    finishEdit(campo, tipo);
                } else {
                    mensajeError();
                }
            })
            .catch((err) => mensajeErrorCatch(err));
        }
    } else {
        mensajeLlenarCampos();
    }
}

function modificarPalabras() {
    if ($("#palabrasClave").val() != "") {
        gql('mutation {modificarEmpresa(id:' + sacarLocalStorage("empresaId") + ', empresa: {palabrasClave: "' + $("#palabrasClave").val() + '" }){palabrasClave}}')
            .then(function(data) {
                if (data.data != undefined) {
                    let obj = data.data.modificarEmpresa;
                    document.getElementById("palabrasClave").value = "";
                    let palabras = obj.palabrasClave.split(",");
                    almacenarLocalStorage("vectorPalabras", palabras);
                    $("#listaPalabras").html("");
                    palabras.forEach(function(palabra) {
                        $("#listaPalabras").append('<li class="collection-item"><div class="row mp0"><span>' + palabra + '</span><img onclick="deleteWord(\'' + palabra + '\')" src="images/BotonesSeleccion/delete.svg" class="logoVer pointer"/><img onclick="editWord(\'' + palabra + '\')" src="images/BotonesSeleccion/edit.svg" class="logoVer pointer"/></div></li>');
                    });
                } else {
                    mensajeError();
                }
            })
            .catch((err) => mensajeErrorCatch(err));
    } else {
        mensajeLlenarCampos();
    }
}

function showEditInput(palabraXEditar){
    let palabras = sacarLocalStorage("vectorPalabras").split(',');
    $("#listaPalabras").html("");
    palabras.forEach(function(palabra) {
        if(palabra !== palabraXEditar)
            $("#listaPalabras").append('<li class="collection-item"><div class="row mp0"><span>' + palabra + '</span><img onclick="deleteWord(\'' + palabra + '\')" src="images/BotonesSeleccion/delete.svg" class="logoVer pointer"/><img onclick="showEditInput(\'' + palabra + '\')" src="images/BotonesSeleccion/edit.svg" class="logoVer pointer"/></div></li>');
        else
            $("#listaPalabras").append('<li class="collection-item"><div class="row mp0"><div class="input-field col s10 m9 l10 mp0"><input id="palabrasClaveEditada" type="text" required="" aria-required="true" style="text-transform: uppercase" value="' + palabra + '" onkeyup="validarPalabraClaveEditada(event, \'' + palabra + '\')"/></div></div></li>');
    });
}

function editWord(palabra){
    gql('mutation {modificarPalabraEmpresa(id:' + sacarLocalStorage("empresaId") + ', palabra: "' + palabra + '", palabraEditada: "' + $("#palabrasClaveEditada").val() + '"){palabrasClave}}')
        .then(function(data) {
            if (data.data != undefined) {
                let obj = data.data.modificarPalabraEmpresa;
                let palabras = obj.palabrasClave.split(",");
                almacenarLocalStorage("vectorPalabras", palabras);
                $("#listaPalabras").html("");
                palabras.forEach(function(palabra) {
                    $("#listaPalabras").append('<li class="collection-item"><div class="row mp0"><span>' + palabra + '</span><img onclick="deleteWord(\'' + palabra + '\')" src="images/BotonesSeleccion/delete.svg" class="logoVer pointer"/><img onclick="showEditInput(\'' + palabra + '\')" src="images/BotonesSeleccion/edit.svg" class="logoVer pointer"/></div></li>');
                });
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}

function deleteWord(palabra){
    gql('mutation {modificarPalabraEmpresa(id:' + sacarLocalStorage("empresaId") + ', palabra: "' + palabra + '"){palabrasClave}}')
        .then(function(data) {
            if (data.data != undefined) {
                let obj = data.data.modificarPalabraEmpresa;
                let palabras = obj.palabrasClave.split(",");
                almacenarLocalStorage("vectorPalabras", palabras);
                $("#listaPalabras").html("");
                palabras.forEach(function(palabra) {
                    $("#listaPalabras").append('<li class="collection-item"><div class="row mp0"><span>' + palabra + '</span><img onclick="deleteWord(\'' + palabra + '\')" src="images/BotonesSeleccion/delete.svg" class="logoVer pointer"/><img onclick="showEditInput(\'' + palabra + '\')" src="images/BotonesSeleccion/edit.svg" class="logoVer pointer"/></div></li>');
                });
            } else {
                mensajeError();
            }
        })
        .catch((err) => mensajeErrorCatch(err));
}