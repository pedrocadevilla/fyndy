CREATE Function [dbo].[SplitString]
(@List Varchar(100), @Delimiter Char(1))
Returns @Items Table (Descripcion Varchar(50),Position_ID int identity(1,1))
As
Begin
	Declare @Item Varchar(50), @Pos Int
	While CHARINDEX(@Delimiter,@List,0) > 0 
	Begin
		insert into @Items (Descripcion) values(SUBSTRING(@List,0,CHARINDEX(@Delimiter,@List,0)))
		set @List = SUBSTRING(@List,CHARINDEX(@Delimiter,@List,0)+1,len(@List)-CHARINDEX(@Delimiter,@List,0)+1)
	End
	if len(@List) > 0
	insert into @Items (Descripcion) values(@List);
	Return
End