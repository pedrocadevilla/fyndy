CREATE FUNCTION [dbo].[fnConvertirCaracteres]

(

    @CadenaxTransformar VARCHAR(100)

)

RETURNS VARCHAR(100)

AS

BEGIN

    RETURN REPLACE ( REPLACE( REPLACE( REPLACE( REPLACE ( REPLACE ( REPLACE( REPLACE( REPLACE( REPLACE (@CadenaxTransformar , '�' , 'a') , '�','e') ,'�','i') ,'�','o' ) ,'�','u') , '�' , 'A') , '�','E') ,'�','I') ,'�','O' ) ,'�','U')

END